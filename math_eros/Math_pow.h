/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/
#pragma once
#pragma warning (disable:4018)

#include <float.h>
#include <math.h>

#include <vector>
using namespace std;

double f0(double x);
double f1(double x);
double f2(double x);
double f3(double x);
double f4(double x);
double f5(double x);
double f6(double x);

double f_inverse(__int64 i);
double f_equal(__int64 i);
double f_limit(double val, double LIMIT=FLT_EPSILON);

class Pow_M
{
	int		index;
	double	exponent;
	bool	fast;
	double	ecart;
	// First method (when passing one argument)
	double ( * f_double)(double);
public:
	Pow_M()
	{
		fast=false;
		f_int=0;
		f_double=0;
		factor=0.;
		size=0;
	};

	void init(double _exp)
	{
		exponent=_exp;
		// define index
		index=(int)floor(exponent*2.0+0.49999999999);
		ecart = fabs(exponent-index/2.);
		if ((ecart<0.1)&&(index<7))
		{
			switch(index)
			{
			case 0:f_double=f0;break;
			case 1:f_double=f1;break;
			case 2:f_double=f2;break;
			case 3:f_double=f3;break;
			case 4:f_double=f4;break;
			case 5:f_double=f5;break;
			case 6:f_double=f6;break;
			}
			fast=true;
		}
	}

	double operator()(double x)
	{
		return fast ? f_double(x):pow(x,exponent);
	}
// Second method (two arguments or more)

private:
	size_t size;
	vector<double>table;

	double	( * f_int)(__int64);
	double	factor;

public:
	void init(double * _table, double _exp, size_t _size, double _factor=1.0)
	{
		size=_size;
		factor=_factor;
		exponent=_exp;

		if (_table && _size)
		{
			size = __min(size, (size_t) 2<<21); // about 32Mo by _table made of double (8 bytes)
			table.resize(size);
			for (size_t i=0;i<size;i++)table[i]=pow(factor*_table[i],exponent);
		} 
		else
		{
			fast=true;
		}
	}

	// Init of the pow series from a function
	// Note that the function must be defined at any value
	// The predefined function f_inverse(i) yields 0 for i=0
	void init(double (*function)(__int64), double _exp, size_t _size, double _factor=1.0)
	{
		exponent = _exp;
		size=_size;
		f_int=function;
		factor=_factor;

		if (function) {
			size = __min(size, (size_t) 2<<21); // about 32Mo by table made of double (8 bytes)
			table.resize(size);
			for (size_t i=0;i<size;i++)table[i]=pow(factor * f_int((__int64)i),exponent);
		} 
		else
		{
			fast=true;
		}

	}

	double operator()(int i)
	{
		if (fast) return factor;
		if (i>0 && i<size) return table[i];
		if (f_int) return pow(factor * f_int((__int64)i),exponent);
		return factor;
	}

	double operator()(__int64 i)
	{
		if (fast) return factor;
		if (i<size) return table[(size_t)i];
		if (f_int) return pow(factor * f_int(i),exponent);
		return factor;
	}

	~Pow_M(){table.clear();}


};

typedef double MATRIX3[9];
double m3_det(MATRIX3 mat);
void m3_identity(MATRIX3 &ma);
void m3_inverse(MATRIX3 mr, MATRIX3 &ma);