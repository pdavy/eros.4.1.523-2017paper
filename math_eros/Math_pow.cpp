/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

#include <math.h>
#include "Math_pow.h"

double f0(double x){return 1;}
double f1(double x){return sqrt(x);}
double f2(double x){return x;}
double f3(double x){return x*sqrt(x);}
double f4(double x){return x*x;}
double f5(double x){return x*x*sqrt(x);}
double f6(double x){return x*x*x;}

double f_inverse(__int64 i){return i>0 ? 1./(double)i:0.;}
double f_equal(__int64 i) {return (double) i;}

double f_limit(double val, double LIMIT)
{
	return val>LIMIT ? val : LIMIT;
}

double m3_det(MATRIX3 mat)
{
    double det;

    det = mat[0] * (mat[4] * mat[8] - mat[7] * mat[5])
        - mat[1] * (mat[3] * mat[8] - mat[6] * mat[5])
        + mat[2] * (mat[3] * mat[7] - mat[6] * mat[4]);

    return det;
}

void m3_identity(MATRIX3 &ma)
{
	for (int i=0; i<9;i++)
		ma[i]=i%3?1.:0.;
}

void m3_inverse(MATRIX3 mr, MATRIX3 &ma)
{
    double det = m3_det(ma);

    if (fabs(det) < 0.0005)
    {
        m3_identity(ma);
        return;
    }

    mr[0] =   ma[4] * ma[8] - ma[5] * ma[7]  / det;
    mr[1] = -(ma[1] * ma[8] - ma[7] * ma[2]) / det;
    mr[2] =   ma[1] * ma[5] - ma[4] * ma[2]  / det;

    mr[3] = -(ma[3] * ma[8] - ma[5] * ma[6]) / det;
    mr[4] =   ma[0] * ma[8] - ma[6] * ma[2]  / det;
    mr[5] = -(ma[0] * ma[5] - ma[3] * ma[2]) / det;

    mr[6] =   ma[3] * ma[7] - ma[6] * ma[4]  / det;
    mr[7] = -(ma[0] * ma[7] - ma[6] * ma[1]) / det;
    mr[8] =   ma[0] * ma[4] - ma[1] * ma[3]  / det;
}
