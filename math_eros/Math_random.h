/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

#pragma once
#pragma warning (disable:4018)


#include <float.h>
#include <math.h>
#include <time.h>

// The random generator
#if defined(_RAND_) 
#elif defined(_MT_RANDOM_)
#include <Random/MTrandom/MTrandom.h>
#else
#define _GLS_RNG_
#include <unvar/random/gsl_rng/gsl_rng_initialisation.h>
#define _GLS_RNG_
#endif
#include <unvar/io_utilities/io_utilities.h>
#include <unvar/io_utilities/arg.h>
#include <unvar/io_utilities/Input_data.h>

class class_random
{
private:
#ifdef _RAND_
#define alea(a) fmod((a)*pas_alea, max_alea)
	double	pas_alea;
	double	max_alea;
	double	x_alea;
#elif !defined(_MT_RANDOM_)
	gsl_rng * GSL_generator;
#endif
	bool flag;

public:
	double	range_exclude;
	double	range_include;
	int		EROS_seed;

public:
	class_random()
	{
		flag=false;
	}
	~class_random()
	{
#if defined(_GLS_RNG_)
		if (GSL_generator) gsl_rng_free (GSL_generator);
#endif
	}
	//------------------------------
	// Initialization
	//------------------------------
	bool init() {return flag;}
	void init(int _seed)
	{
		EROS_seed=_seed;
		if (EROS_seed<0) {
			time_t t;
			time(&t);
			EROS_seed=(int)t;
		}

	#if defined(_RAND_)
		pas_alea=16807.;
		max_alea=pow(2.,31.)-1.;
		srand((unsigned int) EROS_seed);
		x_alea=rand();

		range_include = 1./(double)RAND_MAX;
		range_exclude = 1./((double)RAND_MAX+1.);

#elif defined(_MT_RANDOM_)
		seedMT((uint32)EROS_seed);
		range_include = MT_RANDMAX;
		range_exclude = MT_RANDMAXe;
#else
		GSL_generator = gsl_rng_alloc (gsl_rng_taus);
		gsl_rng_set (GSL_generator, (unsigned long int) EROS_seed);

		range_include = 1./(double)gsl_rng_max(GSL_generator);
		range_exclude = 1./((double)gsl_rng_max(GSL_generator) + 1.0);
#endif
		flag=true;
	}
	//------------------------------
	// draw a random number in ]0,MAX]
	//------------------------------
	double RAND()
	{
		double proba;
#if defined(_RAND_)
		x_alea=alea(x_alea);
		proba=x_alea;
#elif defined(_MT_RANDOM_)
		proba=(double)randomMT();
#else
		proba=(double)gsl_rng_get(GSL_generator);
#endif
		return proba;
	}
	//------------------------------
	// draw a random number in ]0,1]
	//------------------------------
	double rand_exclude() { return (RAND()+1.)*range_exclude; }
	//------------------------------
	// Draw a random number in [0,1[
	//------------------------------
	double rand_include() { return RAND()*range_include; }


};