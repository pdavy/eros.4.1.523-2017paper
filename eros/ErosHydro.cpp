/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

#include "stdafx.h"
//---------------------------------------------------------
// Hydrodynamic functions
//---------------------------------------------------------

//---------------------------------------------------------------
// FLAGS
//---------------------------------------------------------------
// #include "hydro.hpp"
#undef _SLOPE_LOCAL_
//-----------------------------------------------
// Model with the non-stationary (transient) case 
// not yet validated
//-----------------------------------------------
// if not defined: restrict calculations to the stationary case
//---------------------------------------------------
// Stability and/or accuracy of the flow calculation
//---------------------------------------------------
#define _HYDRO_STABILITY_ACCURACY_	
//#undef _HYDRO_STABILITY_ACCURACY_ // more accuracy, less stability
// if defined: more stability, less accuracy

//-----------------------------------------------------------------
// Initialization of the HYDRO function
//-----------------------------------------------------------------
void	EROS::HYDRO__init()
{
}
//-----------------------------------------------------------------
// PRECIPITON DISPLACEMENT FUNCTION
// The basic function that decides where the precipiton is moving
//-----------------------------------------------------------------
//-----------------------------------------------------------------
// Calculation of the neighboring gradients.
// This is the most "time-consuming" procedure of the whole program
// Any optimization is welcome.
//-----------------------------------------------------------------

// THE HYDRO FUNCTION
int EROS::HYDRO(ptr_node & CELL, bool lake)
{
	//---------------------------------------------
	// Calculate the water depth in the neighboring
	//---------------------------------------------
	double Hin=P_height*P_water_t;

	if (FLOW_calcul)
	{
		// Calculate the water depth in all the neighboring
		CELL->water_update_neighbors(EROS_time);
		CELL->water_update(EROS_time);

#ifdef _HYDRO_STABILITY_ACCURACY_
		// If the water depth is increased now, that is before calculating slopes,
		// this gives a better stability, but less accuracy on slope values
		// Increase the water depth
		CELL->water_increase(Hin);
#endif
	}
	//-------------------------------
	// Record the precipiton passages
	//-------------------------------
	// Record a new precipiton in the cell time series
	// if ( CELL->bc&BC_BLOC || CELL->upstream)  // (only once if the point is on a boundary)
	if (!lake) CELL->update_date(EROS_date());

	//----------------------------------------------------------------
	// III. Calculate the topography neigboring and the steepest slope
	//----------------------------------------------------------------
	// 2. Initialize the neighboring (calculate altitude differences and slopes)
	CELL_GRADIENTS.init(CELL,true, FLOW_calcul!=0); // WALK_debug("P1|");
	//---------------------			-----------------------------------------
	// IV. Add flow inertia (see the red EROS notebook)
	//--------------------------------------------------------------
	if (FLOW_Cte_Inertia>0.) 
	{
		CELL_GRADIENTS.add_component(AMONT_GRADIENTS,CELL_GRADIENTS.H_water*FLOW_Cte_Inertia,false);
		AMONT_GRADIENTS=CELL_GRADIENTS;
	}
	// 3. No available neigbors (occur in lakes when backflow is not allowed)
	if (CELL_GRADIENTS.empty()) 
	{
		// set the cell as a hole (no downstream and nil hydrodynamic time)
		CELL->set_hole();
		P_water_t=0;
		return 0;
	}
	//------------------------------------------------------------------
	// 3. Calculate the slope gradients at the vicinity of current pixel 
	//------------------------------------------------------------------
#ifdef _SLOPE_FROM_PLANE_
	// Calculate the downstream direction from the best fitting plane
	// The method does not produce realistic instabilities, 
	// may be because it introduces a low-pass fiter into topography variations 
	// that make impossible the development of instabilities.
	// It may be appropriate for very smooth topography and/or river depth variations
	GRADIENT_D=CELL_GRADIENTS.set_slope_plane();
	// Note that the function is currently written for the topographic surface;
	// it must be also written for the water surface
#else
	// Calculate the downstream direction from the neighbor gradients
	if (FLOW_calcul)
	{
		GRADIENT_D=CELL_GRADIENTS.sort_gradient_hydro();
		CELL_GRADIENTS.slope_w=GRADIENT_D->Gradient_hydro;
	}
	else
	{
		GRADIENT_D=CELL_GRADIENTS.sort_gradient_topo();
		CELL_GRADIENTS.slope_w=GRADIENT_D->Gradient_topo;
	}

#endif
	//----------------------------------------------------------------
	// Increase water depth
	//----------------------------------------------------------------
#ifndef _HYDRO_STABILITY_ACCURACY_
	if (FLOW_calcul) 
	{
		CELL->water_increase(Hin);
	}	
#endif

	//--------------------------------------------
	// Deal with lakes
	// (this precipiton already visited the point)
	//--------------------------------------------
	if (lake)
	{
		CELL_GRADIENTS.slope_w=0.; // No water slope allowed
		CELL_GRADIENTS.slope_e=0.; // No erosion allowed
		
		return HYDRO__end(CELL);
		
	}
	//----------------------------------------------
	// V. Test if the precipiton is still moving on
	//----------------------------------------------
	// 5. Test if the point is a water "hole"
	if ( CELL_GRADIENTS.slope_w <=0. )
		// No need to do something at this point (like the fill_water function of the previous version)
		// The relaxation time becomes infinite and the point will be naturally filled up by the next precipitons
	{

		// Check if the slope is positive after adding the precipiton volume
		if (FLOW_calcul) 
		{
#ifndef _HYDRO_STABILITY_ACCURACY_
			CELL_GRADIENTS.update_gradients_water(Hin);
			GRADIENT_D=CELL_GRADIENTS.sort_gradient_hydro();
			CELL_GRADIENTS.slope_w=POSITIF(GRADIENT_D->Gradient_hydro);
#else
			CELL_GRADIENTS.slope_w=0.0;
#endif
			return HYDRO__end(CELL);
		}
		// else (no flow)
		return 0;
		/*-------------------------//
		// Kill the precipiton path
		hydro_lake(CELL);
		P_water_t=0.;
		CELL->downstream=0;
		// -------------------------*/

	}

	//---------------------------------------------------------
	// VII. Choose the downard precipiton direction 
	//---------------------------------------------------------
	if ( P_Diffusion 
		// && (PROCESS_active()==HILLSLOPE)  // If only hillslope precipitons are likely diffusing
		// && P_step >2 // (TO BE REMOVED) a criteria that help to remove boundary effects for river boundary conditions
		)
	{
#ifdef _SLOPE_FROM_PLANE_
		// The stochasticity is given by adding an aleatory angle in the range [-PI/2, PI/2]
		static double angle_Range = M_PI;
		GRADIENT_D=CELL_GRADIENTS.direction_modify(angle_Range*(E_grid->RANDOM_generator.rand_include()-0.5));
#else
		GRADIENT_D=direction_stochastic(CELL_GRADIENTS);
		/* This case is unlikely occuring but...
		if ( GRADIENT_D==CELL_GRADIENTS.end()) 
		{
			CELL_GRADIENTS.slope_w=0;	

			// A lake (shoud not happen)
			if (FLOW_calcul) hydro_lake(CELL);

			CELL->downstream=0;
			return 0; 
		}
		*/
		CELL_GRADIENTS.set_direction(GRADIENT_D->index());
#endif
	}
	//-----------------------------------------------------------------
	// VIII. Fix the erosion slope and record the downstream properties
	//-----------------------------------------------------------------

	if (E_grid->DIAGONAL_WIDTH_REDUCTION)  CELL_GRADIENTS.slope_w=GRADIENT_D->Gradient_hydro;

	// CELL_GRADIENTS.slope_e is further used to calculate the erosion flux
	// So choosing the hydro or topo slope can change a lot the results.
	if ( EROS_Model_E_slope==0 ) 
		CELL_GRADIENTS.slope_e=POSITIF(GRADIENT_D->Gradient_topo);
	else if ( EROS_Model_E_slope==1 )
		// To be checked...
		// Could be the slope in the stochastically chosen direction or the steepest slope (CELL_GRADIENTS.slope_w)
		//CELL_GRADIENTS.slope_e=POSITIF(GRADIENT_D->Gradient_hydro);
		CELL_GRADIENTS.slope_e=POSITIF(CELL_GRADIENTS.slope_w);
	else
		CELL_GRADIENTS.slope_e=POSITIF((GRADIENT_D->Gradient_topo+GRADIENT_D->Gradient_hydro)/2.);

	//------------------------
	// Update the water depth
	//------------------------
	return HYDRO__end(CELL);
}
//---------------------------------------------
// Define the water depth coefficient of a cell
//---------------------------------------------
int EROS::HYDRO__end(ptr_node & CELL)
{
	CELL->downstream=GRADIENT_D->node();
	CELL->index=GRADIENT_D->link->index;

	if (FLOW_calcul)
	{
		// Fix the hydrodynamic time scale from the steepest slope
		if (!E_grid->DIAGONAL_WIDTH_REDUCTION)
			CELL->coef_time( CELL_GRADIENTS.slope_w  );		
		else
			CELL->coef_time( CELL_GRADIENTS.slope_w, GRADIENT_D->link->dl );
	}

	// Attempt to consider partial transfer downstream
	// The main issue is about the time increment, which should be only part of the time between successive precipitons as in Fonstad's work
	// This is clearly not the case there and the method must be completly (re)written

	if (FLOW_calcul==2)
	{
		double Q=CELL->water_release();
		if (Q<CALCUL_EPSILON)
		{
			CELL->water_increase(Q);
			return 0;
		}
		P_water_t=Q/P_height;

		return 1;
	}
	
	return 1;
}


//---------------------------------
// Some debugging ... if necessary
//---------------------------------
void EROS::WALK_debug(string type)
{
#ifdef _DEBUG_EROS
	if (type[0]=='P') 
	{
		cout << endl << type;
		cout << " p:" << E_grid->n(CELL_GRADIENTS.P_node);
		cout << " h:" << setprecision(4) << CELL_GRADIENTS.P_node->E_topo+CELL_GRADIENTS.P_node->W_depth;
		for (auto G=CELL_GRADIENTS.begin();G!=CELL_GRADIENTS.end();G++)
		{
			cout << "|p:" <<  E_grid->n(G->node());
			cout << " h:" << setprecision(4) << G->node()->E_topo+G->node()->W_depth;
			cout << " g:" << setprecision(4) << G->Gradient_hydro;
		} 
	}
	if (type[0]=='D') {
		cout << endl << type;
		cout << " p:" << E_grid->n(GRADIENT_D->node());
		cout << " h:" << setprecision(4) << GRADIENT_D->node()->E_topo+GRADIENT_D.node()->W_depth;
		//cout << " i:" << GRADIENT_D->link->index;
		cout << " s:" << setprecision(4) << CELL_GRADIENTS.slope_w;
		cout << endl;
		system("pause");
	}
#else
	return;
#endif

}
//---------------------------------------
// Calculate stochastically the direction
// chosen by/for the precipiton
//---------------------------------------
Node_typology::iterator EROS::direction_stochastic(Node_typology & nodes)
{
	// IMPORTANT NOTE:
	// Node_typology must have been sorted according to decreasing Gradient hydro 
	// before using this function
	double ProbaMAX=0.,p;

	size_t n_dir=0;

#ifdef _DEBUG
	static int n_ss=0, n_nss=0;
#endif

	auto lk=nodes.begin(), end_node=nodes.end();
	for (; lk!=end_node; lk++) {
		//	Cumulative probability

		p=lk->Gradient_hydro;

		// The descent direction is calculated with respect to altitude gradients
		if (p>0.)
		{
			// Count the number of admissible pixel
			n_dir++;
			// Calculate the power of p
			p=pow_Diffusion(p);
			//----------------------------------------
			// Both slope and discharge are considered 
			// when assessing the precipiton direction
			//----------------------------------------
			/* ------------------ obsolete ---------------------- //
			if(P_Model_Q)
			{
				int dt=(int)TIME__calcul(lk->link->node_D,EROS_date(),H_RUN);
				if (!dt) p=0.;
				else if (P_Model_Q==1) p =(double)dt;
				else if (P_Model_Q==2) p*=(double)dt;
			}
			// ------------------ obsolete ---------------------- */

//#if defined(_DIAGONAL_WIDTH_REDUCTION_)
			if (E_grid->DIAGONAL_WIDTH_REDUCTION)
				p /= lk->link->dl;
//#endif
		} else  {
			end_node=lk;
			// p=0.;
			break;
		}

		ProbaMAX += p;
		lk->Proba=ProbaMAX;
	}
	
	if (ProbaMAX<DBL_EPSILON) 
	{
		// So peculiar that it does not worth being considered
		// (i.e. the previously calculated steepest slope is the way)
		return nodes.begin();
	}

	//------------------------------------------
	// The precipiton direction is now drawn from
	// the probability distribution
	//------------------------------------------
	ProbaMAX *= E_grid->RANDOM_generator.rand_exclude();
	lk=nodes.begin();
	
	int ss=lk->link->index;
	
	for(;lk!=end_node && lk->Proba<ProbaMAX;lk++);

	if (lk==end_node)
		cout << ".";
#ifdef _DEBUG
	if (lk->link->index!=ss) n_nss++;
	else n_ss++;
#endif

	return lk;
	//int dir=0;while(Proba[dir]<ProbaMAX)dir++;return dir;

}

//--------------------------------------------------
// The procedure change the precipiton volume EROS_P
// if the number of errors is too large or too small
//--------------------------------------------------
double EROS::change_precipiton(int flag)
{
	static double _time=EROS_P,
		increment=EROS_P_min,
		coef=adapt_time_factor;
	static int state_previous=0, 
		state=0, 
		nstate=0;

	int nError;
	switch (adapt_time_type) 	
	{
	case -1:
		nError = EROS_default(E_READ|E_ERODE)+EROS_default(E_READ|E_DEPOSIT);//+EROS_default(E_READ|E_HOLE);
		break;
	case 0: 
		nError = EROS_default(E_READ|E_ERODE)+EROS_default(E_READ|E_DEPOSIT);
		break;
	case 1: 
		nError = EROS_default(E_READ|E_ERODE);
		break;
	case 2: 
		nError = EROS_default(E_READ|E_DEPOSIT);
		break;
	case 3:
		nError = EROS_default(E_READ|E_HOLE);
		break;
	}

	//----------------------------------------------
	// Calculation of the theoretical time increment
	//----------------------------------------------
	if (flag&E_MINIMUM) 
	{
		_time=EROS_P_min;
		state=0;
	} else if (flag&E_ADAPT) {
		// Calculate the state as a function of the number of errors
		state = (nError<=adapt_time_errmin) ? 1: (nError>adapt_time_errmax)? -1 : 0;
	}
	else 
	{
		// Don't change...
		state = 0;
	}
	
	if (state == state_previous) nstate++;
	else nstate=0;

	switch(state) {
	// When there is no error, we accept the accelerateur to ... accelerate!
	case 1:		
		if (adapt_time_op == "*") 
		{
			coef=adapt_time_factor;
			if (adapt_time_errmin && nError) coef=min(coef,(double)adapt_time_errmin/nError ); 
			_time = ceil (_time*adapt_time_factor/EROS_P_min)*EROS_P_min;
		} 
		else // adapt_time_op == "+"
		{
			if ( nstate>=2 )
				increment = 2*increment;
			else 
				increment = max(increment/2, EROS_P_min);

			_time += increment;
		}
		
		if ( (EROS_P_max>0) && (_time > EROS_P_max) )
		{
			_time = EROS_P_max;
			increment = EROS_P_min;
		}
		break;
	
	// Just avoid the time to be larger than the recording time
	case -1:
		if (adapt_time_op == "*")
		{
			coef=adapt_time_factor;
			if (adapt_time_errmin) coef=min(coef,(double)nError/adapt_time_errmin);
			_time = ceil (_time/coef/EROS_P_min)*EROS_P_min;
		} 
		else // adapt_time_op == "+" 
		{
			if ( nstate>=2 )
				increment = 2*increment;
			else 
				increment = max(increment/2, EROS_P_min);

			_time=max(_time-increment,EROS_P_min);
		}
		break;
	}

	//---------------------------------------------------------
	// Set the time increment as a multiple of the drawing time
	//---------------------------------------------------------
	if (EROS_draw_condition==IF___TIME) 
	{
		double delta = EROS_draw-EROS_time;

		double n = ceil(delta/_time); 
		if (n<4)
		{
			if (n<=0.) n=1.;
			EROS_P = max(delta/n,EROS_P_min);
			// if (n>=2) _time=EROS_P; 
		} else EROS_P = _time;
	} 
	else 
	{
		EROS_P = _time;
	}

	//cout << "nError:" << nError << " state:" << state << " p:"<<EROS_P<<endl;
	state_previous=state;

	return EROS_P;

}

//--------------------------------------
// The number of precipiton between
// two successive passages
//--------------------------------------
double EROS::GET_P_date()
{
	return P_date_k/EROS_k;
}
//--------------------------------------
// The precipiton volume
//--------------------------------------

double EROS::GET_P_volume()
{
	return P_water*P_water_t;
}
double EROS::GET_P_height(ptr_node Pn)
{
	return  P_water*P_water_t/Pn->area;
}//--------------------------------------------
// CALCULATION of the precipiton time scale
// that is, the time during which the precipiton
// is eroding
//--------------------------------------------
void EROS::CALCUL_P_dt() { P_dt = Pk_time * P_date_k; }

//--------------------------------------
// The channel width
//--------------------------------------
double EROS::Channel_width()
{
	// The function is only used to save the q values
	double w =  cte_W[PROCESS_active()];
	if (P_date_k) w *= pow_W[PROCESS_active()](P_date_k);

//#if defined(_DIAGONAL_WIDTH_REDUCTION_)
	if (E_grid->DIAGONAL_WIDTH_REDUCTION)
	{
		// The channel width is taken into account in the calculation of q=Q/W
//#if _DIAGONAL_WIDTH_REDUCTION_==2
		// The dependency on Dy is not allowed when the river width is specified by a function
		if (E_grid->DIAGONAL_WIDTH_REDUCTION==2 && EROS_Exp_W_Q[PROCESS_active()]!=0.0) 
		{
			// Don't change w
		} else
//#endif
		{
			// This expression must be changed if the grid is not regularly spaced
			w*=(E_grid->cell_y/Channel_length());
		}
	}
//#endif
	return w;
}

//--------------------------------------
// The channel area (within the cell)
//--------------------------------------
double EROS::Channel_area()
{
//#if !defined(_DIAGONAL_WIDTH_REDUCTION_)
	if (!E_grid->DIAGONAL_WIDTH_REDUCTION)
	{
		if (!P_date_k) return Cell_area();
		return Channel_width()*Channel_length();
	}
//#else
//#if _DIAGONAL_WIDTH_REDUCTION_==2
	if (E_grid->DIAGONAL_WIDTH_REDUCTION==2)
	{
		if (EROS_Exp_W_Q[PROCESS_active()]!=0.0) 
			return Channel_width()*Channel_length();
	}
//#endif
	return Cell_area();
//#endif
}
//--------------------------------------
// The discharge over the pixel
//--------------------------------------
double EROS::Q_P()
{
	if (!P_date_k) return 0.;
	return P_water_t * EROS_Qk / P_date_k;
}

double EROS::q_manning(bool update)
{
	if (update) pCELL_current->water_update(EROS_time);

	if (E_grid->DIAGONAL_WIDTH_REDUCTION==1)
		return pCELL_current->qx()*GRADIENT_D->dl()/FLOW_Cte_Depth;
	else
		return pCELL_current->qx()*E_grid->cell_x/FLOW_Cte_Depth;
}
				
double EROS::Sq()
{
	if (!P_date_k) return 0;
	return P_water_t * EROS_Sk / P_date_k;
}

/*--------------------------------------------------------------

  fonction anaflux: suivi des flux au cours de la marche
  lorsqu'un precipiton passe d'une region topo a une autre

---------------------------------------------------------------*/
int EROS::anaflux( double * fluxsortant, double * fluxentrant, int zone, int position, double delta_alt, double bord)

{


  int pf=0;
  if (bord<0) {
    fluxsortant[zone] += delta_alt;
  }
  else {
    pf = position; // pf= position/EltsParPartie
    if ( pf != zone ) {
      fluxsortant[zone] += delta_alt;
      fluxentrant[pf] += delta_alt;
    }

  }
  return pf;
}


//--------------------------------
//	Fickean diffusion
//	(also called as splash effect)
//--------------------------------

// REVOIR
// The function is not used anymore. 
// If it becomes necessary (which is not obvious), it has to be validated before use.

void EROS::flow_splash(ptr_node P_node)
{
	static double SPLASH_coef = 0.25;

	Node_typology nodes(E_grid, P_node); // Calculation of the surrounding topography
	vector <double> dh(nodes.links,0.);

	double & Hw=P_node->W_depth;
	double g, dhw=0.;

	for (int i=0;i<nodes.links;i++) 
	{
		if ((g=nodes[i].Gradient_hydro)>0.)  // The central point is higher
			dh[i] = min(Hw, g/nodes[i].link->dl*SPLASH_coef);
		else
			dh[i] = max(-nodes[i].link->node_D->W_depth, g/nodes[i].link->dl*SPLASH_coef);
		dhw+=dh[i];
	}
	double coef=1.;
	if (dhw>Hw) coef=Hw/dhw;
	Hw-=dhw*coef;

	for (int i=0;i<nodes.links;i++)
	{
		double & N_w = nodes[i].link->node_D->W_depth;
		N_w += dh[i]*coef;
		N_w = POSITIF(N_w);
	}

}
