/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

//---------------------------------------------------------
// Erosion functions
//---------------------------------------------------------

#include "stdafx.h"


//--------------------------------------------------------
//--------------- Set topo function  ---------------------
//--------------------------------------------------------
void EROS::set_topo(ptr_node Ptr, double value, int flag, double slope)
{
	if (Ptr->IS_BOUNDARY()) return;
#ifdef _DEBUG_EROS
	if (_ERROR_(value,20.0))
		cout <<".";
#endif

	Ptr->E_topo += value;

	// Modifying sediment thickness
	if (!(flag&E_UPLIFT)) 
	{
		Ptr->E_sediment += value; 
		if (Ptr->E_sediment<0.) Ptr->E_sediment=0.;

		// Modifying any linked points (periodic conditions on the boundary)
		if (Ptr->equivalent) 
			Ptr->equivalent->E_topo += value;
		
	}
	// --------------------------------------
	//	Record points
	// --------------------------------------

	printf_points(pCELL_current, value, flag, slope);
}
//--------------------------------------------------------
//--------------- Some variables used here ---------------
//--------------------------------------------------------

static double	ALMOST_ONE=1.-FLT_EPSILON;

//--------------------------------------------------------
// Topography change limiter.
// The variable "defaut" contains information on
// the number of erosion defaults, deposition defaults,
// and truly-eroded lateral neigbors
//-------------------------------------------------------

// This function should be active when there is no water
// A simpler function should replace it by just limiting the deposition/erosion quantity
int EROS::EROSION_limiter(ptr_node & CELL, double & dh)
{
	int defaut=0;

	// if (!FLOW_calcul) 
	if (1)
	{
		type_height &height=pCELL_current->E_topo;

		// Limit deposition
		if (dh>0. && pCELL_current->upstream 
			// && pCELL_current->upstream!=pCELL_current->downstream 
			) {
			// The upstream pixel must remain higher 
			double dhMAX=pCELL_current->upstream->E_topo-height - CALCUL_SMALL;

			// Another possible option is to take the highest surrounding point by sorting neigborhood cells according to their altitude
			// std::sort(CELL_GRADIENTS.begin(),CELL_GRADIENTS.end(), is_altitude_greater);
			// dhMAX=max(-CELL_GRADIENTS.back().Dh, CALCUL_SMALL);

			if (dhMAX>0. && dh>dhMAX) 
			{
				// TODO: if I add in the code zones where there is neither erosion nor deposition,
				// I should avoid that errors happens in these non-eroding areas

				// limit the altitude change
				dh=dhMAX;
				// Record the correction
				return EROS_default(dh>CALCUL_SMALL ? E_DEPOSIT:E_NO);
			}
		}

		// Limit erosion
		if (dh<0.) 
		{
			// The downstream pixel must remain higher 
			double dhMIN = pCELL_current->downstream->E_topo -height + CALCUL_SMALL;

			// Another option is to avoid erosion to exceed (too much) the lowest point
			// dhMIN=min(-CELL_GRADIENTS.front().Dh, -CALCUL_SMALL);
			// end new option --------------------------------------
		
			// erosion does not invert slope gradients
			// case where the topography is already inverted
			if (dhMIN>=0.0) 
			{
				dh=0;
				return EROS_default(dh<-CALCUL_SMALL ? E_ERODE:E_NO);
			}
			if ( /*dhMIN<0. &&*/ dh<dhMIN )
			{
				// TODO: if I add in the code zones where there is neither erosion nor deposition,
				// I should avoid that errors happens in these non-eroding areas

				// limit the altitude change
				dh=dhMIN;

				// Record the correction
				return EROS_default(dhMIN<-CALCUL_EPSILON && dh<-CALCUL_SMALL ? E_ERODE:E_NO);
			}
		}
	}
	else 
	{
		// Limit deposition to the prescribed value or to water depth
		/*
		double hmax=min(EROS_limiter, CELL->W_depth/2.);
		if (dh>hmax) {
			dh=hmax;
		*/
		if (dh>EROS_limiter) {
			dh=EROS_limiter;
			return EROS_default(E_DEPOSIT);
		} 
		else if (dh<-EROS_limiter) {
			dh=-EROS_limiter;
			return EROS_default(E_ERODE);
		}
	}

	return E_NO;

}
//-------------------------------------------------
// EROS_default()
// store or reset erosion or sedimentation defaults 
// during precipitation path
//-------------------------------------------------
int EROS::EROS_default(int defaut)
{
	static int default_erode=0;
	static int default_sedim=0;
	static int default_hole=0;
	static int default_lateral=0;

	if (defaut&E_RESET) {
		default_erode=default_sedim=default_hole=default_lateral=0;
		return E_NO;
	}

	if (defaut&E_READ) {
		if (defaut&E_ERODE) return default_erode;
		if (defaut&E_DEPOSIT) return default_sedim;
		if (defaut&E_LATERAL) return default_lateral;
		if (defaut&E_HOLE) return default_hole;
		return default_erode+default_sedim+default_lateral+default_hole;
	} else {
		if (defaut&E_ERODE) default_erode++;
		if (defaut&E_DEPOSIT) default_sedim++;
		if (defaut&E_LATERAL) default_lateral++;
		if (defaut&E_HOLE) default_hole++;
		return defaut;
	}
	return defaut;
}

//--------------------------------------------
// calculation of the sediment transfer length
//--------------------------------------------
double EROS::EROS_transfer_length()
{
	double length = cte_Ld[PROCESS_active()];
	if (EROS_TransferModel[PROCESS_active()] != LENGTH_CONSTANT)
	{
		/* Option to average the variation of depot length
		averaging(pCELL_current->S_sed_length, pow_Ld[PROCESS_active()](P_date_k), GET_P_date());
		length *= pCELL_current->S_sed_length;
		*/
		// Transfer length = L*q^a
		length *= pow_Ld[PROCESS_active()](P_date_k);
//#if defined(_DIAGONAL_WIDTH_REDUCTION_)
		if (E_grid->DIAGONAL_WIDTH_REDUCTION) // Discharge is by sqrt(2.) to take account of channel width reduction on diagonal links (avoid)
			length *= pow_Ld_y[PROCESS_active()](GRADIENT_D->index()) ;
//#endif
	}
	// Limit
	if (length<EROS_Cte_Ld_min[PROCESS_active()]) 
		length=EROS_Cte_Ld_min[PROCESS_active()];
	if (length>EROS_Cte_Ld_max[PROCESS_active()]) 
		length=EROS_Cte_Ld_max[PROCESS_active()];

	//-------------------------------
	// Take account of sea conditions
	//-------------------------------
	if (!SEA__test()) length *= EROS_Cte_Ld_sea;

	//---------------------------------------------------------------------
	// Calculate the flow transfer length normalized by the pixel width, 
	// and the exponential function that is used to calculate the variation 
	// of sediment load along path
	//---------------------------------------------------------------------
	// The transfer length is divided by the cell length.

	length /= Channel_length();
	length = f_limit(length);

	return length;
}
//---------------------------------------------------------------------
// CALCULATION of the transfer length function
// The function returns the transfer length divided by the CELL length
//---------------------------------------------------------------------
void EROS::EROS_transfer_coef()
{

	// FLOW_dX = GRADIENT_D->dl();
	//--------------------------------------
	// Calculation of the actual flow width
	//--------------------------------------
	// CELL_area = Cell_area();
	SED_transfer = EROS_transfer_length();



#if defined (_ALTITUDE_OF_CHANNEL_)
	SED_transfer_coef = SED_transfer;
	// This preprocessor flag must be activated if DIAGONAL_WIDTH_REDUCTION==1
#elif defined (_ALTITUDE_OF_CELL_)
	// The coef is the transfert length by the channel width divided by the cell area
	SED_transfer_coef = SED_transfer * Channel_area()/Cell_area();
#else 
	Preprocessor definition is missing
#endif

	double _tr=1./SED_transfer;

	if (fabs(_tr)<1.e-6)
		_EXP_transfer = _tr;
	else
		_EXP_transfer = 1. - exp(-_tr);

	return;
	
}


//--------------------------------------
//	MAIN EROSION FUNCTIONS
//--------------------------------------
double EROS::Stress(ptr_node & CELL)
{
	// The basal erosion is defined as a power law of erosion stress (g*H*S) minus threhold
	// The water depth is either calculated from q and S via the Darcy-Weisbach relationship
	if (EROS_Model==2)
		return pow(Sq()/Channel_width()*CELL_GRADIENTS.slope_e,0.6666);

	// or directly from the water depth layer	
	return CELL->W_depth*CELL_GRADIENTS.slope_e;
	
}
//---------------------
// The erosion function
//---------------------
// TODO
// Does the threshold depends on erodibility?
// If yes, it should be incorporated in the EROSION_SP funciton
// If no, it should remain in the EROSION_Stream_Power function

// TODO
// Add a threshold erosion layer

double EROS::EROSION_SP(ptr_node & CELL)
{
	// The flow and slope dependency of the erosion law (i.e. all apart from erodibility)
	// Note that the erosion law is defined from dQ/dy, the flow per unit width of river
	// The result is multiplied by the time spent by the precipiton in the cell.
	// It corresponds to the potential eroded material height if no deposition occurs

	// NEW: add this condition
	if (CELL_GRADIENTS.slope_t<0.) return 0.;

	double Qss = cte_Qs[PROCESS_active()]
		  * pow_Qs[PROCESS_active()](P_date_k);
// #if defined(_DIAGONAL_WIDTH_REDUCTION_)
	if (E_grid->DIAGONAL_WIDTH_REDUCTION)
		Qss *= pow_Qs_y[PROCESS_active()](GRADIENT_D->index());
// #endif

	if (P_water_t<1.)
		Qss *= pow(P_water_t,EROS_Exp_E_q[PROCESS_active()]);
	
	Qss *=pow_S[PROCESS_active()](CELL_GRADIENTS.slope_e);
	// For linear law, we would expect the following expresion to be the fastest
	//Qss *= CELL_slope;
	return Qss;
}


//-------------------------------------------------------------
// Calculation of the (potential) sediment and basement erosion
//-------------------------------------------------------------
//1. Method 1
void EROS::EROSION_Stream_Power(ptr_node & CELL, double &Qss, double &H_sediment, double &H_basement, bool no_sediment)
{

	// Sediment erosion
	if (!no_sediment) 
	{ 
		double E = CELL->E_e_sediment;
		double T = CELL->E_t_sediment;

		H_sediment = Qss*E-T;
		if (H_sediment<0.) H_sediment=0.;
		else H_sediment *= GET_P_dt()  ;
	} else 
		H_sediment =0.;

	// Basement erosion
	{ 
		double E = CELL->E_e(PROCESS_active());
		double T = CELL->E_t(PROCESS_active());


		H_basement = Qss*E-T;
		if (H_basement<0.) H_basement=0.;
		else H_basement *= GET_P_dt()  ;
	}

}
// Method 2.
void EROS::EROSION_Stress(ptr_node & CELL, double &Shear, double &H_sediment, double &H_basement, bool no_sediment)
{
	if (!no_sediment) 
	{ 
		double E = CELL->E_e_sediment;
		double T = CELL->E_t_sediment;

		H_sediment = E*(pow_S[PROCESS_active()](Shear)-T);
		if (H_sediment<0.) H_sediment=0.;
		else H_sediment *= GET_P_dt()  ;
	} else 
		H_sediment =0.;

	// Basement erosion
	{ 
		double E = CELL->E_e(PROCESS_active());
		double T = CELL->E_t(PROCESS_active());


		H_basement = E*(pow_S[PROCESS_active()](Shear)-T);
		if (H_basement<0.) H_basement=0.;
		else H_basement *= GET_P_dt()  ;
	}
}
//--------------------------------------------------------
//	Bulk erosion (both lateral and basal)
//	Main erosion procedure
//--------------------------------------------------------

int EROS::EROS_Erode()
{

	//-----------------------------------------------------
	// The function return information on
	// the number of erosion defaults, deposition defaults,
	// and truly-eroded lateral neigbors
	//-----------------------------------------------------
	int defaut=E_NO;
	//----------------
	// Basic variables
	//----------------
	type_height dh;// height variation
	type_height dS;// river stock variation

	//-----------------
	// Force deposition
	//-----------------

	if (P_force_deposit)
	{
		// When water infiltrates, the precipiton stops walking 
		// when it is empty (i.e. volume very small).
		// This is not supposed to happen at the first step so the upstream cell is never NULL

		// The procedure forces the precipiton to spread its sediment onto the cell
		return HOLE();
		/*
		dh = P_stock / Cell_area() ; 		// Note that a porosity coefficient should be put here

		double dhMAX = pCELL_current->upstream ? pCELL_current->upstream->E_topo-pCELL_current->E_topo : 0.;
		if (dhMAX <= CALCUL_EPSILON) {
			dh=min(dh,CALCUL_EPSILON);
			defaut += (dh > CALCUL_SMALL ? 1<<1 : 0);
		} else if (dh>dhMAX) 
		{
			dh=dhMAX;
			defaut += (dh > CALCUL_SMALL ? 1<<1 : 0);
		}

		// Modifying stream sediment stock, topography and sediment thickness
		P_stock -= (dh * Cell_area());
		set_topo(pCELL_current, dh, E_HOLE);
		return defaut;
		*/
	}

	//--------------------------------------
	// Calculate the basic parameters
	// sediment transfer length, river width
	//--------------------------------------
	EROS_transfer_coef();

	//--------------
	// MASS BALANCES
	//--------------
	// The following mass balances are derived for the height of the river system,
	// and thus for the total altitude of sediment transported, that is the 
	// total volume per unit cell area


	//-------------------------------------------------
	// Reduced variable: _stock is transformed into its
	// equivalent topographic variation by dividing by
	// the channel (or cell) surface
	//-------------------------------------------------
#if defined (_ALTITUDE_OF_CHANNEL_)
	// Calculate the altitude of the channel part of the grid cell
	double _stock = P_stock / Channel_area();
#elif defined (_ALTITUDE_OF_CELL_)
	// Calculate the average altitude of the grid cell
	double _stock = P_stock / Cell_area();
#else
	EROS::Error("Preprocessor definition is lacking");
#endif

	//--------------------------------------------
	// EROSION PROCESS
	// Water erosion (either fluvial or hillslope)
	//--------------------------------------------

	type_height &sediment=pCELL_current->E_sediment;

	bool no_sediment= pCELL_current->E_e_sediment<=0.|| sediment<=0.
		// || pCELL_current->E_e_sediment==pCELL_current->E_e_channel
		;

	double H_sediment=0.;
	double H_basement=0.;
	H_lateral = 0.;

	// Basal erosion is effective only if the topographic slope is positive
	// Note that the slope, which is used to calculate the river power, is this of water.
	
	// Note that the erosion is calculated from the water slope rather than from the topographic slope
	// This can make huge differences
	
	if (P_date_k && SEA__test()) 
	{
		double S;

		if (EROS_Model==0) {
			S=EROSION_SP(pCELL_current);
			EROSION_Stream_Power(pCELL_current, S, H_sediment, H_basement, no_sediment);
		}
		else {
			S=Stress(pCELL_current);
			EROSION_Stress(pCELL_current, S, H_sediment, H_basement, no_sediment);
		}
#if defined(_LATERAL_EROSION_)
		// The lateral erosion
		if (EROS_Cte_El[PROCESS_active()]>0.)  H_lateral =  Erosion_lateral(S);
#endif
	}
	
	//if (_ERROR_(H_sediment,10) || _ERROR_(H_basement,10) || _ERROR_(H_lateral,10)) cout <<".";

	// 3 possibilities for the diagonal problem:
	// -  dh = (type_height) (_stock-Erosion*Ld/dX)*(1-exp(-dX/Ld)); + rather than x
	// -  dh = (type_height) (_stock-Erosion*Ld)*(1-exp(-1/Ld)); x rather than + but Dimitri prefers it
	// -  dh = (type_height) (_stock-Erosion*Ld)*(1-exp(-dX/Ld)); The right formulae (x rather than +)
	// The scaling argument is:
	// if Ld/dX<<0, Stock~Erosion(x-dx)*Ld
	// and dh~(Erosion(x-dx)-Erosion(x))*Ld = -Ld grad(Erosion)*dX
	// if Ld/dX>>0, Stock~0 and 1-exp(-dX/Ld)~dX/ld
	// and dh~-Erosion*dX

	// Potential sedimentation (+) or erosion (-) of the sediment bed
	// River stock increases of dS=((H_sediment+H_lateral)*ld-Stock)*(1-exp(-1/ld))
	// River erosion is such as dh = -dS+H_lateral
	

	if ( no_sediment ) 
	{
		// Stock variation in river (>0 means Stock increases)
		dS = ((H_basement+H_lateral)*SED_transfer_coef-_stock)*_EXP_transfer; 
		// Potential erosion (>0 means height increases)
		dh = -dS + H_lateral ;
		// net erosion
		H_erosion = H_basement ;
#ifdef _DEBUG_EROS
		if (_ERROR_(dh,20))
			cout <<".";
#endif
		H_deposit = H_erosion+ dh;
	}
	else 
	{
		// Stock variation in river (>0 means Stock increases)
		dS = ((H_sediment+H_lateral)*SED_transfer_coef-_stock)*_EXP_transfer; 
		// Potential erosion (>0 means height increases)
		dh = -dS + H_lateral ;
		// net erosion
		H_erosion = H_sediment ;
		H_deposit = H_erosion + dh;
#ifdef _DEBUG_EROS
		if (_ERROR_(dh,20))
			cout <<".";
#endif

		if ( -dh > sediment ) 
		{
			// Means that all the sediment has been eroded
			if (H_basement<=0.0) {
				dh=-sediment; // trivial case. Can't erode the basement
				dS=-dh + H_lateral;

				H_erosion=sediment;
				H_deposit=0.;

			} else  {
#if 0
				// H_sediment is the potential erosion of sediment and H_basement that of basement
				// sediment is supposed to be eroded during a distance x so that
				// -sediment = (S - (H_sediment+H_lateral) * ld) (1-exp[-x/ld]) + H_lateral
				// the rest is eroded during the distance Dx-x with a new river stock of S+sediment
				// -dS=-sediment + ( (S+sediment) - (H_basement+H_lateral) * ld) (1-exp[-(Dx-x)/ld])
				// and dh=-dS+H_lateral

				// Calculation of exp(-x/ld) with x = distance to erode sediment
				double expX =1.-(sediment+H_lateral)/dS*_EXP_transfer;

				// erosion and stock during [0,x]
				dS *= ( (1.-expX)/_EXP_transfer );
				// erosion and stock during [x, Dx]
				dS += ((H_basement+H_lateral)*SED_transfer_coef -(_stock+dS) ) * (1.-(1.-_EXP_transfer)/expX); 
#else
				// The model considers that the precipiton is divided in two (or several) successive volumes.
				// The first erodes all the sediment layer
				// The rest erodes the basement
				// H_sediment is the potential erosion of sediment and H_basement that of basement
				// The percentage of the total volume necessary to erode the sediment is:  sediment/(-dh);
				// The rest is eroding the basement
				double x = 0.; // The portion of water that erodes sediment ( 1-x: the portion that erodes basement)
				if (dh<0.) {
					x = -sediment/dh; // The proportion of water that erodes sediment
					// NOTE that the transfer length could be different from sediment and basement
					dS = x*dS + (1.-x) * ( (H_basement+H_lateral)*SED_transfer_coef -_stock ) * _EXP_transfer; 
				} else { // else: should never happen since -dh is larger than the sediment thickness, which is at minima zero
					dS = ((H_basement+H_lateral)*SED_transfer_coef -_stock ) * _EXP_transfer; 
				}

#endif

				if (_stock+dS<0) {
					dS = -_stock;
					// Should not happen!
					// If you are confident about my program, you can remove this test:
					cout << "?";
				}

				//--------------------------------------------------------
				// A new mass balance to take account of the changes in dS
				//--------------------------------------------------------
				dh=-dS+H_lateral; 

				H_erosion=(1.-x)*H_sediment+x*H_basement;
				H_deposit=H_erosion+dh;

			}
		}
	}


	//-------------------------------
	// EROSION PROCESS
	// Critical slope not to overcome
	//-------------------------------
	if (EROS_S_max>0.0) {
		dh =min(dh,EROS_S_max*Channel_length()-GRADIENT_D->Dh);
	}

	//-------------------------------------------------
	// NUMERICAL TESTS AND DEFAULT MANAGEMENT
	// Erosion is not supposed to invert slope gadients
	//-------------------------------------------------
	// TODO: avoid erosion change to overpass a certain value given as a parameter...
#ifdef _DEBUG
	DEBUG_dh=dh;
#endif	
	defaut = EROSION_limiter(pCELL_current, dh);

	//-------------------------------------------------------
	// The last mass balance (since dh may have been changed)
	//------------------------------------------------------

	// Modifying topography and sediment thickness
	// if _ERROR_(dh,10) cout <<".";
	set_topo(pCELL_current, dh, E_RUN, CELL_GRADIENTS.slope_e);

	// Stock variation and calculation of the total sediment output
	// The sediment stock is considered to be widespread over the whole pixel
	// to remain consistent with tectonics
	
	dS = (-dh+H_lateral);

	// Think about....
	// Try to determine how to make tectonic and erosion flux comparable
	// when the channel area is NOT the cell area.
	// There is no problem in the detachment-limited case,
	// since it comes to assume that the erosion rate is applied uniformly on the whole cell area.
	// In the transport-limited case, the deposition outflux S only concerns
	// the channel part of cell, while tectonic is applied on the whole cell.
	// To remove this inconsistency, we consider the sediment stock to be widespread over the whole pixel

#if defined (_FORCE_DEPOSITION_ON_CHANNEL_AREA_)
	double _area=Channel_area();
#else
	double _area= Cell_area();
#endif
	P_stock += (dS *_area);	
	if (P_stock<0)	P_stock=0.;
	/* 
	// The areal normalization is not necessarily for the other variables 
	// as long as thy are not used for calculating fluxes.
	// In the program, they are only used in the "particle-related" process functions
	H_erosion *= _area;
	H_deposit *= _area;
	H_lateral *= _area;
	*/

	return defaut;


}



//------------------------------------------------------
// The function that treats traps in the precipiton path
//------------------------------------------------------
int EROS::HOLE()
{
	// Note that this function is called when the precipiton is trapped in a hole
	// How do we manage sediments in this case?

	// 1. The precipiton does not contain water anymore
	// P_water_t=0;
	// 2. The precipiton is not able to erode
	H_erosion = H_lateral=0.;
	// 3. Calculate the stock
	double _stock =P_stock / pCELL_current->area;

#ifdef _DEBUG_EROS
	if (_ERROR_(_stock,10) || _stock<0)
		cout <<".";
#endif
	// Rank the neighboring with the 1st element (front) the positive lowest, and the last (back) the (negative) highest.
	std::sort(CELL_GRADIENTS.begin(),CELL_GRADIENTS.end(), is_altitude_greater);

	// Calculate the maximum sediment filling height ...
	double hmax=-CELL_GRADIENTS.front().Dh ;

	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// TO BE TESTED
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	// If on a mount, do almost nothing ... 
	if (hmax<CALCUL_SMALL)
	{
		// continue and try to put your sediment elsewhere
		double deposit = min(_stock, CALCUL_SMALL);
		set_topo(pCELL_current, deposit,E_HOLE);
		_stock -= deposit;
		P_stock=_stock*pCELL_current->area;		
		//  Continue ...
		return EROS_default(E_HOLE);
	}
	
	// The sediment can be deposited in the hole
	if (_stock < hmax+CALCUL_SMALL) 
	{
		// Kill the precipiton and discharge sediment on place
		set_topo(pCELL_current,_stock, E_HOLE);
		P_stock=0.;
		pCELL_current->downstream=0;
		return EROS_default(E_HOLE);
	} 
	{
		// The hole is not deep enough
		// Look for the second highest point
		for(auto G=CELL_GRADIENTS.begin()+1; G!=CELL_GRADIENTS.end(); G++) if (-G->Dh>hmax) {hmax=-G->Dh; break;}
		double deposit=min(_stock,hmax);
		set_topo(pCELL_current,deposit, E_HOLE); // set the topo to hmax
		_stock -= deposit; // remove hmax from the stock
		P_stock=_stock*pCELL_current->area;
		// Continue
		return EROS_default(E_HOLE);
	}

	 //pCELL_current->downstream=CELL_GRADIENTS.front().node(); // Say we mode downstream

	 // Note that, if sediment remains, P_water_t=0 will force the system to spread sediments in the neighboring
	 return EROS_default(E_HOLE);
	
/*
	// DETERMINATION DE L'ALTITUDE LA MOINS HAUTE
	// A priori les points qui tombent dans un trou ont deja fait l'objet d'un calcul
	// de pente (sinon, ils continueraient)

	double & Depot=H_deposit=0.;		// Initialize deposition to zero
	double Dh1 = -GRADIENT_D->Dh;
	
	if (Dh1 > _stock  - EROS_EPSILON ) {
		// Un vrai trou (� EROS_EPSILON pr�s). On d�pose et on fout le camp
		P_stock=0.;
		CELL_PtrAval=NULL;
		set_topo(pCELL_current, _stock, E_HOLE, -Dh1);
		return EROS_default(E_HOLE);
	}
	
	CELL_PtrAval=GRADIENT_D->node();

	// Ranking the cell by decreasing gradients;
	CELL_GRADIENTS.topo_ranking();

	// Recherche du 2�me point le plus bas dont
	// l'altitude est significativement differente de celle du plus bas
	int&CELL_links=CELL_GRADIENTS.links;

	size_t I2=1;
	for (;I2<CELL_links;I2++) {
		if (-CELL_GRADIENTS[I2].Dh  > Dh1+EROS_EPSILON) break;
	}

	if (I2==CELL_links) {
		// Tous les points ont la meme altitude ...� EROS_EPSILON pr�s
		// On continue tout droit en larguant EROS_EPSILON
		Depot = EROS_EPSILON + Dh1;
		set_topo(pCELL_current,Depot, E_HOLE, -Dh1); // Note que Stock est forc�ment sup�rieur � EROS_EPSILON
		P_stock -= Depot * pCELL_current->area;
		return EROS_default(E_HOLE);
	}

	double Dh2 = -CELL_GRADIENTS[I2].Dh;

	// Fill the hole
	double Depot_base = Dh1+EROS_EPSILON;
	
	EROS_transfer_coef();
	Depot = (_stock - Depot_base)*_EXP_transfer+Depot_base;

	Depot = min(Depot,Dh2);

	// L'altitude de I2 est plus haute qu'un depot complet
	// On depose tout ce qu'on peut et on continue
	set_topo(pCELL_current, Depot, E_HOLE, -Dh2);

	double d=Depot*pCELL_current->area;
	P_stock -= d;
	// E_deposit = d; // Area normalization is not necessary ... now
	 return EROS_default(E_HOLE);
*/

}
//--------------------------------
//	Fickean diffusion
//	(also called as splash effect)
//--------------------------------

double EROS::Splash_erosion ()
{

	double flux=0.;

	double TopoM = pCELL_current->E_topo;
	double dZ=0.;

	for(it_link l=pCELL_current->Link.begin(); l!=pCELL_current->Link.end(); l++)
	// The following line is not necesary in the version 
	// where the neighbors are properly defined (no)
	// if ( (PV = l->node_D)!=pCELL_current)
	{
		ptr_node PV = l->node_D;

		if (CALCUL_splash>0. && PV-pCELL_current && l->dl2>0.)
		{
			double dz=CALCUL_splash *(TopoM - PV->E_topo )/ (*l).dl2;
			dZ+= dz;
			if (dz>.5) cout << "splash plus grand que 1!!!" << endl;

			// Change topography, sediment by taking into account boundaries
			if (PV->IS_NOT_BOUNDARY()) set_topo(PV,dz,E_DIFFUSION);
			else flux += dz;
		}
#ifdef _SPLASH_ANISOTROPY_
		if ( (CALCUL_splash_X>0.) && l->X && !l->Y )
		{
			double dz=CALCUL_splash_X *(TopoM - PV->E_topo )/ (*l).dl2;
			dZ+= dz;
			if (dz>.5) cout << "splash (X) plus grand que 1!!!" << endl;

			E_grid->set_topo(PV,dz, E_DIFFUSION);
			else flux += dz;
		}
		if ( (CALCUL_splash_Y>0.) && !l->X && l->Y )
		{
			double dz=CALCUL_splash_Y *(TopoM - PV->E_topo )/ (*l).dl2;
			dZ+= dz;
			if (dz>.5)
				cout << "splash (Y) plus grand que 1!!!" << endl;

			// Change topography, sediment by taking into account boundaries
			if (PV->IS_NOT_BOUNDARY()) set_topo(PV, (type_height) dz, E_DIFFUSION);
			else flux += dz;
		}
#endif
	}

	// Change topography, sediment by taking into account boundaries
	if (pCELL_current->IS_NOT_BOUNDARY()) set_topo(pCELL_current,-dZ, E_DIFFUSION);
	else flux -= dZ;

	return flux * pCELL_current->area;
}

