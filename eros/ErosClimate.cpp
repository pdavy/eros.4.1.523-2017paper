/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

//---------------------------------------------------------
// Rainfall and climate function
//---------------------------------------------------------

#include "stdafx.h"

//-----------------------------------------------------------
// Procedure that inits the algorithm for lauching precipiton
// according to the rain intensity rules
//-----------------------------------------------------------
void EROS::RAIN__init()
{
	EROS_Marcheur=0;
	EROS_Precipitation=0;	

	if (!RAIN_map.init(E_grid))
	{
		cout << "How can the system be eroded??? Don't be silly!" << endl;
		EROS::Error("No rain defined in the system");
	}

	if (!RAIN_map.empty())
	{
		// The landing area is defined by the table rain, 
		// which defines the landing probability for each precipiton
		// The actual landing area corresponds to points where the landing probability is >0
		EROS_TU = (size_t)floor(RAIN_map.size()*EROS_TU_coef);
		// RAIN_map.size is the number of pixels whose rain value is not null.
		// It is defined in the initialisation function Init_rain_boundaries which is called in Init_links
	} else 
	{
		// This case may occur when no rain is defined at first, 
		// generally because it will be defined later by the orography and/or groundwater functions
		// The landing area covers the whole system including boundary conditions
		EROS_TU = (size_t)floor(E_grid->nbreN*EROS_TU_coef);
	}

	if ( EROS_TU <=0)
	{
		EROS::Error("TU is less than 0\n");
	}
	cout << "TU         : " << EROS_TU << endl;

	/*--------------- Initialisation du tableau des dates ---------- */
	EROS_poisson_rank=EROS_poisson;

	// Initialisation des adresses
	type_time val=-(type_time)floor(10.*EROS_TU * log(double(EROS_TU)));
#if _DEBUG
	val=-1;
#else
	if (val>=-1) val=-1;
#endif
	for (size_t i=0; i<E_grid->nbreT; i++) {
		(*E_grid)[i].Date.resize(EROS_poisson_rank, val);
	}

	// Initialisation des probabilites sur un domaine = NbreI*EROS_random_area -
	// -le champ des valeurs possibles est 0-NbreI*EROS_random_area
	// -ProbaPrecipiton donne la probabilite P[i] que la valeur tiree soit dans l'espace [0, NbreI[ apres i tirages successifs

	EROS_extra_probability.clear();
	if (EROS_random_area>1) 
	{
		double p=1./(double)EROS_random_area;
		size_t nbre = (size_t) floor(log(CALCUL_EPSILON/p)/log(1-p));
		double pm=0.;

		for (size_t i=0; i<nbre; i++) 
		{
			pm+=(p*pow(1.-p,(double)i)); // Cumulative probability
			EROS_extra_probability.push_back(pm);
		}
		nEROS_extra_probability = EROS_extra_probability.size();
			
	} else nEROS_extra_probability=0;

}

bool EROS::RAIN__check(ptr_node P)
{
	return P->E_rainfall>0;
}

//-----------------------------------
// Time-related functions
// Actually the core of the method!!!
//-----------------------------------
int EROS::TIME__increase()
{
	// Choix du nombre de tirage supplementaires pour que le marcheur soit entre 0 et nbreT
	int p=0;
	if (nEROS_extra_probability) {
		// The rain is supposed to fall on a surface larger than the considered system.
		// The time is increased by a value p whose value is drawn on the set of return periods
		static double proba;
		proba=E_grid->RANDOM_generator.rand_exclude();

		p=0;while((p<nEROS_extra_probability)&&(EROS_extra_probability[p]<proba)) p++;
		EROS_Precipitation+= p;
	}
	return ++p;
}

type_d_time EROS::TIME__lapse(ptr_node Ptr, type_time T)
{
	if (!Ptr) return 0;
	//return T - Ptr->Date.back();
	type_time t=Ptr->Date[EROS_poisson_1];
	if (t<0) return 0;
	return type_d_time(T - t);//Ptr->Date[EROS_poisson_1];
}

type_d_time EROS::TIME__calcul(ptr_node Ptr, type_time T, HYDRO_PHASE stage) 
{

	static type_d_time ti;

	if (!Ptr) return 0;

	type_time dt=TIME__lapse(Ptr, T);
	if (dt<0) return 0;

	dt=min(dt,UINT_MAX);

	/* ------------------ obsolete ---------------------- 
	if (stage==H_INIT) 
	{
		ti=(type_d_time)dt;
	} 
	else 
	{

		// 0 (default= - Take as it is in the probabilistic distribution -
		//  - This condition generate the maximum of instabilities
		//	P_date_k=dt;
		// 1- Avoid any flux decrease below its initial value
		//  - useful when flow is injected from boundary (neither useful nor to be used actually)

		// 2- Avoid any downstream decrease of flux
		//  - Mean that the flow is roughly convergent
		//	P_date_k=min (dt, P_date_k);

		if (P_Model_Q==1) dt=min (dt, ti);
		else if (P_Model_Q==2) dt=min (dt, P_date_k);

	}
	  ------------------ obsolete ---------------------- */

	return (type_d_time)dt;
}

//------------------------------------------------
//              CLIMATE FUNCTIONS
//------------------------------------------------

void EROS::CLIMATE__init()
{
	// Initialisation of rainfall fields
	Climate.clear();

	FILE * FILE_climate=NULL;

	if ( !E_grid->input_climate.empty() &&
		 ((FILE_climate = fopen(E_grid->input_climate.c_str(), "r"))!=NULL) ) {

		int n=100;


		double t, v;
		char * buf;
		char dataArg[256];

		// Number of lines in the file
		while((buf=fgets(dataArg, 256, FILE_climate))!=0) {
			if ((sscanf(dataArg,"%lf %lf",&t, &v)>0) && (dataArg[0]-'\n') && (dataArg[0]-'/')) {
				Climate.push_back(make_pair(t,v));
			}
		};
		if (Climate.empty()) return;

		fclose (FILE_climate);
		return;
	}

	return;
}

double EROS::CLIMATE__function (double time)
{
	static bool do_season = (EROS_season_recurrence>0.) && (EROS_season_duration<EROS_season_recurrence);

	double climate=0.;
	if (do_season)
	{
		int season = (int)(time/EROS_season_recurrence);
		double time_in_year = time-season*EROS_season_recurrence;
		if (time_in_year < EROS_season_duration) climate += EROS_season_rainfall;
	}

	if (!Climate.empty()) {

		// Look for the corresponding climate value, initially read from the the climate file
		// Note that n is not reinitialized because time is supposed to continuously increase

		for (size_t n=0; n<Climate.size(); n++) {
			if (fabs(Climate[n].first-time)<FLT_EPSILON) return Climate[n].second;
			else if (Climate[n].first>time) {
				if (n>0) return (Climate[n].second + (Climate[n-1].second-Climate[n].second) * (Climate[n].first-time)/(Climate[n].first-Climate[n-1].first));
				else return Climate[n].second;
			}
		}
		// Nothing was found
		return Climate.back().second;

	} else {

		// Use standard parameters
		// climatic function

		climate += EROS_climate_mean + EROS_climate_trend * time + EROS_climate_amplitude * sin (2.0*M_PI*EROS_climate_frequency*time);
		return climate;
	}

}


void EROS::RAIN__free()
{
	EROS_extra_probability.clear();
}

//------------------------------------------------
//              SEA LEVEL FUNCTIONS
//------------------------------------------------

//----------------------------------------------
// TEST if the topography is below the sea level
//----------------------------------------------
int	EROS::SEA__test() 
{
	return (pCELL_current->E_topo+pCELL_current->w_depth()>=E_grid->SEA_level);
}
//--------------------------
// Init the sea level file
//--------------------------
int EROS::SEA__init()
{
	// Initialisation of rainfall fields
	Sealevel.clear();

	FILE * file_sea=NULL;

	if ( !E_grid->input_sealevel.empty() &&
		 ((file_sea = fopen(E_grid->input_sealevel.c_str(), "r"))!=NULL) ) {

		int n=100;


		double t, v;
		char * buf;
		char dataArg[256];

		// Number of lines in the file
		while((buf=fgets(dataArg, 256, file_sea))!=0) {
			if ((sscanf(dataArg,"%lf %lf",&t, &v)>0) && (dataArg[0]-'\n') && (dataArg[0]-'/')) {
				Sealevel.push_back(make_pair(t,v));
			}
		};
		if (Sealevel.empty()) return 0;

		fclose (file_sea);
		return 1;
	}

	return 0;
}

double	EROS::SEA__function (double time)
{

	if (!Sealevel.empty()) {

		// Look for the corresponding climate value, initially read from the the climate file
		// Note that n is not reinitialized because time is supposed to continuously increase

		for (size_t n=0; n<Sealevel.size(); n++) {
			if (fabs(Sealevel[n].first-time)<FLT_EPSILON) return Sealevel[n].second;
			else if (Sealevel[n].first>time) {
				if (n>0) return (Sealevel[n].second + (Sealevel[n-1].second-Sealevel[n].second) * (Sealevel[n].first-time)/(Sealevel[n].first-Sealevel[n-1].first));
				else return Sealevel[n].second;
			}
		}
		// Nothing was found
		return Sealevel.back().second;

	} else {

		// Use standard parameters
		// climatic function

		// Put a formulae....
		return E_grid->SEA_level;
	}

}