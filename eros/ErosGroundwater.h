/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2011 CNRS (Centre National de la Recherche Scientifique), UMR 6118 Geosciences Rennes
 
    This file is part of Gridvisual

    Gridvisual is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gridvisual is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gridvisual.  If not, see <http://www.gnu.org/licenses/>.
*/
typedef map<ptr_node, double> type_overflow;
type_overflow list_overflow;