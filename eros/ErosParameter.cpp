/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

//---------------------------------------------------------
// Input functions
//---------------------------------------------------------


#include "stdafx.h"

//--------------------------------------------
// The list values obtained from the dat files
//--------------------------------------------
vector <string> dat_values;

//--------------------------------------------
// Init the calculation parameters with the
// data stored in file_name
//--------------------------------------------
string EROS::write_title(string file_name)
{
	string str="\n";
	str += "\n//------------------------------------------------\n";
	str += "// ";
	str += file_name;
	str += "\n//------------------------------------------------\n";
	return str;
}

int EROS::Init_parameters (string file_name, ofstream & file_parameter, input_method method)
{

	//--------------------------------------------
	// Read the dat file to fix erosion parameters
	//--------------------------------------------
	ifstream fin(file_name.c_str());
	if (!fin.is_open()){
		cout << "le fichier de donnees " << file_name << " n'existe pas" << endl;
		return(0); // May be replace with the function : ReadDataHand(E_grid);
	}
	//--------------------------------------------
	// Put data into a vector of string
	//--------------------------------------------
	vector <string> data_file;
	while(!fin.eof())
	{
		string str;
		getline(fin,str);
		if (!fin.fail())
		{
			remove_first_blank(str);
			//remove_end_of_line(msg);
			data_file.push_back(str);
		}
	}
	fin.close();

	if (file_parameter.is_open()) file_parameter << write_title(file_name);
	cout << write_title(file_name);

	Init_parameters(data_file,file_parameter,method);
	
	return 1;


}
//------------------------------------------------------
// Init variables and set the default values
//------------------------------------------------------
void EROS::Init_default_parameters()
{
#define DATA_(a,b,c) InitialData.push_back(Cinput_data(a,&b,c))

	double dummy;

	DATA_("transition area", EROS_Q_process, 1.0);
	DATA_("transition flux", EROS_Q_process, 1.0);
	DATA_("aire critique", EROS_Q_process, 1.0);

	//if this variable is 0 (dimitri mode), the process change is an area whatever the climate variable is
	//if this variable is 1 (normal mode), the process change is a flux
	//(see ErosErode.cpp)
	DATA_("mode of process change with rain", EROS_Model_process, true);

	// Flow model

	// Hydraulic diffusion model (obsolete)
	DATA_("hydraulic model", P_Model_Diffusion, 0);
	DATA_("walking model", P_Model_Diffusion, 0);

	// Hydaulic diffusion coefficient
	DATA_("coefficient de diffusion", P_Cte_Diffusion, 1.0);
	DATA_("diffusion coefficient", P_Cte_Diffusion, 1.0);
	DATA_("diffusion", P_Cte_Diffusion, 1.0);
	DATA_("walking diffusion coefficient", P_Cte_Diffusion, 1.0);
	DATA_("walking diffusion", P_Cte_Diffusion, 1.0);
	DATA_("walking coefficient", P_Cte_Diffusion, 1.0);
	DATA_("walking", P_Cte_Diffusion, 1.0);


	// Flow parameters
	// The Poisson coefficient (k)  is used to calculate discharge: Q=k*(precipiton volume)/dtk
	// dtk is the time spent since the precipiton that arrived in the kth rank before this
	DATA_("coefficient de Poisson", EROS_poisson, 10);
	DATA_("poisson coefficient", EROS_poisson, 10);
	DATA_("poisson", EROS_poisson, 10);

	DATA_("flood model", FLOW_calcul, 1);
	DATA_("flood", FLOW_calcul, 1);
	// 0: no water depth
	// 1: stationary water depth
	// 2: kinematic wave approximation

	// Flow depth calculation
	// DATA_("flow model", P_Model_Q, 0); // (obsolete)
	DATA_("flow model", FLOW_Model_string, "manning");
	DATA_("flow friction model", FLOW_Model_string, "manning");
	// may be either chezy or manning

	DATA_("minimum flow coefficient", type_node::coef_min, 0.0);

	// chezy (default) equation is: q=Cf*depth^3/2*slope^1/2 with Cf the friction coefficient (about 20 in SI unit)
	// manning equation is: q=1/n*depth^5/3*slope^1/2 with n the manning coefficient (about 0.03 in SI unit)
	DATA_("Darcy-Weisbach coefficient", FLOW_Cte_Cz, 0.0);
	DATA_("Manning coefficient", FLOW_Cte_Cz, 0.0);
	DATA_("friction coefficient", FLOW_Cte_Cz, 0.0);
	DATA_("friction", FLOW_Cte_Cz, 0.0);
	DATA_("Cf", FLOW_Cte_Cz, 0);
	// Although not appropriate, this expression for the Manning coefficient is taken to keep compatibility with previous version
	DATA_("typical water height", FLOW_Cte_Cz, 0.0);
	DATA_("Initial water height", FLOW_Cte_Cz, 0.0);
	DATA_("flow coefficient", FLOW_Cte_Cz, 0.0);
	// A parameter that limits the water depth at outflow boundaries
	DATA_("water depth limit", FLOW_limit, -1.);
	DATA_("flow limit", FLOW_limit, -1.);
	// The inertia component
	DATA_("flow inertia coefficient", FLOW_Cte_Inertia, 0.0);
	DATA_("inertia", FLOW_Cte_Inertia, 0.0);
	// Lake
	DATA_("fall in lakes", fall_in_lake_max, 100);
	DATA_("lakes", fall_in_lake_max, 100);

	//---------------------------------
	// Channel width and diagonal links
	//---------------------------------
	// If the parameter is not nil, it is assumed that the channel cover the whole grid cell
	// Thus an increase of the channel length along diagonals entails a decrease of the channel width,
	// which is compensated by the probability of moving precipitons.
	// This width reduction gives higher q (flow per unit width) values for diagonals, entailing an increase of erosion rates,
	// which likely compensates the decrease of upstream flow.
	// Even if it is likely the best option, the eventual results is dependent of the erosion law.
	// Tests have been performed on the volcano case.
	DATA_("diagonal width reduction", E_grid->DIAGONAL_WIDTH_REDUCTION, 0);
	DATA_("diagonal", E_grid->DIAGONAL_WIDTH_REDUCTION, 0);
	// 0: no width reduction is applied for diagonal directions
	// 1: the width reduction is always applied 
	// 2: the width reduction is not applied when the river width is given by a function
	// Should be removed when fixed


	//DATA_("flow time constant", FLOW_time_cste, 100);
	//DATA_("typical water time", FLOW_time_cste, 100);
	//DATA_("flow velocity equation", FLOW_equation, 2);
	//DATA_("flow diffusion", FLOW_diffusion, 0.);
	//DATA_("flow averaging method", FLOW_method_avg, 1);
	//DATA_("water method", FLOW_method_avg, 1);

	DATA_("flow depth constant", FLOW_Cte_Depth, 0.);
	DATA_("water depth coefficient", FLOW_Cte_Depth, 0.);
	DATA_("flow depth coefficient", FLOW_Cte_Depth, 0.);

	// Variable that avoid erosion calculations
	DATA_("flow only", FLOW_only, 0);
	DATA_("hydraulic calculation", FLOW_only, 0);

	DATA_("fictious area", EROS_random_area, 1);

	// FLow characteristics
	DATA_("flow width of hillslope", (EROS_Cte_W[HILLSLOPE]), 0.0);
	DATA_("river width of hillslope", (EROS_Cte_W[HILLSLOPE]), 0.0);
	DATA_("river width", (EROS_Cte_W[FLUVIAL]), 0.0);
	DATA_("flow width of channel", (EROS_Cte_W[FLUVIAL]), 0.0);
	DATA_("river width of channel", (EROS_Cte_W[FLUVIAL]), 0.0);

	DATA_("flow exponent of flow width for hillslope", (EROS_Exp_W_Q[HILLSLOPE]), 0.0);
	DATA_("flow exponent of river width for hillslope", (EROS_Exp_W_Q[HILLSLOPE]), 0.0);
	DATA_("flow exponent of river width for channel", (EROS_Exp_W_Q[FLUVIAL]), 0.0);

	// The erosion model
	// DATA_("erosion model", EROS_Model, 0);
	DATA_("erosion model", EROS_Model_string, "stream_power");
	// stream_power (0): based on the stream power assumption e=f(q,s)
	// shear_stress (1): based on the shear stress S: e=E*(S-Sc)^a, where S is density*water_depth*hydraulic_slope
	// shear_stress_2 (2): based on the shear stress with H, the water depth, calculated from q and s from the Darcy-Weisbach formula 

	// Model of erosion threshold
	DATA_("Erosion threshold model", EROS_Model_T, 0);
	//0	: Erosion threshold is constant 
	//1	: Erosion threshold is proportional to flow width W=Q^0.5 (not used anymore)

	// Model of slope erosion
	DATA_("Slope erosion model", EROS_Model_E_slope, 1);
	// 0: topographic slope
	// 1: hydraulic slope
	// 2: combined average of topo and hydro slopes

	// Limit erosion processes to FLUVIAL (instead of HILLSLOPE and FLUVIAL)
	DATA_("Erosion process limiter", PROCESS_limiter, 0);

	// Erosion parameters 
	// Now the ratio between channel and erosion rates at transition discharge
	DATA_("channel overefficiency", EROS_Cte_Er[HILLSLOPE], -1.0); // to be compatible with old versions
	DATA_("hillslope overefficiency", EROS_Cte_Er[FLUVIAL], -1.0);

	DATA_("hillslope erosion coefficient", EROS_Cte_Eb[HILLSLOPE], 1.0);
	DATA_("channel erosion coefficient", EROS_Cte_Eb[FLUVIAL], 1.0);

	DATA_("power exponent for hillslope slope", (EROS_Exp_E_s[HILLSLOPE]), 1.0);
	DATA_("power exponent for channel slope", (EROS_Exp_E_s[FLUVIAL]), 1.0);

	DATA_("stress exponent for hillslope", (EROS_Exp_E_s[HILLSLOPE]), 1.0);
	DATA_("stress exponent for channel", (EROS_Exp_E_s[FLUVIAL]), 1.0);
	DATA_("stress exponent", (EROS_Exp_E_s[FLUVIAL]), 1.0);

	DATA_("power exponent for hillslope discharge", (EROS_Exp_E_q[HILLSLOPE]), 1.0);
	DATA_("power exponent for channel discharge", (EROS_Exp_E_q[FLUVIAL]), 1.5);

	DATA_("river-to-bed transfert length for hillslope", (EROS_Cte_Ld[HILLSLOPE]), 1.0);
	DATA_("river-to-bed transfer length for hillslope", (EROS_Cte_Ld[HILLSLOPE]), 1.0);

	DATA_("river-to-bed transfert length for channel", (EROS_Cte_Ld[FLUVIAL]), 1.0);
	DATA_("river-to-bed transfer length for channel", (EROS_Cte_Ld[FLUVIAL]), 1.0);

	DATA_("Lq_cte", (EROS_Cte_Ld[FLUVIAL]), 1.0);
	DATA_("Lq", (EROS_Cte_Ld[FLUVIAL]), 1.0);

	DATA_("lower limit of transfer length for hillslope", (EROS_Cte_Ld_min[HILLSLOPE]), 0.0);
	DATA_("minimum transfer length for hillslope", (EROS_Cte_Ld_min[HILLSLOPE]), 0.0);

	DATA_("lower limit of transfer length for channel", (EROS_Cte_Ld_min[FLUVIAL]), 0.0);
	DATA_("minimum transfer length for channel", (EROS_Cte_Ld_min[FLUVIAL]), 0.0);

	DATA_("maximum transfer length for hillslope", (EROS_Cte_Ld_max[HILLSLOPE]), FLT_MAX);
	DATA_("maximum transfer length for channel", (EROS_Cte_Ld_max[FLUVIAL]), FLT_MAX);

	DATA_("flow exponent of transfert length for hillslope", (EROS_Exp_Ld_q[HILLSLOPE]), 0.0);
	DATA_("flow exponent of transfer length for hillslope", (EROS_Exp_Ld_q[HILLSLOPE]), 0.0);
	DATA_("flow exponent of transfert length for channel", (EROS_Exp_Ld_q[FLUVIAL]), 0.0);
	DATA_("flow exponent of transfer length for channel", (EROS_Exp_Ld_q[FLUVIAL]), 0.0);

	DATA_("slope exponent of transfert length for hillslope", (EROS_Exp_Ld_q[HILLSLOPE]), 0.0);
	DATA_("slope exponent of transfer length for hillslope", (EROS_Exp_Ld_q[HILLSLOPE]), 0.0);
	DATA_("slope exponent of transfert length for channel", (EROS_Exp_Ld_q[FLUVIAL]), 0.0);
	DATA_("slope exponent of transfer length for channel", (EROS_Exp_Ld_q[FLUVIAL]), 0.0);

	DATA_("sediment erodability", EROS_Cte_Es, -1.0);
	DATA_("sediment threshold", EROS_Cte_Ts, 0.0);
	DATA_("threshold for sediment", EROS_Cte_Ts, 0.0);

	DATA_("basement erodability", EROS_Cte_Eb[FLUVIAL], 1.0);
	DATA_("basement erodability for channel", EROS_Cte_Eb[FLUVIAL], 1.0);
	DATA_("basement erodability for hillslope", EROS_Cte_Eb[HILLSLOPE], 1.0);

	DATA_("threshold for channel", (EROS_Cte_Tb[FLUVIAL]), 0.0);
	DATA_("basement threshold", EROS_Cte_Tb[FLUVIAL], 0.0);
	DATA_("basement threshold for channel", EROS_Cte_Tb[FLUVIAL], 0.0);
	DATA_("threshold slope for channel", (EROS_Cte_Tb[FLUVIAL]), 0.0);

	DATA_("threshold for hillslope", (EROS_Cte_Tb[HILLSLOPE]), 0.0);
	DATA_("basement threshold for hillslope", EROS_Cte_Tb[HILLSLOPE], 0.0);
	DATA_("threshold slope for hillslope", (EROS_Cte_Tb[HILLSLOPE]), 0.0);


	DATA_("gravity slope", EROS_S_max, 0.0);

	DATA_("splash coefficient", EROS_splash, 0.0);
	DATA_("splash", EROS_splash, 0.0);

	// Altitude change limiter
	DATA_("Erosion limiter", EROS_limiter_parameter, 0.);
	DATA_("Erosion max", EROS_limiter_parameter, 0.);
	DATA_("Limiter", EROS_limiter_parameter, 0.);
	// Fix the maximum altitude change during calculation
	// If 0 (default value), the parameter is equal to 1/10000 of the mean altitude

	DATA_("initial sediment stock", EROS_stock_initial, 0.0);
	DATA_("initial stock", EROS_stock_initial, 0.0);
	DATA_("stock initial", EROS_stock_initial, 0.0);
	// The stock is given per precipiton
	DATA_("sediment stock standard deviation", EROS_stock_variation, 0.0);
	DATA_("sediment stock fluctuation", EROS_stock_variation, 0.0);

#ifdef _SPLASH_ANISOTROPY_
	DATA_("splash coefficient along X", EROS_splash_X, 0.0);
	DATA_("splash coefficient along Y", EROS_splash_Y, 0.0);
#endif
	//---------------------
	// rainfall variables
	//---------------------
	// rainfall is a proportionality coefficient that will be applied to the rain map
	// equal to the background rainfall when no rainmap is defined
	DATA_("pluviometry", RAIN_map.rainfall, 1.0);
	DATA_("rainfall", RAIN_map.rainfall, 1.0);
	// a way to fix the total inflow (discharge) if the inflow value is positive
	DATA_("inflow", RAIN_map.Qinflow, -1.0);
	DATA_("discharge", RAIN_map.Qinflow, -1.0);

	//---------------------
	// Climate variables
	//---------------------
	DATA_("climate", EROS_climate_mean, 1.0);
	DATA_("pluviometry", EROS_climate_mean, 1.0); // for compatibility reasons only
	DATA_("climatic mean", EROS_climate_mean, 1.0);
	DATA_("climatic frequency", EROS_climate_frequency, 0.0);
	DATA_("climatic tendency", EROS_climate_trend, 0.0);
	DATA_("amplitude of climatic variations", EROS_climate_amplitude, 0.0);

	DATA_("season recurrence time", EROS_season_recurrence, 0.0);
	DATA_("wet season duration", EROS_season_duration, 1.0);
	DATA_("wet season rainfall", EROS_season_rainfall, 1.0);

	DATA_("sea level", E_grid->SEA_level, 0.0);
	DATA_("undersea coefficient", EROS_Cte_Ld_sea, 0.0);

	//------------------
	// Tectonic input
	//------------------
	DATA_("where is tectonic uplift", E_grid->TECTO_uplift_where, 0);
	DATA_("tectonic uplift", E_grid->TECTO_uplift_rate, 0.0);
	DATA_("tectonic uplift rate", E_grid->TECTO_uplift_rate, 0.0);
	DATA_("tectonic frequency", dummy, 0.0);
	//DATA_("tectonic frequency", EROS_uplift_frequency, 0.0);

	DATA_("thrusting uplift", E_grid->TECTO_thrust_uplift_rate, 0.0);
	DATA_("thrusting rate", E_grid->TECTO_thrust_rate, 0.0);
	DATA_("thrust position", E_grid->TECTO_thrust_position, 0.0);
	DATA_("thrust width", E_grid->TECTO_thrust_width, 0.0);
	DATA_("thrust length", E_grid->TECTO_thrust_length, 0.0);
	DATA_("thrust sense", E_grid->TECTO_thrust_sense, 0);
	DATA_("thrust angle (in degrees)", E_grid->TECTO_thrust_angle, 0.0);

	DATA_("isostatic factor", E_grid->TECTO_isostatic_factor, 0.0);
	DATA_("isostatic length", E_grid->TECTO_isostatic_length, 1.0);
	DATA_("isostatic offset", E_grid->TECTO_isostatic_offset, 0.0);

	DATA_("strike-slip line", E_grid->TECTO_strikeslip_where, 0.);
	DATA_("strike-slip rate", E_grid->TECTO_strikeslip_rate, 0.);
	DATA_("strike-slip sense", E_grid->TECTO_strikeslip, 0);

	//---------------------------------------------
	// TIME and related calculation parameters
	//---------------------------------------------
	DATA_("time", EROS_P, -1.0);
	DATA_("time step", EROS_P, -1.0);
	DATA_("time constant", EROS_P, -1.0);
	DATA_("time coefficient", EROS_P, -1.0);
	DATA_("time minimum", EROS_P_min, -1.0);
	DATA_("time maximum", EROS_P_max, -1.0);

	// STEP_TU: If 1, the time step is defined for 1 TU; if 0, for each precipiton
	DATA_("step TU", STEP_TU, 0);

	DATA_("precipiton", EROS_P_v, -1.0);
	DATA_("precipiton volume", EROS_P_v, -1.0);
	DATA_("minimum volume of precipiton", EROS_P_vmin, -1.0);
	DATA_("minimum volume", EROS_P_vmin, -1.0);

	DATA_("volume min", EROS_P_vmin, -1.0);
	DATA_("maximum volume of precipiton", EROS_P_vmax, -1.0);
	DATA_("maximum volume", EROS_P_vmax, -1.0);
	DATA_("volume max", EROS_P_vmax, -1.0);

	DATA_("precipiton change with climate", EROS_P_climate, false);

	DATA_("skip calcul", EROS_calcul_skip, 0);

	DATA_("writing time", EROS_print_TU, 1);
	DATA_("writing time (TU)", EROS_print_TU, 1);

	DATA_("final time (TU)", EROS_time_TU, -1.0);
	DATA_("temps final (TU)", EROS_time_TU, -1.0);

	DATA_("final time", EROS_time_REAL, -1.0);
	DATA_("temps final", EROS_time_REAL, -1.0);
	DATA_("end", EROS_time_REAL, -1.0);

	DATA_("Initialization time", EROS_initialization, 0.);

	DATA_("drawing-testing writing time (TU)", EROS_draw_TU, -1);
	DATA_("drawing time (TU)", EROS_draw_TU, -1);

	DATA_("drawing-testing writing time", EROS_draw_REAL, -1.0);
	DATA_("drawing time", EROS_draw_REAL, -1.0);

	DATA_("TU", EROS_TU_coef, 1.0);
	DATA_("TU coefficient", EROS_TU_coef, 1.0);
	DATA_("TU time", EROS_TU_coef, 1.0);
	DATA_("grid covering factor", EROS_TU_coef, 1.0);
	DATA_("intermediate time", EROS_TU_coef, 1.0);
	DATA_("print file time", EROS_TU_coef, 1.0);

	DATA_("flux normalization", RAIN_map.flux_scaling, 0);
	// also changed with the option -plain
	// O: the flux is not normalized 
	// 1: the flux is normalized by the total surface except boundaried (as it is for the uplift so that the flux is directly comparable to the uplift rate)
	// 2: the flux is normalized by the inflow Q (volume)
	DATA_("flux area", RAIN_map.flux_area, -1.);


	DATA_("Random generator seed", EROS_seed, -1);
}
//--------------------------------------------
// Init the calculation parameters with the
// data stored in a vector of string
//--------------------------------------------
int EROS::Init_parameters (vector <string> data_file, ofstream & file_parameter, input_method method)
{
	//-----------------------------------
	// Read parameters in the vector
	//-----------------------------------
	for (size_t i=0; i<data_file.size();i++)
	{
		string line=data_file[i];

		//Save results in the .ini file
		if (file_parameter.is_open()) file_parameter << line;

		remove_first_blank(line);
		
		if ( (line[0]-'\n') && (line[0]-'/') )
		{
			string value, comment;
			std::stringstream stream(line);
			stream>>value;
			if (!stream.fail()) 
			{
				getline(stream,comment);
				remove_first_blank(comment);

				switch (Read_parameters(comment,value,method))
				{
				case 0: 
					cout << "UNKNOWN [" << setiosflags(ios::left) << setw(50) << comment << "]  "<< value << endl;
					break;
				case 1:
					cout << "VALID   [" << setiosflags(ios::left) << setw(50) << comment << "]  " << value << endl;
					break;
				case 2:
					cout << "REINPUT [" << setiosflags(ios::left) << setw(50) << comment << "]  " << value << endl;
					if (file_parameter.is_open()) file_parameter << endl << "// the above line replace another one";
					break;
				}
			} // if stream fails
		} //if (line)
		
		if (file_parameter.is_open()) file_parameter << endl;
	}

	if (file_parameter.is_open()) file_parameter.flush();

	return 1;
}
//-----------------------------
// Test and read the parameters
//-----------------------------

int EROS::Read_parameters (string comment, string value, input_method method)
{
	// Variable servant � v�rifier que la ligne est trait�e
	int result = 0;
	// Take the comment
	comment = replace_blank_and_lower(comment);

	// Balayage des valeurs pr�d�finies pour retrouver la cha�ne de 
	// caract�re qui d�crit la variable
	for (size_t i=0; i<InitialData.size(); i++) if ( InitialData[i].data() ) 
	{
		string message=InitialData[i].message;
		bool _test_=(comment==message);
		// Another trial...
		if (!_test_ && method) {
			// Try to find a subset of the initial message in the chain comment
			_test_ = (comment.find(message)!=comment.npos);
			if (_test_ && (method==INPUT_ASK) ) {
				cout << "do you accept [" << comment << "] for [" << message << "] ";
				_test_ = what_I_do ('y','n');
			}
		}

		if (_test_) 
		{
			result=(int)InitialData[i].assign(value);
			vector<string>::iterator it=find(dat_values.begin(),dat_values.end(),InitialData[i].message);
			// if new value/message
			if (it==dat_values.end()) 
				dat_values.push_back(InitialData[i].message);
			else  // if not
			{
				(*it)=InitialData[i].message;
				result = 2;
			}
			// Stop looking for
			break;
		} // if Recherche
	} // for i

	if (!result && (method==INPUT_ASK)) {

		comment += ": Command line not understood. Exit ? ";
		if ( what_I_do ('y','n',comment) ) 
		{
			EROS::Error("don't know what to do");
		}
	
	} // if testData

	return result;

}
/* JR procedure

void Replace_Character( char * string) {

	for(unsigned int i=0; i<strlen(string);i++) {
		if(string[i] == '_')
			string[i] = ' ';
	}
}


int Read_Command_Line ( int argc, char **argv, TypeData InitialData[TYPEDATA_LENGTH])
{
	for(size_t j=0; j<argc; j++) {
		Replace_Character( argv[j]);
		argv[j] = strlwr(strdup(argv[j]));
		// Balayage des valeurs pr�d�finies pour retrouver la cha�ne de
		//		caract�re qui d�crit la variable
		for (size_t i=0; (i<TYPEDATA_LENGTH); i++) if(strlen(InitialData[i].message)) {
			char * stdArg = strlwr(strdup(InitialData[i].message));
			if (strstr(argv[j],stdArg)) {
				if (InitialData[i].type==DOUBLE_TD) {
					double  * v=(double *)InitialData[i].data;
					sscanf( argv[j+1], "%lf", v);
				} else if (InitialData[i].type==INTEGER_TD) {
					int * v= (int*)InitialData[i].data;
					sscanf( argv[j+1], "%d", v);
				} else if (InitialData[i].type==BOOL_TD) {
					bool * v= (bool*)InitialData[i].data;
					sscanf( argv[j+1], "%d", v);
				} else if (InitialData[i].type==CHAR_TD) {
					char * v= (char*)InitialData[i].data;
					sscanf( argv[j+1], "%s", v);
				} else if (InitialData[i].type==STRING_TD) {
					string * v= (string*)InitialData[i].data;
					*v = argv[j+1];
				}
				break;
			} // if Recherche
		} // for i
	}

	return (1);
}

*/