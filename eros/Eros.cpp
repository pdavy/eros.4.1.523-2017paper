/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

//---------------------------------------------------------
// Class initialization
//---------------------------------------------------------

#include "stdafx.h"

//------------------------------------
//----- Erosion parameters -----------
//------------------------------------

EROS::EROS(class_grid * _grid) : E_grid(_grid)
{
	//----------------------
	// Important definitions
	//----------------------
	EROS_seed=-1; // redefined from the parameter list (EROSparameter.hpp)
	EROS_message=0;
	EROS_key=0;
	//-----------------------------
	// The way of initializing data
	//-----------------------------
	EROS_input = INPUT_STRICT;
	//-----------------
	// Calcul variables
	//-----------------

	EROS_P=-1.;
	EROS_P_max = -1.;
	EROS_P_min = -1.;
	EROS_P_v = -1.;
	
	// Option 1: EROS_P_climate = false
	// The precipiton volume is constant through time
	// Option 2: EROS_P_climate = true
	// The precipiton volume is varying with climatic variable
	// But the time step between two recorded values is exactly EROS_P  
	EROS_P_climate = false;

	EROS_calcul_skip=0;
	P_force_deposit=true;

	FLOW_limiter=false;
	FLOW_limit=-1.;

	EROS_time_init=0.;
	stock_equilibrium = false;

	EROS_end_condition = IF______0;
	EROS_end = IF______0;
	EROS_draw_condition = IF______0;
	
	// Basic options for the "saving-grid" parameter (changed in the program)
	E_grid->save_init(SAVE_TXT|SAVE_INI|SAVE_INITIALIZE|SAVE_TOPO|SAVE_HUM|SAVE_SED|SAVE_DISCHARGE|SAVE_CHANNEL|SAVE_STOCK); 

	EROS_end_height	= 0.0;
	EROS_end_flux	= 0.0;
	outflow_averaging=20;

	//--------------------------
	// Time adaptative variables
	//--------------------------
	adapt_time=false;
	adapt_time_errmax=20;
	adapt_time_errmin=-1;
	adapt_time_op="+";
	adapt_time_factor=sqrt(2.);//pow(3.,1./3.);
	adapt_time_type=0;
	//--------------------
	// Discharge variables
	//--------------------
	EROS_Q=1.;	// The characteristic discharge r(*)*A(*)
	EROS_k=1.;	// The normalisation factor used with time_k : nPoisson*Astat
	Pk_time=1.;	// A variable required to calculate V/Q. Defined in the function EROS_coefficients
	//------------------
	// Erosion variables
	//-----------------------
	EROS_Exp_El_q = 0.5;
	//----------------------
	// Rain-related variable
	//----------------------
	P_water=0.0;
	//-----------------------
	// Hydrodynamic variables
	//-----------------------
	CELL_GRADIENTS.grid(E_grid);
	AMONT_GRADIENTS.grid(E_grid);
}
EROS::~EROS()
{
	EROS_extra_probability.clear();
	EROS_outflux.clear();
	InitialData.clear();
}
