/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/
#pragma once

class ErosPrecipiton
{
public:
	ErosPrecipiton(void);
	~ErosPrecipiton(void);

	/* should contain (at least)
	// ptr_node	CELL_PtrAmont;
	ptr_node	pCELL_current;
	//ptr_node	pCELL_aval;
	int		CELL_amont;	// The upstream direction

	Node_typology CELL_GRADIENTS;

	double	P_stock;	// Sediment "stock" within the precipiton
	double	P_water;	// Initial precipiton water volume
	double	P_water_t;	// Precipiton water volume along path (normalized between 0 and 1). Supposed to decrease with infiltration...
	double	P_dt;		// The precipiton time scale V/Q
	double	P_dist;		// The distance run by the precipiton
	int		P_step;	// The distance (in pixel)


	double	GET_P_date();
	double	GET_P_volume();
	double	GET_P_height(ptr_node P);
	*/


};
