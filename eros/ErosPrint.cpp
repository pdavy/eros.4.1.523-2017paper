/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

//---------------------------------------------------------
// Main functions
//---------------------------------------------------------

/*      Programme d'erosion...			        					*/
/*																	*/
/*      programme de marcheurs d'erosion et de sedimentation. 		*/
/*      processus d'erosion: de type diffusion sur le versant,		*/
/*		de type transport dans le reseau. Cette	version du model	*/
/*		permet introduit une depndence non-lineaire de qs=qm		*/

#include "stdafx.h"

/* PARAMETRES DE GRILLE */

/*----------------------------------------------------------------------------------*/
/*----------------- Additional procedures: SAVE, PRINT, ...-------------------------*/
/*----------------------------------------------------------------------------------*/

int		SCREEN_LINES = 25;
//---------------------------------------
// Initialize the variable to be printed
//---------------------------------------
int	EROS::print_init()
{
	// Set all variables to the "no-print" flag
	E_grid->print_init();
	// Choose the variables to be printed
	E_grid->print_init(TOPO,PRINT_AVG,PRINT_SCRN|PRINT_FILE);
	E_grid->print_init(TOPO,PRINT_STD,PRINT_FILE);
	E_grid->print_init(TOPO,PRINT_MAX,PRINT_CALC);
#ifdef _WATER_HEIGHT_
	if (FLOW_calcul) 
	{
		E_grid->print_init(WATER_DEPTH,PRINT_AVG,PRINT_SCRN|PRINT_FILE);
		E_grid->print_init(WATER_DEPTH,PRINT_NUM|PRINT_NONZERO,PRINT_FILE);
	}
#endif
	
	// Save the average discharge
	E_grid->print_init(DISCHARGE_IN,PRINT_AVG|PRINT_BOUNDARY|PRINT_NONZERO,PRINT_SCRN|PRINT_FILE);
#ifdef _WATER_HEIGHT_
	//if (FLOW_calcul) E_grid->print_init(DISCHARGE_OUT,PRINT_AVG|PRINT_BOUNDARY|PRINT_NONZERO,PRINT_SCRN|PRINT_FILE);
#endif
	E_grid->print_init(SLOPE,PRINT_AVG,PRINT_SCRN|PRINT_FILE);
	E_grid->print_init(SLOPE,PRINT_BOUNDARY|PRINT_NONZERO,PRINT_FILE);


	return 1;
}

//-----------------------------------
// Initialize the print file function
//-----------------------------------
FILE* EROS::fprint_init(string File_txt, int reprise, double *time_init, string input_directory)
{

	FILE * file;
	string File;

	char FinDeLigne='\n';
	if (time_init) (*time_init)=0.;

	switch (reprise) {
	case 0:
	case 1:

		if ( (file=fopen(File_txt.c_str(),"wt"))==NULL) 
		{
			cout << "Cannot initialize the file " << File_txt << endl;
			return NULL;
		}
		break;

	case 2:
		//Le fichier texte se poursuit...
		File = File_txt;
		if (!input_directory.empty()) File = change_directory(File,input_directory);
		if ( (file=fopen(File.c_str(),"rt"))==NULL) {
			cout << "Cannot read the file " << File_txt << endl;
			// Try to go on by bypassing the previous file
			return fprint_init(File_txt,1,time_init);
		}


		rewind(file); // to the last element

		// Read the time in file
		char data[10000];
		char *  buf;
		double t;

		while((buf=fgets(data, 10000, file))!=0) 
		{
			if (sscanf(data,"%lf",&t)>0)
			{
				if (time_init) (*time_init)=t;
			}
		};

		// read the last character
		if (fseek(file,-1L,SEEK_CUR)!=0) {
			fclose(file);
			return 0;
		}
		char last_ch=fgetc(file);
		//Close the file
		fclose (file);

		// Initiate (or reopen) the txt file
		if ( (file=fopen(File_txt.c_str(),"at"))==NULL) {
			cout << "Cannot initialize the file " << File_txt << endl;
			return NULL;
		}
		// Write a carriage return if the last character is not a carriage return  
		if (last_ch!=FinDeLigne) {
			last_ch=FinDeLigne;
			fwrite(&last_ch,1,1,file);
		}

		break;

	}

	cout << "text file  : " << File_txt << endl;

	fflush(file);

	/*----------------------------------------------------------------------*/
	/*----------------- scaling of the time --------------------------------*/
	/*----------------------------------------------------------------------*/
	//E_grid->TECTO_uplift_step=(type_time)floor((double)EROS_TU*EROS_uplift_frequency);
	//E_grid->ComptStep=(type_time)floor((double)EROS_TU*EROS_TU_coef);// WritingTime should always be 1

	return file;

}

//-----------------------------------------------
// Init the calculation parameters
//------------------------------------------------
int EROS::print_parameters(Carg_list arg_list, ofstream & file_parameter)
{
	/* ----------- Save parameters in a .ini file ------------------*/

	if ( !file_parameter.is_open() ) return 0;

	file_parameter << "____________________ PROGRAM _______________________" << endl;
	cout << endl << setw(50) << " PROGRAM" << endl;
	file_parameter << arg_list[0].line() << endl;
	cout << arg_list[0].line() << endl;
	
 
	file_parameter << endl << "______________ COMMAND LINE OPTIONS ________________" << endl;
	cout << endl << setw(50) << " COMMAND LINE OPTIONS" << endl;

	for (size_t i=1; i<arg_list.size(); i++) {
		file_parameter << arg_list[i].line() << endl;
		cout << arg_list[i].line() << endl;
	}

	return 1;
}

//---------------------------------------------------
// Print the main results
//---------------------------------------------------
bool EROS::print_result (bool header)
{
	static double _time=EROS_time;
	static int	_header=0;

	if (E_grid->save_is(SAVE_TXT)) fprint_result();
	// Print the header if _header is 0
	if (!_header) print_header_screen();
	
	// Print on screen
	// and print the header every X (about 50) lines
	int _inc = print_screen(header) || !_header ;
	_header = (_header+_inc) % SCREEN_LINES;


	return true;
}

//---------------------------------------------------
// Reset results after printing
//---------------------------------------------------
void EROS::reset_result()
{

	nbre_diag=0;
	nbre_horiz=0;

	nbre_erode=0;

	precipiton_out=0;
	precipiton_in=0;
	precipiton_off=0;
	distance_in=0.;
	distance_out=0.;
	step_in=0;
	step_out=0;

	EROS_outflow=0.;

	// Insert a new value in the outflux table
	EROS_outflux.insert(EROS_outflux.begin(),0.);
	// Delete the latest if the number of value is sufficient
	if (outflow_averaging < EROS_outflux.size()) EROS_outflux.pop_back();

	EROS_influx=0.;
	Bilan=E_grid->print_value(TOPO,PRINT_AVG);
	E_grid->nChannel=0;

}
//---------------------------------------------------
// 1. Print results in the .txt file
//---------------------------------------------------
void EROS::fprint_result (int init)
{
	if (!File_results) return;

	if (init) 
	{
		// Basic variables
		fprintf (File_results,"time");
		fprintf (File_results,"\tdt");
		fprintf (File_results,"\tN(TU)");
		fprintf (File_results,"\tdV_P");
		fprintf (File_results,"\tdH_P");

		fprintf (File_results,"\trainfall");
		if (!Sealevel.empty()) fprintf (File_results,"\tsea level");
		// The main parameters defined in the print_init function
		E_grid->print_header(File_results);
		// Basic variables
		fprintf (File_results,"\tH2O Flow out");
		fprintf (File_results,"\tMat flow out");
		fprintf (File_results,"\tMat flow in");
		if (PROCESS_limit)	fprintf (File_results,"\tChannels");
		fprintf (File_results,"\tPrecip.Out(%%)");
		fprintf (File_results,"\tPrecip.In(%%)");
		fprintf (File_results,"\tPrecip.dist");
		fprintf (File_results,"\tPrecip.step");
		fprintf (File_results,"\tPrecip.dist_out");
		fprintf (File_results,"\tPrecip.dist_in");
		fprintf (File_results,"\tMat eq (%%)");
		fprintf (File_results,"\tErosion defaults");
		fprintf (File_results,"\tDeposition defaults");
		fprintf (File_results,"\tCPU");

		fprintf (File_results,"\n");
	}
	else
	{
		// Basic variables
		fprintf (File_results,"%g",	  EROS_time);
		fprintf (File_results,"\t%g", EROS_P);
		fprintf (File_results,"\t%g", TU(EROS_Marcheur));
		fprintf (File_results,"\t%g", P_water);
		fprintf (File_results,"\t%g", P_height);

		fprintf (File_results,"\t%g", EROS_Climate*RAIN_map.Qtot/RAIN_map.Qarea_eff);
		if (!Sealevel.empty()) fprintf (File_results,"\t%g", E_grid->SEA_level);
		
		// The main parameters defined in the print_init function
		E_grid->print_values(File_results);

		// Basic variables
		fprintf (File_results,"\t%g", EROS_TU_time>0. ? EROS_outflow/EROS_TU_time : 0.0 ); // The total water flux
		fprintf (File_results,"\t%g", EROS_TU_time>0. ? EROS_outflux[0]/RAIN_map.flux_area/EROS_TU_time : 0.0 );
		fprintf (File_results,"\t%g", EROS_TU_time>0. ? EROS_influx/RAIN_map.flux_area/EROS_TU_time : 0.0 );
		if (PROCESS_limit)	fprintf (File_results,"\t%d", E_grid->nChannel);
		fprintf (File_results,"\t%g", (double) precipiton_out/EROS_TU*100.);
		fprintf (File_results,"\t%g", (double) precipiton_in/EROS_TU*100.);
		fprintf (File_results,"\t%g", (double)(distance_in+distance_out)/EROS_TU);
		fprintf (File_results,"\t%g", (double)(step_in+step_out)/EROS_TU);
		fprintf (File_results,"\t%g", precipiton_out ? (double) distance_out/precipiton_out: 0.);
		fprintf (File_results,"\t%g", precipiton_in ? (double) distance_in/precipiton_in: 0.);
		fprintf (File_results,"\t%g", EROS_TU_time>0. ? EROS_influx>0.0 ? EROS_outflux[0]/EROS_influx*100. : E_grid->CALCUL[TOPO].avg/E_grid->TOPO_init*100. : 0.0 );
		fprintf (File_results,"\t%d", EROS_default(E_READ|E_ERODE));
		fprintf (File_results,"\t%d", EROS_default(E_READ|E_DEPOSIT));
		fprintf (File_results,"\t");  print_time(File_results,clck_init);

		fprintf (File_results, "\n");
	}


	switch (EROS_key){
		case 'f':
			fflush (File_results); 
			EROS_key=0;
			break;

		case 'F':
		case 's': 
			fflush (File_results); 
			break;
	}
}

//---------------------------------------------------
// 2. Print images
//---------------------------------------------------

//----------------------------------------
// Test if it is the time to record images
//----------------------------------------
bool EROS::time_record_images()
{
	bool save_test=false;

	switch (EROS_draw_condition) 
	{
	case IF___TIME: save_test = (EROS_time >= (EROS_draw-CALCUL_SMALL)); break;
	case IF_____TU: save_test = (Time_TU >= Draw_TU); break;
	case IF_HEIGHT: save_test = (E_grid->CALCUL[TOPO].avg <= E_grid->TOPO_init*E_grid->hS[E_grid->output_number]); break;
	case IF___FLUX: save_test = (EROS_outflux[0] >=EROS_influx*E_grid->qS[E_grid->output_number]); break;
	default: break;
	}
	
	return save_test;

}
//-------------
// Write images
//-------------
bool EROS::record_images(double & time_drawing)
{

	// Reset the water depth
	// If you do it, this may change the record average values
	// If you do not it, you may record non-updated values
	E_grid->water_update(EROS_time);

	double _time = EROS_time-time_drawing;
	bool not_first = (_time > 0.);

	// Increase the image number except for the first time that the function is called
	if (not_first) ++E_grid->output_number;
	// Write the grid except the first if the calculation pursues another one (i.e. output_number not null)
	if ( not_first || !E_grid->output_number )
	{
		string filename = E_grid->write(E_grid->nodes(), E_grid->_SAVE, E_grid->output_file, E_grid->output_namefile, E_grid->image_number(), output_nx, output_ny, _time);
	}

	double _draw=EROS_draw;
	EROS_draw+=EROS_draw_REAL;

	// Record time=0
	if (_draw<0 && EROS_draw>0) EROS_draw=0.;

	Draw_TU += EROS_draw_TU;
	time_drawing=EROS_time;

	// Print the header next time
	return true;
}
//---------------------------------------------------
// 3. Print the screen header
//---------------------------------------------------
void EROS::print_header_screen()
{
	string message_initial=" Start!";
	static string _message=message_initial;

	if (_message==message_initial)
	{
		cout.fill('_');
		cout << setw(50) << _message << endl << endl;
		cout.fill(' ');
		_message="";
	}
	else
	{
		//cout << endl;
	}
	/*--------------- STL version 
	cout << setiosflags( ios::left )
		 << setw(10) << "t"
		 << setw( 8) << "TU"
		 << setw( 8) << "<h>"
		 << setw( 9) << "rough"
		 << setw( 8) << "rain"
		 << setw( 9) << "discharge"
		 << setw( 9) << "channel"
		 << setw( 9) << "Qout"
		 << setw( 9) << "outflux"
		 << setw( 8) << "influx"
		 << setw( 6) << "eq.%"
		 << setw( 7) << "dist"
		 << setw( 5) << "out"
		 << setw( 4) << " |  "
		 << setw( 7) << "defaut"
		 << setw( 4) << "  | "
		 << "CPU" 
		 << endl;
	------------------------------*/
	//--------------- Print version 
	printf (  "%-9s",	"t");
	printf ("  %-5s",	"TU");
	printf ("  %-6s",	"dt");
	printf ("  %-7s",	"dV");
	if (RAIN_map.type==RAIN_VARY) 
		printf ("  %-5s",	"rain");
	if (!Sealevel.empty()) 
		printf ("  %-5s",	"sea");

	// Print the main variables
	E_grid->print_header();

#if 1 // #ifdef _VERBOSE_
	printf ("  %-7s",	"outflow");
#endif
	if (!FLOW_only) {
		printf ("  %-6s",	"influx");
		printf ("  %-6s",	"outflux");
	}
#ifdef _VERBOSE_
	printf ("  %-4s",	"eq.%");
#endif
	printf ("  %-6s",	"dist");
	printf ("  %-6s",	"steps");
	printf ("  %-4s",	"out");
	printf ("  %-4s",	"in ");
#ifdef _VERBOSE_
	printf ("  %-7s",	"channel");
#endif
	if (!flow_only)
	{
		if (adapt_time_type==-1) 
			printf (" | %14s ","defaults");
		else
			printf (" | %9s ","defaults");
	}

	printf ("%s\n",	"| CPU");

}

//---------------------------------------------------
// 4. Print on screen
//---------------------------------------------------
int EROS::print_screen(bool force_header=false)
{

	static int _write=-1;

	if (!((++_write)%EROS_print_TU) || force_header) {

		/*--------------- STL version 
		cout << setiosflags( ios::left )
			 << setw(10) << EROS_time
			 << setw( 8) << TU(EROS_Marcheur)
			 << setw( 8) << EROS_Climate
			 << setw( 8) << E_grid->SEA_level
			 << setw( 8) << setprecision(3) << E_grid->CALCUL[TOPO].avg;
			 << setw( 9) << setprecision(4) << E_grid->CALCUL[TOPO].std
			 << setw( 9) << E_grid->nChannel
			 << setw( 9) << setprecision(3) << EROS_TU_time>0. ? EROS_outflow/EROS_TU_time : 0.0 ); // The total water flux
			 << setw( 9) << setprecision(3) << EROS_outflux[0]/RAIN_map.flux_area/EROS_TU_time )
			 << setw( 8) << setprecision(3) << EROS_influx/RAIN_map.flux_area/EROS_TU_time )
			 << setw( 6) << setprecision(2) << (EROS_influx>0 ? EROS_outflux[0]/EROS_influx*100. : E_grid->CALCUL[TOPO].avg/E_grid->TOPO_init*100. )
			 << setw( 7) << setprecision(4) << (double)(distance_in+distance_out)/EROS_TU
			 << setw( 5) << setprecision(4) << (double) precipiton_in/EROS_TU*100.
			 << setw( 2) << " |"
			 << setw( 5) <<  EROS_default(E_READ|E_ERODE)
			 << setw( 1) <<  " "
			 << setw( 5) <<  EROS_default(E_READ|E_DEPOSIT)
			 // << setw( 1) <<  " "
			 // << setw( 5) <<  EROS_default(E_READ|E_HOLE)
			 << setw( 2) << "| ";
		print_time (stdout, clck_init);
		cout << endl;
		--------------------------------*/

		//--------------- Print version 
		printf (  "%-9g",	EROS_time);
		printf ("  %-5g",	TU(EROS_Marcheur));
		printf ("  %-6g",	EROS_P);
		printf ("  %-7.2g",	P_water);
		if (RAIN_map.type==RAIN_VARY) 
			printf ("  %-5g",	EROS_Climate*RAIN_map.Qtot/RAIN_map.Qarea_eff);
		if (!Sealevel.empty()) 
			printf ("  %-5g",	E_grid->SEA_level);

		// Print the main variable
		E_grid->print_values();

#if 1 // #ifdef _VERBOSE_
		printf ("  %-7.2f",	EROS_TU_time>0. ? EROS_outflow/EROS_TU_time : 0.0 );
#endif
		if (!FLOW_only) {
			printf ("  %-6.2f",	EROS_TU_time>0. ? EROS_influx/RAIN_map.flux_area/EROS_TU_time : 0.0 );
			printf ("  %-6.2f",	EROS_TU_time>0. ? EROS_outflux[0]/RAIN_map.flux_area/EROS_TU_time : 0.0 );
		}
#ifdef _VERBOSE_
		printf ("  %-4.0f",	EROS_TU_time>0. ? EROS_influx>0 ? EROS_outflux[0]/EROS_influx*100. : E_grid->CALCUL[TOPO].avg/E_grid->TOPO_init*100.  : 0.0 );
#endif
		printf ("  %-6.1f",	(double)(distance_in+distance_out)/EROS_TU);
		printf ("  %-6.0f",	(double)(step_in+step_out)/EROS_TU);
		printf ("  %-4.0f",	(double) precipiton_out/EROS_TU*100.);
		printf ("  %-4.0f",	(double) precipiton_in/EROS_TU*100.);
#ifdef _VERBOSE_
		printf ("  %-7d",	E_grid->nChannel);
#endif
		if (!flow_only)
		{
			switch (adapt_time_type) {
			case -1:
				printf (" |%4d  %4d %4d ", EROS_default(E_READ|E_ERODE), EROS_default(E_READ|E_DEPOSIT), EROS_default(E_READ|E_HOLE));
				break;
			default:
			case 0:
				printf (" |%4d %4d ", EROS_default(E_READ|E_ERODE), EROS_default(E_READ|E_DEPOSIT));
				break;
			}
		}

		print_time (stdout, clck_init);
		printf ("\n");

		_write=0;

		return true;

	}

	return false;

	// Bilan : variation d'altitude / Flux
	//printf ("  %7.0e | ", (E_grid->CALCUL[TOPO].avg-Bilan)*E_grid->NbreI - EROS_influx+EROS_outflux[0]);
	// Elements diagonaux de la grille
	//printf ("%3.1f | ", (double)nbre_diag/(double)nbre_horiz);
	// Valeurs limites
	//printf ("%-7g| ", GET_P_volume(true));

}

//---------------------------------------------------
// 5. Print specific points
//---------------------------------------------------
void EROS::printf_points(ptr_node ptr, double dz, int flag, double e_slope)
{

	if (E_grid->Points.empty()) return;
	static vector <FILE*> files(E_grid->Points.size(), 0);

	vector<int>&Pts=E_grid->Points; 
	size_t	size_Pts=Pts.size(),
			point=size_Pts;

	if (ptr && !(flag&E_CLOSE) ) 
	{
		size_t current=E_grid->N(ptr);
		for (size_t i=0;i<size_Pts;i++) if (current==Pts[i]) {point=i;break;}
		if (point==size_Pts) return;

		if (!files[point] ) 
		{
			// Open the file and write the header
			std::string name;
			name  = E_grid->output_file+".pt="+convert<string>(E_grid->Points[point])+".txt" ;

			if (files[point]=fopen(name.c_str(),"wt+")) {
				fprintf (files[point],"time");
				fprintf (files[point],"\thumidity");
				E_grid->print_header(files[point]);
				fprintf (files[point],"\tdischarge");
#ifdef _WATER_HEIGHT_
				if (FLOW_calcul) fprintf (files[point],"\twater_depth");
#endif
				fprintf (files[point],"\ttopo");
				fprintf (files[point],"\tsediment");
				fprintf (files[point],"\tdh");
				fprintf (files[point],"\tmode");
				fprintf (files[point],"\tnet erosion");
				fprintf (files[point],"\tnet deposit");
				fprintf (files[point],"\tlateral erosion");
				fprintf (files[point],"\tdischarge");
				fprintf (files[point],"\tslope");
				fprintf (files[point],"\tStock");
				fprintf (files[point],"\tAval");
				fprintf (files[point],"\n");
			}
		}
		if (files[point]) 
		{
			// write values
			fprintf (files[point],"%g", EROS_time);
			fprintf (files[point],"\t%g", ptr->S_humidity);
			fprintf (files[point],"\t%g", ptr->S_active>0 ? ptr->S_dischargeX/ptr->S_active : GRD::no_data_value);
#ifdef _WATER_HEIGHT_
			if (FLOW_calcul) fprintf (files[point],"\t%g", ptr->w_depth(EROS_time));
#endif
			fprintf (files[point],"\t%g", ptr->E_topo);
			fprintf (files[point],"\t%g", ptr->E_sediment);
			fprintf (files[point],"\t%g", dz);

			string str_type = flag&E_RUN ? "stream" :
							  flag&E_DIFFUSION ? "diffusion":
							  flag&E_LATERAL ? "lateral":
							  flag&E_HOLE ? "hole":
							  flag&E_UPLIFT ? "uplift" :
							  "unknown";

			fprintf (files[point],"\t%s", str_type.c_str());

			fprintf (files[point],"\t%g", flag&E_RUN ? H_erosion : dz<0. ? -dz:0.); // erosion
			fprintf (files[point],"\t%g", flag&E_RUN ? H_deposit : dz>0. ? dz:0.); // deposit
			fprintf (files[point],"\t%g", flag&E_RUN ? H_lateral : 0.); // lateral erosion
			fprintf (files[point],"\t%g", Q_P());
			fprintf (files[point],"\t%g", e_slope);
			fprintf (files[point],"\t%g", P_stock);
			fprintf (files[point],"\t%d", flag&E_RUN ? E_grid->N(pCELL_current->downstream) : -1); // ptr aval
			fprintf (files[point], "\n");

			fflush(files[point]);
		}
		return;
	}
	else { // either ptr=0 or flag=E_CLOSE
		if (ptr) // close one file
		{
			if (files[point]) fclose (files[point]); 
			files[point]=0;
		} else  // close all files
		{
			for (size_t i=0;i<size_Pts;i++) {
				if (files[i])  fclose(files[i]); 
				files[i]=0;
			}
		}

	}
}
//---------------------------------------------------
// 6. Print time
//---------------------------------------------------
void EROS::print_time(FILE * file, clock_t Clck)
{
	double T;
	int Minutes;
	int Sec, Secondes;
	int Heures;

	T = (double) (clock()-Clck)/CLOCKS_PER_SEC;
	Sec = (int) T;
	Secondes = Sec%60;
	Minutes = (Sec/60)%60;
	Heures = Sec/3600;

	fprintf (file, "| %dh %2d'%2d\"",Heures,Minutes,Secondes);
	//printf ("temps CPU %.3f\" (%.0f')\n", (double)(TU_FIN-clck_init)/CLOCKS_PER_SEC, (double)(TU_FIN-clck_init)/CLOCKS_PER_SEC/60.);
}

//---------------------------------------------------
// 7. Close open files
//---------------------------------------------------
void EROS::end_result()
{
	if (File_results) fclose (File_results);

	printf_points(0, 0., E_CLOSE);
}

//---------------------------------------------------
// Additional functions
//---------------------------------------------------
double EROS::table_average (vector<double>tableau)
{
	double f=0.;

	if (tableau.empty())return 0.;

	size_t Sz=tableau.size(); 
	for (size_t i=0;i<Sz;i++) f+=tableau[i];

	return f/(double)Sz;
}
