/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

//---------------------------------------------------------
// Main functions
//---------------------------------------------------------

/*      Programme d'erosion...			        					*/
/*																	*/
/*      programme de marcheurs d'erosion et de sedimentation. 		*/
/*      processus d'erosion: de type diffusion sur le versant,		*/
/*		de type transport dans le reseau. Cette	version du model	*/
/*		permet introduit une depndence non-lineaire de qs=qm		*/

#include "stdafx.h"

/* PARAMETRES DE GRILLE */

/*----------------------------------------------------------------------------------*/
/*------------------------------ The heart of EROS ---------------------------------*/
/*----------------------------------------------------------------------------------*/

int EROS::Calcul ()
{
	//-------------------------------------------------------------------------
	// Variable initialization
	//-------------------------------------------------------------------------
	int 		UpliftCompt = 0;
	int			nImage=-1;
	char		chImage='a';
	double		Marche=0.;
	type_dist	P_marche_limit = (E_grid->size())*10;
	double		outFlux=0.0;
	clck_init = clock();

	// The final time must be changed with the option -continue (-end:XXX)
	//EROS_time_REAL += EROS_time_init;
	Time_TU = 0;
	Draw_TU = 0;

	EROS_key=0;
	EROS_TU_time=0.0; // which produces an error if this values are not calculated

	//-------------------------------------------------------------------------
	// Function initialization
	//-------------------------------------------------------------------------
	E_grid->print_CALCUL();
	reset_result();

	//-------------------------------------------------------------------------
	// Time initialization
	//-------------------------------------------------------------------------
	EROS_time = EROS_time_init;
	EROS_draw = EROS_time_init;

	// If the calculation starts from a new grid, a phase of no-erosive initialization is processed
	if (!E_grid->continue_mode) 
		EROS_time -= EROS_initialization;
	// Save the grid during the initialization phase
	if (E_grid->save_is(SAVE_INITIALIZE))
		EROS_draw -= EROS_initialization;
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	// THE MAIN LOOP
	// Todo: an initialization phase (N log N) could be useful for the Date table
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	
	cout << endl;
	// Initialization of the active cell
	pCELL_current=NULL;
	CELL_amont=-1;
	P_stock=0.;



	// Initialize the drawing time
	double time_drawing = EROS_time;

	// Initialize the "hydro" time
	E_grid->water_time(EROS_time);

	do {	// do while (EROS_Marcheur < NbMarcheurs)

		// 
		int flag_condition=E_NO;
		if (FLOW_only) FLAG(flag_condition,E_FLOW_ONLY);
		if (EROS_time>=EROS_time_init)  
		{
			static bool first=true;
			if (first) { UNFLAG(flag_condition,E_ADAPT); first=false; }
			else FLAG(flag_condition,E_ADAPT);
		} 
		else 
		{
			FLAG(flag_condition,E_FLOW_ONLY);
		}
		flow_only = flag_condition&E_FLOW_ONLY;

		/* ------------------------------------------------------------------- */
		/* ---------------------------- Record the data ---------------------- */
		/* --------------------------- calculate weather --------------------- */
		/* ------------------------- and put tectonic flux ------------------- */
		/* ------------------------------------------------------------------- */


		// Calculate the outflux to test the flux-related stop condition
		outFlux = EROS_TU_time>0. ? EROS_outflux[0]/RAIN_map.flux_area/EROS_TU_time : 0.0 ;//table_average(EROS_outflux)/EROS_flux_area/EROS_TU_time;
		
		//---------------------
		// Printing the results
		//---------------------
		// Calculate the average values
		E_grid->print_CALCUL();
		// Test if it's time to record images
		bool _draw=time_record_images();
		// Print images if it is (not so often...)
		if (_draw)
			record_images(time_drawing);
		// Print grid-averaged parameters both on screen and in a file
		bool _print=print_result(_draw);

			
		// Reset variables
		if (_print) reset_result();
		if (_draw) E_grid->Parameter_reset(); // Reset the grid printing variablesi	

		//---------------------
		// Test stop conditions
		//---------------------
		if (   ((EROS_end & IF___TIME) && (EROS_time >= (EROS_time_REAL-CALCUL_SMALL)))
			|| ((EROS_end & IF_____TU) && (Time_TU	>= EROS_time_TU))
			|| ((EROS_end & IF__STAGE) && (E_grid->output_number >= (int)E_grid->hS.size() ) )
			|| ((EROS_end & IF_HEIGHT) && (E_grid->CALCUL[TOPO].avg <= EROS_end_height))
			|| ((EROS_end & IF___FLUX) && (outFlux >= EROS_end_flux)) )
		{
			cout << "That's the end, folks !!!!" << endl;
			if (!_draw) record_images(time_drawing);
			break;
		}

		//----------------------------------------------------
		// Change the precipiton volume to fasten calculations
		//----------------------------------------------------
		if (adapt_time) EROS_P=change_precipiton(flag_condition);
		EROS_default(E_RESET); // Set or reset variables for the next calculation

		//------------------------------------------------
		// Change the weather
		//------------------------------------------------
		// Get climate and sealevel value
		EROS_Climate = CLIMATE__function(EROS_time);
		E_grid->SEA_level = SEA__function(EROS_time);
		// The Poisson parameter can be changed at this point
		EROS_poisson = max(EROS_poisson, 1);
		EROS_poisson = min(EROS_poisson, EROS_poisson_rank);
		EROS_poisson_1=EROS_poisson-1;

		int change_climate=0;	
		//------------------------------------------------
		// The time interval between successive precipiton
		//-----------------------------------------------

		if (STEP_TU)
			P_time = EROS_P/EROS_TU;
		else
			P_time = EROS_P;

		if (!EROS_P_climate)  P_time /= EROS_Climate; // This option makes the precipiton volume not depend on climate (EROS_P_climate==false)
		//------------------------------------------------------------------
		// THE TU time, i.e. the time interval between two precipiton groups 
		// used for record, uplift, groundwater, etc.
		//------------------------------------------------------------------
		EROS_TU_time = EROS_TU * P_time;

		//----------------------------------------
		// Update the total flux with climate
		//----------------------------------------
		if (RAIN_map.Qinflow<0.) 
			EROS_Q=RAIN_map.Qtot*EROS_Climate;
		else
			EROS_Q=RAIN_map.Qinflow*EROS_Climate;
		
		//----------------------------------------
		// Init the rain map
		//----------------------------------------
		if (change_climate) RAIN_map.init_map();	
		//--------------------------------------------------
		// Calculate precipiton volume and time coefficients
		//--------------------------------------------------
		EROS_coefficients();
		//----------------------------------------
		// Check some erosion/deposition processes
		//----------------------------------------
		bool _splash_ = (EROS_splash>0.);
#ifdef _SPLASH_ANISOTROPY_
		_splash_ |= ( (EROS_splash_X>0.) || (EROS_splash_Y>0.) );
#endif
		//----------------------
		// Add the tectonic flux
		//----------------------
		if (!flow_only)
		{
			EROS_influx += E_grid->Uplift (EROS_TU_time);
			E_grid->Strike_slip (EROS_TU_time);
			EROS_influx += E_grid->Thrusting(EROS_TU_time);
		}
		

		// Variables used to manage periodic boundaries
		ptrdiff_t	EQ_offset = 0;
		ptr_node	EQ_start=NULL;
		type_node	CELL_Empty;

		//-------------------------------------------------------------------
		// The (main) precipiton loop
		// Calculate precipiton action (hydrodynamics + erosion) during 1 TU
		//-------------------------------------------------------------------
		// Do it only if there is a chance not to get water
		if (!RAIN_map.empty() && EROS_Q>0.0) 
		for (size_t Compt=0; Compt<EROS_TU; Compt++)
		{

			//-----------------------------------------------
			// Choose a precipiton
			//-----------------------------------------------

			bool recharge_init=!pCELL_current; // Mean that there is something to recharge
			if (!pCELL_current 
				//|| !RAIN__check(pCELL_current) // can be removed .. avoid reintroducing precipiton where it is not allowed to rain 
				) 
			{
				// Take the current precipiton according to "rain" rules
				// (defined in ErosPluie.cpp)
				pCELL_current=RAIN_map.RAIN__drop();
				pCELL_current->upstream=NULL;
				CELL_amont=-1;
				P_date_k=0;
				AMONT_GRADIENTS.clear();
			}

			if (!E_grid->valid(pCELL_current)) 
				cout <<"ERROR"<<endl;

			// TO BE CHECKED IF RECHARGE
			pCELL_current->upstream=NULL;
			
			EQ_start = pCELL_current;
			//----------------------------------------
			// Initialization of the precipiton volume
			// P_water_t is actually a percentage
			// of P_water
			//----------------------------------------
			P_water_t=1.;
			//----------------------------------------
			// Fickean diffusion
			// also known as the Splash effect
			//----------------------------------------
			if (_splash_ && !flow_only)
				EROS_outflux[0] += Splash_erosion();

			//-------------------------------------------
			// Initialization of the precipiton parameter
			//-------------------------------------------
			P_force_deposit=false;
			PROCESS_get(H_INIT);
			P_date_k = TIME__calcul(pCELL_current,EROS_date(),H_INIT);
			//-------------------------------------
			// Initialization of the sediment stock
			//-------------------------------------
			if (( pCELL_current && !(pCELL_current->bc & BC_RECHARGE) ) && 
				  !flow_only )
			{
				// define the sediment stock
				if (stock_equilibrium)
					P_stock = outFlux *GET_P_volume();
				else
					P_stock = InitialStock()*GET_P_volume();
			} 
			// else take the same stock

			
			// Since GET_P_volume() gets the equivalent of a volume of water,
			// the mass of sediment launched with a precipiton
			// is simply taken as the product of Stock by GET_P_volume (whatever EROS_Climate)
			// For 1 TU the total amount of sediment is Stock*TU,
			// and the flux is divided by the time lapse (EROS_TU_time)
			// and by the landing surface (=TU).
			// It is thus equal to Stock
			EROS_influx += P_stock;

			//-------------------------------
			// Count some walk variables like
			// lakes, step number, ...
			//-------------------------------
			int	fall_in_lake=0; 
			int SEA_end=0;
			P_dist=0.;
			P_step=0;
			pCELL_amont=pCELL_current;
#ifdef SPECIAL_RECORD
			vector<ptr_node> precipiton_cells;
#endif
			//	--------------------------------------
			//	PRECIPITON WALK
			//	--------------------------------------
			while (pCELL_current && pCELL_current->IS_NOT_BOUNDARY()) 
			{
				// The precipiton is now running on top of topography
				// int numero=E_grid->n(pCELL_current);
				// Check for validity (this should not occur at anytime)
				if (!E_grid->valid(pCELL_current)) 
				{ 
					cout <<"ERROR"<<endl;
					break;
				}
#ifdef SPECIAL_RECORD
				// Record the list of cells where the precipiton goes through
				precipiton_cells.push_back(pCELL_current);
#endif
				//--------------------------------------------
				// Record upstream parameters (when necessary)
				//--------------------------------------------
				P_water_t_in = P_water_t;
				P_date_k_in = P_date_k;
				//------------------------------------------------------------
				// Test whether the precipiton has already walked on this cell
				//------------------------------------------------------------
				bool lake = pCELL_current->is_date(EROS_date());
				//------------------------------------------------------------
				// Get the precipiton "time", basically used to calculate flow
				//------------------------------------------------------------
				// Calculate the time interval from the passages of the previous precipiton
				P_date_k = lake ? 0 : TIME__calcul(pCELL_current,EROS_date(),H_RUN);
				// The precipiton travel time V/Q
				CALCUL_P_dt();

				// Fix the volume of water in the  precipiton
				double P_volume=GET_P_volume();

				if ( lake ) {
					// flag for lake (can be used to limit the lake filling)
					fall_in_lake++;
				}
				else
				{
					// Increase humidity table
						// REVOIR LA NORMALISATION DE HUMIDITY
					pCELL_current->S_humidity+=P_volume;			
					// flag for lake
					// fall_in_lake=0;
				}
				//--------------------------------------------
				// Manage undersea precipitons
				// Only deposition is permitted below sealevel
				//--------------------------------------------
				if (E_grid->boundary_sea&BC_SEALAND && !SEA__test()) 
				{
					// if no deposition is permitted, stop
					if (SEA_stop) { SEA_end=1; break; }

					// if there is no sediment, stop
					if (P_stock<CALCUL_SMALL) {
						set_topo(pCELL_current, P_stock/pCELL_current->area, E_HOLE); 
						break;
					} 
				}

				//-----------------------------------
				// CALCULATION OF THE EROSION PROCESS
				// The transition from FLUVIAL to 
				// HILLSLOPE is forbidden
				//-----------------------------------
				PROCESS_get(H_RUN);


				// INCREMENTATION DES VARIABLES 'RESEAU'
				if (!lake && (PROCESS_active()==FLUVIAL)) {
					pCELL_current->S_channel+=P_volume;
					E_grid->nChannel++;
				}

				//----------------------------------
				// THE HYDRODYNAMIC FUNCTION
				// calculation of the downstream path
				//----------------------------------
				//---------------------------------
				// Calculate of the downstream path
				//---------------------------------
#ifdef _DEBUG
				int x_current=E_grid->X(pCELL_current),
					y_current=E_grid->Y(pCELL_current);
#endif
				int walk = HYDRO(pCELL_current); //, false);
				//------------------------------------
				// THE GEOMORPHOLOGICAL FUNCTION
				//------------------------------------
				if ( !walk ) 
				{
					//-----------------
					// Filling the hole
					//-----------------
					if(!flow_only) HOLE();
					else break;

				} else 
				{
					//---------------------------
					// Erosion and sedimentation
					//---------------------------
					if (!flow_only) 
					{
						EROS_Erode();
						nbre_erode++;
						if (E_grid->save_is(SAVE_DEFAULT)) pCELL_current->S_default += EROS_default(E_READ);
					}
					//---------------------------
					// Save parameters
					//---------------------------
					pCELL_current->S_active+=P_volume;
										
					if (E_grid->save_is(SAVE_DISCHARGE)||E_grid->save_is(SAVE_DISCHARGE_MANNING))
					{
						// Record the discharge
						double q_in=Q_P()/Channel_width();
						double q_out=q_manning();
						pCELL_current->E_discharge_in = q_in;
						pCELL_current->E_discharge_out = q_out;

						int & dir=GRADIENT_D->index();
						double q=P_volume/E_grid->GRID_Dl[dir];
						// The discharge is calculated either as the (input) precipiton discharge or as the (output) friction flow
						if (E_grid->save_is(SAVE_DISCHARGE)) q*=q_in; 
						else q*=q_out;
	
						pCELL_current->S_dischargeX += q*class_grid::Vp[0][dir][0]*E_grid->cell_x;
						pCELL_current->S_dischargeY += q*class_grid::Vp[0][dir][1]*E_grid->cell_y;
					}
					if (E_grid->save_is(SAVE_LENGTH)) 
						pCELL_current->S_sed_length += SED_transfer*P_volume;
					if (E_grid->save_is(SAVE_SLOPE)) 
					{
						pCELL_current->E_slope = CELL_GRADIENTS.slope_w;
						pCELL_current->S_slope += CELL_GRADIENTS.slope_w*P_volume;
					}
					// Record the river stock - not used yet
					if (E_grid->save_is(SAVE_STOCK)) 
						pCELL_current->S_river_stock += P_stock/Channel_width()*P_volume;


				} // if HYDRO

				//	-----------------------------
				//	Prepare the next displacement
				//	-----------------------------
				pCELL_amont = pCELL_current;
				pCELL_current = pCELL_current->downstream;
#ifdef _DEBUG
				x_current=E_grid->X(pCELL_current);
				y_current=E_grid->Y(pCELL_current);
#endif
				//----------------------------------------
				// Condition 1 to stop the precipiton walk
				//----------------------------------------
				if (!pCELL_current)  
					break;
				//--------------------------------------------------------
				// Update precipiton 
				//--------------------------------------------------------
				pCELL_current->upstream = pCELL_amont;
				// Actualize precipiton variables
				CELL_amont=E_grid->CELL_Opposite(GRADIENT_D->index());
#ifdef _DEBUG_EROS
				// Check if there is a (strong) control of particle paths by grid directions
				if (GRADIENT_D->index()%2) {
					// diagonal link
					nbre_diag++;
				} else {
					// non diagonal link
					nbre_horiz++;
				}
#endif
				//--------------------------------------------------------
				// Increment walking parameters
				//--------------------------------------------------------
				P_dist+=GRADIENT_D->link->dl;
				P_step++;
				//--------------------------------------------------------
				// Condition 2 to stop the precipiton walk
				//--------------------------------------------------------
				if ( pCELL_current->IS_BOUNDARY() ) 
				{
					break;
				}
				//---------------------------------------------------------
				// Conditions 3 & 4 to stop the precipiton walk
				//	3: If the precipiton is empty
				//	4: If the walk is much too long (in lakes for instance)
				//---------------------------------------------------------
				
				// REVOIR LA VALEUR DE fall_in_lake_max
				if ( P_water_t<CALCUL_EPSILON ||  P_step>P_marche_limit || fall_in_lake>=fall_in_lake_max)
				{
					// Stop if there is no sediment
					if (P_stock<CALCUL_SMALL) {
						set_topo(pCELL_current, P_stock/pCELL_current->area, E_HOLE); 
						break;
					} else {
						// Continue until the sediment stock is fully deposited
						P_force_deposit=true;
					}
				}
				//-------------------------------------------	|
				//--------------------------------------------	|
			}	// End of the precipiton walk					|
				// now: pCELL_current->bc&BC_BOUNDARY			|
				// ex: pCELL_current->S_humidity<0			|
				// +break(s) if pCELL_current==0 or others...	|
				//--------------------------------------------	|
				//--------------------------------------------	|
			
			Marche += P_dist;


			//---------------------------
			// Management of the walk end
			//---------------------------
			if (pCELL_current) 
			{	
				
				// 1. The precipiton reaches boundary
				if (pCELL_current->IS_BOUNDARY() || SEA_end)
				{
					//------------------------------------------------
					// Update the water depth
					//------------------------------------------------
#ifdef _WATER_HEIGHT_
					if (FLOW_calcul) 
					{
						// The sea level fixes the water level
						if (pCELL_current->bc&BC_SEALEVEL)
						{
							pCELL_current->W_depth=E_grid->SEA_level-pCELL_current->E_topo;
						}
						// The friction equation with the upstream topo gradient fixes water depth
						else
						{
							// Fix the river heigth
							if (FLOW_calcul==1) pCELL_current->water_update(EROS_time);
							pCELL_current->water_increase(P_height*P_water_t);
							if (FLOW_limiter) pCELL_current->water_limit(FLOW_limit);

							// The Manning equation is calculated with the upstream hydraulic slope
							// already in CELL_GRADIENTS.slope_w

							HYDRO__end(pCELL_current);
						}
					}
#endif
					
					//pCELL_current->E_topo = E_grid->Mer;
					precipiton_out++;
					distance_out+=P_dist;
					step_out+=P_step;

					// Total outflowing water
					EROS_outflow += (P_water_t*P_water);
					// add the river stock
					EROS_outflux[0] += P_stock;

					size_t outpixel=pCELL_current-E_grid->nodes();
					if (E_grid->nCL[outpixel]==-1) {
						//	TO BE CHANGED.....
						//	This could happen with advection or tectonics 
						//	because the nCL vector is not reinitialized after a tectonic step
						//	cout << "ERREUR Davy t'es nul...." << endl;
					} else {
						E_grid->AireCL[ E_grid->nCL[outpixel] ]++;
						E_grid->FluxCL[ E_grid->nCL[outpixel] ]+=P_stock;
					}
						
				}  // end if boundary or sea level
				// 2. The precipiton does not reach boundaries
				else 
				{
					precipiton_in++;
					distance_in+=P_dist;
					step_in+=P_step;
					if (flow_only && E_grid->save_is(SAVE_DEFAULT)) pCELL_current->S_default++;
		}
			} // end if (pCELL_current)
			else // pCELL_current==0
			{
				precipiton_off++;
				// Save where this "default" occurs
				if (pCELL_amont) 
				{
					//if (flow_only && E_grid->save_is(SAVE_DEFAULT)) pCELL_amont->S_default++;
				}
				// Even worse: no upstream cell!!!
				else
				{
					cout<<".";
				}
			}

			//----------------------------------------------------------------------
			// Manage periodic "recharge" conditions, for which
			// the precipiton is reinjected in the system with its sediment content.
			// If the recharge flag is on, the system evolves at constant mass (i.e. slope)
			//-----------------------------------------------------------------------
			
			ptr_node EQ_cell = pCELL_current;
			pCELL_current=E_grid->recharge_manage(pCELL_current, EQ_start);


#ifdef TO_BE_DEBUGGED
			// Old method that replaced recharge_manage / removed when checked
			if (pCELL_current && !pCELL_current->Link.empty()) 
			{
				// The "Link" vector is not empty only if a periodic boundary conditions is defined
				// with one of these flags: BC_XMIN,BC_XMAX, BC_YMIN, BC_YMAX
				// The precipiton is moved up on the corresponding cell
				pCELL_current = pCELL_current->Link[0].node_D;
			}

			// 2. Check if the cell has all required properties (RAIN__check)
			ptrdiff_t offset = 0;
			if (pCELL_current /*&& RAIN__check(pCELL_current)*/ )
			{
				// A procedure that is supposed to correlate cells of precipiton initialization...
				offset = EQ_start-pCELL_current;
				if (E_grid->boundary_recycle & BC_CORRELAT) 
				{
					// offset the precipiton to the starting point
					pCELL_current=E_grid->cell_offset(pCELL_current,EQ_offset);
					if (pCELL_current)
						// Change the "recharge map" to center it on EQ_start
						E_grid->recharge_offset(-EQ_offset, E_grid->check_in(EQ_cell));
					else
						E_grid->recharge_offset(0);
					//int mX=E_grid->x((int)EQ_offset), mY=E_grid->y((int)EQ_offset);
					//pCELL_current = E_grid->Neighbor(EQ_start, mX, mY);
				} else if ( E_grid->recharge_map.find(pCELL_current)==E_grid->recharge_map.end() )
				{
					pCELL_current=NULL;
				}
			} else // Case with no recharge
				pCELL_current=NULL;
			
			EQ_offset = offset;

			// 3. Modify the upstream pointer
			if (pCELL_current) 
			{
				pCELL_current->upstream = &CELL_Empty.copy_data(*(pCELL_current->upstream));
				pCELL_current->upstream->E_topo -= (EQ_cell->E_topo - pCELL_current->E_topo);
			} 
#endif
			
			//----------------
			// Increasing time
			//----------------
			EROS_Precipitation+=TIME__increase();
			EROS_Marcheur++;
			EROS_time += P_time;


		} // for Compt (i.e., 1 TU of calculation)

		//-----------------------------------
		// TEST
		//-----------------------------------
		// Calculate the total water input in the grid
#ifdef _DEBUG
		double water_stock_1=E_grid->water_stock(1);
		double water_stock_2=E_grid->water_stock(1);
#endif
		//--------------
		// Increase time
		//--------------
		Time_TU++;

		// CONDITIONS PARTICULIERES
		bool init_key=false;

		do {
			if (EROS_key == 's') {
				// Save an image
				E_grid->write( E_grid->nodes(), E_grid->_SAVE, 
					E_grid->output_file, E_grid->output_namefile, E_grid->image_number(true), output_nx, output_ny, EROS_time-time_drawing);
				if (E_grid->save_is(SAVE_TXT)) fprint_result();
				init_key=true;
			} else if (EROS_key=='q') {
				//switch (MessageBox( NULL," DO YOU WANT TO SAVE FILE ?","EROS",MB_YESNOCANCEL | MB_TOPMOST)) {
				//case IDCANCEL: EROS_key=0; break;
				//case IDYES: EROS_key='Q'; break;
				//case IDNO:	EROS_key='q'; break;
				//}
			} else if (EROS_key && EROS_key != 'p') {
				// Provide information about the program
				cout << setw(8) 
				//	 << setprecision(3) 
						<< EROS_time 
						<< " " 
						<< TU(EROS_Marcheur) 
						<< " _________________________________" 
						<< E_grid->output_namefile 
						<< endl;
				init_key=true;
			}

		} while (EROS_key=='p');
		
		// Re-init the key
		if (init_key) EROS_key=0;

	} while ( (EROS_key-'q') && (EROS_key-'Q'));


	if (EROS_key=='Q') {

		E_grid->write(E_grid->nodes(), E_grid->_SAVE, E_grid->output_file, E_grid->output_namefile, E_grid->image_number(true), output_nx, output_ny, EROS_time-time_drawing);
		if (E_grid->save_is(SAVE_TXT)) fprint_result();
	}

	cout << "FINI" << endl;


	end_result();
	RAIN__free();

	E_grid->free_grid();


	print_time(stdout, clck_init);

	// mean that the process is over
	EROS_message = 0;
	return EROS_message;

} // fin du corps principal
