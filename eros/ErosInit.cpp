/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

//---------------------------------------------------------
// Main functions
//---------------------------------------------------------

/*      Programme d'erosion...			        					*/
/*																	*/
/*      programme de marcheurs d'erosion et de sedimentation. 		*/
/*      processus d'erosion: de type diffusion sur le versant,		*/
/*		de type transport dans le reseau. Cette	version du model	*/
/*		permet introduit une depndence non-lineaire de qs=qm		*/

#include "stdafx.h"

/* PARAMETRES DE GRILLE */

#define _Pause_(a) {cout << (a) << " - hit RETURN to leave";char key=0;do key=_getch();while(key-13);}

//____________________________________________________________________________________
//
//		GENERAL INITIALIZATION 
//____________________________________________________________________________________


//-------------------------------------------------
// THE MAIN INIT FILE
//-------------------------------------------------
int EROS::Init (Carg_list arg_list)
{

	cout << "_________________________________" << endl;
	cout << "        Program EROS" << endl;
	cout << "_________________________________" << endl;
	cout << endl;

	string output_directory;
	string input_directory;

	string file_reprise;

	vector<string>input_dat;
	string file_name_ref;
	string dir_name_ref;

	vector<string>file_options;

	/*------------------- DECLARATION DE LA LIGNE DE COMMANDE -------------- */
	E_grid->input_files.resize(PARAMETER_END,string());

	E_grid->input_uplift.clear();
	E_grid->input_climate.clear();
	E_grid->input_sealevel.clear();

	E_grid->output_file.clear();
	E_grid->output_namefile.clear();

	E_grid->nbreT=0;

	E_grid->boundary=BC_NULL;
	E_grid->boundary_sea=BC_NULL;
	E_grid->boundary_periodic=BC_NULL;
	E_grid->boundary_semiperiodic=BC_NULL;
	E_grid->boundary_recycle=BC_NULL;

	/* Options par d�faut */
	E_grid->Random_Noise=0.;
	
	E_grid->normalize_bool = true;

	RAIN_map.type=RAIN_UNDEF;
	//-------------------------------------------
	//Initialize default parameters
	//-------------------------------------------
	Init_default_parameters();
	//-------------------------------------------
	// The options in the program are written as:
	// -option:value
	//-------------------------------------------

	size_t szt = arg_list.size();
	for (size_t i=1; i<szt; i++) {

        // The general form of arguments is -option:value
		Carg arg=arg_list[i];
		string arg_str, arg_str2;

		bool is_written = false; // Check if the write options are already processed in reference_name or reference_value
		
		if (arg.option("dir_output") || arg.option("directory") || arg.option("output_dir") || arg.option("output_directory")) 
		{			
			arg.value(output_directory);
		}
		
		if (arg.option("dir")||arg.option("name"))
		{
			is_written=arg.reference_name(file_name_ref, dir_name_ref);
		}

		// The option "dir" is processed specifically in the reference_value fonction

		if (arg.option("dir_file") || arg.option("dir_input") ||arg.option("dir_reprise") || arg.option("input_dir") || arg.option("input_directory")) 
		{
			arg.value(input_directory);
		} 

		if (arg.option("c"))
		{
			EROS_end=IF__STAGE;
		}
		
		if (arg.option("h"))
		{
			arg.value(EROS_end_height);
			EROS_end = IF_HEIGHT;
			// Exception
			if (EROS_end_height<=E_grid->SEA_level)  EROS_end = IF___TIME;
		}

		if (arg.option("q"))
		{
			arg.value(EROS_end_flux);
			if (EROS_end_flux>0.) EROS_end = IF___FLUX;
			else EROS_end = IF___TIME;
		}

		if (arg.option("i")) {
			E_grid->image_bool=false;
		}

		if (arg.option("stop")||arg.option("pause")) 
		{
			E_grid->pause_bool=true;
		}

		//if (arg.option("nolimit")) EROS_limiter_parameter=false;
		//if (arg.option("limit")) EROS_limiter_parameter=true; // default value

		//if (arg.option("a")) E_grid->add_cells_bool=true;

		//---------------------------------------------------------
		// option 17-07-2007 (improved)
		// Define if boundary conditions are sea-like, periodic, side by side
		//---------------------------------------------------------
		if (arg.option("boundary") || arg.option("boundaries") || arg.option("border") ) 
		{
			bool tst=true;
			while (tst=arg.value(arg_str, true)) 
			{
				if (arg_str=="l" || arg_str=="left")		FLAG(E_grid->boundary,BC_XMIN);
				else if (arg_str=="r" || arg_str=="right")	FLAG(E_grid->boundary,BC_XMAX);
				else if (arg_str=="t" || arg_str=="top")	FLAG(E_grid->boundary,BC_YMAX);
				else if (arg_str=="b" || arg_str=="bottom")	FLAG(E_grid->boundary,BC_YMIN);
				else if (arg_str=="all") FLAG(E_grid->boundary,BC_XMIN|BC_XMAX|BC_YMIN|BC_YMAX);
			}

			if (!E_grid->boundary)
				FLAG(E_grid->boundary,BC_XMIN|BC_XMAX|BC_YMIN|BC_YMAX);
		}

		if (arg.option("boundary_sea") || arg.option("sea") || arg.option("sea_condition") )
		{
			bool tst=true;
			while (tst=arg.value(arg_str, true)) 
			{
				if (arg_str=="l" || arg_str=="left")		FLAG(E_grid->boundary_sea,BC_XMIN);
				else if (arg_str=="r" || arg_str=="right")	FLAG(E_grid->boundary_sea,BC_XMAX);
				else if (arg_str=="t" || arg_str=="top")	FLAG(E_grid->boundary_sea,BC_YMAX);
				else if (arg_str=="b" || arg_str=="bottom")	FLAG(E_grid->boundary_sea,BC_YMIN);
				else if (arg_str=="all") FLAG(E_grid->boundary_sea,BC_XMIN|BC_XMAX|BC_YMIN|BC_YMAX);
				else 
				{
					// Fix sea level
					double sea_level;
					if (convert_and_check<double,string>(arg_str,sea_level)) file_options.push_back(arg_str +"\t"+ "sea level");
				}
			}
			if (!E_grid->boundary_sea) FLAG(E_grid->boundary_sea,BC_XMIN|BC_XMAX|BC_YMIN|BC_YMAX|BC_SEALAND);
			// Write the option
			is_written=arg.reference_value(file_name_ref, dir_name_ref);
		}

		if (arg.option("periodic")) {
			int n_val=0;
			while (arg.value(arg_str, true)) {
				if (arg_str=="l" || arg_str=="left")	FLAG(E_grid->boundary_periodic,BC_XMIN);
				if (arg_str=="r" || arg_str=="right")	FLAG(E_grid->boundary_periodic,BC_XMAX);
				if (arg_str=="t" || arg_str=="top")		FLAG(E_grid->boundary_periodic,BC_YMAX);
				if (arg_str=="b" || arg_str=="bottom")	FLAG(E_grid->boundary_periodic,BC_YMIN);
				n_val++;
			}
			// keep compatibility with previous versions
			if (!n_val)
				FLAG(E_grid->boundary_periodic,(BC_XMIN|BC_XMAX));
		}
		if (arg.option("semiperiodic")) {
			int n_val=0;
			while (arg.value(arg_str, true)) {
				if (arg_str=="l" || arg_str=="left")	FLAG(E_grid->boundary_semiperiodic,BC_XMIN);
				if (arg_str=="r" || arg_str=="right")	FLAG(E_grid->boundary_semiperiodic,BC_XMAX);
				if (arg_str=="t" || arg_str=="top")		FLAG(E_grid->boundary_semiperiodic,BC_YMAX);
				if (arg_str=="b" || arg_str=="bottom")	FLAG(E_grid->boundary_semiperiodic,BC_YMIN);
				n_val++;
			}
			// keep compatibility with previous versions
			if (!n_val)
				FLAG(E_grid->boundary_periodic,BC_YMAX);
		}
		//------------------------------------------------------
		// option 17-07-2007
		// Recharge the inflow boundary with outflow precipitons
		// This option defines a kind of periodic condition
		// improve 10-10-2007;
		// add the correlation option
		//------------------------------------------------------
		if (arg.option("recycle") || arg.option("recharge")) {
			while (arg.value(arg_str, true)) {
				if (arg_str=="l" || arg_str=="left")	FLAG(E_grid->boundary_recycle,BC_XMIN); 
				if (arg_str=="r" || arg_str=="right")	FLAG(E_grid->boundary_recycle,BC_XMAX);
				if (arg_str=="t" || arg_str=="top")		FLAG(E_grid->boundary_recycle,BC_YMAX);
				if (arg_str=="b" || arg_str=="bottom")	FLAG(E_grid->boundary_recycle,BC_YMIN);
				
				// with or without correlation (yet to be developped)
				//if (arg_str=="correlate" || arg_str=="correlated")	FLAG(E_grid->boundary_recycle,BC_CORRELAT);
			}
		}

		//-----------------------------------------------------------
		// option defines the files that are saved during calculation
		//-----------------------------------------------------------
		if (arg.option("write"))	{
			while (arg.value(arg_str,true)){
				if (arg_str=="topo")		E_grid->save_(SAVE_TOPO);
				if (arg_str=="sed")			E_grid->save_(SAVE_SED);
				if (arg_str=="sediment")	E_grid->save_(SAVE_SED);
				if (arg_str=="stock")		E_grid->save_(SAVE_STOCK);
				if (arg_str=="hum")			E_grid->save_(SAVE_HUM);
				if (arg_str=="humidity")	E_grid->save_(SAVE_HUM);
				if (arg_str=="res")			E_grid->save_(SAVE_CHANNEL);
				if (arg_str=="reseau")		E_grid->save_(SAVE_CHANNEL);
				if (arg_str=="channel")		E_grid->save_(SAVE_CHANNEL);
				if (arg_str=="dis")			E_grid->save_(SAVE_DISCHARGE);
				if (arg_str=="discharge")	E_grid->save_(SAVE_DISCHARGE);
				if (arg_str=="height")		E_grid->save_(SAVE_WATER);
				if (arg_str=="water")		E_grid->save_(SAVE_WATER);
				if (arg_str=="slope")		E_grid->save_(SAVE_SLOPE);
				if (arg_str=="downward")	E_grid->save_(SAVE_DOWNWARD);
				if (arg_str=="default")		E_grid->save_(SAVE_DEFAULT);
				if (arg_str=="defaults")		E_grid->save_(SAVE_DEFAULT);
				if (arg_str=="vegetation")	E_grid->save_(SAVE_VEGETATION);
				if (arg_str=="groundwater")	E_grid->save_(SAVE_GROUNDWATER);
				if (arg_str=="particles")	E_grid->save_(SAVE_PARTICLES);
				if (arg_str=="length")		E_grid->save_(SAVE_LENGTH);
				if (arg_str=="rainfall")	E_grid->save_(SAVE_RAIN);
				if (arg_str=="rain")		E_grid->save_(SAVE_RAIN);

			}
		}

		if (arg.option("nowrite"))
		{
			while (arg.value(arg_str,true))
			{
				if (arg_str=="all")			E_grid->_SAVE = SAVE_NO;
				if (arg_str=="topo")		E_grid->save_not(SAVE_TOPO);
				if (arg_str=="sed")			E_grid->save_not(SAVE_SED);
				if (arg_str=="sediment")	E_grid->save_not(SAVE_SED);
				if (arg_str=="stock")		E_grid->save_not(SAVE_STOCK);
				if (arg_str=="hum")			E_grid->save_not(SAVE_HUM);
				if (arg_str=="humidity")	E_grid->save_not(SAVE_HUM);
				if (arg_str=="res")			E_grid->save_not(SAVE_CHANNEL);
				if (arg_str=="reseau")		E_grid->save_not(SAVE_CHANNEL);
				if (arg_str=="channel")		E_grid->save_not(SAVE_CHANNEL);
				if (arg_str=="dis")			E_grid->save_not(SAVE_DISCHARGE);
				if (arg_str=="discharge")	E_grid->save_not(SAVE_DISCHARGE);
				if (arg_str=="height")		E_grid->save_not(SAVE_WATER);
				if (arg_str=="water")		E_grid->save_not(SAVE_WATER);
				if (arg_str=="slope")		E_grid->save_not(SAVE_SLOPE);
				if (arg_str=="downward")	E_grid->save_not(SAVE_DOWNWARD);
				if (arg_str=="default")		E_grid->save_not(SAVE_DEFAULT);
				if (arg_str=="vegetation")	E_grid->save_not(SAVE_VEGETATION);
				if (arg_str=="groundwater")	E_grid->save_not(SAVE_GROUNDWATER);
				if (arg_str=="particles")	E_grid->save_not(SAVE_PARTICLES);
				if (arg_str=="length")		E_grid->save_not(SAVE_LENGTH);
				if (arg_str=="rainfall")	E_grid->save_not(SAVE_RAIN);
				if (arg_str=="rain")		E_grid->save_not(SAVE_RAIN);
			}
		}
		
		//-----------------
		// Tectonic options
		//-----------------
		if (arg.option("d")) E_grid->TECTO_strikeslip=1;
		if (arg.option("dd")) E_grid->TECTO_strikeslip=2;

		//--------------------------------
		// Continuing calculation options
		//--------------------------------
		if (arg.option("continue") || arg.option("R")) 
		{
			E_grid->continue_mode=2;
			if (!arg.value(E_grid->continue_number)) E_grid->continue_number=-1;	
		}
		if (arg.option("from") || arg.option("reprise") || arg.option("r")) 
		{
			if (arg.value(E_grid->continue_number)) E_grid->continue_mode=1;
		}
		//--------------------------------------
		// A fast way to provide time parameters 
		//--------------------------------------
		//old version: -timing:precipiton volume:final time (if >0) or initialization time (if <0):drawing time
		//new version: -timing:step:value(:TU)(:time)(:volume):end:value:draw:value:init:value
		//	with step=time step (default or :time) or precipiton volume (:volume),
		//		 end=final time, draw=drawing interval, init=initialization time
		//	The option (:TU) indicates that the time step is for 1 TU
		if ( arg.option("timing") )
		{
			enum {STEP=0,FINAL,DRAW,INIT,NBRE};
			vector<double>time_(NBRE,0.);
			vector<bool>do_(NBRE,false);
			string label_[NBRE]={"step","end","draw","init"};
			string name_[NBRE]={"time constant","final time","drawing time","initialization time"};
			double	val;

			while ( arg.value(arg_str) )
			{
				int i=0;
				for (;i<NBRE;i++) if (arg_str==label_[i]) {
					do_[i]=arg.value(time_[i]); break;
				}

				if (i==NBRE) {
					if (arg_str=="TU")
						file_options.push_back("1\tstep TU");
					else if (string_lower(arg_str)=="time")
						name_[STEP]="time constant";
					else if (string_lower(arg_str)=="volume" || string_lower(arg_str)=="precipiton")
						name_[STEP]="precipiton volume";
					else if ( arg.convert<double>(val))
					{
						for (int j=0;j<NBRE;j++) if (!do_[j])  {
							time_[j]=val; do_[j]=true; break;
						}
					}
				}
			}
			// A few specific cases that must be treated before (respect the order)
			if (do_[FINAL] && time_[FINAL]<0.) {
				do_[FINAL]=false;do_[INIT]=true;time_[INIT]=-time_[FINAL];
			}
			if (do_[INIT]) {
				if (EROS_time_REAL<0.) file_options.push_back("0.0\tfinal time");
			}
			// Print the values
			for (int i=0;i<NBRE;i++) if (do_[i])  {
				file_options.push_back(convertInString(time_[i]) + "\t" + name_[i]); 
			}

			if (do_[STEP] && name_[STEP]=="time constant")
				file_options.push_back("-1\tprecipiton volume"); 
			if (do_[STEP] && name_[STEP]=="precipiton volume") 
				file_options.push_back("-1\ttime constant"); 
			if (do_[DRAW]) {
				E_grid->save_(SAVE_INITIALIZE);
			}
			

			// Write the option
			is_written=arg.reference_value(file_name_ref, dir_name_ref);
		}
		//-------------------------------------
		// The shallow water parameters
		//-------------------------------------
		// -hydrodynamic:Manning coefficient:law:flow depth coefficient:flux
		// exemple: -hydro:0.04:manning ou -hydro:10:laminar
		if ( arg.option("hydrodynamic") || arg.option("hydro") || arg.option("hydrodynamic_only") || arg.option("hydro_only") )
		{
			double val;

			if (arg.option_string().find("_only")!= string::npos ) 
				file_options.push_back("1\thydraulic calculation");


			// Set the walker diffusion coefficient to 0.5
			// file_options.push_back("0.5\tdiffusion coefficient");

			E_grid->save_( SAVE_WATER|SAVE_HUM|SAVE_SLOPE|SAVE_DISCHARGE|SAVE_DOWNWARD|SAVE_RAIN );
		
			int _coef=0;
			while ( arg.value(arg_str) )
			{
				if ( arg.convert<double>(val) ) 
				{
					switch (++_coef) 
					{
					case 1: file_options.push_back(arg_str +"\t"+ "Manning coefficient"); break;
					case 2: file_options.push_back(arg_str+"\t"+"flow depth constant"); break;
					}
				} 
				else if (arg_str=="manning" || arg_str=="chezy" || arg_str=="laminar")
					file_options.push_back(arg_str+"\t"+"flow friction model");
				else if (arg_str=="min") 
				{
					if (arg.value(val))
						file_options.push_back(convert<string>(val)+"\t"+"minimum flow coefficient");
				}
			}
			// Default values
			if (_coef<=1 && FLOW_Cte_Depth<=0.) file_options.push_back("1.0\tflow depth constant");
			
			// Write the option
			is_written=arg.reference_value(file_name_ref, dir_name_ref);
		}
		//-------------------------------------
		// The groundwater parameters
		//-------------------------------------
		// -groundwater:diffusivity:infiltration coefficient:porosity
		if ( arg.option("GW") )
		{
			int		n_val=0;
			double	val;
			while ( arg.value(arg_str) )
			{
				if (arg.convert<double>(val))
				{
					switch (n_val++) 
					{
					case 0: file_options.push_back(arg_str +"\t"+ "diffusivity"); break;
					case 1: file_options.push_back(arg_str +"\t"+ "porosity"); break;
					case 2: file_options.push_back(arg_str +"\t"+ "Groundwater infiltration"); break;
					}
				} 
			}

			if (n_val>=1)
				E_grid->save_( SAVE_GROUNDWATER|SAVE_RAIN );
			if (n_val<=2) 
				// no infiltration defined. Assume 100% by default
				file_options.push_back("1.0\tGroundwater infiltration");
			
			// Write the option
			is_written=arg.reference_value(file_name_ref, dir_name_ref);
		}
		//------------------------------------
		// Ending options
		//------------------------------------

		if (arg.option("S") || arg.option("s") ) {
			int nStages; arg.value(nStages);
			E_grid->hS.resize(nStages);
			E_grid->qS.resize(nStages);
			EROS_end =IF__STAGE;
			EROS_draw_condition=IF__STAGE;
		}

		if (arg.option("T")) {
			arg.value(outflow_averaging); 
			if (outflow_averaging>20) outflow_averaging=20; 
		}

		//-----------------------------------
		// Adapting calculation time options
		//-----------------------------------
		if (arg.option("adapt")) 
		{
			adapt_time = true;
			arg.value(adapt_time_errmax);
			arg.value(adapt_time_errmin);

		} // The Time "EROS_P" can be changed to fasten the calculation

		if (arg.option("adapt_method") && arg.value(arg_str)) 
		{
			adapt_time = true;
			
			if (arg_str=="all") adapt_time_type=0;
			if (arg_str=="erosion") adapt_time_type=1;
			if (arg_str=="deposition") adapt_time_type=2;
			
			arg.value(adapt_time_op);
			arg.value(adapt_time_factor);
		} // The Time "EROS_P" can be changed to fasten the calculation

		if (arg.option("default")) {
			arg.value(adapt_time_errmax);
			arg.value(adapt_time_errmin);
		} // Fix the maximum number of default allowed in the calculation

		if (arg.option("dm")) {
			arg.value(adapt_time_errmin); // Min number of default allowed by the program
		}
		if (arg.option("dM")) {
			arg.value(adapt_time_errmax); // Min number of default allowed by the program
		}

		if (arg.option("river") || arg.option("castor")) {
			RAIN_map.flux_scaling = 2; // see the file ErosPluie.cpp
		}
		if (arg.option("flux")) // also "flux_normalization" from the parameter list
		{
			arg.value(RAIN_map.flux_scaling);
		}
		// This option fixes the characteristic surface
		//	over which the average flux per unit surface is calculated
		// if RAIN_map.flux_scaling = 0, the flux is not normalized by any surface
		// if RAIN_map.flux_scaling = 1, the surface is defined as the total surface which is not on
		//		boundary conditions. This option is convenient for tectonic fluxes which
		//		express in uplift rates.
		// if RAIN_map.flux_scaling = 2, the referential surface is the one that receive water. This
		//		option is convenient for the alluvial-plain model, where the average discharge
		//		for each influx pixel is 1, and the influx is calculated per influx pixel.
		//		In this case, we can calculate the relationship between average slope s and river
		//		width W at equilibrium:
		//			- mass balance: Q^m' * s^n = Stock/ld, where Stock is the stock per precipiton
		//			- water balance: Q*W=Qo*Wo=Wo (since Qo=1)

		if (arg.option("equilibrium")) {
			stock_equilibrium = true;
		}

		// Add a random noise to the topography (see Init_links in ErosGrid.cpp)
		if (arg.option("noise")) 
		{
			arg.value(E_grid->Random_Noise);
		}

		// Decide wether or not the variable in the .dat file has to be normalized
		if (arg.option("normalized")) {
			E_grid->normalize_bool=false;
		}

		// Fix the "seed" of the random generator
		if (arg.option("seed")) {
			arg.value(EROS_seed);
		}

		//---------------------------------
		// Record point options
		//---------------------------------
		if (arg.option("P")||arg.option("Pts")||arg.option("Point")){
			int n; 
			while (arg.value(n)) {
			// if (arg.value(n)) {
				E_grid->Points.push_back( n );
			}
		}// store the physical characteristics of some defined points -P:1:10 etc...

		//---------------------------------
		// Define file names
		// To avoid confustion with the input parameters in the .dat file,
		// it would be better to define the file name with
		// -file:alt:name rather than-alt:name
		//---------------------------------
		/* exemple of what should be done...
		if (arg.option("file") && arg.value(arg_str) )
		{
			if (arg_str=="output") arg.value(E_grid->output_file);
			...
		}
		*/
		{ // Read the file names
			// Input file and initialization method ----------------------
			if (arg.option("input"))
			{
				while (arg.value(arg_str, true))
				{
					if (arg_str=="strict") 
						EROS_input=INPUT_STRICT;
					else if (arg_str=="ask")
						EROS_input=INPUT_ASK;
					else if (arg_str=="subset")
						EROS_input=INPUT_SUBSET;
					else
						E_grid->input_files[CONFIG]=arg_str;
				}
			}
			// Ouput file ------------------------------------------------
			if (arg.option("output")||arg.option("file")||arg.option("f")) 
			{
				arg.value(E_grid->output_file);
			}
			// dat (config) file ---------------------------------------------------
			if (( arg.option("dat")||arg.option("param") ) && (arg.value(arg_str)) )
			{
				if (is_file_exist(arg_str))
				{
					input_dat.push_back(arg_str);
					// look if the dat file is a reference for name
					is_written=arg.reference_name(file_name_ref, dir_name_ref);
				}
				else
				{
					cout << "Attention .... the dat file " << arg_str << " does not exist" << endl;
				}
			}

			// Config file -------------------------------------------------------
			if (arg.option("config"))
			{
				if (arg.value(E_grid->input_files[CONFIG])) 
					is_written=arg.reference_name(file_name_ref, dir_name_ref);
			}

			if (arg.option("alt")||arg.option("topo")||arg.option("topography")) 
			{
				if (arg.value(E_grid->input_files[TOPO],E_grid->input_files[CONFIG]+".alt")) 
					is_written=arg.reference_name(file_name_ref, dir_name_ref);
			
				// Build the generic input file name
				if (E_grid->input_files[CONFIG].empty()) E_grid->input_files[CONFIG]=remove_extension(E_grid->input_files[TOPO]);
			}

			// Sediment file -------------------------------------------------------
			if (arg.option("sed"))
			{
				if (arg.value(E_grid->input_files[SEDIMENT],E_grid->input_files[CONFIG]+".sed"))
					is_written=arg.reference_name(file_name_ref, dir_name_ref);
			}
			// Water depth file -------------------------------------------------------
			if (arg.option("water")) 
			{
				if (arg.value(E_grid->input_files[WATER_DEPTH],E_grid->input_files[CONFIG]+".water"))
					is_written=arg.reference_name(file_name_ref, dir_name_ref);
			}

			// Flux (precipiton volume per unit time) file -----------------------------
			if (arg.option("hum") || arg.option("humidity") )
			{
				if (arg.value(E_grid->input_files[HUMIDITY],E_grid->input_files[CONFIG]+".hum")) {
					RAIN_map.type=RAIN_CSTE;
					is_written=arg.reference_name(file_name_ref, dir_name_ref);
				}
			}
		

			// Rainfall file -------------------------------------------------------
			if (arg.option("pluie")||arg.option("rain")) 
			{
				if (arg.value(E_grid->input_files[RAINFALL],E_grid->input_files[CONFIG]+".rain")) 
				{
					RAIN_map.type=RAIN_VARY;
					is_written=arg.reference_name(file_name_ref, dir_name_ref);
				}
			}

			// Erodability file -------------------------------------------------------
			if (arg.option("erode")) 
			{
				if (arg.value(E_grid->input_files[CHANNEL_ERODIBILITY],E_grid->input_files[CONFIG]+".erode")) 
					is_written=arg.reference_name(file_name_ref, dir_name_ref);
			}
#ifdef _WATER_HEIGHT_
			// Friction coefficient file (either manning or Darcy-Weisbach) ---------------------
			if (arg.option("manning")) 
			{
				if (arg.value(E_grid->input_files[WATER_MANNING],E_grid->input_files[CONFIG]+".manning"))
					is_written=arg.reference_name(file_name_ref, dir_name_ref);
			}
#endif

			// Uplift map file ---------------------------------------------------
			if (arg.option("uplift")) 
			{
				if (arg.value(E_grid->input_uplift,E_grid->input_files[CONFIG]+".uplift"))
					is_written=arg.reference_name(file_name_ref, dir_name_ref);
			}
			// Climate parameter (x rainfall) file --------------------------------
			if (arg.option("climate"))
			{
				if (arg.value(E_grid->input_climate,E_grid->input_files[CONFIG]+".climate"))
					is_written=arg.reference_name(file_name_ref, dir_name_ref);
			}

			// Sea level file -------------------------------------------------------
			if (arg.option("sealevel")) 
			{
				if (arg.value(E_grid->input_sealevel,E_grid->input_files[CONFIG]+".sealevel"))
					is_written=arg.reference_name(file_name_ref, dir_name_ref);
			}
		} // end of the file name input
		//-----------------------------------------------------
		// Define file name from the extension string
		// Convenient but old fashioned since not that explicit
		//-----------------------------------------------------
		if (arg.option() &&
			arg.value(arg_str) &&
			arg_str.find("//")!=0) 
		{ // This is the case when there is no "-"
			string ext = extension(arg_str);
			if (ext=="dat") 
				input_dat.push_back(arg_str);
			else if (ext=="alt") {
				E_grid->input_files[TOPO]=arg_str;
				// Build the generic input file name
				if (E_grid->input_files[CONFIG].empty()) E_grid->input_files[CONFIG]=remove_extension(E_grid->input_files[TOPO]);
			}
			else if (ext=="sed")
				E_grid->input_files[SEDIMENT]=arg_str;
			else if (ext=="water")
				E_grid->input_files[WATER_DEPTH] = arg_str;
			else if (ext=="hum") 
				E_grid->input_files[HUMIDITY]=arg_str;
			else if (ext=="rain") 
				E_grid->input_files[RAINFALL]=arg_str;
			else  if (ext=="erode") 
				E_grid->input_files[CHANNEL_ERODIBILITY]=arg_str;
			else  if (ext=="threshold") 
				E_grid->input_files[CHANNEL_THRESHOLD]=arg_str;
			else  if (ext=="manning") 
				E_grid->input_files[WATER_MANNING]=arg_str;
			else if (ext=="uplift") 
				E_grid->input_uplift=arg_str;
			else if (ext=="climate") 
				E_grid->input_climate=arg_str;
			else if (ext=="sealevel")
				E_grid->input_sealevel=arg_str;
		
			is_written=arg.reference_name(file_name_ref, dir_name_ref);

		}

		// Save the option string for a later processing
		if (!arg.option()) // if the option is not empty
		{	
			// If the arg has not been processed, write it as an option
			if (!arg.is_processed() && !(arg_str=arg.value_string(0)).empty()) 
			{	// Put the option passed by command line
				file_options.push_back(arg_str +"\t"+ arg.option_string());
			}
			// If the arg has not been fully processed, write the name or dir options
			if (!arg.is_fully_processed() )
			{
				// modify the file and directory names if it was not already done
				if (!is_written) arg.reference_value(file_name_ref, dir_name_ref);
			}

		}

	}

	//-------------------------------------------
	// Test if the basic file exists
	//-------------------------------------------
	if (input_dat.empty() && file_options.empty()) 
	{
		cout << endl << "NO OPTIONS (only default values)! Take care ..." << endl;
		// return 0;
	}

	//-------------------------------------------
	// Build the basic output file names
	//-------------------------------------------
	// The file is built
	// Either with the -f:file_name or -file:file_name
	// Or with the :name :dir(\) of the option list
	// Or with the topo and dat files as described below
	if (E_grid->output_file.empty()) 
	{
		// if the dat file is not defined, take the name of the ".dat" files to build up a file name
		if (file_name_ref.empty())  
		{
			file_name_ref = remove_directory(remove_extension(E_grid->input_files[TOPO]));
			file_name_ref += "_" + extension(E_grid->input_files[TOPO]);

			for (int i=0;i<input_dat.size();i++) 
			{
				string name=remove_directory(remove_extension(input_dat[i]));
				file_name_ref += "." + name + "_" + extension(input_dat[i]);
			}
		}
		E_grid->output_file =  file_name_ref ;
		cout << "Output image files: "<< E_grid->output_file << endl;
	}

	// Manage the output directory (create it if necessary)
	output_directory=concat_directory(output_directory,dir_name_ref,".");

	if (output_directory.empty())
	{
		output_directory=directory(E_grid->output_file);
		// Can't leave the output directory empty!
		if (output_directory.empty()) output_directory=file_name_ref;
	}

	// Change the directory ... if any
	E_grid->output_file = change_directory(E_grid->output_file, output_directory);

	// Create the directory (if necessary)
	if (!output_directory.empty() && !create_directories(output_directory) )
	{
		cout << "unable de create the directory " << output_directory << endl;
		return 0;
	}

	//-------------------------------------------------------------
	// "Reprise" or "continue" option
	// Manage calculation that continue, or start from, another one
	//-------------------------------------------------------------
	if (!E_grid->continue_mode) 
	{
		E_grid->output_number = 0;
	}

	if (E_grid->continue_mode) 
	{
		// Find the last file recorded
		if (E_grid->continue_number == -1)
		{
			int p=1;
			string out_f = change_directory(E_grid->output_file,input_directory);
			while ( file_exists( out_f+"."+ convertInString(p) + ".alt" )) p++;
			if (p==1) {
				cout << "unable to find the files from which continuing the calculation (from " << out_f << ")" << endl;
				return 0;
			} else E_grid->continue_number=p-1;
		}

		E_grid->input_files[TOPO] = change_directory(E_grid->output_file,input_directory) // no change if input_directory is empty
								+ "." + convertInString(E_grid->continue_number)+ ".alt";
		E_grid->output_number=E_grid->continue_number;
	}
	if (E_grid->continue_mode==1)
	{
		E_grid->output_file += "." + convertInString(E_grid->continue_number);
		E_grid->output_number = 0;
	}
	//-------------------------------------------
	// Test if the basic file exists
	//-------------------------------------------
	if (E_grid->input_files[TOPO].empty()) 
	{		
		cout << endl << "FATAL ERROR: no height file" << endl;
		return 0;
	}
	//------------------------------------------
	// Look for some basic files for calculation
	//------------------------------------------
	// Define the sediment file
	if (E_grid->input_files[SEDIMENT].empty()) {
		E_grid->input_files[SEDIMENT]=change_extension(E_grid->input_files[TOPO],"sed");
	}

	// Define the water height file
	if (E_grid->input_files[WATER_DEPTH].empty()) {
		E_grid->input_files[WATER_DEPTH]=change_extension(E_grid->input_files[TOPO],"water");
	}

	// Define the humidity and/or rainfall file, which controls both rainfall and boundary conditions
	// Note that the .rain file prevails upon the .hum file in defining the boundary conditions.
	// Thus no default .rain file must be created if you don't want to use it
	// (get default from the generic file rather than from the height file)
	if (RAIN_map.type==RAIN_UNDEF) // No data used
	{
		E_grid->input_files[RAINFALL]=change_extension(E_grid->input_files[TOPO],"rain");
		E_grid->input_files[HUMIDITY]=change_extension(E_grid->input_files[TOPO],"hum");
		RAIN_map.type=RAIN_MAY_VARY;
	}
	//-------------------------------------------------------------
	// Init calculation parameters from .dat files and command line
	//-------------------------------------------------------------
	// Define the output_ini file, which contains all the calculation parameters
	E_grid->output_ini = E_grid->output_file + (E_grid->continue_mode==2 ? "." + convertInString(E_grid->continue_number): "") + ".ini";

	ofstream file_parameter;
	if (E_grid->save_is(SAVE_INI))
	{
		file_parameter.open(E_grid->output_ini.c_str(),ios_base::trunc);
		if ( !file_parameter.is_open())  {
			cout << "Failed to create " << E_grid->output_ini << endl << "bye..." << endl; 
			return 0;
		}
		print_parameters(arg_list,file_parameter);
	}

	// Variable initialisation (please respect the order: command line overwrites dat files)
	for (size_t i=0; i<input_dat.size(); i++) Init_parameters(input_dat[i],file_parameter, EROS_input);

	file_parameter << write_title("command line parameters");
	cout << write_title("command line parameters");
	Init_parameters(file_options,file_parameter,EROS_input);// mean that only exact strings are considered

	if (file_parameter.is_open()) file_parameter.close();

	//----------------------------------------------
	// Initialize topography and boundary conditions
	//----------------------------------------------
	//Initialize topography and sediment, if any, layers
	if (!E_grid->Init_topography()) {
		// If I can't even open the topography, I prefer stop working and fish.
		_Pause_("Cannot open the initial grid");
		return 0;
	}

	// Define the random generator
	E_grid->RANDOM_generator.init(EROS_seed);	

	// Initialize rainfall conditions
	if (E_grid->Init_rain_boundaries(RAIN_map.type)==RAIN_UNDEF) 
	{
		// If I can't even open the boundaries, I prefer stop working and fish.
		_Pause_("Cannot open the rainfall/bc file");
		return 0;
	}
	// Apply the boundary conditions defined from the command line
	if (!E_grid->set_boundary_conditions()) {
		cout << "problem with the boundary conditions defined from the command line" << endl;
	}

	if (!E_grid->test_boundary_conditions()) {
		_Pause_("No boundary defined");
		return 0;
	}

	// Apply sea level on no-data points and boundaries
	E_grid->set_sealevel();

	// Initialize boundary conditions
	if (!E_grid->Init_links()) {
		// If I can't even build the grid, I prefer stop working and fish.
		_Pause_("Cannot build up the grid connectivity");
		return 0;
	}

	//Calculation of the mean altitude from the file input_file (useful if continue_mode)
	string msg=E_grid->input_files[CONFIG]+".alt";
	//TO BE CHANGED for taking account of nodata
	E_grid->TOPO_init=E_grid->GRD_average(msg.c_str()) * (double) E_grid->nbreT / (double) E_grid->NbreI;

	if (E_grid->add_cells_bool) {
		// The function CompleteGrid has to be validated in this version
		//if (!CompleteGrid (E_grid, &output_nx,&output_ny)) return 0;
	} else {
		output_nx=E_grid->nX;
		output_ny=E_grid->nY;
	}

	// cout << "Reset grid variables" << endl;
	// Determine the list of parameters to save
	E_grid->Parameter_init();
	E_grid->Parameter_reset();

	//----------------------------------------------
	// Initialization of the erosion parameter files
	//----------------------------------------------
	// Initialize the number of active processes
	// cout << endl;
	Init_processes(); 
	// cout << endl;
	/*
	// Define the erodibility file (get default from the generic file rather than from the height file)
	if (E_grid->input_files[CHANNEL_ERODIBILITY].empty()) {
		E_grid->input_files[CHANNEL_ERODIBILITY]=E_grid->input_files[CONFIG]+".erode";
	}

	// Define the erosion threshold file (get default from the generic file rather than from the height file)
	if (E_grid->input_files[CHANNEL_THRESHOLD].empty()) {
		E_grid->input_files[CHANNEL_THRESHOLD]=E_grid->input_files[CONFIG]+".erode";
	}

	*/
	if (!E_grid->Init_property(CHANNEL_ERODIBILITY, E_grid->input_files[CHANNEL_ERODIBILITY], EROS_Cte_Eb[FLUVIAL]))
	{
		// If I can't even initialize erosion parameters, I prefer stop working and fish.
		_Pause_("Cannot open the channel basement erodability grid");
		return 0;
	}
	if (!E_grid->Init_property(HILLSLOPE_ERODIBILITY, E_grid->input_files[HILLSLOPE_ERODIBILITY], EROS_Cte_Eb[HILLSLOPE]))
	{
		// If I can't even initialize erosion parameters, I prefer stop working and fish.
		_Pause_("Cannot open the hillslope basement erodability grid");
		return 0;
	}

	if (!E_grid->Init_property(CHANNEL_THRESHOLD, E_grid->input_files[CHANNEL_THRESHOLD], EROS_Cte_Tb[FLUVIAL]))
	{
		// If I can't even initialize erosion parameters, I prefer stop working and fish.
		_Pause_("Cannot open the basement erodability grid");
		return 0;
	}
	if (!E_grid->Init_property(HILLSLOPE_THRESHOLD, E_grid->input_files[HILLSLOPE_THRESHOLD], EROS_Cte_Tb[HILLSLOPE]))
	{
		// If I can't even initialize erosion parameters, I prefer stop working and fish.
		_Pause_("Cannot open the basement erodability grid");
		return 0;
	}

	if (!E_grid->Set_value(SEDIMENT_ERODIBILITY, EROS_Cte_Es))
	{
		// If I can't even initialize erosion parameters, I prefer stop working and fish.
		_Pause_("Cannot open the sediment erodability grid");
		return 0;
	}
	if (!E_grid->Set_value(SEDIMENT_THRESHOLD, EROS_Cte_Ts))
	{
		// If I can't even initialize erosion parameters, I prefer stop working and fish.
		_Pause_("Cannot open the sediment erosion threshold grid");
		return 0;
	}

	
	
	//---------------------------------------
	// Initialization of the sediment file
	//---------------------------------------
	if (!E_grid->Init_property(SEDIMENT, E_grid->input_files[SEDIMENT], 0.))
	{
		// If I can't even initialize water depth, I prefer stop working and fish.
		_Pause_("Cannot open the water depth grid");
		return 0;
	}
	
	//---------------------------------------
	// Initialization of the water depth file
	//---------------------------------------
	// The no-water condition
	if (FLOW_Cte_Cz<=0.0 || FLOW_Cte_Depth==0.0) {
		cout << "no water depth calculation because the flow friction parameter is "<<FLOW_Cte_Cz<<" or the flow depth coefficient is " <<FLOW_Cte_Depth <<endl;
		FLOW_calcul=0;
	} 
	// else FLOW_calcul is defined in the parameter list 

	// O: no flow
	// 1: the stationary solution
	// 2: the dynamic wave solution

#ifdef _WATER_HEIGHT_
	if (FLOW_calcul && !E_grid->Init_property(WATER_DEPTH, E_grid->input_files[WATER_DEPTH], 0., 0.))
	{
		// If I can't even initialize water depth, I prefer stop working and fish.
		_Pause_("Cannot open the water depth grid");
		return 0;
	}
	if (FLOW_calcul && !E_grid->Init_property(WATER_MANNING, E_grid->input_files[WATER_MANNING], FLOW_Cte_Cz))
	{
		// If I can't even initialize the manning coefficient, I prefer stop working and fish.
		_Pause_("Cannot open the manning coefficient grid");
		return 0;
	}
	if (FLOW_calcul) 
		E_grid->Init_water(FLOW_Cte_Depth);
#endif

	//-------------------------------------
	// Init flow relationships
	//-------------------------------------
	// if (FLOW_Cte_Depth<=0. || FLOW_Cte_Cz<=0.) FLOW_equation=0;
	FLOW_Cte_Inertia /= max(E_grid->cell_x, E_grid->cell_y); // Should be replaced by a better formula
	if (FLOW_limit>=0.0) FLOW_limiter=true;
	//------------------------------------
	// Init pluie, climate and calculation
	//------------------------------------
	cout << write_title("Initialization of the different fields and calculation");
	RAIN__init();	// Variables Erosion et scaling (necessarily AFTER RAIN__init)
	CLIMATE__init();
	SEA__init();
	HYDRO__init();
	Init_calcul();
	
	E_grid->Grid_modify();
	

	//------------------------------------
	// Init tectonics
	//------------------------------------
	if (!E_grid->Init_tectonic()) {
		// If I can't even initialize tectonics, I prefer stop working and fish.
		_Pause_("Cannot open the uplift field");
		return 0;
	}
	//------------------------------------
	// Init the result files
	//------------------------------------
	// Read the initial time from file if the "continue" mode is defined
	EROS_time_init=0.0; // Default value. Change in the fprint_init with -continue option
	File_results = 0; // Defaut value
	
	print_init();
	
	if ( E_grid->save_is(SAVE_TXT) )
	{
		if ( !(File_results=fprint_init (E_grid->output_file + ".txt", E_grid->continue_mode, &EROS_time_init, input_directory)) )
		{
			_Pause_("Cannot initialize the recording file");
			return 0;
		}

		// Print the header line for results
		if ( E_grid->continue_mode != 2 ||  input_directory!=output_directory ) 
			fprint_result(1);
	}


	//------------------------------------
	// Some necessary tests
	//------------------------------------
	if (EROS_time_init<0.) {
		_Pause_("EROS_time_init<0")
		return 0; // Sans autre forme de proces
	}
	if (PROCESS_limit==FLUVIAL) E_grid->save_not(SAVE_CHANNEL);


	cout << "final time " << EROS_time_REAL << endl;
	if (EROS_time_REAL>=0.) 
	{
		EROS_end_condition=IF___TIME;
		cout << "Final time (in real unit): " << EROS_time_REAL << endl;
	} else if (EROS_time_TU>=0.) {
		EROS_end_condition=IF_____TU;
		cout << "Final time (in TU): " << EROS_time_TU << endl;
	} else {
		cout << "No final time ?" << endl;
		EROS_end_condition=IF______0;
	}

	EROS_end |= EROS_end_condition;


	if (!EROS_end) {
		_Pause_("No stop conditions!\n");
		return 0;
	}

	if (EROS_end & IF__STAGE) {
		// The stop conditions is fixed by a certain number of stages...
		if (E_grid->TECTO_uplift_rate>0.) {
			EROS_draw_condition=IF___FLUX;
			cout << "Stop condition: " << E_grid->qS.back() << " tectonic flux" << endl;
		} else {
			EROS_draw_condition=IF_HEIGHT;
			cout << "Stop condition: " << E_grid->hS.back() << " initial altitude" << endl;
		};
	}

	if (!EROS_draw_condition) 
	{
		// meaning that EROS_draw_condition is not already defined

		if (EROS_draw_REAL>0.)
			EROS_draw_condition=IF___TIME;
		else
			if (EROS_draw_TU>0)
				EROS_draw_condition=IF_____TU;
		else
			EROS_draw_condition=-1; // No drawing
	}

	// Rules to adapt the calculation time step
	if ( (adapt_time) && (adapt_time_errmin<0.) )
	{
		adapt_time_errmin = adapt_time_errmax / 2;
	}

	// Some prints.....
	cout << "image file : " << E_grid->output_file << endl;

	cout << "altitude   : " << E_grid->input_files[TOPO] << endl;
	cout << "erodability: " << E_grid->input_files[CHANNEL_ERODIBILITY] << endl;
	cout << "threshold  : " << E_grid->input_files[CHANNEL_THRESHOLD] << endl;
	cout << "pluie      : " << E_grid->input_files[RAINFALL] << endl;
	cout << "uplift     : " << E_grid->input_uplift << endl;

	cout << "E_grid[" << E_grid->nX << "," << E_grid->nY << "]" << endl;
	cout << "Boundary: " << E_grid->NbreCL << endl;
	cout << "TU: " << EROS_TU << endl;

	cout << "geometry   : " << E_grid->Geometry << endl;

	cout << "time step  : " << (adapt_time?"adaptative":"fix") << endl;


	cout << "Time adaptation method: " << (adapt_time ? "yes" : "no") << endl;
	if (adapt_time)
	{
		cout << "	Method: " << (adapt_time_type==0 ? "all" : (adapt_time_type==1 ? "erosion" : "deposition")) << endl;
		cout << "	Error max: " << adapt_time_errmax << endl;
		cout << "	Error min: " << adapt_time_errmin << endl;
		cout << "	Variation operation: " << adapt_time_op << endl;
		cout << "	Variation factor: " << adapt_time_factor << endl;
	}

	// Mean that everything is alright and the process can begin
	EROS_message = 1;
	return EROS_message;


} // fin du corps principal



//--------------------------------------
//	Determination of the erosion process
//--------------------------------------
void EROS::PROCESS_get(HYDRO_PHASE stage)
// Determining which erosion process is active
{
	if (stage==H_INIT) {
		PROCESS_active_=PROCESS_past_= ( PROCESS_limit==FLUVIAL ? FLUVIAL : HILLSLOPE);
		return;
	}

	if (!SEA__test() || (PROCESS_limit==FLUVIAL) ) {
		// Below Sea Level
		PROCESS_active_=PROCESS_past_=FLUVIAL;
		return;
	}

	if ( PROCESS_limiter && (PROCESS_past_==HILLSLOPE) ) 
	{
		PROCESS_active_=FLUVIAL;
	}
	else
	{

		// DETERMINATION DU MODE D'EROSION
		switch (PROCESS_transition) {
		case 0: 
			PROCESS_active_ = FLUVIAL;
			break;

		case 1:
			// TRANSITION=0: nHILLSLOPE = nCHANNEL
			//  	HILLSLOPE=0 <=> (Q<1)
			//  	FLUVIAL=1   <=> (Q>1)

			// EROS_Model_process is a variable that Dimitri asked for
			//	just to keep the transition independent of climate variation
			if (EROS_Model_process)
				// The transition depends on flow value
				PROCESS_active_ = (Q_P()<EROS_Q_process ? HILLSLOPE : FLUVIAL);
			else
				// The transition depends on Drainage area = Flow/EROS_Climate)
				PROCESS_active_ = (Q_P()<EROS_Climate*EROS_Q_process ? HILLSLOPE : FLUVIAL);
			break;

		case 2:
			// nHILLSLOPE > nCHANNEL
			//		Critical slope = (P_date_k/Freq.Crit)^PROCESS_Exp_transition
			//		HILLSLOPE if (large slopes, small discharges)
			//		FLUVIAL if (small slopes, large discharges)
			PROCESS_active_ = (CELL_GRADIENTS.slope_e>pow(Q_P(),-PROCESS_Exp_transition) ? HILLSLOPE : FLUVIAL);
			break;
		case 3:
			// The reverse of case 2
			PROCESS_active_ = (CELL_GRADIENTS.slope_e<pow(Q_P(),-PROCESS_Exp_transition) ? HILLSLOPE : FLUVIAL);
			break;
		}

	};

	PROCESS_past_=PROCESS_active_;
}
//-------------------------------------------
//	Calculation of the initial sediment stock
//-------------------------------------------
double EROS::InitialStock ()
{
	//if (Slope_In_Stock>0) P_stock = ErosionFlux (FLUVIAL, P_date_k, Slope_In_Stock)*EROS_Cte_Ld[FLUVIAL];
	double p=E_grid->RANDOM_generator.rand_exclude()-0.5;
	return  (EROS_stock_initial + EROS_stock_variation * p);
}

//------------------------------------------
// INIT/RELEASE/NORMALISATION FUNCTIONS
//------------------------------------------
void EROS::Init_processes()
{
	cout << setw(25) << "erosion processes";
	cout << " : ";

	//--------------------------------------------
	// Define the flow model
	//--------------------------------------------
	string MODEL=string_lower(FLOW_Model_string);
	if (MODEL=="laminar") 
		type_node::flow_model=LAMINAR;
	else if (MODEL=="chezy") 
		type_node::flow_model=CHEZY; // Chezy...
	else  // if (MODEL=="manning")
		type_node::flow_model=MANNING;


	//--------------------------------------------
	// Define the erosion model
	//--------------------------------------------
	if (EROS_Model_string=="stream_power") EROS_Model=0;
	else if (EROS_Model_string=="shear_stress") EROS_Model=1;
	else if (EROS_Model_string=="shear_stress_2") EROS_Model=2;
	else EROS::Error("no erosion model defined");
	// stream_power (0): based on the stream power assumption e=f(q,s)
	// shear_stress (1): based on the shear stress S: e=E*(S-Sc)^a, where S is density*water_depth*hydraulic_slope
	// shear_stress_2 (2): based on the shear stress with H, the water depth, calculated from q and s from the Darcy-Weisbach formula 


	//--------------------------------------------
	// Definition des processus et des transitions
	// si EROS_Q_process<=Cell area: un seul processus

	PROCESS_limit=PROCESS_END;
	//	1:	only channel (plus diffusion if any)
	//	2:	both hillslope and channel 
	PROCESS_transition = 0;
	//	0:	no transition
	//	1:	both hillslope and channel with a critical discharge
	//	2:	both hillslope and channel with a slope-discharge transition and small slopes 
	//		to channel
	//	3:	idem 2 but with large slopes to channels

	if (EROS_Q_process<=E_grid->cell_area) {
		cout << "channel only" << endl;
		EROS_Q_process=E_grid->cell_area;
		PROCESS_limit=FLUVIAL;
		PROCESS_transition=0;
	} else {
		PROCESS_limit=HILLSLOPE;
		cout << "both hillslope and channel" << endl;

		if (fabs(EROS_Exp_E_s[HILLSLOPE]-EROS_Exp_E_s[FLUVIAL])<=FLT_MIN) {
			PROCESS_transition=1;
		} else {
			if (EROS_Exp_E_s[HILLSLOPE]>EROS_Exp_E_s[FLUVIAL])
				 PROCESS_transition=1;/* en principe 2*/
			else PROCESS_transition=1; /*en principe 3*/

			PROCESS_Exp_transition=(EROS_Exp_E_q[FLUVIAL]-EROS_Exp_E_q[HILLSLOPE])/(EROS_Exp_E_s[FLUVIAL]-EROS_Exp_E_s[HILLSLOPE]);
		}
	}

	//----------------------------------------------------------
	// Make both ErosionProcesses equal at A=EROS_Q_process
	// (only if hillslope erosion coefficient is not defined)
	//----------------------------------------------------------
	// A VERIFIER (selon Dimitri Lague ...)
	if (PROCESS_limit==HILLSLOPE)
	{
		// A negative value of EROS_Cte_Eb or a positive value of EROS_Cte_Er put the calculation in the relative mode.
		// In the relative mode, EROS_Cte_Er is the ratio between channel and erosion rates at transition discharge
		bool EROS_ErosionCoefficient_mode[2]={false};

		for (int p=FLUVIAL; p<=PROCESS_limit;p++) 
		{
			bool &mode=EROS_ErosionCoefficient_mode[p];
			double &coef=EROS_Cte_Eb[p], &coef_relative=EROS_Cte_Er[p];
			mode=coef<0. || coef_relative>0.;
			if (mode && coef_relative<0.) coef_relative=fabs(coef);
		}

		if (EROS_ErosionCoefficient_mode[HILLSLOPE] && EROS_ErosionCoefficient_mode[FLUVIAL]) 
		{
			EROS::Error("Error: both erosion coefficients are in the relative mode");
		}

		if (EROS_ErosionCoefficient_mode[HILLSLOPE])
		{
			EROS_Cte_Eb[HILLSLOPE]= EROS_Cte_Er[HILLSLOPE] *
				EROS_Cte_Eb[FLUVIAL] *
				pow(EROS_Q_process,EROS_Exp_E_q[FLUVIAL]-EROS_Exp_E_q[HILLSLOPE]);
		}

		if (EROS_ErosionCoefficient_mode[FLUVIAL])
		{
			EROS_Cte_Eb[FLUVIAL]= EROS_Cte_Er[FLUVIAL] *
				EROS_Cte_Eb[HILLSLOPE] *
				pow(EROS_Q_process,EROS_Exp_E_q[HILLSLOPE]-EROS_Exp_E_q[FLUVIAL]);
		}
	}

	cout << endl;

}
//------------------------------------
// Initialization of calcul parameters
// and basic tests
//------------------------------------
void EROS::Init_calcul()
{

	ptr_node E_node=E_grid->nodes();

	do_calcul_gradient=2*EROS_calcul_skip-10;
	// Initialize the random area
	if (EROS_random_area<1) EROS_random_area=1;
	// Initialize the CELL_GRADIENTS typology class
	CELL_GRADIENTS.resize(E_grid->Geometry);



	/*----------------- Initialisation de la fonction erosion -----------------
	On considere que le temps retour le plus long est A*log(A) o� A d�signe
	le nombre de possibilit�s independantes de tirage.
	On peut montrer que N=A*log(A) est le nombre de coups qu'il faut tirer pour que
	toutes les cases soient remplies au moins 1 fois
	Attention toutefois � la taille m�moire et au faut qu'il faille calculer tous les TU
	---------------------------------------------------------------------------*/
	double size = (double)EROS_random_area*E_grid->nbreN; 
	int pow_memory= (int)(size * log(size));

	//-------------------------------------------
	// Look for the maximum of erodibility
	//-------------------------------------------
	for (size_t i=0; i<E_grid->nbreT; i++) {
		if (EROS_Cte_Emax<E_node[i].E_e_channel || !i)
			EROS_Cte_Emax=E_node[i].E_e_channel;
		if (EROS_Cte_Emax<E_node[i].E_e_hillslope)
			EROS_Cte_Emax=E_node[i].E_e_hillslope;
	}

	//-----------------------------------------------------
	// Some important parameters about cell and river width
	//-----------------------------------------------------
	EROS_W = E_grid->cell_x;// This expression must be changed if the grid is not regularly spaced

	for (int process=FLUVIAL; process<=PROCESS_limit; process++) 
	{

		// EROS_coefficientsof the transfer distance by the pixel size
		if (EROS_Cte_Ld[process]<=0)
		{
			EROS::Error("Prohibited sediment transfer length of " +convertInString<double>(EROS_Cte_Ld[process]));
		}

		//-------------------------------------------
		// Procedure that accelerates the calculation
		//-------------------------------------------

		//-------------------------------------------
		// Calculation of 
		// Qs=width*erosion rate
		// The function depends on the flow per unit width dQ/dy
		//-------------------------------------------
		// pow_Qs: function used with cte_Qs to define the power-law dependency of Q
		// pow_Qs_y: function used to define the power-law dependency of Dx

		// Note that Dy is calculated as Dy = Cell_area/link_length, and pow_Qs_y= (1/Dy)^Power	
		// The width is either a function (can be constant) or proportional to the pixel size
		pow_Qs[process].init(f_inverse, EROS_Exp_E_q[process]*(1.-EROS_Exp_W_Q[process]), pow_memory);


		
//#if defined(_DIAGONAL_WIDTH_REDUCTION_)
		if (E_grid->DIAGONAL_WIDTH_REDUCTION) // The channel width is taken into account in the calculation of q=Q/W
		{
			
//#if _DIAGONAL_WIDTH_REDUCTION_==2
			if (E_grid->DIAGONAL_WIDTH_REDUCTION==2 && EROS_Exp_W_Q[process]!=0.0) // The dependency on Dy is not allowed when the river width is specified by a function
			{
				pow_Qs_y[process].init((double*)0, 0., 0, 1.);
			} else
// #endif
			{
				/* TODO */
				// TO avoid diagonal terms, a possibility would be to consider differently HILLSLOPE and FLUVIAL terms			
				if (process==HILLSLOPE)
				{
					vector<double>DL(GEOMETRY,min(E_grid->cell_x, E_grid->cell_y));
					pow_Qs_y[process].init(&DL[0],EROS_Exp_E_q[process],GEOMETRY,1.);
				}
				else 
				{
					pow_Qs_y[process].init(E_grid->GRID_Dl,EROS_Exp_E_q[process],GEOMETRY,1.);
				}
			}
		}
//#endif


		//-------------------------------------------
		// Calculation of 
		// the flow width
		// The function depends on the total flow Q
		//-------------------------------------------
		// Fix the flow width equal to the cell size
		if (EROS_Cte_W[process]==0.0) EROS_Cte_W[process]=EROS_W;
		// pow_W: function used with cte_W to define the power-law dependency of Q
		if (EROS_Exp_W_Q[process]!=0.0)
			pow_W[process].init(f_inverse,EROS_Exp_W_Q[process], pow_memory);
		else 
			pow_W[process].init((double*)0, 0., 0, 1.);

		//-------------------------------------------
		// Calculation of 
		// the transfer length
		// The function depends on the flow per unit width dQ/dy
		//-------------------------------------------
		// pow_Ld: function used with cte_Ld to define the power-law dependency of Q
		// pow_Ld_y: function used to define the power-law dependency of Dx

		if (EROS_Exp_Ld_q[process]==0.)
		{
			EROS_TransferModel[process]=LENGTH_CONSTANT;
		//} else if (EROS_Exp_Ld_q[process]==1.) {
		//	EROS_TransferModel[process]=LENGTH_DISCHARGE;
		} else {
			EROS_TransferModel[process]=LENGTH_ANY;

			// The width is defined by a function (can be constant) or by the pixel size (proportional to Dy)
			if (EROS_Exp_Ld_q[process]!=0.)
				pow_Ld[process].init(f_inverse, EROS_Exp_Ld_q[process]*(1.-EROS_Exp_W_Q[process]), pow_memory);
			else
				pow_Ld[process].init((double*)0, 0., 0, 1.);

		
// #if defined(_DIAGONAL_WIDTH_REDUCTION_)
			if (E_grid->DIAGONAL_WIDTH_REDUCTION)// The channel width is taken into account in the calculation of q=Q/W
			{				
// #if _DIAGONAL_WIDTH_REDUCTION_==2		
				if (E_grid->DIAGONAL_WIDTH_REDUCTION==2 && EROS_Exp_W_Q[process]!=0.0) // The dependency on Dy is not allowed when the river width is specified by a function
				{
					pow_Ld_y[process].init((double*)0, 0., 0, 1.);
				} else
//#endif
				{
					/* TODO */
					// TO avoid diagonal terms, a possibility would be to consider differently HILLSLOPE and FLUVIAL terms
					if (process==HILLSLOPE)
					{
						vector<double>DL(GEOMETRY,min(E_grid->cell_x, E_grid->cell_y));
						pow_Ld_y[process].init(&DL[0],EROS_Exp_Ld_q[process],GEOMETRY,1.);
					}
					else 
					{
						pow_Ld_y[process].init(E_grid->GRID_Dl,EROS_Exp_Ld_q[process],GEOMETRY,1.);
					}
					
				}
			}
//#endif		
		} 

		pow_S[process].init(EROS_Exp_E_s[process]);

	} // for process

	
	//----------------------------
	// Lateral erosion parameters
	//----------------------------
	if ((EROS_Cte_El[HILLSLOPE]<=0.) && (EROS_Cte_El[FLUVIAL]<=0.)) 
		EROS_Model_El=0;

	if (EROS_Model_El>=2) {
		pow_Ql.init(f_inverse, EROS_Exp_El_q,pow_memory);
	}

	//------------------------------------------------------------------
	// Initialization of the diffusion function used for precipiton walk
	// from the diffusion coefficient
	//------------------------------------------------------------------
	pow_Diffusion.init(P_Cte_Diffusion);

	if (P_Cte_Diffusion<=0.) P_Diffusion=0;
	else P_Diffusion=1;

	//--------------------------------
	// Initialize climate and rainfall
	//--------------------------------
	EROS_Climate=EROS_climate_mean>0. ? EROS_climate_mean:1.;

	// The inflow must be multiplied by the grid cell
	RAIN_map.Qinflow *= E_grid->cell_x;
	// A first estimate of EROS_Q required to define the precipiton volume
	if (RAIN_map.Qinflow>0.)
	{	
		EROS_Q=RAIN_map.Qinflow;
	} 
	else if (RAIN_map.Qtot>0.) // The total inflow = Sum(rain(i)*area(i))
	{
		EROS_Q=RAIN_map.Qtot;
	}
	else if (RAIN_map.Qarea*RAIN_map.rainfall>0.) 
	{
		EROS_Q=RAIN_map.Qarea*RAIN_map.rainfall;
	}
	else if (RAIN_map.Qarea>0.) 
	{
		EROS_Q=RAIN_map.Qarea;
	}
	else 
	{
		EROS_Q=EROS_TU*E_grid->cell_area;
	}
	
	EROS_Q *= EROS_Climate;
	//-------------------------------------------
	// Limit some coefficients to abnormal values
	//-------------------------------------------
	if (EROS_Cte_Ld_sea<=0.) {
		SEA_stop = 0; // No depostion below sealevel
		EROS_Cte_Ld_sea=1.;
	} 
	else {
		SEA_stop = 1; // Deposition may occur below sealevel
	}

	//-------------------------------------------
	// Determination of the time scale
	//-------------------------------------------
	// Note that EROS_TU must be defined before
	if (EROS_P>0.) 
	{
		if (STEP_TU)
		{
			EROS_P_v = EROS_P/EROS_TU*EROS_Q;
			EROS_P_min = EROS_P_vmin/EROS_TU*EROS_Q;
			EROS_P_max = EROS_P_vmax/EROS_TU*EROS_Q;
		} else
		{
			EROS_P_v = EROS_P*EROS_Q;
			EROS_P_min = EROS_P_vmin*EROS_Q;
			EROS_P_max = EROS_P_vmax*EROS_Q;
		}

	} 
	else if (EROS_P_v>0.) 
	{
		if (STEP_TU)
			EROS_P = (EROS_P_v/EROS_Q)*EROS_TU;
		else
			EROS_P = (EROS_P_v/EROS_Q);
	}
	else 
	{
		EROS::Error("No time coefficient defined");
		// EROS_P=0.1;
	}

	if (EROS_P_min<0.)
		EROS_P_min = EROS_P/10.;

	if (EROS_P_max<0.)
		EROS_P_max = EROS_P*10.;

	// Check for right values
	if (EROS_print_TU<=0) EROS_print_TU=1;
	

}
//----------------------------------------------
//	Calcul of the basic calculation coefficients
//  The function is called at each TU
//----------------------------------------------
void EROS::EROS_coefficients()
{
	static int firstin = 1;
	

	//------------------------------------------------
	// Max topographic change coefficient
	//------------------------------------------------
#ifdef _WATER_HEIGHT_
	if (FLOW_calcul) 
	{
		if (EROS_limiter_parameter<=0.)  EROS_limiter = E_grid->CALCUL[TOPO].avg*CALCUL_LITTLE;
		else 
			EROS_limiter=EROS_limiter_parameter;
	}
#endif
	//------------------------------------------------
	// Flow-related coefficient
	//------------------------------------------------
	// The transition_time is used to defined the water discharge, and the erosion process
	// By normalizing with EROS_Q_process, we ensure that both hillslope and fluvial process
	// are as efficient for a discharge equivalent to the drainage area "EROS_Q_process"
	// The total influx (EROS_Q  = RAIN.Qtot * EROS_Climate) must be calculated after the climate change function
	EROS_k  = max(1,EROS_poisson-1) * EROS_random_area;
	// (to implement) If EROS_poisson=1, the integral diverges and the coefficient is the exponential integral: -Ei(-1)
	EROS_Qk = EROS_Q*EROS_k;
	EROS_Sk = EROS_Qk/FLOW_Cte_Cz;

	//----------------------------------------------------
	// Initialisation of the elementary precipiton volume
	//----------------------------------------------------
	P_water = EROS_Q*P_time; 
	
	//-------------------------------------------------------
	// Pk_time * time_k is the precipiton time constant Vp/Q
	//-------------------------------------------------------
	Pk_time = P_time / EROS_k;

	/* The smallest possible coefficient (used to avoid infinitely lasting cells with zero slope)
	// Could be calculated in a smarter way...

	if (EROS_draw_REAL>0.) 
		type_node::coef_min=1./(10.*EROS_draw_REAL); 
	else 
		type_node::coef_min=1e-5;
	*/

	// FLOW_pQ = FLOW_time_cste>0. ? P_time/FLOW_time_cste : 0.;
	// FLOW_q = EROS_Qk / EROS_W;
	// Define the flow depth on points where water flows in (and which do not belong to boundaries).
	// Note that it may be necessary to define an arbitrary depth, 
	// which will be different for a rain droplet and a river slice.
	// TODO ..

	//------------------------------------------------
	// The flow depth of a precipiton
	//------------------------------------------------
	P_height = FLOW_Cte_Depth * P_water/ E_grid->cell_area;
	//------------------------------------------------
	// Splash (diffusion) coefficient
	//------------------------------------------------
	CALCUL_splash		= P_water * 4. /E_grid->Geometry * EROS_splash ;
#ifdef _SPLASH_ANISOTROPY_
	// Anisotropic diffusion coefficients
	CALCUL_splash_X	= P_water * EROS_splash_X;
	CALCUL_splash_Y	= P_water * EROS_splash_Y;
#endif

	//------------------------------------------------
	// Basic test to restrict some parameter values
	//------------------------------------------------
	if ( EROS_k<=0 ) 
	{
		string msg = "Bad poisson number (EROS_k="+convertInString(EROS_k)+").";
		EROS::Error(msg);
	}
	if ( EROS_Q<=0. )
	{
		string msg="No flux (EROS_Q ="+convertInString(EROS_Q)+").";
		// EROS::Error(msg);
		cout << msg << endl;

	}
	//------------------------------------------------
	// Process dependent coefficients
	//------------------------------------------------
	for (int process=FLUVIAL; process<=PROCESS_limit; process++) 
	{
		//-------------------------------------------
		// Calculation of 
		// Qs=width*erosion rate
		// The function depends on the flow per unit width dQ/dy
		//-------------------------------------------
		// cte_Qs: function used with pow_Qs to define the power-law dependency of Qs

		cte_Qs[process]	= EROS_Cte_Eb[process]
						* pow(EROS_Qk,EROS_Exp_E_q[process]);
//#if !defined(_DIAGONAL_WIDTH_REDUCTION_)
		if (!E_grid->DIAGONAL_WIDTH_REDUCTION)
		{
			cte_Qs[process] /= pow(EROS_Qk,EROS_Exp_W_Q[process]*EROS_Exp_E_q[process]);
			cte_Qs[process] /= pow(EROS_Cte_W[process],EROS_Exp_E_q[process]);
		} else {
//#else
			cte_Qs[process]	/= pow(E_grid->cell_area,EROS_Exp_E_q[process]);
		}
//#endif

		//-------------------------------------------
		// Calculation of 
		// the flow width
		// The function depends on the total flow Q
		//-------------------------------------------
		// cte_W: function used with pow_W to define the power-law dependency of Q
		cte_W[process] = EROS_Cte_W[process] * pow(EROS_Q*EROS_k,EROS_Exp_W_Q[process]);
		

		//-------------------------------------------
		// Calculation of 
		// the transfer length
		// The function depends on the flow per unit width dQ/dy
		//-------------------------------------------
		// cte_Ld: function used with pow_Ld to define the power-law dependency of Q

		cte_Ld[process]	= EROS_Cte_Ld[process] 
						* pow(EROS_Qk, EROS_Exp_Ld_q[process]);
//#if defined(_DIAGONAL_WIDTH_REDUCTION_)
		if (E_grid->DIAGONAL_WIDTH_REDUCTION)
			cte_Ld[process]	/= pow (E_grid->cell_area, EROS_Exp_Ld_q[process]);
		else {
//#else 
			cte_Ld[process]	/= pow(EROS_Qk, EROS_Exp_Ld_q[process]*EROS_Exp_W_Q[process]);
			cte_Ld[process]	/= pow (EROS_Cte_W[process],EROS_Exp_Ld_q[process]);
		}
//#endif

		// Lateral erosion normalization in case...
		// This may have to be modified according to the width option
		cte_El[process] = pow(EROS_Qk,EROS_Exp_El_q);
	} // for process

}
