/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2011 CNRS (Centre National de la Recherche Scientifique), UMR 6118 Geosciences Rennes
 
    This file is part of Gridvisual

    Gridvisual is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gridvisual is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gridvisual.  If not, see <http://www.gnu.org/licenses/>.
*/

//---------------------------------
// Groundwater functions
//---------------------------------
#include "stdafx.h"

#ifdef _GROUNDWATER_

#include "ErosGroundwater.h"
// Evolution of the water table
// The precipiton reached the grid cell Ptr 
// with a volume P_w

// The outflowing volume can be greated
inline double EROS::GW_Porosity(ptr_node Ptr)
{
	return (GW_porosity * Ptr->area);
}
//-----------------------------------------------
// The groundwater dynamics
// based on the diffusion-like equation
// of unconfined aquifers
//-----------------------------------------------


// Increase the water table and test if it is outcropping
// return the actual change of the water table
double EROS::GW_change_test(ptr_node Ptr, double w)
{
	// Increase the topography
	type_overflow::iterator it_overflow=list_overflow.find(Ptr);
	if (Ptr->IS_NOT_BOUNDARY()) {
		// Put overflowing if it exists
		if (it_overflow!=list_overflow.end())
			w += it_overflow->second / GW_Porosity(Ptr);
		// Increase the water table
		Ptr->E_groundwater += w; 
	} // else the groundwater table does not increase but the water flows away

	// Test whether the water table overflows
	double dz=Ptr->E_groundwater - Ptr->E_topo;
	if (dz>=0.)
	{
		list_overflow[Ptr]=dz*GW_Porosity(Ptr);
		Ptr->E_groundwater = Ptr->E_topo;
	}
	else {
		if (it_overflow!=list_overflow.end()) list_overflow.erase(it_overflow);
	}
	return w;
}
ptr_node EROS::GW_diffusion (ptr_node Ptr, double P_w)
{

	if (GW_coefficient<=0.0) return Ptr;
	list_overflow.clear();


	// Increase the groundwater table of the central point
	GW_change_test(Ptr, P_w/GW_Porosity(Ptr));

	// Change the neighbors according to the diffusion rule
	double & Z_gw = Ptr->E_groundwater;
	double dZ=0.;
	ptr_node PV;
	type_link_set & Links=Ptr->Link;

	for(auto l=Links.begin(); l!=Links.end(); l++)
	// The initialization part of the following line is necesary 
	// Not the line itself if the neighbors are properly defined
	if ( (PV = l->node_D)!=Ptr)
	{
		double C=GW_coefficient/(*l).dl2;
		if (C>.5) {
			cout << "splash " << C << " much too large!!!" << endl;
			C=0.5;
		}

		dZ += GW_change_test(PV, (Z_gw - PV->E_groundwater)*C);
	}
	// Modify the central point according to the diffusion rule
	//	1. If the cental was supposed to overflow, avoid the table to fall down below the topography
	GW_change_test(Ptr, -dZ);

	if (list_overflow.empty()) 
	{
		P_water_t=0.;// No outflow
		return NULL;
	}

	// choose one of the overflowing cell to 
	type_overflow::iterator it_overflow=list_overflow.begin();
	double P_max=0.0, p_max=it_overflow->second;
	for( type_overflow::iterator it_list = list_overflow.begin(); it_list != list_overflow.end(); ++it_list)
	{
		double &value=(*it_list).second;
		if (value>p_max) it_overflow=it_list;
		value=P_max+=value;
	}
	// P_max>P_w means that the outflow is larger than the precipiton volume 
	// (i.e. a contribution from groundwater

	// Take randomly within the overflowing pixel with a weight proportional to overflow
	/*
	P_max=E_grid->RANDOM_generator.rand_exclude();
	for(it_overflow=list_overflow.begin(); it_overflow != list_overflow.end() && it_overflow->second<P_MAX; ++it_overflow)
	if (it_overflow==list_overflow.end()) cout << "couille dans le pate" << endl;
	*/
	// The following relationship is actually the definition of P_water_t
	P_water_t = P_max/P_water;
	// return either the central ptr if it overflows or the largest overflowing pixel
	type_overflow::iterator it_ptr=list_overflow.find(Ptr);
	return it_ptr !=list_overflow.end() ?  it_ptr->first : it_overflow->first;
}

#endif
