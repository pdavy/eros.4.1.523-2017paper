#pragma once
 
#define SVN_LOCAL_MODIFICATIONS 0  // 1 if there are modifications to the local working copy, 0 otherwise
#define SVN_REVISION            523       // Highest committed revision number in the working copy
#define SVN_TIME_NOW            2019/07/05 16:09:09       // Current system date &amp; time
 
