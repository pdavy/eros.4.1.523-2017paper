//--------------------------------------------------------------------
// 2nd method
// The fast gradient calculations (to be developed)
// Method that calculates the tangential plane from the 8 neighbors
// and deduce the steepest slope from it
// The result is independant of the central point as long as the
// 8 neighbors actually exist (which may not be the case along borders)
//--------------------------------------------------------------------
// calculate the direction from the direction cosine of the plane
// angle is an additional angle for randomizing the direction

void direction_randomize(double &Sx, double &Sy, double angle)
{
	double cs=cos(angle), sn=sin(angle);
	Sx=Sx*cs-Sy*sn;
	Sy=Sy*cs+Sx*sn;
}

int direction_V8(double Sx, double Sy)
{
	static const double max_PI8=cos(M_PI/8.);
	static const double min_PI8=sin(M_PI/8.);

	int dir=0;
	if		(Sy>= max_PI8) dir =0;
	else if (Sy<=-max_PI8) dir=4;
	else if (Sx>= max_PI8) dir=2;
	else if (Sx<=-max_PI8) dir=6;
	else if (Sy>0)dir=(Sx>0)? 1:7;
	else dir=(Sx>0)? 3:5;
	
	return dir;
}

type_gradient EROS::calculate_slope (ptr_node P_node, Node_typology & nodes)
{
	static double Cx=1/(6.0*E_grid->cell_x);
	static double Cy=1/(6.0*E_grid->cell_y);
	// The method calculate the tangential plane from the 8 neighbors

	int CELL_links=nodes.cells;
	/*
	for(size_t i=0; i<CELL_links;i++) 
	{
		ptr_node P = nodes[i].link->linkedNode;
		nodes[i].Dh = P->Topo+P->Water_h;
	}*/

	double & Sxz=nodes.Sx = 0.;
	double & Syz=nodes.Sy = 0.;
	int dir_opt;

	if ( CELL_links == 8) 
	{
		// int V8[8][2] ={{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,-1},{-1,0},{-1,1}};
		Sxz = +nodes[1].Dh
			  +nodes[2].Dh
			  +nodes[3].Dh
			  -nodes[5].Dh
			  -nodes[6].Dh
			  -nodes[7].Dh;
		Syz = +nodes[0].Dh
			  +nodes[1].Dh
			  -nodes[3].Dh
			  -nodes[4].Dh
			  -nodes[5].Dh
			  +nodes[7].Dh;
	}
	// A "line" limit, where 3 points are not considered
	else if ( CELL_links == 5) 
	{
		// Find the line
		if (P_node->bc & BC_XMAX) {
			Sxz =  2.* (// + nodes.H 
						+ nodes[0].Dh 
						+ nodes[1].Dh
						- nodes[2].Dh 
						- nodes[3].Dh 
						- nodes[4].Dh);
			Syz =  3.* (nodes[0].Dh - nodes[1].Dh);
			dir_opt=3;
		} else if (P_node->bc & BC_XMIN) {
			Sxz =  2.* (//- nodes.H
						- nodes[0].Dh 
						+ nodes[1].Dh
						+ nodes[2].Dh 
						+ nodes[3].Dh 
						- nodes[4].Dh);
			Syz =  3.* (nodes[0].Dh - nodes[4].Dh);
			dir_opt=0;
		} else if (P_node->bc & BC_YMAX) {
			Sxz =  3.* (nodes[0].Dh - nodes[4].Dh);
			Syz =  2.* (//+ nodes.H
						+ nodes[0].Dh 
						- nodes[1].Dh
						- nodes[2].Dh 
						- nodes[3].Dh 
						+ nodes[4].Dh);
			dir_opt=2;
		} else if (P_node->bc & BC_YMIN) {
			Sxz =  3.* (nodes[2].Dh - nodes[3].Dh);
			Syz =  2.* (//- nodes.H
						+ nodes[0].Dh 
						+ nodes[1].Dh
						- nodes[2].Dh 
						- nodes[3].Dh 
						+ nodes[4].Dh);
			dir_opt=0;
		}
	}
	// A "corner" limit, where 5 points are not considered
	else if  (CELL_links == 3) 
	{
		if (P_node->bc&BC_XMAX && P_node->bc&BC_YMAX)
		{
			Sxz= /*+ 4.*nodes.H*/ + 2*nodes[0].Dh - 2*nodes[1].Dh - 4*nodes[2].Dh;
			Syz= /*+ 4.*nodes.H*/ - 4*nodes[0].Dh - 2*nodes[1].Dh + 2*nodes[2].Dh;
			dir_opt=1;
		} else if (P_node->bc&BC_XMAX && P_node->bc&BC_YMIN)
		{
			Sxz= /*- 4.*nodes.H*/ - 2*nodes[0].Dh + 4*nodes[1].Dh + 2*nodes[2].Dh;
			Syz= /*- 4.*nodes.H*/ + 4*nodes[0].Dh - 2*nodes[1].Dh + 2*nodes[2].Dh;
			dir_opt=2;
		} if (P_node->bc&BC_XMIN && P_node->bc&BC_YMAX)
		{
			Sxz= /*- 4.*nodes.H*/ + 4*nodes[0].Dh + 2*nodes[1].Dh - 2*nodes[2].Dh;
			Syz= /*+ 4.*nodes.H*/ + 2*nodes[0].Dh - 2*nodes[1].Dh + 2*nodes[2].Dh;
			dir_opt=1;
		} if (P_node->bc&BC_XMIN && P_node->bc&BC_YMIN)
		{
			Sxz= /*- 4.*nodes.H*/ - 2*nodes[0].Dh + 2*nodes[1].Dh + 4*nodes[2].Dh;
			Syz= /*- 4.*nodes.H*/ + 4*nodes[0].Dh + 2*nodes[1].Dh - 2*nodes[2].Dh;
			dir_opt=1;
		}
	} 
	else
	{
		cout << "The number of neigbors is bizarre and not taken into account: " << CELL_links << endl;
	}
	Sxz *= Cx;
	Syz *= Cy;
	double S = _hypot(Sxz, Syz);

	// The direction cosine of the steepest slope
	Sxz=Sxz/S;
	Syz=Syz/S;
	int dir = direction_V8(Sxz,Syz);

	if (CELL_links < 8) 
	{
		int d=0; for(;d<CELL_links;d++) if (nodes[d].link->index==dir) break;
		if (d==CELL_links) {
			// The direction steepest slope direction is outside
			// Correct by using the direction perpendicular to boundaries towards inside
			dir = dir_opt;
		}
		else dir=d;
	}


	type_gradient gradient = nodes[dir];
	gradient.Dh = nodes.H-gradient.Dh;

	int idx=gradient.link->index;
	P_node->Slope = gradient.Gradient = S;

	return gradient;
}
