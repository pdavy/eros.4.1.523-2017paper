/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/
//{{NO_DEPENDENCIES}}
// Fichier Include généré de Microsoft Visual C++.
// Utilisé par Eros.rc
//
#define VER_VER_DEBUG                   0
#define WINAPI_PARTITION_DESKTOP        0x00000001
#define VS_VERSION_INFO                 1
#define VFFF_ISSHAREDFILE               0x0001
#define VFF_CURNEDEST                   0x0001
#define VIFF_FORCEINSTALL               0x0001
#define SVN_LOCAL_MODIFICATIONS         1
#define VERSION_MINOR                   1
#define WINAPI_PARTITION_APP            0x00000002
#define VFF_FILEINUSE                   0x0002
#define VIFF_DONTDELETEOLD              0x0002
#define VFF_BUFFTOOSMALL                0x0004
#define VERSION_MAJOR                   4
#define VS_USER_DEFINED                 100
#define SVN_REVISION                    320

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
