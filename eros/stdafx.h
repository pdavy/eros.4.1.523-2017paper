/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

// stdafx.h : Fichier Include pour les fichiers Include syst�me standard,
// ou les fichiers Include sp�cifiques aux projets qui sont utilis�s fr�quemment,
// et sont rarement modifi�s
//

#pragma once

#pragma warning (disable : 4800 4267 4996)

#ifndef _NO_THREAD
#include <windows.h>
#include <process.h>    /* _beginthread, _endthread */
#endif

#include <stddef.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <conio.h>
#include <float.h>
#include <math.h>
#include <time.h>
#include <errno.h>

#include <xmmintrin.h>	// Need this for SSE compiler intrinsics

#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <list>
#include <deque>
#include <map>

using namespace std;

#include <windows.h>

#ifdef _OMP
#include <omp.h>
#endif

#ifndef min
#define min(a,b) (a)<(b)?(a):(b)
#endif

#ifndef max
#define max(a,b) (a)>(b)?(a):(b)
#endif

#ifndef POSITIF
#define	POSITIF(a) ((a)>0.0?(a):0.0)
#endif


#ifndef TEST_EXUTOIRE
#define TEST_EXUTOIRE 0
#endif

// A class for treating the argument of a function
#include <unvar/io_utilities/io_utilities.h>
#include <unvar/io_utilities/arg.h>
#include <unvar/io_utilities/Input_data.h>

#include <Math_Eros/Math_pow.h>
#include <Math_Eros/Math_random.h>

#include <grid/GRD.h>
#include <grid/GridOptions.h>

#include <grid/grid.hpp>
#include <grid/Node_typology.h>

#include "ErosPrecipiton.h"
#include "Rain\rain.h"


#include "EROS.hpp"

