//---------------------------------------------------------
//
// Eros by Philippe Davy (UMR CNRS 6118 Geosciences Rennes)
// 
//---------------------------------------------------------

#include "stdafx.h"
//---------------------------------------------------------
// Water height functions
//---------------------------------------------------------

// Still in development

//---------------------------------------------------------------------
//---------------------- The water height functions -------------------
//---------------------------------------------------------------------
// It works but is still preliminary. The formulae have to be checked as 
// well as the optimization of the following procedures.

#ifdef _WATER_HEIGHT_

//-------------------------------------------------------
// Simplified functions that calculates the water height
// as a function of discharge, slope, etc.
//-------------------------------------------------------
#if _WATER_HEIGHT_==1
//-----------------------------------------------------------------------------
// Flow depth method 1
//-----------------------------------------------------------------------------
// The flow depth is calculated as a stationary function of discharge and slope
// according to the Weisbach, Manning or Darcy relationships.
// To avoid high flow height variations due to the stochasticiy of discharge,
// the flow depth varies a firs-order kinetic relationship with a time constant.
//------------------------------------------------------------------------------

double EROS::flow_depth_local (ptr_node P, double q)
{
	/*-----------------------------------------------------------
	// Method 1 (not developed) to calculate the water height
	// return P_time_k>0 ? 0.5e+0*pow(EROS_k/P_time_k, 0.666):0.;

	// No change
	// return P->W_depth;

	// Another possibility is to put the water height to the upstream cell
	// The total height is put just below the upstream cell
	return CELL_PtrCurrent->upstream->E_topo + CELL_PtrCurrent->upstream->W_depth - CELL_PtrCurrent->E_topo - FLT_EPSILON;
	-------------------------------------------------------------*/
	
	// CASE 0: constant flow (commented since the function is not called in this case)
	// if (FLOW_equation==0) return 0.;
	//P->N_hydro++;
	// CASE 1: not so much physically sound (except if slope is about constant), but fast
	
	if (FLOW_equation==1) return FLOW_manning_coef*q; 
	
	double s=max(CELL_gradients.slope,1.e-4); // limit the "slope" effect to 10....
	// CASE 2: The Darcy-Weisbach equation
	if (FLOW_equation==2) return FLOW_manning_coef * pow(q, 0.666)*pow(s,-0.333);
	// CASE 3: The Manning equation
	if (FLOW_equation==3) return FLOW_manning_coef * pow(q, 0.6)*pow(s,-0.3);
	
	return 0.;
}

void EROS::flow_depth_modify(ptr_node P, type_d_time dt, double h_limit)
{
	
	//----------------------------------------
	// Averaging method for flow depth
	//----------------------------------------
	//	averaging(P->W_depth, h_limit, dt*dtime());

	double&	hw=P->W_depth, Dh=0.;

	switch (FLOW_method_avg) {
		// Method 0:
		// No calculation is made
		// Means that the water layer is calculated elsewhere (in ErosMain)
		case 0:
			return;
		// Method 1: The water height is directly related to the (stochastic) discharge
		case 1:
			Dh=h_limit-hw;
			break ;
		
		// Method 2 (default): 1st-order kinetic equation
		//case 2:	
		default:
		{
			Dh = (h_limit - hw)*(1.-exp(-dt*FLOW_pQ));
			break;
		}
		/*
		// Method 3: geometric averaging. Just useless and a bit crazy...
		case 3:
			Dh= (hw<CALCUL_EPSILON ? h_limit : sqrt(hw*h_limit))-hw;
			break;
		*/
	}
	// Limit the variations of the water height
	// TODO: The height must be calculated as the true volume height (i.e. multiplied by the inflow)
	
	if (Dh==0.) return; // Don't change anything
	int sgn = Dh>0.? 1: -1;

	double dv=GetP_height(P);
	if (fabs(Dh)>dv) Dh= sgn*dv;

	// Do it
	hw += Dh;
	hw=POSITIF(hw);
	printf_points(P, Dh, E_WATER);
}

int EROS::calculate_flow_depth(ptr_node P)
{
	// Calculate what theoretically the flow depth should be
	double depth_th=0.;
	if (P_time_k) depth_th = flow_depth_local(P, FLOW_q/P_time_k);
	// else no change

	flow_depth_modify(P, P_time_k, depth_th);


	ptr_node CELL_PtrAmont=CELL_PtrCurrent->upstream;
	
	if ( CELL_PtrAmont )
	{
		double dh=CELL_PtrAmont->W_depth+CELL_PtrAmont->E_topo - P->E_topo;
		P->W_depth =min( dh, P->W_depth );
		P->W_depth =POSITIF(P->W_depth );
	}
	
	// calculate the flow depth for neighbors
	
	// Don't consider the very first point
	if (!P->upstream) return 1; 
	
	double dw = GetP_height(P)*FLOW_diffusion, dh, D;
	vector<type_link>&L = P->Link;

	for (it_link l=L.begin(); l!=L.end(); l++)
	{
			ptr_node P_l=l->linkedNode;
			if ( P_l-P->downstream && P_l-P->upstream && P_l->IS_NOT_BOUNDARY() ) 
			{
				// Diffusion
				dh = (P_l->E_topo+P_l->W_depth) - (P->E_topo+P->W_depth);
				if ( dh<0 ) {
					D = min(-dh,min(dw,P->W_depth));
					P_l->W_depth += D;
					P->W_depth -= D;
				} else {
					D = min(dh,min(dw,P_l->W_depth));
					P_l->W_depth -= D;
					P->W_depth += D;
				}
				// flow_depth_modify(l->linkedNode, P_time_k);
			}
	}
	//
	
	// The following option intends to apply a diffusive-like spreading of the water table 
	// Both the function and the theoretical background (why using it?) have yet to be fixed.
	// It is not actually used because too much time consuming since applied all along the precipiton path
	//flow_splash(P);
	return 1;
	
}

#elif _WATER_HEIGHT_==2
//-----------------------------------------------
// Flow depth: method 2
//-----------------------------------------------

//-----------------------------------------------
// Calculation of the water level
// from the difference between the in and outflow
// during the precipiton time
//-----------------------------------------------
// NOTE: this method allows each grid cell to be infilled by
// an unlimited water quantity
// Except if dh is limited by some value...

int	EROS::calculate_flow_depth(ptr_node P)
{

#if _DEBUG
	int i=E_grid->X(P),
		j=E_grid->Y(P);
	double H = P->E_topo;
#endif
	if (!CELL_PtrCurrent->upstream || !P_time_k_in) return 0;

	double &Hw = P->W_depth;

	// dh is the difference between in- and out-flow
	// The flow is: P_water_t * EROS_Qk / P_time_k
	double dh=P_height/P->area *FLOW_depth_coef * (P_water_t_in*P_time_k/P_time_k_in - P_water_t);
	Hw += dh;
	Hw = POSITIF( Hw );

	// Limit the altitude change to maintain the continuity of water flow
	// (the upstream water table must remain higher)
	double h=CELL_PtrCurrent->upstream->W_depth+CELL_PtrCurrent->upstream->E_topo - P->E_topo - CALCUL_EPSILON;
	Hw = min( h, Hw );
	
	// In theory, this is an explicit solving: the parameter update is applied for future flow
	// It is thus not necessary to update the cell gradients: CELL_gradients.update(P);
	
	// This option applies a diffusive-like spreading of the water table 
	// Both the function and the theoretical background (why using it) have yet to be fixed.
	// In any case, this is quite a time consuming process since it is applied all along the precipiton path
	//flow_splash(P);
	return 1;

}

#endif // _WATER_HEIGHT_==2

//-----------------------------------------------
// Manage precipitons that fall into a water hole
//-----------------------------------------------
bool EROS::fill_water_new(ptr_node P_node)
{
	double & Hw=P_node->W_depth;
	
	// If the flow depth is nil, no way to escape
	if (Hw<=0.) return true;

	// CELL_gAval.Gradient_hydro is negative
	// Check whether it is a true hole (the top of water is smaller than the surrounding topography)
	// or not

	// Ranking the cell by decreasing outlets;
	type_gradient gradient=CELL_gradients.sort_hydro();
	// Calculate the precipiton height when spread in a cell
	double P_depth = EROS::GetP_height(P_node);
	double hole = gradient.Dw + P_depth - CALCUL_EPSILON;

	if (hole < 0.) 
	{
		// It is a true hole
		// Fill the hole
		P_node->W_depth += P_depth;
		// Remove any water from the precipiton
		P_water_t = 0.;
		// (to be checked) Not necessary to update the neighboring parameters
		// CELL_gradients.update(P_node);
		return true;
	}
	else
	{
		// It is not a "true" hole (water can overflow topography)
		// Fill the hole and let the precipiton flow downwards
			// NOTE that the precipiton depth is much larger because its basal surface is smaller than the one of the cell 
		P_node->W_depth += (-gradient.Dw + CALCUL_EPSILON);
		// Remove the precipiton part that has been "deposited"
		P_water_t *= (hole / P_depth);

		// Update the neighboring parameters
		CELL_gradients.update(P_node);
		return false;
	}
}
// TO CHANGE: 
// fill the water depression and, if any, let water overflow (P_water_t <1).
// If part of the precipiton water remains trapped in the water depression, deposit also sediment
// in the same ratio as water

double EROS::fill_lake(ptr_node P_node, type_gradient & Aval)
{
	double h= -Aval.Gradient_hydro*Aval.link->dl+CALCUL_EPSILON;
	P_node->W_depth +=h;
	//printf_points(P_node, h, E_WATER_FILL);

	return h;
}
bool EROS::fill_water(ptr_node P_node)
{
	// The precipiton falls down in a water hole

	if (!FLOW_equation) return true;
	double & Hw=P_node->W_depth;

	// CELL_gAval.Gradient is negative
	// Check whether it is a true hole (the top of water is smaller than the surrounding topography)
	// or not

	// Ranking the cell by decreasing gradients;
	type_gradient G_aval=CELL_gradients.sort_topo();
	bool _test= (P_node->W_depth <= G_aval.Dh);
	if (1 || !_test ) // force the water to route downward by filling "water" holes
	{
		// Change the flow direction (may be not necessary...)
		type_gradient &CELL_gAval=CELL_gradients.front();
		// Fill up to the cell to allow water to flow
		double h=fill_lake(P_node, CELL_gAval);
		// Reset CELL_gradients
		for (size_t i=0; i<CELL_gradients.links; i++) {
			type_gradient &g=CELL_gradients[i];
			g.Dh += h;
			g.Gradient_hydro+=h/g.link->dl;
		}
		return false;
		// It is not a "true" hole (water can overflow topography)
	}
	else
	{
		// It is a true hole
		return true;
	}
}
#endif //define _WATER_HEIGHT_