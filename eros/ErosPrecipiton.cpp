/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

#include "StdAfx.h"
#include "ErosPrecipiton.h"

ErosPrecipiton::ErosPrecipiton(void)
{
}

ErosPrecipiton::~ErosPrecipiton(void)
{
}
