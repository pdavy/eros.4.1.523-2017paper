/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

#include "stdafx.h"

void __cdecl CheckKey( void *pVoid);

/*---------------------- PROCEDURE PRINCIPALE------------------------------- */

int main (int argc, char * argv[])
{
	// Getting the program options
	Carg_list arg_list(argc, argv,".arg");
	// Initializing the Grid
	class_grid Grid;
	// Initializing Eros
	EROS Eros(&Grid);
	if (!Eros.Init(arg_list)) 
	{
		Eros.Error("Initialization fails");
	}
	// Launching threads for key interception
#ifndef _NO_THREAD
	_beginthread( CheckKey, 0, (void *)&Eros);
#endif
	// Launching calculations
	Eros.Calcul();

	if (Grid.pause_bool) {
		do clock(); // or whatever you want
		while (Eros.EROS_key-'q');
	}
	return 1;
}

//----------------------------------------
// The check key function
//----------------------------------------


void __cdecl CheckKey( void *_Eros_)
{
	EROS * Eros_ =(EROS *)_Eros_;

	static char ch;
	string CommandLine;
	
	string GridVisual = "C:\\Program Files\\GridVisual\\GridVisual.exe";
	
	char * gridvisual;
	if ( (gridvisual = getenv("GRIDVISUAL"))!=NULL)
		GridVisual = string(gridvisual); 

	char & key=Eros_->EROS_key;


    while (Eros_->EROS_message) {
		switch (ch =_getch()) {
		case 'q':
			key=ch; // Quit or try to do it...
			break;

		case 'f':
			key=ch; // change the end...
			break;

		case 'P':
			key=ch; // METTRE EN PAUSE

			//if (MessageBox( NULL, " STOP PAUSE ?","EROS",MB_OK | MB_TOPMOST)==IDOK) 
			{
				key=0;
			}

			break;

		case 'i':
			CommandLine = GridVisual + " ";
			CommandLine.append(Eros_->E_grid->output_namefile);
			WinExec(CommandLine.c_str(),SW_SHOWDEFAULT);
			break;
		case 'F': 
			key='p'; // The key to pause...
			if (Eros_->EROS_end & IF___TIME) 
			{
				cout << endl << "-------------------------------------" << endl;
				cout << "New final time? "; 
				cin >> Eros_->EROS_time_REAL;
			} else if (Eros_->EROS_end & IF_____TU) {
				cout << "New final time (TU unit)? ";
				cin  >> Eros_->EROS_time_TU;
			} 
			cout << "-------------------------------------" << endl;
			key=0;
			break;


		default:
			key=ch;
		}
	}
	_endthread();

}

//-------------------------------------
// The bad or happy end function
//-------------------------------------
void EROS::Error(string msg) {
	cout << endl << "----------------------- !STOP! -------------------------------"<<endl;
	cout << "Something goes wrong. You haven't respected an elementary principle of this model:" << endl;
	cout << msg << endl;
	cout << endl << "Bad news Milord:'-( you are now fired from the game. Bye bye!!" << endl;
	system("pause");
	exit(-1);
}
