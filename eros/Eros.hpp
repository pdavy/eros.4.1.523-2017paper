/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/


#pragma once

using namespace std;
//---------------------
// Parametres de calcul
//---------------------
#ifndef GEOMETRY
#define GEOMETRY 8
#endif
//---------------------
// Parametres de calcul
//---------------------
#define IF______0	(0)
#define IF___TIME	(1<<0)
#define IF_____TU	(1<<1)
#define IF__STAGE	(1<<2)
#define IF_HEIGHT	(1<<3)
#define IF___FLUX	(1<<4)

//-------------------------
// Erosion and default type 
//-------------------------
#define	E_NO		(0)
#define E_RUN		(1<<0)
#define E_SET		(1<<0)
#define E_ERODE		(1<<1)
#define E_DEPOSIT		(1<<2)
#define E_LATERAL	(1<<3)
#define E_HOLE		(1<<4)
#define E_DIFFUSION	(1<<5)
#define E_UPLIFT	(1<<6)
#define E_RESET		(1<<7)
#define E_READ		(1<<8)
#define E_CLOSE		(1<<9)
#define E_ADAPT		(1<<10)
#define E_FLOW_ONLY	(1<<11)
#define E_MINIMUM	(1<<12)
#define E_FIXED		(1<<13)
//---------------------
// erosion parameters
//---------------------


enum HYDRO_PHASE { H_RUN=0, H_INIT };
enum type_transfermodel { LENGTH_CONSTANT, LENGTH_DISCHARGE, LENGTH_ANY }; 
//enum input_method { INPUT_STRICT=0, INPUT_SUBSET, INPUT_ASK };

//-----------------------------------------------
// EROS
// defines the eros calculation parameters
//-----------------------------------------------

class EROS
{
public:

	class_grid * E_grid;

	EROS(class_grid * _grid);
	~EROS();

	//---------------------------
	// Time and output variables
	//---------------------------
	type_time EROS_Precipitation;
	type_time EROS_Marcheur;

	double	EROS_initialization;

	double	EROS_time;
	double	EROS_time_init;
	double	EROS_time_TU;
	double	EROS_time_REAL;
	int		Time_TU;
    int     STEP_TU; // If 1 the time step is defined for 1 TU; if 0, for each precipiton
	
	// EROS_date return the today date 
	// Note that changes can been done from a precipiton time or the actual time (EROS_time)
	// This entails modifying the flow calculation
	inline type_time & EROS_date(){return  EROS_Precipitation;} 
	int			TIME__increase();
	type_d_time	TIME__calcul(ptr_node Ptr, type_time T, HYDRO_PHASE stage);
	type_d_time	TIME__lapse(ptr_node Ptr, type_time T);

	size_t	EROS_TU;		// The number of precipiton launched during an elementary "rainfall" (TU) event
	double	EROS_TU_coef;	// The number of times the surface is "sampled" by rain during a TU event
	double	EROS_TU_time;	// The duration of a TU event


	double	EROS_draw;
	double	EROS_draw_REAL;
	int		EROS_draw_condition;

	int 	EROS_draw_TU;
	int		Draw_TU;

	int		EROS_print_TU;


	// The volume of time interval for each precipiton
	double	EROS_P_v;	
	double	EROS_P_vmax;		// maximum time
	double	EROS_P_vmin;		// minimum time

	double	EROS_P;			// Time interval between two precipiton groups
	double	EROS_P_max;		// maximum time
	double	EROS_P_min;		// minimum time
	
	bool	EROS_P_climate;// fix if the precipiton volume is changing with climate variations
	

	int		EROS_end_condition;
	int		EROS_end;
	double	EROS_end_height;
	double	EROS_end_flux;


	int		output_nx;
	int		output_ny;

	size_t	outflow_averaging; // For averaging flux values...

	bool	stock_equilibrium;// Flag to force the input sediment flux to match permanently the output flux

	clock_t clck_init; // Use to calculate the calculation time

	//--------------------------
	// Time adaptative variables
	//--------------------------
	bool	adapt_time;
	int		adapt_time_errmin;
	int		adapt_time_errmax;
	double	adapt_time_factor;
	// adapting time operation ("+" or "*")
	string	adapt_time_op;
	// method to adapt time: 0=all errors, 1: only deposit, 2: only erosion, 
	int		adapt_time_type;

	//-------------------------------------------------------
	// Definition of the actual running precipiton
	//-------------------------------------------------------
	ptr_node	pCELL_current;
	ptr_node	pCELL_amont;
	ptr_node	pCELL_aval;
	int			CELL_amont;	// The upstream direction

	Node_typology	CELL_GRADIENTS;	// The local gradient vector of pCELL_current
	Node_typology	AMONT_GRADIENTS;	// The local gradient vector of pCELL_current

	Node_typology::iterator	GRADIENT_D;// The downstream gradient

	type_d_time	P_date_k;		// The precipiton "time", basic to the numerical method
	type_d_time P_date_k_in;	// upstream value of the preceding parameter
	double		GET_P_date();

	double		P_dt;			// The precipiton time scale V/Q
	void		CALCUL_P_dt();
	inline double GET_P_dt() { return P_dt; }

	//------------------------------------------------
	// time inteval between two successive precipitons
	//------------------------------------------------
	double		EROS_Q;			// The characteristic discharge r(*)*A(*)
	double		P_time;			// The time constant between two successive precipitons (i.e. the ratio EROS_P/EROS_TU)
	double		Q_P();			// The water discharge associated with precipiton (total=integrated over the channel width)

	double		P_stock;		// Sediment "stock" within the precipiton
	double		P_water;		// Initial precipiton water volume
	double		P_water_t;		// Precipiton water volume along path (normalized between 0 and 1). Supposed to decrease with infiltration...
	double		P_water_t_in;	// upstream value of the preceding parameter
	double		P_height;		// Initial precipiton depth by cell area
	double		P_dist;			// The distance run by the precipiton
	type_dist	P_step;			// The distance (in pixel)

	bool		FLOW_limiter;	// Tell if the water depth should be limited at boundary
	double		FLOW_limit;		// Tell how much water depth at boundary

	double	GET_P_volume();
	double	GET_P_height(ptr_node P);

	double	Sq();	// The discharge component of the shear stress
	double	q_manning(bool update=false); // The discharge per unit time from the friction equation

	inline	double Cell_area(){return pCELL_current->area;}
	double	Channel_width();
	inline	double	Channel_length(){ return GRADIENT_D->dl(); }
	double	Channel_area();

	//-----------------
	// Output variables
	//-----------------
	int		precipiton_in;
	int		precipiton_out;
	int		precipiton_off;
	double	distance_out;
	double	distance_in;
	type_dist step_out;
	type_dist step_in;

	int		nbre_diag;
	int		nbre_horiz;
	int		nbre_erode;

	double	EROS_outflow; // Total water outflow
	double	EROS_influx; // Total material influx 
	double	Bilan;
#ifdef _DEBUG
	double	DEBUG_dh; // Track erosion (for debugging)
#endif
	vector<double> EROS_outflux; // Total material outflux


	//-------------------------------------------
	// Variables used to simplify calculation
	//-------------------------------------------
	int		EROS_calcul_skip;

	bool	P_force_deposit;
	int		do_calcul_gradient;	// Used to avoid the calculation of slope gradients
	bool	do_gradients;	// Mean that the slope was calculated around the current precipiton
	
	//-------------------------------------------
	// Variables used to define erosion processes
	//-------------------------------------------
private:
	//	0:	only channel (plus diffusion if any)
	//	1:	both hillslope and channel with a critical discharge
	//	2:	both hillslope and channel with a slope-discharge transition and small slopes 
	//		to channel
	//	3:	idem 2 but with large slopes to channels
	type_process	PROCESS_active_;	// The actual erosion process
	type_process	PROCESS_past_;	// ... and the previous one
public:
	inline	type_process PROCESS_active() { return PROCESS_active_; }
	int		PROCESS_limit;		// The number of erosion processes
	int		PROCESS_transition; // The type of transition between erosion process 
	void	PROCESS_get(HYDRO_PHASE stage);

	double  PROCESS_Exp_transition;// The transition exponent for erosion process
	int		PROCESS_limiter;

	// Altitude change limiter
	double	EROS_limiter_parameter; 
	// Fix the maximum altitude change during calculation
	// If 0 (default value), the parameter is equal to 1/10000 of the mean altitude
	double	EROS_limiter; 
	//------------------------------------------
	// Precipiton and discharge parameters
	//------------------------------------------
	double	P_Cte_Diffusion;
	// Hydraulic diffusion model (obsolete)
	int		P_Model_Diffusion;
	//0 = (default) dependent on slope (see the diffusion coefficient for defining the slope dependency)
	//1 = inversely dependent on discharge (which tends to spread out the river flow)
	//2 = dependent on both slope and 1/Q

	// Define if precipiton can "diffuse" over topographic slopes
	int		P_Diffusion;

	// Precipiton flow model (obsolete)
	// int		P_Model_Q;
	//0	= (default) normal
	//1	= flow never decreases below its initial value
	//2	= flow never decreases downstream

	// Stochastic variables for calculating Q
	int		EROS_random_area;	// A coefficient that enlarges the random area for precipiton landing (1=only the actual area)
	int		EROS_poisson;		// The poisson coefficient for calculating fluxes
	int		EROS_poisson_1;		// EROS_poisson - 1 (intermediate variable)
	int		EROS_poisson_rank;	// (intermediate variable)

	double	EROS_k;		// The normalisation factor used with time_k : nPoisson*Astat
	double	Pk_time;	// A variable required to calculate V/Q. Defined in the function EROS_coefficients
	double	EROS_Qk;	// The product EROS_Q by EROS_k
	double	EROS_Sk;	// The ratio between q and Cf

	//----------------------------------
	// Rainfall variables
	//----------------------------------
	// Cumulative Rain vector
	class_rain		RAIN_map;		// The rainfall map that associates a rainfall volume with a grid cell
	
	//----------------------------------
	// Precipiton displacement functions
	// .. in other words: hydrodynamics
	//----------------------------------
	/* (OBSOLETE. See now the Node_typology class)
	void	fast_calculate_gradients();
	void	calculate_gradients (ptr_node P);
	*/
	Node_typology::iterator	direction_stochastic(Node_typology & nodes);
	int		HYDRO (ptr_node & CELL, bool lake=false);
	void	HYDRO__init();
	int		HYDRO__end(ptr_node & CELL);
	void	WALK_debug(string type);


	// A flag to allow only hydrological calculations without morphology changes
	int	FLOW_only;
	int flow_only;
	// A flag to say that the flow is not calculated; the reference surface is thus topography
	int FLOW_calcul; 
	// The number of allowed lake sinking
	int fall_in_lake_max;
	//--------------------
	// The flow parameters
	//--------------------
	// The flow model: either "chezy" or "manning";
	string	FLOW_Model_string;
	// Hydraulic parameters that defines the river flow depth
	// The general expression is q=Cz*depth^3/2*slope^1/2
	// The Cz (manning) coefficient

	// The flow resistance coefficient (about inverse of the Manning coefficient)
	double	FLOW_Cte_Cz ; 
	// The flow inertia coefficient (see the red EROS notebook)
	double	FLOW_Cte_Inertia;
	
	// A dumping coefficient for smoothing water depth variations 
	// No water depth when set to zero ...
	double	FLOW_Cte_Depth; 

	// Determine the method to calculate the flow
	// 0: the (input) precipiton flow
	// 1: the (ouput) friction flow
	int		FLOW_method;

	// OBSOLETE FUNCTIONS
	// double	flow_depth_local(ptr_node P, double q); // (not used anymore)
	// void	flow_depth_modify(ptr_node P, type_d_time dt, double h_limit); // (not used anymore)

	// double	FLOW_constant; // Cste used to calculate the water height (when divided by time_k)
	// int		FLOW_method_avg; // a time averaging method to filter the high-frequency variations of water depth (not used anymore)
	// int		FLOW_equation; // Equation relating flow depth and discharge (not used anymore)
	// double	FLOW_diffusion; // The coefficient of flow diffusion from and to neighbors (not used anymore)
	// double	FLOW_pQ; // A constant for flow averaging (not used anymore)
	// double	FLOW_q; // A constant for flow discharge (not used anymore)
	// double	FLOW_time_cste; // a time constant to filter the high-frequency variations of water depth (not used anymore)

	// Water surface diffusion (neither finished nor used yet)
	void	flow_splash(ptr_node P_node);
	
	// OBSOLETE FUNCTIONS
	// (old) methods for calculating flow depth
	int		calculate_flow_depth(ptr_node P); // same as calculate gradients
	bool	fill_water_new(ptr_node P); // The new version of fill_water
	bool	fill_water(ptr_node P_node);  // Fill depression with water
	double	fill_lake(ptr_node P_node, type_gradient & Aval);

	void	Infiltration(ptr_node P_node);

	int		anaflux( double * fluxsortant, double * fluxentrant, int zone, int position, double delta_alt, double bord);

	//--------------------------------
	// Erosion law parameters
	//--------------------------------
	
	double	EROS_splash; // The splash coefficient
	double	CALCUL_splash;
#ifdef _SPLASH_ANISOTROPY_
	double	EROS_splash_X;
	double	EROS_splash_Y;
	double	CALCUL_splash_X; // The splash coefficient along X
	double	CALCUL_splash_Y; // The splash coefficient along Y
#endif

	// Variable name
	// Eb	: bedrock erodability
	// Er	: relative erodability between hillslope and channel
	// Es	: sediment erodability
	// El	: lateral erosion
	// Tb	: bedrock erosion threshold
	// Ts	: sediment erosion threshold
	// Ld	: sediment transfer length
	
	// W	: river width
	// S	: slope
	// Q	: discharge
	// q	: discharge per unit width (=U*h)
	// U	: flow velocity
	// h	: river depth

	// The flow width
	double	EROS_W;
	double	EROS_Cte_W[2];
	double	EROS_Exp_W_Q[2];

	// The erosion law parameters
	double	EROS_Cte_Emax;
	double	EROS_Cte_Eb[2];
	double  EROS_Cte_Er[2];
	double	EROS_Cte_Es;
	double	EROS_Cte_Tb[2];
	double	EROS_Cte_Ts;
	double	EROS_Cte_El[2];
	double	EROS_Cte_El_max;
	double	EROS_El_inbend;
	double	EROS_El_outbend;
	double	EROS_El_straight;
	double	EROS_Cte_Ld[2];
	double	EROS_Cte_Ld_max[2];
	double	EROS_Cte_Ld_min[2];
	double	EROS_Cte_Ld_sea;	// coefficient for undersea deposition (consider no erosion)

	double	EROS_Exp_E_q[2];
	double	EROS_Exp_E_s[2];
	double	EROS_Exp_Ld_q[2];
	double	EROS_Exp_El_q;

	
	type_transfermodel EROS_TransferModel[2];

	// Gravity slope
	double	EROS_S_max;

	// The erosion model
	string	EROS_Model_string;
	int		EROS_Model;
	// stream_power (0): based on the stream power assumption e=f(q,s)
	// shear_stress (1): based on the shear stress S: e=E*(S-Sc)^a, where S is density*water_depth*hydraulic_slope
	// shear_stress_2 (2): based on the shear stress with H, the water depth, calculated from q and s from the Darcy-Weisbach formula 

	// Model of lateral erosion
	int		EROS_Model_El;
	//0	: Lateral erosion is simply proportional to bed erosion
	//1	: Lateral erosion is proportional to bed erosion by bank slope
	//2	: Lateral erosion is proportional to bed erosion by flow heigth (q^0.5)
	//3	: Lateral erosion is proportional to bed erosion by bank slope by flow heigth (q^0.5)

	// Model of slope erosion
	int		EROS_Model_E_slope;
	// 0: topographic slope
	// 1: hydraulic slope
	// 2: combined average of topo and hydro slopes

	// Model of erosion threshold
	int		EROS_Model_T;
	//0	: Erosion threshold is constant 
	//1	: Erosion threshold is proportional to flow width W=Q^0.5 (not used anymore)

	// Parameters that defines the transition between hillslope and channel processes
	double	EROS_Q_process; // The critical discharge (or area) that defines the transition between hillslope and channel processes
	// EROS_Model_process is a variable that Dimitri asked for		
	bool	EROS_Model_process;
	// true		: (default) the transition is a discharge
	// false	: the transition an area, independent of climate variation



	//-----------------------------------------------------------------
	// Power functions whose use is supposed to accelerate calculations
	//-----------------------------------------------------------------
	Pow_M	pow_Qs[2];
	double	cte_Qs[2];
	Pow_M	pow_Ql;
	double	cte_El[2];
	Pow_M	pow_Ld[2];
	double	cte_Ld[2];
//#if defined(_DIAGONAL_WIDTH_REDUCTION_)
	Pow_M	pow_Ld_y[2];
	Pow_M	pow_Qs_y[2];
	//Pow_M	pow_Dy[2];
//#endif
	Pow_M	pow_S[2];
	Pow_M	pow_W[2];
	double	cte_W[2];
	Pow_M	pow_Diffusion;// Define the function that is used for routing the precipiton

	//-----------------------------------------
	// flux or state parameters
	//-----------------------------------------
	double	EROS_stock_initial;
	double	EROS_stock_variation;


	//------------------------------------------------
	// Variables used to simplify erosion calculations
	//------------------------------------------------
	double	SED_transfer;
	double	CHANNEL_area;
	double	SED_transfer_coef;
	double	_EXP_transfer;
	
	//-------------------------------
	// Random variables and functions
	//-------------------------------
	int		EROS_seed;

	int		nEROS_extra_probability;
	vector<double>EROS_extra_probability;
	//-------------------
	// FUNCTIONS
	//-------------------
	input_method	EROS_input; // 0: only exact messages are accepted 

	vector	<Cinput_data> InitialData;
	string	write_title(string file_name);
	void	Init_default_parameters();
	int		Init_parameters (string file_name, ofstream & file_parameter, input_method method=INPUT_STRICT);
	int		Init_parameters (vector <string> file, ofstream & file_parameter, input_method method=INPUT_STRICT);
	int		Read_parameters (string comment,string value, input_method input=INPUT_STRICT);
	
	int		Init(Carg_list arg_list);
	int		Calcul ();
	int		EROS_message;
	char	EROS_key;

	void	Error(string msg);
	void	CheckKey( void *pVoid );
	//----------------
	// Print functions
	//----------------
	int		print_init();
	int		print_parameters(Carg_list arg_list, ofstream & file_parameter);
	FILE *	fprint_init (string File_txt, int reprise, double *time_init=0, std::string input_directory=std::string());
	bool	print_result (bool header=false);
	

	FILE *	File_results;
	void	fprint_result (int init=0);

	void	print_header_screen();
	int  	print_screen(bool force);
	bool	time_record_images();
	bool	record_images(double & time_drawing); // Draw images and return if it does it
	void	printf_points(ptr_node ptr, double dz, int erosion_stage, double slope=0.);
	void	print_time(FILE *, clock_t);

	void	end_result();
	void	reset_result();

	double	table_average (vector <double>tableau);
	double	change_precipiton(int flag=E_NO);

	//---------------------------------
	// Percipiton (rainfall) functions
	//---------------------------------
	bool	RAIN__check(ptr_node P);
	void	RAIN__init ();
	void	RAIN__free();

	//------------------
	// Erosion functions 
	//------------------
	void	Init_processes();
	void	Init_calcul ();
	double	Stress(ptr_node & CELL); // The stress function
	double	EROSION_SP(ptr_node & CELL); // The erosion function (of discharge and slope)
	void	EROSION_Stream_Power(ptr_node & CELL, double &Qss, double &H_sediment, double &H_basement, bool no_sediment=false); // Calculation based on the stream power formulation
	void	EROSION_Stress(ptr_node & CELL, double &Shear, double &H_sediment, double &H_basement, bool no_sediment=false); // Calculation based on the shear stress
	// Qss is the contribution of flow to erosion / slope is the actual slope

	int		EROSION_limiter(ptr_node & CELL, double &h); // Limit the erosion or sedimentation to prevent numerical errors
	int		EROS_default(int defaut); // Store or reset erosion or sedimentation calculation defaults 

	void	EROS_transfer_coef();
	void	EROS_coefficients();
	double	EROS_transfer_length();

	int		EROS_Erode (); // return the default if any
	int		HOLE(); // return the default if any
	
	double	Splash_erosion ();
	double	Erosion_lateral(double & Qss);

	// Change topography
	void	set_topo(ptr_node Ptr, double value, int flag, double slope=0.);

	int 	STRESS_stochastic; // A flag that determines how the stress is calculated

	double	H_erosion; // erosion flux
	double	H_deposit; // deposition flux
	double	H_lateral; // lateral erosion

	double&	Qerosion(){return H_erosion;}
	double&	Qdeposit(){return H_deposit;}
	double&	Qlateral(){return H_lateral;}

	//----------------------------------
	// Climate variables
	//----------------------------------
	double	EROS_Climate;
	double	EROS_climate_mean;
	double	EROS_climate_frequency;
	double	EROS_climate_trend;
	double	EROS_climate_amplitude;

	double	EROS_season_recurrence;
	double	EROS_season_duration;
	double	EROS_season_rainfall;

	vector < pair<double,double> > Climate;

	void	CLIMATE__init ();
	double	CLIMATE__function (double time);
	//--------------------
	// Sea level functions
	//--------------------
	vector < pair<double,double> > Sealevel;

	int		SEA__init();
	int		SEA__test();
	double	SEA__function (double time);	
	int		SEA_stop; // If deposition occurs under sea level

	//-------------------------------
	// Boundary or initial conditions
	//-------------------------------
	double	InitialStock ();


};


//--------------------
// Useful  definitions
//--------------------
#define TU(a) ((a)/double(EROS_TU))
#define IS_RAIN(i) ((*E_grid)[i].E_rainfall>0.)
