/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

//---------------------------------------------------------
// Output functions
//---------------------------------------------------------

#include "stdafx.h"





/*
int SauvegardeFlux ()
{
	int i, j;

	fprintf (FileFlux, "A %.2f", TU(EROS_Marcheur) );
	for (i=j=0; i<nbreT; i++)	if (Noeud[i].IS_BOUNDARY()) // Point sur la condition aux limites
		fprintf (FileFlux, "\t%g", AireCL[j++]);
	fprintf(FileFlux,"\nQ %.2f", TU(EROS_Marcheur) );
	for (i=j=0; i<nbreT; i++)	if (Noeud[i].IS_BOUNDARY()) // Point sur la condition aux limites
		fprintf (FileFlux, "\t%g", FluxCL[j++]);
	fprintf(FileFlux,"\n"); fflush(FileFlux);
	// Remise A Zero
	for (i=0; i<NbreCL; i++) AireCL[i]=FluxCL[i]=0.f;

	return 1;

}
*/
