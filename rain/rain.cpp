/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

#include "stdafx.h"
#include "rain.h"

//----------------------------------------------
// INITIALIZATION FUNCTION
//----------------------------------------------
// Calculate basic parameters of the rainfall map
int class_rain::init(class_grid * grid)
{
	// Initialisation of the grid
	E_grid=grid;
	if (grid==0) return 0;

	// Initialization of the generator
	rand_generator = &E_grid->RANDOM_generator;
	// Initialization of the cell area parameter
	cell_area = E_grid->cell_area;
	
	// Flux scaling function
	if (flux_area>0.) flux_scaling = -1; // The flux area has already been fixed


	// Store the background rain map in the rainfall_ini vector
	rainfall_ini.resize(E_grid->nbreT);
	Qtot=Qarea=0.0;

	for (size_t i=0; i<E_grid->nbreT; i++) 
	{
		type_node & NODE=E_grid->nodes(i);
		
		// Multiply by the "rainfall" factor
		NODE.E_rainfall *= rainfall;
		
		if(NODE.IS_NOT_BOUNDARY()) 
		{
			rainfall_ini[i] = NODE.E_rainfall;
			Qtot += (NODE.E_rainfall*NODE.area);
			Qarea += NODE.area;
		} else {
			rainfall_ini[i] = 0.0;
			NODE.E_rainfall = 0.0;
		}
	}
	// Calculation of the rain map
	if (!init_map()) {
		// In this case: Qtot=0.0;
		Qarea_eff=Qarea;
	}

	cout << "RAIN flux (total water per unit time):" << Qtot << endl;

	scale_parameters();
	return 1;
}
//--------------------------------------------
// Re-init the rain field to its initial value
//--------------------------------------------
double class_rain::reinit()
{
	int size_=rainfall_ini.size();
	if (!size_) return 0;
	double R=0.0;
	#pragma omp parallel reduction(+:R)
	for (int i=0; i<size_;i++) 
	{
		type_node & NODE=E_grid->nodes(i);
		if(NODE.IS_NOT_BOUNDARY()) 
		{
			NODE.E_rainfall=rainfall_ini[i];
			R+=(NODE.E_rainfall*NODE.area);
		}
	}
	return R;
}
//------------------------------------
// Build up the rain map
//------------------------------------
int class_rain::init_map()
{
	// Clear the rain map
	class_grid & GRID = (*E_grid);
	clear();
	double	sum=0.0,
			sum2=0.0,
			sum0=0.0;
	Qarea_eff = 0.0;
	for (size_t i=0; i<E_grid->nbreT; i++) 
	{
		type_node & NODE=E_grid->nodes(i);
		if(NODE.IS_NOT_BOUNDARY()) 
		{
			double r=NODE.E_rainfall*NODE.area;
			if (r>0.)
			{
				Qarea_eff += NODE.area;
				sum += r;			
				sum2+= r*r;
				sum0+= 1.0;
				push_back(make_pair(&GRID[i],sum));
			}
		}
	}
	// Change the rng_pluie, which defines the largest rainfall probability number
	rng_pluie = rand_generator->range_exclude*sum;

	type=RAIN_VARY;
	/*
	// Is the rain field uniform? This option is intended to fasten simulations, but it has still to be checked ...
	sum/=sum0;
	sum2/=sum0;
	if ( (double)fabs(sum2-sum*sum))<CALCUL_EPSILON ) type=RAIN_CSTE;
	*/
	return size();

}
//----------------------------------------------
// INIT scaling parameters
//----------------------------------------------
int class_rain::scale_parameters()
{

	// flux_area is used to normalize the in-and out-fluxes, so that the flux is given as a velocity.	
	switch (flux_scaling) {
	case 0:
		flux_area = 1.;
		break;
	case 1:
		// EROS_flux_area = E_grid->NbreI;
		flux_area = Qarea; 
		break;
	case 2:
		// EROS_flux_area = EROS_Q_Area;
		flux_area = Qarea_eff;
		break;
	default:
		break;
	}

	return 1;
}
//----------------------------------------------
// THE RAIN DROP FUNCTION
// Procedure that launchs  precipiton according to 
// the rain intensity rules
//------------------------------------------------
ptr_node class_rain::RAIN__drop ()
{
	auto Base = begin();
	
	// Note that rng_pluie has to be recalculated if the volume change
	// It can also be calculated as: rng_pluie = range_exclude*RAIN_map.back().second;
	
	// This option is likely fastening the code, but it has still to be checked
#ifdef _RAIN_CST_OPTION_
	if (type==RAIN_CSTE) 
	{
		int b=(int)floor(rand_generator->RAND()*rng_pluie/cell_area);
		Base += b;
		return Base->first;
	}
	else 
#endif
	{
		// Tirage au sort dans une s�rie pond�r�e par rain
		double proba = (rand_generator->RAND()+1.)*rng_pluie;
		for (; Base!=end() && Base->second<proba;Base++);
#if _DEBUG
		if (Base==end()) { 
			cout << "!!"; return 0; 
		}
#endif
		return Base->first;
	}


	//	} while !(*(Base+b)).second->IS_NOT_BOUNDARY();

	return Base->first;
}

