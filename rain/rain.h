/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

#pragma once

//------------------------------------
// Class rain for eros functions
//------------------------------------
class class_rain : public vector <pair<ptr_node,type_height>>
{
private:
	class_grid * E_grid;
	// Initial rain map (used when the rainfall map is modified in orographic functions)
	vector<type_height>	rainfall_ini;		
public:
	//-------------------------------
	// Random variables and functions
	//-------------------------------
	// TODO: initialization of all the below variables
	class_random * rand_generator;

	double 	rng_pluie; 
	double	cell_area; 


public:
	
	int			flux_scaling;
	// O: the flux is not normalized 
	// 1: the flux is normalized by the total surface except boundaried (as it is for the uplift so that the flux is directly comparable to the uplift rate)
	// 2: the flux is normalized by the inflow Q (volume)
	double		flux_area; 	// The normalization factor for the in- and out-fluxes

	double		Qtot;	// The total rainfall flux (rainfall*total_area)
	double		Qarea;	// The rainfall area, on which precipitons can land (i.e. everywhere but boundaries)
	double		Qarea_eff; // The effective rainfall area, on which precipitons are actually landing
	type_RAIN	type;	// Tell if the rain is uniform or not (defined in the Grid project)

	double		rainfall;	// The background rainfall (constant if not associated with a .rain file, or a multiplicative factor for the .rain file)
	double		Qinflow;	// Fix the total inflow if positive (defaut:-1.0)

public:
	class_rain(class_grid * grid=0)
		: E_grid(grid)
	{
		Qtot=0.;
		Qarea=0.;
		type=RAIN_UNDEF;
		init(grid);
	}

	~class_rain(void){}

	// INITIALIZATION FUNCTIONS
	int init(class_grid * grid);
	double reinit();
	int init_map(); 
	int scale_parameters();
	int init_random(); 
	// RAINING function
	ptr_node RAIN__drop();

};

