/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/
// stdafx.h�: fichier Include pour les fichiers Include syst�me standard,
// ou les fichiers Include sp�cifiques aux projets qui sont utilis�s fr�quemment,
// et sont rarement modifi�s
//


#pragma once

#pragma warning (disable : 4800 4267 4996)

#include <stddef.h>

#include <stdlib.h>
#include <stdio.h>
#include <xmmintrin.h>	// Need this for SSE compiler intrinsics

#include <string>
#include <map>
#include <iostream>

using namespace std;

#ifdef _OMP
#include <omp.h>
#endif

#include <Math_Eros/Math_random.h>

#include <grid/GRD.h>
#include <grid/GridOptions.h>

#include <grid/grid.hpp>


#include <fftw3.h>


// TODO: faites r�f�rence ici aux en-t�tes suppl�mentaires n�cessaires au programme
