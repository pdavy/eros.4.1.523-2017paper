# eros.4.1.523-2017paper



## introduction

The source code files are supplementary materials of the article:

Davy, P., Croissant, T., & Lague, D. (2017). A precipiton method to calculate river hydrodynamics, with applications to flood prediction, landscape evolution models, and braiding instabilities. Journal of Geophysical Research: Earth Surface, 122(8), 1491-1512. doi:10.1002/2016JF004156


## Licence

The source code is provided as it is. The copyright is owned by Philippe Davy and the CNRS.
The code can be used for academic project by mentionning the authorship. Collaboration proposal are also welcome.
For commercial projects, please contact us (philippe.davy@univ-rennes1.fr)

## downloading files
cd existing_repo
git remote add origin https://gitlab.com/pdavy/eros.4.1.523-2017paper.git
git branch -M main
git push -uf origin main
