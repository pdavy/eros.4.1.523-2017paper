/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

#include "StdAfx.h"
#include "Grid.hpp"
#include "Node_typology.h"

//----------------------------------------------
// A function that defines how to sort gradients
//----------------------------------------------
const bool is_altitude_greater(type_gradient A, type_gradient B){
	return A.Dh>B.Dh;
}
const bool is_gradient_hydro_greater(type_gradient A, type_gradient B){
	return A.Gradient_hydro>B.Gradient_hydro;
}
const bool is_hydro_greater(type_gradient A, type_gradient B){
	return A.Dw>B.Dw;
}
const bool is_gradient_topo_greater(type_gradient A, type_gradient B){
	return A.Gradient_topo>B.Gradient_topo;
}
const bool is_index(type_gradient A, type_gradient B){
	return A.index()<B.index();
}


//--------------------------
// Class constructor
//--------------------------
Node_typology::Node_typology (class_grid * _grid, ptr_node P, bool use_water)
	: P_node(P), N_grid(_grid)
{ 
	direction=0;
	Sx=Sy=slope_w=slope_t=slope_e=0.;
	if (P) init (P, N_grid, use_water);
}
Node_typology::~Node_typology(void)
{
}
//------------------------------
// Initialize the node gradients
//------------------------------
Node_typology & Node_typology::init(ptr_node P, bool back_flow, bool use_water) 
{ 
	// Note that _grid must have been defined before
	if (P) { 
		// Link the node to a grid cell
		P_node = P;
		clear();
		// make the links according to P
		type_time t=P->date();

		type_link_set & Links=P->Link;
		for(auto lk=Links.begin();lk!=Links.end();lk++) 
			if (back_flow || lk->node_D->is_date(t))
		{
			push_back(type_gradient( &(*lk) ));
		}
		links=size();
	} else
		// if not, we assume that it has been already defined
		P=P_node;

	H = P->E_topo;
#ifdef _WATER_HEIGHT_
	H_water = P->w_depth();
#endif
	for(auto lk=begin();lk!=end();lk++) 
	{
		ptr_node neighbor = lk->link->node_D;
		lk->Dh = H - neighbor->E_topo ;
		lk->Gradient_topo = lk->Dh / lk->link->dl; 

#ifdef _WATER_HEIGHT_
		lk->Dw = lk->Dh;
		lk->Gradient_hydro = lk->Gradient_topo;
		if (use_water) 
		{
			double w= H_water- neighbor->w_depth();
			lk->Dw += w;
			lk->Gradient_hydro += w / lk->link->dl;
		}
#endif
		// link->dl can be replaced by GRID_Dl[i]
		// It may be not necessary to calculate both the hydro and topo gradients
	}
	
	return *this;
}
//---------------------------
// add a spectific component
//---------------------------
// Must be performed just after initialization
Node_typology & Node_typology::add_component(Node_typology & node, double value, bool ordering)
{
	if (node.size()==size() && value>0.0)
	{
		// These two functions are necessary only if the function is not called just after initialization
		if (ordering) 
		{
			indexing();
			node.indexing();
		}

		for(auto lk=begin(),lk_node=node.begin();lk!=end();lk++,lk_node++) 
		{
			lk->Gradient_hydro += (value*lk_node->Gradient_hydro);
		}
	}
	return *this;
}

//-------------------------
// Update the function
// after calculating height
//-------------------------
// update the water depth
Node_typology &  Node_typology::update_gradients_water(double dh_w) 
{
	for(auto lk=begin();lk!=end();lk++) 
	{
		lk->Dw += dh_w;
		lk->Gradient_hydro += dh_w/lk->link->dl;
	}
	H_water+=dh_w;
	return *this;
}
// update the topo
Node_typology &  Node_typology::update_gradients_topo(double dh) 
{
	for(auto lk=begin();lk!=end();lk++) 
	{
		double dh_l=dh/lk->link->dl;
		lk->Dh+=dh;
		lk->Gradient_topo += dh_l;
#ifdef _WATER_HEIGHT_
		lk->Gradient_hydro += dh_l;
#endif
	}
	H+=dh;
	return *this;
}
// Update with a new 
Node_typology &  Node_typology:: update_gradients(ptr_node P) 
{ 
	double dh=P->E_topo-H;
	if (dh!=0.0) update_gradients_topo(dh);
#ifdef _WATER_HEIGHT_
	double dh_w=P->w_depth()-H_water;
	if (dh_w!=0.0) update_gradients_water(dh_w);
#endif
	return * this;
}
//------------------
// Sorting functions
//------------------
Node_typology::iterator Node_typology::sort_topo()
{
	//std::sort(begin(),begin()+links, is_altitude_greater);
	std::sort(begin(),end(), is_altitude_greater);
	return begin();
}
Node_typology::iterator Node_typology::sort_hydro()
{
	//std::sort(begin(),begin()+links, is_gradient_greater);
	std::sort(begin(),end(), is_hydro_greater);
	return begin();
}
Node_typology::iterator Node_typology::sort_gradient_topo()
{
	//std::sort(begin(),begin()+links, is_gradient_greater);
	std::sort(begin(),end(), is_gradient_topo_greater);
	return begin();
}
Node_typology::iterator Node_typology::sort_gradient_hydro()
{
	//std::sort(begin(),begin()+links, is_gradient_greater);
	std::sort(begin(),end(), is_gradient_hydro_greater);
	return begin();
}
Node_typology::iterator Node_typology::indexing()
{
	//std::sort(begin(),begin()+links, is_gradient_greater);
	std::sort(begin(),end(), is_index);
	return begin();
}

//------------------------------------------------
// Calculate the slope from the best fitting plane
//------------------------------------------------
Node_typology::iterator Node_typology::set_slope_directions(bool gradient_hydro)
{
	direction = 0; // Take the largest value
	if (gradient_hydro) return sort_gradient_hydro();
	else return sort_gradient_topo();
}
//------------------------------------------------
// Calculate the slope from the best fitting plane
//------------------------------------------------
Node_typology::iterator Node_typology::set_slope_plane ()
{
	static double Cx=1/(6.0*N_grid->cell_x);
	static double Cy=1/(6.0*N_grid->cell_y);
	// The method calculate the tangential plane from the 8 neighbors

	/*
	for(size_t i=0; i<links;i++) 
	{
		ptr_node P = at[i].link->node_D;
		at[i].Dh = P->E_topo+P->w_depth();
	}*/

	double Sxz,Syz;
	dir_opt=0;

	if (links < 8) return manage_boundary();


	// int V8[8][2] ={{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,-1},{-1,0},{-1,1}};
	Sxz = +(*this)[1].Dh
		  +(*this)[2].Dh
		  +(*this)[3].Dh
		  -(*this)[5].Dh
		  -(*this)[6].Dh
		  -(*this)[7].Dh;
	Syz = +(*this)[0].Dh
		  +(*this)[1].Dh
		  -(*this)[3].Dh
		  -(*this)[4].Dh
		  -(*this)[5].Dh
		  +(*this)[7].Dh;

	Sxz *= Cx;
	Syz *= Cy;
	slope_t = _hypot(Sxz, Syz);
	if (slope_t < DBL_EPSILON ){
		// Keep the same direction 
		Sx=Sy=slope_t=0.;
		return begin()+P_node->index;
	}

	// The direction cosine of the steepest slope
	Sx=Sxz/slope_t;
	Sy=Syz/slope_t;

	return  direction_V8();

}
//-------------------------------------------------------
// Calculate the flow direction from the direction cosine
//-------------------------------------------------------
Node_typology::iterator Node_typology::direction_V8()
{
	static const double max_PI8=cos(M_PI/8.);
	static const double min_PI8=sin(M_PI/8.);

	// Don't both with boundaries. Take the perpendicular direction...
	if (links <8) { direction=dir_opt; return begin()+direction; }

	//direction=0;
	if		(Sy>= max_PI8) direction=0;
	else if (Sy<=-max_PI8) direction=4;
	else if (Sx>= max_PI8) direction=2;
	else if (Sx<=-max_PI8) direction=6;
	else if (Sy>0)direction=(Sx>0)? 1:7;
	else direction=(Sx>0)? 3:5;

	/*
	if (links < 8) 
	{
		int dir=0; for(;dir<links;dir++) if ((*this)[dir].link->index==direction) break;
		if (dir==links) {
			// The direction steepest slope direction is outside
			// Correct by using the direction perpendicular to boundaries towards inside
			direction = dir_opt;
		}
		else direction=dir;
	}
	*/
	
	// Slope remains unchanged
	return begin()+direction;
}
//-------------------------------------------------------
// Modify the flow direction by an arbitrary angle
//-------------------------------------------------------
Node_typology::iterator Node_typology::direction_modify(double angle)
{
	double cs=cos(angle), sn=sin(angle);
	double _Sx=Sx*cs-Sy*sn;
	double _Sy=Sy*cs+Sx*sn;

	slope_t *= (Sx*_Sx+Sy*_Sy);
	Sx=_Sx;
	Sy=_Sy;

	return direction_V8();
}
//--------------------------------
// Dealing with boundaries
//--------------------------------
Node_typology::iterator Node_typology::manage_boundary()
{
	// Actually don't bother too much with boundaries
	// Take the direction perpendicular to them
	direction=dir_opt=P_node->manage_boundary();
	Node_typology::iterator dir_it=begin()+direction;

	slope_t =dir_it->Gradient_topo;
	return dir_it;

	//-----------------------------------------------------------
	// If you want to calculate the actual slope direction cosine
	// take the following expressions
	//------------------------------------------------------------
	static double Cx=1/(6.0*N_grid->cell_x);
	static double Cy=1/(6.0*N_grid->cell_y);
	double Sxz,Syz;

	if ( links == 5)  	// Boundary is  a line defined by 5 points (only a half-plane is absent)
	{
		if (P_node->bc & BC_XMAX) {
			Sxz =  2.* (// + (*this).H 
						+ (*this)[0].Dh 
						+ (*this)[1].Dh
						- (*this)[2].Dh 
						- (*this)[3].Dh 
						- (*this)[4].Dh);
			Syz =  3.* ((*this)[0].Dh - (*this)[1].Dh);
			dir_opt=3;
		} else if (P_node->bc & BC_XMIN) {
			Sxz =  2.* (//- (*this).H
						- (*this)[0].Dh 
						+ (*this)[1].Dh
						+ (*this)[2].Dh 
						+ (*this)[3].Dh 
						- (*this)[4].Dh);
			Syz =  3.* ((*this)[0].Dh - (*this)[4].Dh);
			dir_opt=0;
		} else if (P_node->bc & BC_YMAX) {
			Sxz =  3.* ((*this)[0].Dh - (*this)[4].Dh);
			Syz =  2.* (//+ (*this).H
						+ (*this)[0].Dh 
						- (*this)[1].Dh
						- (*this)[2].Dh 
						- (*this)[3].Dh 
						+ (*this)[4].Dh);
			dir_opt=2;
		} else if (P_node->bc & BC_YMIN) {
			Sxz =  3.* ((*this)[2].Dh - (*this)[3].Dh);
			Syz =  2.* (//- (*this).H
						+ (*this)[0].Dh 
						+ (*this)[1].Dh
						- (*this)[2].Dh 
						- (*this)[3].Dh 
						+ (*this)[4].Dh);
			dir_opt=0;
		}
	}
	else if  (links == 3) // Boundary is  a corner defined by 3 points 
	{
		if (P_node->bc&BC_XMAX && P_node->bc&BC_YMAX)
		{
			Sxz= /*+ 4.*(*this).H*/ + 2*(*this)[0].Dh - 2*(*this)[1].Dh - 4*(*this)[2].Dh;
			Syz= /*+ 4.*(*this).H*/ - 4*(*this)[0].Dh - 2*(*this)[1].Dh + 2*(*this)[2].Dh;
			dir_opt=1;
		} else if (P_node->bc&BC_XMAX && P_node->bc&BC_YMIN)
		{
			Sxz= /*- 4.*(*this).H*/ - 2*(*this)[0].Dh + 4*(*this)[1].Dh + 2*(*this)[2].Dh;
			Syz= /*- 4.*(*this).H*/ + 4*(*this)[0].Dh - 2*(*this)[1].Dh + 2*(*this)[2].Dh;
			dir_opt=2;
		} if (P_node->bc&BC_XMIN && P_node->bc&BC_YMAX)
		{
			Sxz= /*- 4.*(*this).H*/ + 4*(*this)[0].Dh + 2*(*this)[1].Dh - 2*(*this)[2].Dh;
			Syz= /*+ 4.*(*this).H*/ + 2*(*this)[0].Dh - 2*(*this)[1].Dh + 2*(*this)[2].Dh;
			dir_opt=1;
		} if (P_node->bc&BC_XMIN && P_node->bc&BC_YMIN)
		{
			Sxz= /*- 4.*(*this).H*/ - 2*(*this)[0].Dh + 2*(*this)[1].Dh + 4*(*this)[2].Dh;
			Syz= /*- 4.*(*this).H*/ + 4*(*this)[0].Dh + 2*(*this)[1].Dh - 2*(*this)[2].Dh;
			dir_opt=1;
		}
	} else 
	{
		cout << "you are wrong somewhere...."<< endl;
	}

	Sxz *= Cx;
	Syz *= Cy;
	slope_t = _hypot(Sxz, Syz);

	// The direction cosine of the steepest slope
	Sx=Sxz/slope_t;
	Sy=Syz/slope_t;

	return  direction_V8();

}

