/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

#pragma once

//-----------------------------------------------
// type_gradient, defines the precipiton direction 
// (slope, network link, etc.)
//-----------------------------------------------
class type_gradient
{
public:
	type_gradient(ptr_link link=0):link(link){}
	~type_gradient(){}
	// Corresponding link
	ptr_link link;
	// Altitude difference
	double Dh;
	// Topographic gradient
	double Gradient_topo;
	// Occurence probability
	double Proba;
	// (Lateral) erosion term
	double Erosion;

	ptr_node & node() { return link->node_D; }
	int & index() { return link->index; }
	double dl(){return link->dl; }
#ifdef _WATER_HEIGHT_
	// Hydraulic gradient
	double Gradient_hydro;	
	// The water surface 
	double Dw;
#else
#define Gradient_hydro Gradient_topo
#define Dw Dh
#endif
};

typedef type_gradient * ptr_gradient;

//----------------------------------------------
// A function that defines how to sort gradients
//----------------------------------------------
const bool is_gradient_topo_greater(type_gradient A, type_gradient B);
const bool is_altitude_greater(type_gradient A, type_gradient B);
const bool is_gradient_hydro_greater(type_gradient A, type_gradient B);
const bool is_hydro_greater(type_gradient A, type_gradient B);


class Node_typology : public std::vector<type_gradient>
{
private:
	class_grid*	N_grid; // The grid associated on which the typology pattern is calculated
public:
	Node_typology (class_grid * _grid=NULL, ptr_node P=0, bool use_water=true);
	~Node_typology(void);

	int			links; // The number of cells (faster than size())
	ptr_node	P_node; // The "constitutive" P_node 
	double		H;	// The altitude of P_node
	// double		Dhmin, Dhmax; 
#ifdef _WATER_HEIGHT_
	double		H_water; // The flow depth above P_node
#endif
	double		Sx;	// The direction cosine of the plane
	double		Sy; // The direction sine of the plane
#ifdef _WATER_HEIGHT_
	double		slope_w; // the water surface slope - generally the steepest slope but this can be changed
#else
#define slope_w	slope_t
#endif
	double		slope_t; // the topographic surface slope
	double		slope_e; // the erosion slope - generally the water surface slope but this can be changed
	
	int			direction; // the (chosen or calculated ) flow direction in X8 coordinates. It can be steepest slope or not
	int			dir_opt; // the optimum direction for nodes at the boundaries (taken perpendicular)

	void		set_direction(int _dir) { direction=_dir; }
	// type_gradient &	get_direction() { return at(direction); } // The downstream direction
	// ptr_node		ptr_direction() { return at(direction).link->node_D;} // The next downstream cell
	//------------------------------
	// Initialize the node gradients
	//------------------------------
	Node_typology & init(ptr_node P, bool back_flow=false, bool use_water=true);
	Node_typology & add_component(Node_typology & node, double value=1., bool ordering=false);
	Node_typology & grid(class_grid * _grid) {N_grid=_grid; return (*this);}
	//-------------------------
	// Update the function after calculating height
	//-------------------------
	Node_typology & update_gradients_water(double dh); 
	Node_typology & update_gradients_topo(double dh);
	Node_typology & update_gradients (ptr_node P); // a change in the characteristics of P
	type_gradient update (int dir); // change in the direction
	//------------------
	// Sorting functions
	//------------------
	Node_typology::iterator sort_topo(); // Rank the neighbor directions by decreasing topography
	Node_typology::iterator sort_gradient_topo(); // Rank the neighbor directions by decreasing topographic gradients
	Node_typology::iterator sort_gradient_hydro(); // Rank the neighbor directions by decreasing hydraulic gradients
	Node_typology::iterator sort_hydro(); // Rank the neighbor directions by decreasing water surface altitude
	Node_typology::iterator indexing(); // Rank the neigbor directions by increasing index
	//-------------------------------------------
	// Calculate gradients from the fitting plane
	//--------------------------------------------
	Node_typology::iterator set_slope_directions(bool gradient_hydro=true);	// Calculate the steepest slope from the neighbor gradients either hydro or topo
	Node_typology::iterator set_slope_plane();		// Calculate the steepest slope from the best-fitting plane 
	Node_typology::iterator manage_boundary();
	Node_typology::iterator direction_V8();
	Node_typology::iterator direction_modify(double angle);

};

