/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/
//-------------------------------------------
// List of options used in the calculation
//-------------------------------------------
#include <list>

//----------------------------------------
// Basic network geometry
//----------------------------------------
#define GEOMETRY	8
//-----------------------------------------------------------------
// Take either the manning or chezy equation for flow
//-----------------------------------------------------------------
#define CHEZY	0
#define MANNING	1
#define LAMINAR	2
// 0: Chezy
// 1: Manning
#define FLOW_EQUATION CHEZY
#if !defined(FLOW_EQUATION)
#define FLOW_EQUATION CHEZY
#endif
//-----------------------------------------------------------------
// Define the method to calculate slope gradients
// local (ensure exact mass balance)
// or from the best-fitting plane around the point (more versatile)
//-----------------------------------------------------------------
#undef _SLOPE_FROM_PLANE_
//#define _SLOPE_FROM_PLANE_
// we strongly suggest not to use this option
//-------------------------------------------------------
// Model without basement (only a uniform sediment layer)
// This option desactivates some part of the codes, 
// which fastens the calculation. But it avoids using
// different properties for basement and sediment layer
//-------------------------------------------------------
//#define _NO_BASEMENT_	
#undef _NO_BASEMENT_	
//-------------------------------------------------------------------
// In further improvements, we aim at calculating on irregular grids
//-------------------------------------------------------------------
#undef _IRREGULAR_GRID_
//-------------------------
// Model with water height
//-------------------------
#define	_WATER_HEIGHT_	1
// 1: the flow depth is calculated from a Manning-style relationship filtered by a kinetic-type relationship
// 2: the flow depth is calculated from a mass balance between in- and out-flow
//#undef _WATER_HEIGHT_
// No water layer

//-------------------------------------
// Tell how much information you want
// to be printed on screen
//-------------------------------------
#undef _VERBOSE_
//-------------------------
// Allow splash anisotropy
//-------------------------
//#define _SPLASH_ANISOTROPY_
#undef _SPLASH_ANISOTROPY_
//---------------------
// Mass balance of cell 
//---------------------
// Either record the average altitude of the grid cell,
//#define (_ALTITUDE_OF_CELL_)
// or record the altitude of the channel part of the grid cell
// #define (_ALTITUDE_OF_CHANNEL_)

//---------------------------------
// Channel width and diagonal links
//---------------------------------
// If defined, it is assumed that the channel cover the whole grid cell
// Thus an increase of the channel length along diagonals entails a decrease of the channel width,
// which is compensated by the probability of moving precipitons.
// This width reduction gives higher q (flow per unit width) values for diagonals, entailing an increase of erosion rates,
// which likely compensates the decrease of upstream flow.
// Even if it is likely the best option, the eventual results is dependent of the erosion law.
// Tests have been performed on the volcano case.
//#undef _DIAGONAL_WIDTH_REDUCTION_	
//#define _DIAGONAL_WIDTH_REDUCTION_	1
// 1: the width reduction is always applied
// 2: the width reduction is not applied when the river width is given by a function

// (discontinued and replaced by the parameter DIAGONAL_WIDTH_REDUCTION)

//---------------------------
// Channel area and cell area
//---------------------------
// If defined, erosion and deposition operate on the channel surface, 
// which can be different from the cell surface if the flow is not widespread (for diagonals for instance).
// This induces an unlikely difference between erosion and other fluxes such as tectonics,
// which operate on the cell surface
// For this reason, we recommand not to use this flag
#undef _FORCE_DEPOSITION_ON_CHANNEL_AREA_

#if _DIAGONAL_WIDTH_REDUCTION_==1
#undef _FORCE_DEPOSITION_ON_CHANNEL_AREA_
#endif

// This option applies when the flow does not spread entirely through cells
// In the one case (_ALTITUDE_OF_CHANNEL_), "Topo" is the channel altitude;
// in the other case (_ALTITUDE_OF_CELL_), it is the cell altitude
#define _ALTITUDE_OF_CHANNEL_
//#undef _ALTITUDE_OF_CHANNEL_

#if !defined(_DIAGONAL_WIDTH_REDUCTION_) || _DIAGONAL_WIDTH_REDUCTION_==1 || !defined(_ALTITUDE_OF_CHANNEL_)
#define _ALTITUDE_OF_CELL_
#else
#undef _ALTITUDE_OF_CELL_
#endif

//----------------
// Lateral erosion
//----------------
#undef _LATERAL_EROSION_

#if defined (_LATERAL_EROSION_)
// Number of neighbors for which lateral erosion applies
// (tested) 1 means that all the neighbors are potential erodable
// (untested) 2 means that only the "vertical" and "horizonal" neighbors can be laterally eroded
#ifndef _LAT_NEIGHBOR_NUMBER_
#define _LAT_NEIGHBOR_NUMBER_	1
#endif

#define _LAT_DIAG_WEIGHTING_1
#endif

//------------------------
// Diffusion coefficient
//-----------------------
// The maximum diffusion coefficient for explicit scheme
// In theory, it could be increased up to 0.99
#define MAX_DIFFUSION_COEFFICIENT	0.5

//-----------------------------
// Some usefule definitions
//------------------------------
typedef __int64 type_time;
typedef __int64 type_d_time;
typedef __int64 type_dist;
//typedef std::deque<type_time>type_date;
//typedef std::list<type_time>type_date;
typedef std::vector<type_time>type_date;
typedef double	type_height;

class type_node;
typedef type_node * ptr_node;


// Water stored in the cell
struct type_water_in_cell {
	double hw;
	double tend;
};

typedef std::list<type_water_in_cell> type_water;