/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

//---------------------------------------------------------
// Tectonic functions
//---------------------------------------------------------

#include "stdafx.h"


//----------------------------------------
// Init uplift
//----------------------------------------

int class_grid::Init_tectonic()
{
	return Init_uplift() + (Init_thrusting()<<1) ;
}
int class_grid::Init_uplift()
{
 	/*-------------------------- uplift ---------------------------*/

	uplift.resize(nbreT);

	if (input_uplift.empty()) {
		for (size_t i=0; i<nbreT; i++) 
		{
			if ( nodes(i).IS_NOT_BOUNDARY() )uplift[i]=1.;
			else uplift[i]=0.;
		}
	} else {
		cout << "Fichier uplift: " << input_uplift << endl;
		GRD grd(input_uplift.c_str());
		if ( grd.size() - nbreT ) {
			cout << "Le fichier " << input_uplift << "n'est pas valide" << endl;
			return 0;
		}

		double Moyenne=0.f;
		for (size_t i=0; i<nbreT; i++) {
			uplift[i]=(type_height) grd[i];
			Moyenne+=grd[i];
		} Moyenne /= (double) nbreT;
		cout << "uplift Moyen: " << Moyenne << endl;
	}
	return 1;
}

int class_grid::Init_thrusting()
{

	// Trust position
	if (0) { // thrusting along X is defined either from a file or a value
		thrusting["X"].resize(nbreT);
	}
	if (0) { // thrusting along Y is defined either from a file or a value
		thrusting["Y"].resize(nbreT);
	}
	if (0) { // thrusting along Z is defined either from a file or a value
		thrusting["Z"].resize(nbreT);
	}
	// Define the thrust line
	thrust.resize(nX);

	int x_border=0, x_border_max = int(nX)-1;
	
	int thrust_width = I(TECTO_thrust_width);
	if (thrust_width>0) {
		x_border=max((nX-thrust_width)/2, 0); // define the model border that is not thrusted
		x_border_max = x_border + thrust_width;
	}

	for (size_t x=0; x<nX; x++) {
		if (x>=(size_t)x_border && x<=(size_t)x_border_max) {
			double offset =  (double)x*sin(TECTO_thrust_angle/180.*M_PI)*cell_x/cell_y;
			thrust[x]=I(TECTO_thrust_position) + (int)offset;
		} else
			thrust[x]=0;
	}

	return 1;
}
//--------------------------------------------------
// uplift procedure
//--------------------------------------------------
// Note the flux takes account of the grid cell area
double class_grid::Uplift (double dt)
{
	double flux=0.;
	int nInitial=TECTO_uplift_where;
	int nFinal=nbreT;


	if (TECTO_uplift_rate>0.)
	{
		double Value = dt * TECTO_uplift_rate;

		for (int i=nInitial; i<nFinal; i++) {
			type_node &node=nodes(i);
			//if (node.IS_NOT_BOUNDARY()) 
			{
				type_height f=(type_height) Value * uplift[i];
				node.change_topo(f);
				flux += f*node.area;
			}
		}
	}

	if (TECTO_thrust_uplift_rate>0)
	{
		double  Value = dt * TECTO_thrust_uplift_rate;
		int		thrust_length=I(TECTO_thrust_length);
		for (size_t x=0; x<nX; x++)
		{
			int ymin=max(0,thrust[x]-thrust_length);
			int ymax=min(thrust[x]+1,(int)nY);
			for (int y=ymin; y<ymax; y++)
			{
				type_node &node=nodes(x,y);
				if (node.IS_NOT_BOUNDARY()) {
					node.change_topo(Value);
					flux+=Value*node.area;
				}
			}
		}
	}


	return flux;
}

//--------------------------------------------------
// Thrusting
//--------------------------------------------------
double class_grid::Thrusting (double dt)
{

	if (TECTO_thrust_rate<=0.0) return 0.; 
	static double time = 0.;
	static double time_last = 0.;
	static int line_ref = 0;

	// Calculate the number of lines that are thrusted (i.e. disappears)
	int dep_=int(time*TECTO_thrust_rate/cell_y) - line_ref;
	
	double volume=0.;

	if (dep_)
	{
		double vol_=0.;
		double dT = time - time_last;

		for (size_t x=0; x<nX; x++) 
		{
			int thrust_=max(thrust[x],0);
			// The lines that disappear
			// CHECK which expression is correct (I have a doubt...)
			// type_node plate=nodes(x,thrust_+1);
			type_node &plate=nodes(x,thrust_+1);
			if ( thrust_+1<int(nY) && plate.IS_NOT_BOUNDARY() )
			{
				for (int y=max(thrust_-dep_+1,0); y<=thrust_; y++) {
					if (TECTO_thrust_sense==0) {
						// The plate is deposited over the other one
						plate.change_topo(nodes(x,y).E_topo-plate.E_topo);
					} else {
						// The plate is subducted
						vol_ += nodes(x,y).E_topo;
					}
				}
			}
			// The lines are shifted
			for (int y=min(thrust_-dep_,int(nY)-1); y>=0; y--) {
				nodes(x,y+dep_).copy_data(nodes(x,y));
			}
			// New lines are created that have the topography of the boundary
			int y2=min(dep_,int(nY));
			for (int y=0; y<y2; y++) {
				// Create a node with the same characteristics than the boundary condition
				nodes(x,y).copy_data(nodes(x,0));
			}

		}
		// Put the volume that diseappars below the rest
		vol_ = vol_/(double)nY;

		for (size_t x=0; x<nX; x++)
		{
			for (size_t y=thrust[x]; y<nY; y++) {
				// Requires a compensation function
				double t=vol_ * Compensation(y-thrust[x]);
				type_node &node=nodes(x,y);
				// Change topo (but not sediment)
				node.set_value(TOPO,t);
				volume+=t;
			} 
		}
		
		line_ref+=dep_;
		time_last = time;
	}

	time += dt;
	
	return volume;
}

//--------------------------------------------------
// Isostatic compensation for thrusting
//--------------------------------------------------
double class_grid::Compensation (int y)
{
	double f=cell_y / TECTO_isostatic_length, x=(f*y-TECTO_isostatic_offset);
	return TECTO_isostatic_factor * f * exp( -x*x );
}
//--------------------------------------------------
// Strike-slip (the procedure is not validated)
//--------------------------------------------------
void class_grid::Strike_slip (double dt)
{
	if (TECTO_strikeslip_rate <= 0.) return;

	int y_ss = J(TECTO_strikeslip_where);
	int y_I = TECTO_strikeslip ==1 ? 0 : y_ss;
	int y_F = TECTO_strikeslip ==1 ? y_ss : nY;

	static double time=0.;
	static int line_ref=0;

	// Problem 1: the procedure is written for an offset of 1 grid mesh (dep_="1)
	// Problem 2: except for periodic conditions, I am not sure that the procedure is correct at the grid ends
	int dep_=(int)(time*TECTO_strikeslip_rate/cell_x) - line_ref;
	if (dep_>0)
	{
		for (size_t y=y_I; y<y_F; y++) 
		{
			for(size_t x=0; x<dep_; x++) nodes(nX-dep_+x,y).copy_data(nodes(x,y));
			for (size_t x=0; x<nX-dep_; x++) nodes(x,y).copy_data(nodes(x+dep_,y));
		}
		line_ref+=dep_;
	}

	time += dt;
}

