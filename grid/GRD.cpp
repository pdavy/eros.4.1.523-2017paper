/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

//-----------------------------------------
// GRD library
// used to read .grd (surfer format) files 
// to input into the EROS program
//-----------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <direct.h>

#include <string>
#include <iostream>
#include <fstream>

#include "GRD.h"

const float GRD::no_data_value=-9999.0f;
//-------------------------------------------
// I/O files
//-------------------------------------------

// Read a GDR file (basic)
size_t  GRD::read (const char *fichier)
{
	if (!fichier || !strlen(fichier)) return 0;

	char CurrentPath[_MAX_PATH];
	_getcwd(CurrentPath, _MAX_PATH);

	char EnteteGRD[4];
	short nXY[2];
	clear();

	FILE * stream;
	errno_t err = fopen_s( &stream, fichier, "rb" );
	if( err !=0 ) {
#ifdef _DEBUG_EROS
		char buf[_MAX_PATH];
		strerror_s(buf,_MAX_PATH, err);
		//std::cout << buf;
		//std::cout << endl << "current path: "<< CurrentPath << endl;
#endif
		return 0;
	}

	if ( fread (EnteteGRD,sizeof(char), 4, stream) -4 ) return 0;
	if ( fread (nXY, sizeof(short),2, stream) -2) return 0;
	if ( fread (x_bound, sizeof(double),2,stream) -2 ) return 0;
	if ( fread (y_bound, sizeof(double),2,stream) -2 ) return 0;
	if ( fread (z_bound, sizeof(double),2,stream) -2 ) return 0;

	if ( strncmp (EnteteGRD,"DSB",3)) return 0;

	nX=(size_t)nXY[0];
	nY=(size_t)nXY[1];

	size_t n=nX*nY;
	resize(n);
	if (empty()) return 0;
	
	float * v=&((*this)[0]);
	if ( fread (v,sizeof(float),n, stream) - n) {
		//std::cout << "I/O error";
		clear();
		fclose(stream);
		return 0;
	} 

	fclose (stream);
	_open = (bool)(n>0);

	return n;

	//--------------------------------------------------------
	// to replace by ..
	/*
	#include <fstream>

	std::ifstream infile(fichier,std::ios_base::binary);
	if (!infile) {
		return 0;
	}
	
	infile.read(EnteteGRD, 4*sizeof(char));
	infile >> nXY[0];
	infile >> nXY[1];
	infile >> x_bound[0];
	infile >> x_bound[1];
	infile >> y_bound[0];
	infile >> y_bound[1];
	infile >> z_bound[0];
	infile >> z_bound[1];
 
	if ( strncmp (EnteteGRD,"DSB",3)) return 0;

	nX=(size_t)nXY[0];
	nY=(size_t)nXY[1];

	size_t n=nX*nY;
	resize(n);
	if (empty()) return 0;
	
	float * v=&((*this)[0]);
	infile.read((char*)v,n*sizeof(float));
	if (infile.fail()) {
		//std::cout << "I/O error";
		clear();
		infile.close();
		return 0;
	} 

	infile.close();
	*/
}

size_t GRD::write (const char * fichier) 
{

	if ( empty() ){
		printf("The GRD file is empty\n"); 
		return 0;
	}

	if (size()-nX*nY) {
		printf("The GRD file is not correct\n");
		return 0;
	}


	errno_t err;
	FILE * stream;
	if( (err  = fopen_s( &stream, fichier, "wb" )) !=0 ) {
		printf("I/O error when writing %s\n", fichier); 
		return 0;
	}

	char EnteteGRD[5]="DSBB";
  	short _nX, _nY;
  		
	_nX=(short) nX; 
	_nY=(short) nY;

	fwrite (EnteteGRD,	sizeof(char), 	4, stream);
	fwrite (&_nX, 		sizeof(short),	1, stream);
	fwrite (&_nY , 		sizeof(short),	1, stream);
	fwrite (x_bound,sizeof(double),	2, stream);
	fwrite (y_bound,sizeof(double),	2, stream);
	fwrite (z_bound,sizeof(double),	2, stream);

	float * v=&((*this)[0]);
	fwrite(v,sizeof(float),size(), stream);

	fclose(stream);

	return size();
}

double GRD::average()
{
	if (empty()) return 0.;

	double Average=0.;
	size_t sz=size();
	for (size_t i=0; i<sz; i++) {
		Average += (double)(*this)[i];
	}
	return Average/(double)size();
}

