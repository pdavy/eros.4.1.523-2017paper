/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

//---------------------------------------------------------
// Grid water functions
//---------------------------------------------------------


#include "stdafx.h"
/******************************************/
/******************************************/
/******************************************/
//-------------------------------------------------------
// Hydrodynamic
// Flow depth calculated from the Manning relaxation time
//-------------------------------------------------------
// Based on the integral of the Manning formula with constant slope
// h(i) = (h(to)^-.5+Cz*slope^0.5/(2*Dx)*(t-to))^-2
// When a precipiton run through the case: h(t)=h(t)+DV/A

//------------------------------------------------------------------------
// Calculate the time coefficient that is used for water depth relaxation
// The slope weighthing is the slope divided by the Dx distance to the square
//------------------------------------------------------------------------
// Initialization of the static variables
type_height type_node::coef_min=0.0;
int	type_node::flow_model=CHEZY;
//---------------------------------------
// Calculate the water depth at any time
//---------------------------------------
type_height type_node::w_depth_time(type_height dt)
{

	if (W_depth < CALCUL_EPSILON) return W_depth;

	type_height w;
	switch(flow_model) 
	{

	case CHEZY:
//#if FLOW_EQUATION==CHEZY
		w=1./sqrt(W_depth)+W_coef*dt;
		return 1./w/w;
	case LAMINAR:
//#elif FLOW_EQUATION==LAMINAR		
		w=1./W_depth/W_depth+W_coef*dt;
		return 1./sqrt(w);
	case MANNING:
	default:
//#elif FLOW_EQUATION==MANNING
		return W_depth*pow(1.0+W_coef*pow(W_depth,MANNING_COEF)*dt,-MANNING_COEF_INV);
//#endif
	}

}
//-------------------------------
// Fix the water time coefficient
//-------------------------------
void type_node::coef_time(type_height slope, double dx)
{
	
	switch(flow_model) {

//#if FLOW_EQUATION==MANNING
	case MANNING:
	case CHEZY:
		W_coef=max(sqrt(slope)*W_manning/dx,coef_min);
		return;
	case LAMINAR:
		W_coef=max(slope*W_manning/dx,coef_min);
		return;
	}

//#endif
}
//---------------------------------------
// Calculate the water depth at any time
inline type_height type_node::w_depth()
{
	// In this option, W_depth is directly the river depth
	return W_depth;
	// 2nd method _______________________________ 
	// In this option, W_depth is the inverse of the square root of river depth
	// return W_depth>0 ? 1./W_depth/W_depth:0.;
}
//-----------------------------------------------------------
// Calculate the water depth and keep track of depth and time
type_height type_node::w_depth(type_height t)
{
	water_update(t);
	return w_depth();
}
//---------------------------------------
// Increase water depth of Dh
void type_node::water_increase(double Dh)
{
	// Formally the expression is:
	// W_depth+=Dv/area;
	// We save time by considering that the area is constant
	W_depth += Dh;
	// 2nd method _______________________________ 
	// if (W_depth>0.) W_depth = W_depth/sqrt(1.+W_depth*W_depth*Dh); else W_depth=1./sqrt(Dh);
}
// void type_node::water_increase(type_height t, double Dh) { water_update(t); water_increase(Dh); }

//---------------------------------------
// Limit water depth of Dh
void type_node::water_limit(double Hlimit)
{
	if (W_depth>Hlimit) W_depth=Hlimit;
}
//--------------------------------------------------------------------
// Relax the water depth and record the time when it has been updated
void type_node::water_update(type_height t)
{
	if (t!=W_time)
	{
		double h= w_depth_time(t-W_time);

		W_stock += (W_depth-h);
		W_depth = h;
		// 2nd method _______________________________ 
		// if (W_depth>0.) W_depth += W_coef*(t-W_time);
		W_time=t;
	}
	//cout << ".";
}
//------------------------------------------------------
// Release the water stored
double type_node::water_release()
{
	double S=W_stock;
	W_stock=0.;
	return S;
}
//------------------------------------------------------
// Init the water "time"
void type_node::water_time(type_height t)
{
	W_time=t;
}
//---------------------------------------------
// Set water depth at the precipiton arrival
void type_node::water_update_neighbors(type_height t)
{
	for(auto link=Link.begin();link!=Link.end();link++) {
		link->node_D->water_update(t);
	}
}

// void type_node::water_update_neighbors(type_height t, double Dv) { water_update_neighbors(t); water_update(t); water_increase(Dv); }

//-------------------------
// Calculate flow discharge
//-------------------------
type_height	type_node::qx()
{
//#if FLOW_EQUATION==MANNING
	switch (flow_model) {
	case CHEZY:
		return W_coef*CHEZY_COEF_INV*W_depth*sqrt(W_depth);
	case LAMINAR:
		return W_coef*LAMINAR_COEF_INV*W_depth*W_depth*W_depth;
	default:
	case MANNING:
		return W_coef*MANNING_COEF_INV*pow(W_depth,1+MANNING_COEF);

	}
//#endif
}
//------------------------
// Set the cell as a hole
//------------------------
void type_node::set_hole()
{
	coef_time(0.);
	downstream=0;
}
void type_node::set_lake()
{
	coef_time(0.);
}

//---------------------------------------
// Discharge functions
// Record the passage time of precipitons
//---------------------------------------
bool type_node::update_date(type_time T)
{
	if (T > Date.front())
	{
		Date.pop_back();
		//Ptr->Date.push_front(T);
		Date.insert(Date.begin(),T);

		return true;
	}

	// The precipiton has already passed the cell during its pathway
	return false;
}
//--------------------------------------------------
// class_grid function
// Reset water table
//--------------------------------------------------
int	class_grid::Init_water(double coef)
{
	// Calculate the friction coefficient
	int nbre=size(), i;
		
	switch(type_node::flow_model) {

	case CHEZY:
		#pragma omp parallel for
		for (i=0; i<nbre; i++) at(i).W_manning = coef*at(i).W_manning*CHEZY_COEF;
		break;
	case LAMINAR:
		#pragma omp parallel for
		for (i=0; i<nbre; i++) at(i).W_manning = coef*at(i).W_manning*LAMINAR_COEF;
		break;
	default:
	case MANNING:
		#pragma omp parallel for
		for (i=0; i<nbre; i++) at(i).W_manning = coef/at(i).W_manning*MANNING_COEF;
		break;
	}

	if (!DIAGONAL_WIDTH_REDUCTION)
	{
		#pragma omp parallel for
		for (i=0; i<nbre; i++) at(i).W_manning /= sqrt(at(i).area);
	}

	return 1;
	
}
void class_grid::water_update (double t)
{
	int nbre=size();
	#pragma omp parallel for
	for (int i=0; i<nbre; i++) at(i).water_update(t);

}

void class_grid::water_time (double t)
{
	int nbre=size();
	#pragma omp parallel for
	for (int i=0; i<nbre; i++) at(i).water_time(t);

}
double class_grid::water_stock(int flag)
{
	int nbre=size();
	double sum=0.0;
	#pragma omp parallel for reduction(+: sum)
	for (int i=0; i<nbre; i++) 
	{
		double s;
		switch (flag)
		{
		case 0: s=(at(i).W_depth+at(i).W_stock)*at(i).area; break;
		case 1: s=at(i).W_depth*at(i).area; break;
		case 2: s=at(i).W_stock*at(i).area; break;
		}
		sum += s;
	}
	return sum;

}

