/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

#ifndef _GRD_
#define _GRD_

#include <vector>
using namespace std;

class GRD : public vector<float>
{
private:
	bool _open;

public:
	size_t	nX;
	size_t	nY;
	size_t	nbre;
	double	x_bound[2];
	double	y_bound[2];
	double	z_bound[2];

	const static float no_data_value;

public:
	GRD(size_t nx=0, size_t ny=0, 
		double xlo=0., double xhi=0., double ylo=0., double yhi=0., double zlo=0., double zhi=0.) 
		:nX(nx),nY(ny)
	{
		init(nx*ny,xlo,xhi,ylo,yhi,zlo,zhi);
	}
	GRD(size_t nx, size_t ny, 
		double * x_b, double * y_b, double * z_b)
		:nX(nx),nY(ny)
	{
		init(nx*ny,x_b[0],x_b[1],y_b[0],y_b[1],z_b[0],z_b[1]);
	}
	GRD(const char *fichier)
	{
		init(0);
		if(!read(fichier))clear();
	}

	void clear(){_open=false;vector::clear();}

	void init(size_t n, double xlo=0., double xhi=0., double ylo=0., double yhi=0., double zlo=0., double zhi=0.)
	{
		nbre=n;
		x_bound[0]=xlo;
		x_bound[1]=xhi;
		y_bound[0]=ylo;
		y_bound[1]=yhi;
		z_bound[0]=zlo;
		z_bound[1]=zhi;

		if (!n) {nX=nY=0;}
		else {
			resize(n,0.);
		}
		_open = (bool)(n>0);
	}

	~GRD(){}

	size_t	read (const char *fichier);
	size_t	write(const char * fichier);
	
	float	GetValue(size_t n) { return _open?(*this)[n]:(float)no_data_value; }
	float	GetValue(size_t i, size_t j) { return GetValue(i+j*nX); }

	double	average();
};


#endif
