/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

#pragma once

#include <vector>
#include <map>

// A class for treating the argument of a function
#include <unvar/io_utilities/io_utilities.h>
#include <unvar/io_utilities/arg.h>
#include <unvar/io_utilities/Input_data.h>


using namespace std;

//---------------------
// Bit-wise operation
//---------------------
#define NON(a) (0xFFFFFF^a)
#define OUI(a) (a)

#define FLAG(flag,value)	flag|=value
#define UNFLAG(flag,value)	flag&=NON(value)

//--------------------
// Boundary conditions
//--------------------
#define BC_NULL		(0<<0)
#define BC_BLOC		(1<<0)
#define BC_BOUNDARY	(1<<1)
#define BC_NODATA	(1<<2)
#define BC_XMIN		(1<<3)
#define BC_XMAX		(1<<4)
#define BC_YMAX		(1<<5)
#define BC_YMIN		(1<<6)
#define BC_CORRELAT (1<<7)
#define BC_RECHARGE (1<<8)
#define	BC_PARTICLE	(1<<9)
#define BC_SEALEVEL	(1<<10)
#define BC_SEALAND	(1<<11)


// Additional definitions for compatibility
#define BC_LEFT		BC_XMIN
#define BC_RIGHT	BC_XMAX
#define BC_TOP		BC_YMAX
#define BC_BOTTOM	BC_YMIN

//-----------------------------------------------
// calcul variables
//-----------------------------------------------
// The smallest value manageable in calculations
#define CALCUL_EPSILON  1.e-010
// Could be either 10.*DBL_EPSILON or FLT_EPSILON
#define CALCUL_SMALL	1.e-007
// Could be either 100.*DBL_EPSILON or 10.*FLT_EPSILON
#define CALCUL_LITTLE	1.e-4
// The no data value
#define NODATA	-9999.0

// Manning, Chezy coefficients
#define MANNING_COEF		0.6666666666666666666
#define MANNING_COEF_INV	1.5
#define CHEZY_COEF			0.5
#define CHEZY_COEF_INV		2.0
#define LAMINAR_COEF		2.0
#define LAMINAR_COEF_INV	0.5
//-----------------------------------------------
// type_node, define the grid node
// (area, network links, etc.)
//-----------------------------------------------
struct type_link {
	int index;
#ifdef _SPLASH_ANISOTROPY_
	int X;
	int Y;
#endif
	ptr_node node_D;
	double dl;
	double dl2;
};
typedef type_link * ptr_link;

class type_link_set:public vector<type_link>
{
public:
	type_link_set(){};
	~type_link_set(){};
	inline ptr_node & node(size_t i) { return at(i).node_D; }
};
typedef type_link_set::iterator it_link;

//-----------------------------------------------
// Erosion modes and calcul variables
//-----------------------------------------------
enum type_process:int {FLUVIAL=0, HILLSLOPE, PROCESS_END=HILLSLOPE};

//------------------------
// Grid parameters
//------------------------
enum PARAMETER 
{
	TOPO=0, 
	SEDIMENT, 

	WATER_DEPTH,
	WATER_MANNING,
	WATER_TIME,
	WATER_TIME_COEF,
	WATER_STOCK,
	SEDIMENT_ERODIBILITY, 
	SEDIMENT_THRESHOLD,
	CHANNEL_ERODIBILITY, 
	HILLSLOPE_ERODIBILITY, // Keep this order so that HILLSLOPE_ERODIBILITY=CHANNEL_ERODIBILITY+HILLSLOPE
	CHANNEL_THRESHOLD,
	HILLSLOPE_THRESHOLD, // Keep this order so that HILLSLOPE_THRESHOLD=CHANNEL_THRESHOLD+HILLSLOPE
	DISCHARGE_IN,
	DISCHARGE_OUT,
	RAINFALL,
	SLOPE,
//	UPLIFT,
	PARAMETER1_END,
	HUMIDITY=PARAMETER1_END, 
	HUMIDITY_ACTIVE,
	CHANNEL,
	RIVERSTOCK, 
	LENGTH_DEPOSIT, 
	DISCHARGEX,
	DISCHARGEY,
	SLOPE_P,
	DEFAULTS,
	LANDSLIDE,
	PARAMETER2_END,
	CONFIG=PARAMETER2_END,
	PARAMETER_END
};

static string Parameter_name[] = 
{
	"Topo", 
	"Sediment",
	
	"Water",
	"Manning",
	"Time of flow depth",
	"Time coef of flow depth",
	"Water stock release",

	"sediment erodibility",
	"sediment threshold",
	"channel erodibility",
	"hillslope erodibility",
	"channel threshold",
	"hillslope threshold",
	"q(in)",
	"q(out)",
	"Rainfall",
	"Slope",
	"disch.",
	"active precipitons",
	"river load",
	"sediment transport length",
	"channel network", 
	"discharge",
	"flow direction",
	"slope",
	"defaults",
	"landslide"
};


//-----------------------
// CALCULATION VARIABLES
//-----------------------
#define	E_topo			Var[TOPO]
#define	E_sediment		Var[SEDIMENT]

#define W_depth			Var[WATER_DEPTH]
#define	W_manning		Var[WATER_MANNING]
#define	W_time			Var[WATER_TIME]
#define	W_coef			Var[WATER_TIME_COEF]
#define W_stock			Var[WATER_STOCK]

//------------------------
// CONSTITUTIVE PARAMETERS
//------------------------
// Note: HILLSLOPE_ERODIBILITY=CHANNEL_ERODIBILITY+HILLSLOPE
#define	E_e(a)			Var[CHANNEL_ERODIBILITY+a] 
#define	E_e_sediment	Var[SEDIMENT_ERODIBILITY]
#define E_e_channel		Var[CHANNEL_ERODIBILITY]
#define E_e_hillslope	Var[HILLSLOPE_ERODIBILITY]
// Note: HILLSLOPE_THRESHOLD=CHANNEL_THRESHOLD+HILLSLOPE
#define	E_t(a)			Var[CHANNEL_THRESHOLD+a]
#define	E_t_sediment	Var[SEDIMENT_THRESHOLD]
#define E_t_channel		Var[CHANNEL_THRESHOLD]
#define E_t_hillslope	Var[HILLSLOPE_THRESHOLD]
#define E_discharge_in	Var[DISCHARGE_IN]
#define E_discharge_out	Var[DISCHARGE_OUT]
#define	E_rainfall		Var[RAINFALL]
//#define	T_uplift	Var[UPLIFT]
#define E_slope			Var[SLOPE]

//-------------------
// PRINTING VARIABLES
//-------------------
#ifndef _PARAMETER_MAP_
#define Parameters		Var
#endif

#define	S_humidity		Parameters[HUMIDITY]
#define S_active		Parameters[HUMIDITY_ACTIVE]
#define	S_channel		Parameters[CHANNEL]
#define	S_river_stock	Parameters[RIVERSTOCK]
#define	S_sed_length	Parameters[LENGTH_DEPOSIT]	
#define S_dischargeX	Parameters[DISCHARGEX]
#define S_dischargeY	Parameters[DISCHARGEY]
#define S_slope			Parameters[SLOPE_P]
#define S_default		Parameters[DEFAULTS]


//-------------------------
// Generate grid parameters
//-------------------------
enum TypeGenerator {EXTERNAL_FILE=0, RANDOM, FRACTAL};
//------------------------
// Recording options
//------------------------
#define SAVE_NO			(0<<0)
#define SAVE_TXT		(1<<0)
#define SAVE_INI		(1<<1)
#define SAVE_TOPO		(1<<2)
#define SAVE_SED		(1<<3)
#define SAVE_HUM		(1<<4)
#define SAVE_CHANNEL	(1<<5)
#define SAVE_STOCK		(1<<6)
#define SAVE_DISCHARGE	(1<<7)
#define SAVE_WATER		(1<<8)
#define SAVE_SLOPE		(1<<9)
// Not used yet
#define	SAVE_VEGETATION	(1<<10)
#define SAVE_GROUNDWATER (1<<11)
#define SAVE_PARTICLES	(1<<12)
#define SAVE_LENGTH		(1<<13)
#define SAVE_RAIN		(1<<14)
#define SAVE_LANDSLIDE	(1<<15)
#define SAVE_DOWNWARD	(1<<16)
#define SAVE_DEFAULT	(1<<17)
#define SAVE_INITIALIZE (1<<18)
#define SAVE_DISCHARGE_MANNING (1<<19)

#define	PRINT_NO		(0<<0)
#define PRINT_CALC		(1<<0)
#define	PRINT_SCRN		(1<<1)
#define	PRINT_FILE		(1<<2)
#define	PRINT_AVG		(1<<3)
#define	PRINT_SUM		(1<<4)
#define	PRINT_STD		(1<<5)
#define	PRINT_MIN		(1<<6)
#define	PRINT_MAX		(1<<7)
#define	PRINT_NUM		(1<<8)
#define	PRINT_BOUNDARY	(1<<9)
#define	PRINT_NONZERO	(1<<10)
//--------------------------
// Basic error function
//-------------------------
#define _ERROR_(a,b) (_isnan(a) || !_finite(a) || fabs(a)>b )

//-----------------------------------------------
// Rainfall types
//-----------------------------------------------
enum type_RAIN { RAIN_UNDEF=0, RAIN_CSTE, RAIN_VARY, RAIN_MAY_VARY};
//--------------------------
typedef std::map<PARAMETER,type_height> type_values;
struct type_print {
	int		scrn;
	int		file;
	int		calc;
	double	num;
	double	avg;
	double	sum;
	double	std;
	double	min;
	double	max;
	type_print() {
		file=scrn=PRINT_NO;
		calc=PRINT_NO;
	}
};
//------------------------
// Basic node class
//------------------------
class type_node 
{
public:
	// The table contains the basic properties of a node
#ifdef _PARAMETER_MAP_
	type_height	Var[PARAMETER1_END];
	// The map contains the properties that will be used for saving averages
	type_values	Parameters;
#else
	type_height	Var[PARAMETER2_END];
#endif
	// Other properties of a node
	double		area;
	ptr_node	upstream; // The upstream node
	ptr_node	downstream; // The downstream node
	ptr_node	equivalent; // Indicate the corresponding node when reinjecting precipiton
	int			index;		// Indicate the direction index 
	bool		keep_stock; // Indicate whether the precipiton is reinjected with sediments

	// vector of time passages
	type_date	Date; 
	type_time & date(){return Date.front();}
	bool		is_date(type_time t) { return t==Date.front();}
	// list of precipitons within cell (assuming that the precipitons are staying a certain time in cells)
	//type_water	Water; 

	type_link_set Link; // vector of links starting from this node
	int	bc; // define the boundary conditions and the type of node (inside, on one of the 4th borders (xmin, xmax, ymin, ymax), at one of the 4th corners, ...)

	type_node()	
	{
		upstream=0;
		downstream=0;
		equivalent=0;
		area=-1;
		bc=BC_NULL;
		keep_stock=false;
		index=0;
		for (size_t i=0;i<PARAMETER1_END;i++) Var[i]=0.0;
	}
	~type_node()
	{
		Link.clear();
		Date.clear();
	}

	//---------------------------------------------------------------------
	// Neigboring and boundary conditions
	//---------------------------------------------------------------------
	// Check if the node belongs to the grid
	// Modified 18/02/2013
	inline bool	IS_NOT_BOUNDARY() {return !(bc&BC_BOUNDARY);} // { return bool(S_humidity>=0.);}
	inline bool	IS_BOUNDARY() {return bc&BC_BOUNDARY;} // {return bool(S_humidity<0.);}
	inline void SET_BOUNDARY() { FLAG(bc,BC_BOUNDARY); } // { S_humidity=-1; }
	inline void SET_NOT_BOUNDARY() { UNFLAG(bc,BC_BOUNDARY); } // { S_humidity=-1; }
	inline void SET_NODATA() {FLAG(bc,BC_NODATA);}
	inline bool IS_NODATA(){return bc&BC_NODATA;}
	// check if the point is on a boundary and return the optimal direction

	int manage_boundary()
	{
		if (bc==BC_BLOC) return -1;
		if		(bc&BC_XMAX && bc&BC_YMAX) { return 1; } 
		else if (bc&BC_XMAX && bc&BC_YMIN) { return 2; } 
		else if (bc&BC_XMIN && bc&BC_YMAX) { return 1; } 
		else if (bc&BC_XMIN && bc&BC_YMIN) { return 1; }
		else if	(bc & BC_XMAX) { return 3; }
		else if (bc & BC_XMIN) { return 0; } 
		else if (bc & BC_YMAX) { return 2; } 
		else if (bc & BC_YMIN) { return 0; }
		return -1;
	}
	//-------------------------------------------
	// Copy the node characteristics from another
	//-------------------------------------------
	type_node& copy_data(type_node &source) 
	{
		// Copy the source to the target
		for (size_t i=0; i<PARAMETER1_END; i++) 
		{
			Var[i]=source.Var[i];
			/* Old fashion
			double &v=source.Var[i];

			if (&v==&source.S_humidity){
				// The target is not a boundary condition
				if (Var[i]>=0) {
					// if the source is a boundary condition, initialize the target to 0 
					if (v<0.)Var[i]=0.;
					// If not copy the target
					else Var[i]=v;
				} else ; // if the target is a boundary condition, don't change it
			} else 
				Var[i]=v;
			*/
		}
	
		// Don't copy the boundary condition flag (this is the main reason why having a spectificy copy function)
		Date=source.Date;
		area=source.area;
		return *this;
	}
	//-----------------------------------------------------------------------------
	// A function that modifies the ptr_node parameters (topography, sediment, etc.
	//-----------------------------------------------------------------------------
	void set_value(PARAMETER param, double value, bool rel=true)
	{
		//-----------------
		// Basic tests
		//-----------------
		if (IS_BOUNDARY()) return;

		//-------------------------------
		// Set absolute or relative value 
		//-------------------------------
		double & V = Var[param];
		if (rel) 
			V += value;
		else 
			V=value;
		//-----------------
		// Additional tests
		//-----------------
		// No negative sediment
		if (param==SEDIMENT && V<0.)  V=0.;
	}
	//-----------------------------------------------------------------------------
	// A function that modifies the referential of a node (topo)
	//-----------------------------------------------------------------------------
	void change_topo(double value, bool rel=true)
	{
		double &V=Var[TOPO];
		double dv;

		if (rel)
			dv=value;
		else
			dv=value-V;

		// Change topo
		V+=value;
	}
	
	//-----------------------------------------------------------------------------
	// Hydrodynamic
	// Flow depth calculated from the Manning relaxation time
	//-----------------------------------------------------------------------------
	// Based on the integral of the Manning formula with constant slope
	// h(i) = (h(to)^-.5+Cz*slope^0.5/(2*Dx)*(t-to))^-2
	// When a precipiton run through the case: h(t)=h(t)+DV/A

	// Calculate the water depth at any time
	static int	flow_model;
	static type_height	coef_min;
	type_height	w_depth();
	type_height	w_depth(type_height t);
	void		water_increase(double Dv);
	//void		water_increase(type_height t, double Dv);
	void		water_limit(double Hlimit);
	type_height	w_depth_time(type_height dt);
	void		water_update_neighbors(type_height t); 
	// void		water_update_neighbors(type_height t, double Dv); 
	void		water_update(type_height t);
	void		water_time(type_height t);
	double		water_release();
	void		coef_time(type_height slope, double dx=1.);
	type_height	qx();	// The water discharge divided by Dx calculated with the Manning friction rule
	type_height	F();
	type_height	diffusion(type_height FLOW_coefficient);

	void		set_hole();
	void		set_lake();
	
	//---------------------------------------------------------
	// Updating the time series
	bool		update_date(type_time T);


};

//-----------------------------------------------
// class_grid, define the grid structure
// (nodes, calcul parameters, etc.)
//-----------------------------------------------
class class_grid : public vector<type_node>
{
public:
	//-------------------------
	// Grid geometry parameters
	//-------------------------
	size_t Geometry;
	const static int (*Vp[2])[2];
	const static double *Dx;
	double GRID_Dl[GEOMETRY];
	
	// Fix how does EROS weigtht the diagonals when calculating the Manning time scale (should be removed once fixed)
	int		DIAGONAL_WIDTH_REDUCTION; 
	


	// Constructor
	class_grid(int _geometry=8)
	{	
		Geometry = _geometry;
		DIAGONAL_WIDTH_REDUCTION=0;


		nX=nY=nbreT=NbreI=NbreCL=0;

		output_number=0;
		image_bool=false;
		continue_mode=0;
		add_cells_bool = false;
		pause_bool = false;

		hS.push_back(0.95);
		hS.push_back(0.90);
		hS.push_back(0.80);
		hS.push_back(0.70);
		hS.push_back(0.60);
		hS.push_back(0.50);
		hS.push_back(0.40);
		hS.push_back(0.30);
		hS.push_back(0.20);
		hS.push_back(0.10);
		hS.push_back(0.05);

		qS.push_back(0.10); 
		qS.push_back(0.20); 
		qS.push_back(0.30); 
		qS.push_back(0.40); 
		qS.push_back(0.50); 
		qS.push_back(0.60); 
		qS.push_back(0.70); 
		qS.push_back(0.80); 
		qS.push_back(0.90); 
		qS.push_back(0.95); 
		qS.push_back(0.99);

		uplift.clear();
		thrust.clear();

		nCL.clear();
		AireCL.clear();
		FluxCL.clear();
		recharge_map.clear();
	}
	// Destructor
	~class_grid(){}

	size_t	nX;
	size_t	nY;
	size_t	nbreT;	// Total number of points in the grid
    size_t  nbreN;  // Total number of points except no-data values
	size_t	NbreI;	// Nbre de precipitons par unit� de temps (pas de pluie sur les conditions aux limites
	size_t	NbreCL;	// Nbre de points sur la condition aux limites
	// size_t	nbreQ;	// The number of cell on which it is raining

	// ZZ: we assume that nX is defined when using X, Y and N
	int		X(int i){return i%nX;}//{return nX>0?i%nX:-1;}
	int 	X(ptr_node Ptr){return (Ptr-nodes())%nX;}
	int 	Y(int i){return i/nX;}//{return nX>0?i/nX:-1;}
	int 	Y(ptr_node Ptr){return (Ptr-nodes())/nX;}
	int		N(int x, int y){return x+y*nX;}//{return nX>0?x+y*nX:-1;}
	int		N(ptr_node Ptr){return X(Ptr)+Y(Ptr)*nX;}//{return nX>0?x+y*nX:-1;}
	int		I(double x) {
		int n=(int)floor((x-x_bound[0])/(x_bound[1]-x_bound[0])*(nX-1.)+0.5);
		return clipX(n);
	}
	int		J(double y) {
		int n=(int)floor((y-y_bound[0])/(y_bound[1]-y_bound[0])*(nY-1.)+0.5);
		return clipY(n);
	}
	int		Nij(double x, double y) {return N(I(x),J(y));}

	int		clipX(int i) { return i>=0 ? i<nX ? i : nX-1 : 0; }
	int		clipY(int i) { return i>=0 ? i<nY ? i : nY-1 : 0; }

	double	dist(ptr_node cell_a, ptr_node cell_b) 
	{ 
		return hypot(cell_x*(X(cell_a)-X(cell_b)),cell_y*(Y(cell_a)-Y(cell_b)));
	}

	double	cell_x;
	double	cell_y;
	double	cell_min;	// The smallest distance between two nearest point (useful for diffusion problems)
	double	cell_min_2;	// The smallest distance between two nearest point to the square (useful for diffusion problems)
	bool	sgn_x;
	bool	sgn_y;
	double	cell_area;
	double	scale_z;
	double	area;
	double	x_bound[2];
	double	y_bound[2];
	double	z_bound[2];
	// --------------------
	// Boundary conditions
	// --------------------
	// Fix boundary conditions
	int	boundary; 
	// Boundary conditions at sea level
	int	boundary_sea; 
	// periodic boundary conditions
	int	boundary_periodic;
	// define semiperiodic boundary conditions 
	// means that the first half of a boundary is symmetrically linked to the second half, and respectively
	int boundary_semiperiodic;
	// define a recharge from one place to another
	// with either periodic or correlated conditions
	int	boundary_recycle;
	map<ptr_node,ptr_node>recharge_map;

	//-----------------------
	// The main table
	//-----------------------
	ptr_node nodes(){return &front();}

	//type_node& operator[](int n){return at[n];}
	//type_node& operator()(int n){return at[n];}
	__int64 n(ptr_node P){return P-nodes();}

	type_node& operator()(int x, int y){return at(x+y*nX);}

	type_node& nodes(size_t x, size_t y) {return at(x+y*nX);}
	type_node& nodes(size_t n) {return at(n);}

	// Return the neighbor node
	int		CELL_Neighbor(int P, int i) { return (P+i+Geometry)%Geometry; }
	// Opposite direction (int)
	int		CELL_Opposite(int i)  {return  (i+Geometry/2)%Geometry; }

	//-----------------------------------
	// The recording parameter for images
	//-----------------------------------
	__int64		_SAVE;// Indicate which parameter to save
	void save_init(__int64 value) { _SAVE=value; }
	void save_	(__int64 value) { FLAG(_SAVE,value); }
	void save_not(__int64 value) { UNFLAG(_SAVE,value); }
	bool save_is(__int64 value) { return _SAVE&value; }
	//-------------------------------------
	// The recording parameter for printing
	//-------------------------------------
	std::map<PARAMETER,type_print>	CALCUL;
	//---------------------------------
	// Global parameter print functions
	//---------------------------------
	void print_init();
	void print_init(PARAMETER P, int flag, int type=PRINT_CALC);
	void print_CALCUL();
	void print_header(FILE * file=0);
	void print_values(FILE * file=0);
	double print_value(PARAMETER P, int flag);
	//------------------------------
	// RANDOM GENERATOR
	//------------------------------
	class_random RANDOM_generator;

	///------------------------------
	// INPUT FILES
	//------------------------------
	// A vector of file name containing TOPO, HUMIDITY, WATER_DEPTH, etc.
	vector <string> input_files ;
	// Additional layers 
	string	input_climate;		// Climate function
	string	input_uplift;		// Uplift grid
	string	input_sealevel;		// Sea level time variations

	string	input_dat;
	string	output_file;
	string	output_namefile;
	string	output_ini;

	int		output_number;

	double	TOPO_init; // The mean average altitude
	int		nChannel;

	vector <int> nCL;
	vector<type_height> AireCL;
	vector<type_height> FluxCL;

	//-----------------------
	// TECTONIC parameters
	//-----------------------
	//type_time ComptStep;
	//type_time TECTO_uplift_step;

	// Uplift field (defined over the whole grid)
	vector<type_height> uplift;
	// Thrust line
	vector <int> thrust; 
	// Thrusting field in X, Y and Z
	map<string, vector<type_height>> thrusting;

	double	TECTO_uplift_rate;
	int		TECTO_uplift_where;
	int		TECTO_uplift_step;

	double	TECTO_thrust_rate;
	double	TECTO_thrust_uplift_rate;
	int		TECTO_thrust_sense;
	double	TECTO_thrust_length;
	double	TECTO_thrust_width;
	double	TECTO_thrust_position;
	double	TECTO_thrust_angle;
	double	TECTO_isostatic_factor;
	double	TECTO_isostatic_length;
	double	TECTO_isostatic_offset;

	double	TECTO_strikeslip_where;
	double	TECTO_strikeslip_rate;
	int		TECTO_strikeslip;

	// Tectonics
	void	Strike_slip(double dt);
	double	Uplift (double dt);
	double	Thrusting (double dt);
	double	Compensation (int y);


	//-----------------------
	// SEA LEVEL parameters
	//-----------------------

	// Change the sea level
	// define boundary conditions if any
	double	SEA_level;
	bool	SEA_apply;	// Apply the sea level at predetermined boundaries

	double	Random_Noise;


	int		continue_mode;
	int		continue_number;

	// Decide wether or not you want to work with real or normalized variables
	bool	normalize_bool;
	bool	image_bool;
	bool	add_cells_bool;
	bool	pause_bool;

	vector < int > Points;
	vector <double>	hS;
	vector <double>	qS;

	int		Init_tectonic();
	int		Init_uplift();
	int		Init_thrusting();
	int		Init_topography();
	type_RAIN	Init_rain_boundaries(type_RAIN &var);
	int		Init_links();
	int		Init_property(PARAMETER P, string file_name);
	int		Init_property(PARAMETER P, string file_name, type_height default_value_in, type_height default_value_bc);
	int		Init_property(PARAMETER P, string file_name, type_height default_value_in) {return Init_property(P, file_name,default_value_in,default_value_in); }
	int		Set_value(PARAMETER P, type_height default_value_in, type_height default_value_bc);
	int		Set_value(PARAMETER P, type_height default_value_in){return Set_value(P,default_value_in,default_value_in);}

	int 	Scale_area(PARAMETER P, bool bc=true);
	//int		Init_erodibility();
	//int		Init_vegetation();
	//int		Init_groundwater();
	int		limit_height_above(PARAMETER P, type_height high_level) { return limit_height_above(P, high_level, high_level); }
	int		limit_height_above(PARAMETER P, type_height high_level, type_height replace_by);
	int		limit_height_below(PARAMETER P, type_height high_level) { return limit_height_below(P, high_level, high_level); }
	int		limit_height_below(PARAMETER P, type_height high_level, type_height replace_by);
	
	bool	empty(){return !nbreT;}
	void	free_grid ();
	void	Parameter_reset();
	void	Parameter_init(double default_value=0.0);
	void	Parameter_init(PARAMETER P, double default_value=0.0);
	int		CompleteGrid (int *, int*);


	int		set_boundary_conditions();
	int		test_boundary_conditions();
	void	set_sealevel();

	ptr_node	valid(ptr_node, int boundary=0);

	int		check_out(int x, int y);
	
	int		check_in(size_t x, size_t y);
	int		check_in(size_t i) {return check_in(X(i),Y(i));}
	int		check_in(ptr_node P) {return check_in((size_t)ptrdiff_t(P-nodes()));}

	ptr_node Neighbor (ptr_node P, int & mX, int & mY);
	ptr_node cell_offset(ptr_node P, ptrdiff_t offset);

	void	make_link (ptr_node P, ptr_node Nb, int k, double dX, double dY);
	void	make_link (ptr_node P, int mX, int mY, int k, double dX, double dY);
	void	make_link (ptr_node P, int m, int k, double dX, double dY);

	void	make_links(size_t i, bool straight=false);
	void	make_boundary_links(ptr_node Src, ptr_node Dst);
	void	recharge_offset(ptrdiff_t offset, int boundary=0);

	ptr_node recharge_manage(ptr_node P_current, ptr_node P_start=NULL);

	int		link_nodes();
	void	Grid_modify();

	double	calcul_M0(PARAMETER P, int flag=0);
	double	calcul_M1(PARAMETER P, int flag=0);
	double	calcul_M2(PARAMETER P, int flag=0);
	double	calcul_Min(PARAMETER VAR, int flag=0);
	double	calcul_Max(PARAMETER VAR, int flag=0);

	// void	calcul_result();
	// void	calcul_result_per_block (int partie, int nD, int nF); (discontinued)

	double	GRD_average(const char * Fichier);
	size_t	write(_int64 Save, GRD & grd, string save_file, double coef=1.);
	size_t	write(_int64 Save, string save_file, double coef=1.);
	string	write(ptr_node P, _int64 Save,
					string FichierSauvegarde, 
					string &output_file,
					string numero_Image,
					int nX=-1, int nY=-1,
					double coef=1.);
	string	image_number(bool additional=false);
	size_t 	read(const char * fichier, PARAMETER value, double defaultValue);
	size_t 	read(const char * fichier, PARAMETER value);
	//---------------------------------
	// Water functions
	//--------------------------------
	void	water_update(double t);
	void	water_time(double t);
	double	water_stock(int flag=0);
	int		Init_water(double coef=1.0);
};

//--------------------
// Useful  definitions
//--------------------
#define IS_DIAG(n)		(n%2)
#define IS_NOT_DIAG(n) !(n%2)

//----------------------
// Mathematical constant
//----------------------
#define _NULL 0.
#define SQR2	1.4142135623730950488016887242097
#define SQR_2	0.70710678118654752440084436210485
#ifndef M_PI
#define M_PI 3.14159265359
#endif
//-----------------------
// Mathematical functions
//-----------------------
double angle_q(double qx, double qy);