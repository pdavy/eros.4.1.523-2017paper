/*
 * Project:  EROS
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Fluid and sediment transport on Landscape
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2016 Philippe Davy, CNRS (Centre National de la Recherche Scientifique)
 
    This file is part of the RIVER.lab project
*/

//---------------------------------------------------------
// Grid functions
//---------------------------------------------------------


#include "stdafx.h"

/******************************************/
/************ Neighborhood rules **********/
/******************************************/
#if GEOMETRY==4 

#ifndef DIAGONALE
static const int V4[4][2] ={{0,-1},{-1,0},{0,1},{1,0}};
const int (*class_grid::Vp[2])[2]={V4,V4};
double Dx4[4]={   1.,    1.,   1.,   1.};
const double * class_grid::Dx=Dx4;
#else
int V4X[4][2] ={{+1,+1},{+1,-1},{-1,-1},{-1,+1}};
const int (*class_grid::Vp[2])[2]={V4X,V4X};
double Dx4X[4]={  SQR2,   SQR2,   SQR2,  SQR2};
const double *class_grid::Dx=Dx4X;
#endif

#elif GEOMETRY==6
int V6[2][6][2]={{{0,-1},{-1,0},{-1,1},{0,1},{1,1},{1,0}}, //1: GEOMETRY 6 LIGNES PAIRES
				{{0,-1},{-1,-1},{-1,0},{0,1},{1,0},{1,-1}}}; //1: GEOMETRY 6 LIGNES IMPAIRES
const int (*class_grid::Vp[2])[2]={V6[0],V6[1]};
double Dx6[6]={   1.,    1.,	   1.,   1.,   1.,    1.};
const double * class_grid::Dx=Dx6;

#else  // GEOMETRY==8
int V8[8][2] ={{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,-1},{-1,0},{-1,1}};
const int (*class_grid::Vp[2])[2]={V8,V8};
double Dx8[8]={   1.,   SQR2,    1.,   SQR2,  1.,  SQR2,  1.,  SQR2};
const double *class_grid::Dx=Dx8;

#endif
	

//-------------------------------------------------
// Init functions
//-------------------------------------------------
int class_grid::Init_links()
{
	//Define the connections between link nodes
	cout << setw(25) << "link node connections";
	cout << " : ";

	link_nodes();
	cout << "done! ";

	// Manage boundary conditions
	nCL.resize(nbreT, -1);
	NbreCL=0;
	for (size_t i=0; i<nbreT; i++) if (nodes(i).IS_BOUNDARY()) nCL[i]=NbreCL++;
	cout << NbreCL << " points on boundaries" << endl;

	NbreI=nbreT-NbreCL;

	AireCL.resize(NbreCL);
	FluxCL.resize(NbreCL);

	return 1;

}

void class_grid::free_grid ()
{
	clear();

	FluxCL.clear();
	AireCL.clear();
	nCL.clear();
	uplift.clear();
	thrust.clear();
	//rain.clear();
}
// -----------------------------------
// Set default parameter for all nodes
//------------------------------------

// Initialize the list of extra parameters that will be used to save data
void class_grid::Parameter_init(double default_value)
{
	Parameter_init(HUMIDITY,default_value);
	Parameter_init(HUMIDITY_ACTIVE,default_value);
	if (save_is(SAVE_DEFAULT))	Parameter_init(DEFAULTS,default_value);
	if (save_is(SAVE_DISCHARGE)||save_is(SAVE_DISCHARGE_MANNING)) 
	{ 
		Parameter_init(DISCHARGEX,default_value); 
		Parameter_init(DISCHARGEY,default_value); 
	}
	if (save_is(SAVE_LENGTH)) Parameter_init(LENGTH_DEPOSIT,default_value);
	if (save_is(SAVE_SLOPE))  Parameter_init(SLOPE_P,default_value);
	if (save_is(SAVE_STOCK))  Parameter_init(RIVERSTOCK,default_value);
}
void class_grid::Parameter_init(PARAMETER P, double default_value)
{
	int nbre=size();
	#pragma omp parallel for
	for (int i=0;i<nbre;i++)
		at(i).Parameters[P]=default_value;
}

void class_grid::Parameter_reset()
{
	static bool _first = true;
	int nbre=size();

	Parameter_init(0.0);
	for (size_t i=0; i<NbreCL; i++) AireCL[i]=FluxCL[i]=0.;
	
	_first=false;
}

//--------------------------------------
// Initialization of the topography file
//--------------------------------------
int class_grid::Init_topography ()
{
	// Read the topographic file
	cout << setw(25) << Parameter_name[TOPO];
	cout << " : ";
	cout << "try to read from "  << input_files[TOPO];

	if (!read(input_files[TOPO].c_str(), TOPO)) {
		cout << ": failed !FATAL ERROR!" << endl;
		return 0;
	} 

	sgn_x = (x_bound[1]>=x_bound[0]);
	sgn_y = (y_bound[1]>=y_bound[0]);

	cell_min = cell_y = fabs(y_bound[1]-y_bound[0])/(nY-1.);
	// Considering only 1 point enables building 1D models
	// Note that this is allowed only for the X direction.
	if (nX>1) 
	{
		cell_x = fabs(x_bound[1]-x_bound[0])/(nX-1.);
		cell_min=min(cell_x, cell_y);
	}
	else {
		cell_x = 1.;
		// cell_min is not changed since no diffusion is possible in the x-direction
	}
	cell_min_2=cell_min*cell_min;

	cell_area	= cell_x*cell_y;
	scale_z		= 1.;

	cout << ": success!"
		 << " nx=" << nX
		 << " ny=" << nY 
		 << " dx=" << cell_x 
		 << " dy=" << cell_y 
		 << endl;

	// Define the cell areas (which can be changed in a future version for taking account of non regular grids)
	for (size_t i=0; i<nbreT; i++)
	{
		nodes(i).area=cell_area;
	}
	return 1;
}
//--------------------------------------
// Initialization of the boundary files
//--------------------------------------
type_RAIN class_grid::Init_rain_boundaries(type_RAIN &rain_var)
{

	/*----------------------- Conditions aux limites --------------*/

	// REVOIR EN METTANT LA POSSIBILITE D'UN FICHIER *.pluie QUI CONTIENDRAIT
	// LES INDICATIONS SUR LES CONDITIONS AUX LIMITES

	// Try to read the file ... if any

	// This variables are now defined in the Eros class
	double	area_rain = 0.;
	size_t	nbreQ=0;

	int load = 0;

	if (rain_var==RAIN_VARY || rain_var==RAIN_MAY_VARY) 
	{
		// Read the rainfall file
		cout << setw(25) << Parameter_name[RAINFALL];
		cout << " : ";

		cout << "try to read from "  << input_files[RAINFALL];

		GRD grd(input_files[RAINFALL].c_str());
		if ( grd.empty() || (grd.size() - nbreT) )  
		{
			cout << ": failed - does not exist or match with the topo file " << input_files[0] << endl;
			rain_var=RAIN_CSTE;
		} 
		else
		{
			// fix the boundary conditions
			// rain is (now) one of the grid elements
		
			double volume_rain=0.;
			for (size_t i=0; i<nbreT; i++) 
			{
				if (grd[i]>=0.) 
				{
					if (grd[i]>0.) nbreQ++;
					double r= grd[i];
					nodes(i).E_rainfall = r;
					//nodes(i).S_humidity = 0.;
					volume_rain += r*nodes(i).area;
					area_rain+=nodes(i).area;
					nodes(i).SET_NOT_BOUNDARY();
				} else {
					nodes(i).E_rainfall = 0.;
					nodes(i).SET_BOUNDARY();
				}

			}

			if (volume_rain==0.) 
			{
				//rain.clear();
				nbreQ=nbreT;
				cout << ": failed - no rainfall in the file" << endl;
				return RAIN_UNDEF;
			} 
			cout << ": success! total \"rainy\" points " << area_rain/cell_area  << " | total rain " << volume_rain << endl;
			load = 1;
		} 
	}

	if (rain_var==RAIN_CSTE) 
	{
		// No file ".pluie"
		// Take the .hum file or try to make it form the .alt file
		// If it does not exist, a default .hum file has been built from the generic name
		cout << setw(25) << Parameter_name[HUMIDITY];
		cout << " : ";
		cout << "try to read from "  << input_files[HUMIDITY];

		// Read the file
		GRD grd(input_files[HUMIDITY].c_str());

		if (!grd.empty() && (grd.size() == nbreT )) 
		{
			// fix the boundary conditions from data
			for (size_t i=0; i<nbreT; i++) 
			{
				if (grd[i]<0.) {
					nodes(i).SET_BOUNDARY();
					nodes(i).E_rainfall = 0.;
				} else {
					nodes(i).SET_NOT_BOUNDARY();
					//nodes(i).S_humidity = 0.;
					nodes(i).E_rainfall = 1.0; // will be multiplied by rainfall
					area_rain+=nodes(i).area;
				}
			}
			nbreQ = nbreT;
			load = 1;
			
			cout << ": success! total \"rainy\" points " << area_rain/cell_area << endl;
		}
		else 
		{
			cout << ": failed - does not exist or match with the topo file " << input_files[0] << endl;
		}

	}

	// Fix the rain if no boundary conditions have been defined 
	// Failed if no indication is given to boundary condition
	if (rain_var==RAIN_UNDEF || !load) 
	{
		for (size_t i=0; i<nbreT; i++) 
		{
			if (nodes(i).IS_NOT_BOUNDARY()) 
			{
				nodes(i).E_rainfall = 1.0;
				// area_rain+=nodes(i).area;
				// volum_rain += nodes(i).area;
			} else
				nodes(i).E_rainfall = 0.;
		}
		load=1;
		rain_var=RAIN_CSTE;
	}

	return rain_var;
}
//----------------------------------------------
// Standard boundary conditions
//----------------------------------------------
int class_grid::set_boundary_conditions ()
{
	cout << setw(25) << "predefined boundaries";
	cout << " : ";

	// for (size_t n=0; n<nbreT; n++) nodes(n).S_humidity=0.;

	// Top and bottom boundaries
	if (boundary & BC_YMIN) {
		cout << " YMIN";
		for (size_t x=0; x<nX; x++) nodes(x,0).SET_BOUNDARY();
	}

	if (boundary & BC_YMAX) {
		cout << " YMAX";
		for (size_t x=0; x<nX; x++) nodes(x,nY-1).SET_BOUNDARY();
	}

	// Left and right boundaries
	if (boundary & BC_XMIN) {
		cout << " XMIN";
		for (size_t y=0; y<nY; y++) nodes(0,y).SET_BOUNDARY();
	}
	if (boundary & BC_XMAX) {
		cout << " XMAX";
		for (size_t y=0; y<nY; y++) nodes(nX-1,y).SET_BOUNDARY();
	}

	// Remove bad values
    nbreN=0;
	for (auto P=begin(); P!=end(); P++)
	{
		if (P->IS_NODATA()) 
		{
			P->SET_BOUNDARY();
			type_link_set & Links=P->Link;
			for(auto lk=Links.begin();lk!=Links.end();lk++)
				lk->node_D->SET_BOUNDARY();
		} 
		else
		{
			nbreN++;
		}
	}

	
	// nbreQ=nbreT;
	cout << endl;

	return 1;
}

int class_grid::test_boundary_conditions()
{
	int n=0;
	for (auto P=begin(); P!=end(); P++)
		if (P->IS_BOUNDARY()) n++;
	return n;
}
//---------------------------------------------
// A function that set sea level
//--------------------------------------------
void class_grid::set_sealevel()
{
	cout << setw(25) << "sea level conditions";
	cout << " : ";


	// Top and bottom boundaries
	if (boundary_sea&BC_YMIN) 
	{
		cout << " YMIN";
		for (size_t x=0; x<nX; x++) { nodes(x,0).E_topo=SEA_level; nodes(x,0).bc|=BC_SEALEVEL; }
	}
	if (boundary_sea&BC_YMAX) 
	{
		cout << " YMAX";
		for (size_t x=0; x<nX; x++) { nodes(x,nY-1).E_topo=SEA_level; nodes(x,nY-1).bc|=BC_SEALEVEL; }		
	}
	// Left and right boundaries
	if (boundary_sea&BC_XMIN) 
	{
		cout << " XMIN";
		for (size_t y=0; y<nY; y++) { nodes(0,y).E_topo=SEA_level; nodes(0,y).bc|=BC_SEALEVEL; }		
	}
	if (boundary_sea&BC_XMAX) 
	{
		cout << " XMAX";
		for (size_t y=0; y<nY; y++) { nodes(nX-1,y).E_topo=SEA_level; nodes(nX-1,y).bc|=BC_SEALEVEL; }		
	}
	
	/* No-data condition
	// Not necessary if the boundary conditions are defined for all the neighbors of any nodata point
	for (auto P=begin();P!=end(); P++) 
	{
		if (P->IS_NODATA()) { P->E_topo=SEA_level; P->bc|=BC_SEALEVEL; }		
	}
	*/
	cout << endl;
}
//---------------------------------------
// Initialization of the different layers
//---------------------------------------
int class_grid::Init_property(PARAMETER P, string file_name)
{
	if (file_name.empty()) return -1;
	cout << "try to read from " << file_name;

	GRD grd(file_name.c_str());
	if ( grd.empty() || grd.size()!=nbreT) 
	{
		cout << ": failed! ";
		return 0;
	} 
	
	double Moyenne=0.;
	for (size_t i=0; i<nbreT; i++) {
		nodes(i).Var[P]=grd[i];
		Moyenne+= grd[i];
	} Moyenne /= (double) nbreT;
	
	cout << ": success! average " << Moyenne << endl;

	return nbreT;
}
int	class_grid::Init_property(PARAMETER P, string file_name, type_height default_value_in, type_height default_value_bc)
{
	cout << setw(25) << Parameter_name[P];
	cout << " : ";
	if (Init_property(P,file_name)==nbreT) return 1;
	
	cout << "set to default value " << default_value_in;
	if (default_value_bc!=default_value_in) cout << " or " << default_value_bc << " on boundaries";
	cout << endl;

	// else no file read. Default values used
	return Set_value(P,default_value_in,default_value_bc);
	

}
// -----------------------------------
// Set default value for all nodes
//------------------------------------
int class_grid::Set_value(PARAMETER P, type_height default_value_in, type_height default_value_bc)
{

	// Set default values
	for (auto node=begin();node!=end();node++) 
	{
		if (node->IS_NOT_BOUNDARY())
			node->Var[P]=default_value_in;
		else node->Var[P]=default_value_bc;
	}	
	return 1;
}

// -----------------------------------
// Scale parameter by area
//------------------------------------
int class_grid::Scale_area(PARAMETER P, bool bc)
{
	// Scale by area
	for (auto node=begin();node!=end();node++) 
	{
		if (bc || node->IS_NOT_BOUNDARY()) node->Var[P]*=node->area;
	}
	return 1;

}
//----------------------------------------------
// Some useful functions for initializng layers
//----------------------------------------------
int class_grid::limit_height_above(PARAMETER P, type_height high_level, type_height replace_by)
{
	for (auto node=begin();node!=end();node++)
		if (node->Var[P]>high_level) node->Var[P]=replace_by;
	return 1;
}
int class_grid::limit_height_below(PARAMETER P, type_height high_level, type_height replace_by)
{
	for (auto node=begin();node!=end();node++)
		if (node->Var[P]<high_level) node->Var[P]=replace_by;
	return 1;
}
//---------------------------------------
// Link the nodes and form the final grid
//---------------------------------------
// 1. Test if a point is inside the grid or not
ptr_node class_grid::valid(ptr_node P, int boundary)
{
	if (P<&front()) return NULL;
	if (P>&back()) return NULL;
	if (!boundary || (check_in(P) & boundary) ) return P;
	return NULL;
}
int class_grid::check_out(int x, int y)
{
	int res=BC_NULL;
	if (x	<	0)	res|=BC_XMIN;
	if (x>=(int)nX)	res|=BC_XMAX;
	if (y	<	0)	res|=BC_YMIN;
	if (y>=(int)nY)	res|=BC_YMAX;
	return res;
}
int class_grid::check_in(size_t x, size_t y)
{
	size_t res=BC_BLOC;
	if (x ==	0)	res|=BC_XMIN;
	if (x == nX-1)	res|=BC_XMAX;
	if (y ==	0)	res|=BC_YMIN;
	if (y == nY-1)	res|=BC_YMAX;
	return res;
}	

//-------------------------------------------------------------------
// Calculate the neighbor according to (periodic) boundary conditions
//-------------------------------------------------------------------
ptr_node class_grid::Neighbor (ptr_node P, int & mX, int & mY)
{
	int x=X(P),y=Y(P);

	int _bc;
	_bc=BC_YMIN;if (check_out(x+mX,y+mY) & _bc) {
		if (boundary_semiperiodic & _bc) { mY=0; mX=nX-1-mX-2*x; }
		if (boundary_periodic & _bc) mY+=nY;
		else { mX=mY=0; P->bc|=_bc; }
	}
	_bc=BC_YMAX;if (check_out(x+mX,y+mY) & _bc) {
		if (boundary_semiperiodic & _bc) { mY=0; mX=nX-1-mX-2*x; }
		if (boundary_periodic & _bc) mY-=nY;
		else { mX=mY=0; P->bc|=_bc; }
	}
	_bc=BC_XMIN;if (check_out(x+mX,y+mY) & _bc) {
		if (boundary_semiperiodic & _bc) { mX=0; mY=nY-1-mY-2*y; }
		if (boundary_periodic  &  _bc) mX+=nX;
		else { mX=mY=0; P->bc|=_bc; }
	}
	_bc=BC_XMAX;if (check_out(x+mX,y+mY) & _bc){
		if (boundary_semiperiodic & _bc) { mX=0; mY=nY-1-mY-2*y; }
		if (boundary_periodic & _bc) mX-=nX;
		else { mX=mY=0; P->bc|=_bc; }
	}

	// keep inside
	if (check_out(x+mX,y+mY)) {
		cout << "::error in the neighboring procedure/("<<x<<"+"<<mX<<","<<y<<"+"<<mY<<") is out of the system::" << endl;
		mX=mY=0;
	}


	ptr_node neighbor=&nodes(x+mX,y+mY);
	if (!valid(neighbor)) return NULL;

	if (!mX && !mY) return NULL;
	return neighbor;
}
//------------------------------------
// Make a proper offset of the pointer
// i.e. along the same boundary
//------------------------------------
ptr_node class_grid::cell_offset(ptr_node P, ptrdiff_t offset)
{
	int bc=check_in(P);
	ptr_node O=P+offset;
	if (!valid(O)) return NULL;
	return check_in(O)&bc ? O:NULL;
}
//------------------------------------------------------------
// Create a link between P and its neigbor at (mX,mY) distance
//------------------------------------------------------------
void class_grid::make_link (ptr_node P, ptr_node Nb, int k, double dX, double dY)
{
	if ( !Nb || Nb==P ) return;

	if (!valid(Nb)) return;

	//In case of lateral erosion, the neighbors must be listed in same order.
	//otherwise, it is possible to store non-eligible neighbors
	type_link link;
	link.index = k;
#ifdef _SPLASH_ANISOTROPY_
	link.X = mX;
	link.Y = mY;
#endif
	link.node_D=Nb;
	link.dl=hypot(dX,dY);
	link.dl2=(link.dl)*(link.dl);
	P->Link.push_back(link);
}

void class_grid::make_link (ptr_node P, int mX, int mY, int k, double dX, double dY)
{
	return make_link(P, P+N(mX,mY),k,dX,dY);
}
void class_grid::make_link (ptr_node P, int m, int k, double dX, double dY)
{
	return make_link(P, &nodes(m),k,dX,dY);
}
//------------------------------------------------------------
// Link the node i with all its neighbors
//------------------------------------------------------------
void class_grid::make_links (size_t i, bool straight)
{
	int NxM=nX-1;
	int NyM=nY-1;

	int x=X(i);
	int y=Y(i);
	if (x<0 || y<0) return;

	int _parity_ = (y % 2);

	int nL=0;

	nodes(i).Link.clear();

	if (straight==false) // The default value
	{
		for (size_t k=0; k<Geometry;k++)
		{
			int mX=Vp[_parity_][k][0];
			int mY=Vp[_parity_][k][1];

			// The distance are defined before checking periodic boundary conditions
			double dX=mX*cell_x;
			double dY=mY*cell_y;
			
			// Calculate the neighbor according to (periodic) boundary conditions,
			ptr_node nb = Neighbor(&nodes(i), mX, mY);
			// Test if the cell is valid (some problems happen in the grid corners with periodicity flags)
			if (!valid(nb)) 
				nb=NULL;
			// and make the link
			make_link (&nodes(i), nb, k, dX, dY);
		} /* for k (voisins) */
	} 
	else 
	{
		// This option is not used. It is related to the "IS_RAIN" in the next function
		int bc=nodes(i).bc=check_in(x,y);
		if (bc&BC_YMIN) make_link(&nodes(i), 0, 1,0,0.,cell_y);
		if (bc&BC_YMAX) make_link(&nodes(i), 0,-1,4,0.,cell_y);
		if (bc&BC_XMIN) make_link(&nodes(i), 1, 0,2,cell_x,0.);
		if (bc&BC_XMAX) make_link(&nodes(i),-1, 0,6,cell_x,0.);
	}



}

//-------------------------------------
// Make all links
//-------------------------------------
int class_grid::link_nodes()
{
	// Calculate the standard link length

	for (size_t k=0; k<Geometry; k++) 
	{
		GRID_Dl[k]=hypot(Vp[0][k][0]*cell_x, Vp[0][k][1]*cell_y);
	}

	for (size_t i=0; i<nbreT; i++) 
	{
		nodes(i).bc|=check_in(i);

		// TO VALIDATE
		// Links are calculated on boundaries.
		// if (nodes(i).IS_NOT_BOUNDARY())
		{
			// The point is not on a system boundary
			make_links(i/*, IS_RAIN(i)*/);
		}
		if (nodes(i).IS_BOUNDARY())
		//else 
		{
			// The point is on a system boundary
			int mX=0,mY=0, bc=check_in(i);
			// Only the "recycling" condition is processed
			// A re-injection of precipiton without stock could be defined as it did before
			if ( boundary_recycle&bc || boundary_periodic&bc ) 
			{
#define BC_TEST(a) ( bc&a && (boundary_recycle&a || boundary_periodic&a) )
				if BC_TEST(BC_XMIN) {mX=+(nX-1);mY=0;}
				if BC_TEST(BC_XMAX) {mX=-int(nX-1);mY=0;}
				if BC_TEST(BC_YMIN) {mY=+(nY-1);mX=0;}
				if BC_TEST(BC_YMAX) {mY=-int(nY-1);mX=0;}
				// if ( boundary_periodic & bc) make_link (&nodes(i), mX, mY, -1, 0., 0.);
				
				ptr_node Dst = &nodes(i)+N(mX,mY);
				if ( boundary_recycle&bc ) 
					Dst->bc|=BC_RECHARGE;
				if ( boundary_periodic&bc ) 
					make_boundary_links(&nodes(i), Dst);
			}
			/*
			if (boundary_recycle&bc || reinject_condition&bc)
				make_boundary_links(&nodes(i), mX, mY);
				*/
		}
	} /* for i */

	return 1;
}
//----------------------------------------
// Management of periodicity at boundaries
//----------------------------------------
void class_grid::make_boundary_links (ptr_node Src, ptr_node Dst)
{
	if (Dst == Src || !valid(Dst) || Dst->IS_BOUNDARY() ) return;
	Dst->equivalent = Src;
	recharge_map.insert(make_pair(Src,Dst));
}

void class_grid::recharge_offset(ptrdiff_t offset, int boundary)
{
	for (map<ptr_node,ptr_node>::iterator it=recharge_map.begin(); it!=recharge_map.end(); it++)
	{
		it->second->equivalent = valid(it->first+offset,boundary);
	}
}
//----------------------------------------------------------------------
// Manage periodic "recharge" conditions, for which
// the precipiton is reinjected in the system with its sediment content.
// If the recharge flag is on, the system evolves at constant mass (i.e. slope)
//-----------------------------------------------------------------------
ptr_node class_grid::recharge_manage(ptr_node P_current, ptr_node P_start)
{
	static ptrdiff_t P_offset = 0;
	ptr_node P_cell = P_current;

	if ( !P_current || recharge_map.find(P_current)==recharge_map.end() ) {
		P_current = NULL;
		return NULL;
	}
	// 1. Look for an equivalent cell (periodic conditions) and load it
	P_current = recharge_map[P_current];
	// old but equivalent method
	// if (!P_current ->Link.empty()) P_current = P_current->Link.node(0);

	if (!P_current) return NULL;
	
	// 2. Procedure that is supposed to keep a correlation of initialization cells...
	// not really fixed yet.
	if (boundary_recycle & BC_CORRELAT) 
	{
		ptrdiff_t offset = P_start-P_current;
		
		// offset the precipiton to the starting point
		P_current=cell_offset(P_current,P_offset);
		if (P_current)
			// Change the "recharge map" to center it on EQ_start
			recharge_offset(-P_offset, check_in(P_cell));
		else
			recharge_offset(0);
		//int mX=x((int)P_offset), mY=y((int)P_offset);
		//P_current = Neighbor(EQ_start, mX, mY);
		
		P_offset = offset;
	}

	return P_current;
}

//-------------------------
// Modification of the grid
//-------------------------
void class_grid::Grid_modify()
{
	// Add a random noise
	if (Random_Noise==0.) return;

	if (!RANDOM_generator.init()) RANDOM_generator.init(-1);

	for (size_t i=0; i<nbreT; i++) 
	{
		if (nodes(i).IS_NOT_BOUNDARY()) 
			nodes(i).E_topo+=Random_Noise * (RANDOM_generator.rand_exclude()-0.5);
	}
}

//-------------------------------------------
// I/O files
//-------------------------------------------

// Read a GDR file (basic)

/*---------------- calcul des min, max et average d'un fichier GRD --------*/

double class_grid::GRD_average(const char * fichier)
{

	GRD grd(fichier);
	if (grd.empty()) 
	{
		printf ("le fichier %s n'existe pas\n", fichier);
		return 0.;
	}
	return grd.average();
}


/*----------------------------------------------------------------

	procedure de sauvegarde des images: topographies
	reseau et humidite (nombre depassages).

-----------------------------------------------------------------*/
double angle_q(double qx, double qy)
{
	double teta;
	if (qx>0) {
		return atan(qy/qx);
	} else if (qx<0.) {
		teta=atan(qy/qx);
		if (teta>0) return teta-M_PI;
		else return teta+M_PI;
	} else { // qx=0
		if (qy>0) 
			return M_PI/2.;
		else 
			return -M_PI/2.;
	}
	// in case...
	return 0.0;
}

#define _print_(_name, _value) { \
	string name = save_file + _name; \
	for (size_t i=0; i<grd.nbre; i++) grd[i]=nodes(i).IS_NODATA()?GRD::no_data_value : float(_value); \
	res+=grd.write(name.c_str()); } \

size_t	class_grid::write(_int64 Save, GRD & grd, string save_file, double coef)
{
	size_t res=0;
	// current topography
	if (Save&SAVE_TOPO)		_print_(".alt", nodes(i).E_topo);		
	// current sediment layer
	if (Save&SAVE_SED)		_print_(".sed",nodes(i).E_sediment);	
	// current flow depth
	if (Save&SAVE_WATER)	_print_(".water",nodes(i).w_depth() );
	// current rainfall
	if (Save&SAVE_RAIN)		_print_(".rain", nodes(i).E_rainfall);
	
	// current humidity (per unit time)
	if (Save&SAVE_HUM)		_print_(".hum",nodes(i).S_humidity/coef);
	// mean of discharge	
	if (Save&SAVE_DISCHARGE)_print_(".discharge", nodes(i).S_active>0 ? 
				hypot(nodes(i).S_dischargeX,nodes(i).S_dischargeY)/nodes(i).S_active 
				: GRD::no_data_value); 
	if (Save&SAVE_DOWNWARD)	_print_(".downward", nodes(i).S_active>0 ? 
				angle_q(nodes(i).S_dischargeX,nodes(i).S_dischargeY)
				: GRD::no_data_value);

	// Total volume of sediment per unit time
	if (Save&SAVE_STOCK && coef!=0.) _print_(".stock", nodes(i).S_river_stock/coef);

	// percentage of precipitons flagged as "channel"
	if (Save&SAVE_CHANNEL)	_print_(".res", nodes(i).S_humidity>0 ? nodes(i).S_channel/nodes(i).S_humidity : GRD::no_data_value);
	// mean of transport length
	if (Save&SAVE_LENGTH)	_print_(".length", nodes(i).S_active>0 ? nodes(i).S_sed_length/nodes(i).S_active: GRD::no_data_value); 
	// mean of slope
	if (Save&SAVE_SLOPE)	_print_(".slope",  nodes(i).S_active>0 ? nodes(i).S_slope/nodes(i).S_active : GRD::no_data_value);	
	// total of defaults
	if (Save&SAVE_DEFAULT)	_print_(".default", nodes(i).S_default); 

	return res;
}
size_t	class_grid::write(_int64 Save, string save_file, double coef)
{
	// Record sea level
	z_bound[0]=SEA_level;
	z_bound[1]=calcul_Max(TOPO);

	GRD grd(nX,nY,x_bound,y_bound,z_bound);

	return write(Save,grd,save_file,coef);         

}
string class_grid::write(ptr_node P, __int64 Save,
					string save_name, 
					string &output_namefile,
					string numero_Image,
					int nx, int ny, 
					double coef)
{
	// Default values
	if (nx<0) nx=nX;
	if (ny<0) ny=nY;
	if (coef==0.) coef=1.;
	
	// Record sea level
	z_bound[0]=SEA_level;
	z_bound[1]=print_value(TOPO,PRINT_MAX);

	GRD grd(nx,ny,x_bound,y_bound,z_bound);

	string save_file = save_name + "." + numero_Image;
	cout << "______________________________________________" << endl;
	cout << save_file << " | ";
	cout << "N[" << nx  << "," << ny<<"] " 
		 << "X[" << grd.x_bound[0] << "," << grd.x_bound[1] <<"] "
		 << "Y[" << grd.y_bound[0] << "," << grd.y_bound[1] <<"] "
		 << "Z[" << grd.z_bound[0] << "," << grd.z_bound[1] <<"] "
		 // << "cell[" << cell_x  << "," << cell_y <<"] " 
		 << endl;
	cout << "ŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻ" << endl;
	
	size_t res=write(Save,grd,save_file,coef);
	if (Save & SAVE_TOPO) output_namefile = save_file +".alt";

	return save_file;         
}

//---------------------------------
// Function that numbers images
//--------------------------------
string class_grid::image_number(bool additional)
{
	static int nImage = output_number;
	static char ch='a';

	if (additional) {
		if (output_number-nImage)  ch='a'; 
		else ch++; 
		nImage=output_number;
	}
	string res=convert<string>(output_number);
	if (additional) 
		res += ch;

	return res;		
}

//---------------------------------------------------------------
// Read and/or initialize topographies, network and humidity maps
//---------------------------------------------------------------
size_t class_grid::read(const char * fichier, PARAMETER value, double defaultValue)
{

	GRD grd(fichier);
	if (grd.empty()) 
	{
		// Nothing has been read
		if ( !nbreT ) {
			// I don't know what to do, sir!!
			return 0;
		} 
		
		// Resize the file
		resize(nbreT);

		// Initialize to the default value
		for (size_t i=0; i<nbreT; i++) at(i).Var[value]=defaultValue;
		return 1;
	}

	if ( !nbreT || empty()) 
	{
		// That first time a grid is open
		nX=grd.nX;
		nY=grd.nY;
		nbreT=grd.size();
		// Allocation du tableau de Noeuds
		resize(nbreT);
	}

	if (value==TOPO)
	{
		// The actual pixel dimensions
		for (size_t i=0;i<2;i++){
			x_bound[i]=grd.x_bound[i];
			y_bound[i]=grd.y_bound[i];
			z_bound[i]=grd.z_bound[i];
		}
	}

	for (size_t i=0; i<nbreT; i++) 
	{
		if (grd[i]==GRD::no_data_value) at(i).SET_NODATA();
		at(i).Var[value]=grd[i];
	}

	return grd.size();
}

size_t class_grid::read(const char * fichier, PARAMETER value)
{
	return read(fichier,value,0.);
}

//---------------------------------------------------------
// Perform a parallel calculation of the grid parameter sum
//---------------------------------------------------------
double class_grid::calcul_M0(PARAMETER VAR, int flag)
{
	double sum=0.0;
	bool exclude_boundary=flag&PRINT_BOUNDARY;
	bool exclude_zero=flag&PRINT_NONZERO;
	if (VAR<PARAMETER1_END)
	{
		#pragma omp parallel for reduction(+: sum)
		for (int i=0;i<nbreT;i++) 
		{
			type_node & node=nodes(i);
			if ((!exclude_boundary||node.IS_NOT_BOUNDARY()) && (!exclude_zero||node.S_active>0))  sum+=1.;
		}
	}
	else
	{
		#pragma omp parallel for reduction(+: sum)
		for (int i=0;i<nbreT;i++) 
		{
			if (nodes(i).IS_NOT_BOUNDARY() && nodes(i).S_active>0.0) sum+=1.;
		}
	}
	return sum;
}
double class_grid::calcul_M1(PARAMETER VAR, int flag)
{
	double sum=0.0;
	bool exclude_boundary=flag&PRINT_BOUNDARY;
	bool exclude_zero=flag&PRINT_NONZERO;

	if (VAR<PARAMETER1_END)
	{
		#pragma omp parallel for reduction(+: sum)
		for (int i=0;i<nbreT;i++) 
		{
			type_node & node=nodes(i);
			if ((!exclude_boundary||node.IS_NOT_BOUNDARY()) && (!exclude_zero||node.S_active>0)) 
			{
				sum+=node.Var[VAR];
			}
		}
	} 
	else
	{
		#pragma omp parallel for reduction(+: sum)
		for (int i=0;i<nbreT;i++) 
		{
			type_node & node=nodes(i);
			if (node.IS_NOT_BOUNDARY()) 
			{
				double value; 
				if (VAR==DISCHARGEX)
					value=hypot(node.S_dischargeX,node.S_dischargeY);
				else if (VAR==DISCHARGEY)
					value=angle_q(node.S_dischargeX,node.S_dischargeY);
				else value=node.Parameters[VAR];

				if (VAR!=CHANNEL && node.S_active>0.0) 
					sum+=(value/node.S_active); 
				else if (VAR==CHANNEL && node.S_humidity>0.0) 
					sum+=(value/node.S_humidity);
			}
		}
	}
	return sum;
}

double class_grid::calcul_M2(PARAMETER VAR, int flag)
{
	double sum=0.0;
	bool exclude_boundary=flag&PRINT_BOUNDARY;
	bool exclude_zero=flag&PRINT_NONZERO;

	if (VAR<PARAMETER1_END)
	{
		#pragma omp parallel for reduction(+: sum)
		for (int i=0;i<nbreT;i++)
		{
			type_node & node=nodes(i);
			if ((!exclude_boundary||node.IS_NOT_BOUNDARY()) && (!exclude_zero||node.S_active>0)) 
			{
				double value=node.Var[VAR];
				sum+=value*value;
			}
		}
	}
	else
	{
		#pragma omp parallel for reduction(+: sum)
		for (int i=0;i<nbreT;i++) 
		{
			type_node & node=nodes(i);
			if (node.IS_NOT_BOUNDARY()) 
			{
				double value;
				if (VAR==DISCHARGEX)
					value=hypot(node.S_dischargeX,node.S_dischargeY);
				else if (VAR==DISCHARGEY)
					value=angle_q(node.S_dischargeX,node.S_dischargeY);
				else
					value=node.Parameters[VAR];

				if (node.S_active>0.0) 
				{
					value/=nodes(i).S_active;
					sum+=(value*value);
				}

			}
		}
	}
	return sum;
}
	//---------------------------------------------------------
// Perform a parallel calculation of the grid parameter min
//---------------------------------------------------------
double class_grid::calcul_Min(PARAMETER VAR, int flag)
{
	double shared_value=nodes(0).Var[VAR];
	bool exclude_boundary=flag&PRINT_BOUNDARY;
	bool exclude_zero=flag&PRINT_NONZERO;

	#pragma omp parallel 
	{
		double value = shared_value;
		#pragma omp for nowait
		for(int i=1; i<nbreT;i++)
		{
			type_node & node=nodes(i);
			if ((!exclude_boundary||node.IS_NOT_BOUNDARY()) && (!exclude_zero||node.Var[VAR]!=0.0))
				value = min(node.Var[VAR], value);
		}
		#pragma omp critical 
		{
			shared_value = min(shared_value, value);
		}
	}
	return shared_value;
}
//---------------------------------------------------------
// Perform a parallel calculation of the grid parameter max
//---------------------------------------------------------
double class_grid::calcul_Max(PARAMETER VAR, int flag)
{
	double shared_value=nodes(0).Var[VAR];
	bool exclude_boundary=flag&PRINT_BOUNDARY;
	bool exclude_zero=flag&PRINT_NONZERO;

	#pragma omp parallel 
	{
		double value = shared_value;
		#pragma omp for nowait
		for(int i=1; i<nbreT;i++)
		{
			type_node & node=nodes(i);
			if ((!exclude_boundary||node.IS_NOT_BOUNDARY()) && (!exclude_zero||node.Var[VAR]!=0.0))
				value = max(node.Var[VAR], value);
		}
		#pragma omp critical 
		{
			shared_value = max(shared_value, value);
		}
	}
	return shared_value;
}	

//---------------------------------
// Global parameter print functions
//---------------------------------
void class_grid::print_init() { 
	for (PARAMETER P=TOPO;P<PARAMETER1_END;P=PARAMETER(int(P)+1)) CALCUL[P].scrn=CALCUL[P].file=CALCUL[P].calc=PRINT_NO; 
}
void class_grid::print_init(PARAMETER P, int flag, int what) 
{ 
	type_print & Prt = CALCUL[P];
	if ( what & PRINT_SCRN )	{ FLAG(Prt.scrn,flag); FLAG(Prt.calc,flag); }
	if ( what & PRINT_FILE )	{ FLAG(Prt.file,flag); FLAG(Prt.calc,flag); }
	if ( what & PRINT_CALC )	{ FLAG(Prt.calc,flag); }

	if (Prt.calc & PRINT_STD) FLAG(Prt.calc,PRINT_AVG); 
}
void class_grid::print_CALCUL()
{
	int calc;

	for (PARAMETER P=TOPO;P<PARAMETER1_END;P=PARAMETER(int(P)+1))
	{
		if((calc=CALCUL[P].calc)!=PRINT_NO)
		{
			double M0=calcul_M0(P,calc);
			if (calc & PRINT_AVG ) CALCUL[P].avg=M0>0.?calcul_M1(P,calc)/M0:0.0;
			if (calc & PRINT_SUM ) CALCUL[P].sum=calcul_M1(P,calc);
			if (calc & PRINT_STD ) CALCUL[P].std=M0>0.?sqrt(calcul_M2(P,calc)/M0-CALCUL[P].avg*CALCUL[P].avg):0.0;
			if (calc & PRINT_MIN ) CALCUL[P].min=calcul_Min(P,calc);
			if (calc & PRINT_MAX ) CALCUL[P].max=calcul_Max(P,calc);
			if (calc & PRINT_NUM)  CALCUL[P].num=M0;
		}
	}
}

void class_grid::print_header(FILE * file)
{
	FILE *	f;
	string	fmt;
	string	Name;
	int		flag;

	for (PARAMETER P=TOPO;P<PARAMETER1_END;P=PARAMETER(int(P)+1))
		if(CALCUL.find(P)!=CALCUL.end())  
		{
			if ( file ) 
			{
				f=file; 
				fmt="\t%s"; 
				Name=Parameter_name[P]+"_";
				flag= CALCUL[P].file;
			} else 
			{
				f=stdout; 
				fmt=" %-6s"; 
				Name=Parameter_name[P].substr(0,1)+"_";
				flag=CALCUL[P].scrn;
			}
			if ( flag & PRINT_AVG ) fprintf(f,fmt.c_str(),Parameter_name[P].c_str());
			if ( flag & PRINT_SUM ) fprintf(f,fmt.c_str(),Parameter_name[P].c_str());
			if ( flag & PRINT_STD ) fprintf(f,fmt.c_str(),string(Name+"std").c_str());
			if ( flag & PRINT_MIN ) fprintf(f,fmt.c_str(),string(Name+"min").c_str());
			if ( flag & PRINT_MAX ) fprintf(f,fmt.c_str(),string(Name+"max").c_str());
			if ( flag & PRINT_NUM ) fprintf(f,fmt.c_str(),string(Name+"nbre").c_str());
			
		}
}

void class_grid::print_values(FILE * file)
{
	FILE *	f;
	string	fmt;
	int		flag;

	for (PARAMETER P=TOPO;P<PARAMETER1_END;P=PARAMETER(int(P)+1))
		if(CALCUL.find(P)!=CALCUL.end())  
		{
			if ( file ) 
			{
				f=file; 
				fmt="\t%g"; 
				flag= CALCUL[P].file;
			} else 
			{
				f=stdout; 
				fmt=" %-6.4f"; 
				flag=CALCUL[P].scrn;
			}

			if ( flag & PRINT_AVG) fprintf(f,fmt.c_str(),CALCUL[P].avg);
			if ( flag & PRINT_SUM) fprintf(f,fmt.c_str(),CALCUL[P].sum);
			if ( flag & PRINT_STD) fprintf(f,fmt.c_str(),CALCUL[P].std);
			if ( flag & PRINT_MIN) fprintf(f,fmt.c_str(),CALCUL[P].min);
			if ( flag & PRINT_MAX) fprintf(f,fmt.c_str(),CALCUL[P].max);
			if ( flag & PRINT_NUM) fprintf(f,fmt.c_str(),CALCUL[P].num);
		}
}

double class_grid::print_value(PARAMETER P, int flag)
{
	if (!CALCUL[P].calc & flag )  
	{
		double M0, M1;
		if ( flag & PRINT_AVG ||  
			flag & PRINT_STD ) { M1=calcul_M1(P,flag); M0=calcul_M0(P,flag);CALCUL[P].avg=M0>0.?M1/M0:0.0; }
		if ( flag & PRINT_SUM ) CALCUL[P].sum=calcul_M1(P,flag);
		if ( flag & PRINT_STD ) CALCUL[P].std=M0>0.?sqrt(calcul_M2(P,flag)/M0-CALCUL[P].avg*CALCUL[P].avg):0.0;
		if ( flag & PRINT_MIN ) CALCUL[P].min=calcul_Min(P,flag);
		if ( flag & PRINT_MAX ) CALCUL[P].max=calcul_Max(P,flag);
		if ( flag & PRINT_NUM )	CALCUL[P].num=M0;
	}
	if ( flag & PRINT_AVG) return CALCUL[P].avg;
	if ( flag & PRINT_SUM) return CALCUL[P].sum;
	if ( flag & PRINT_STD) return CALCUL[P].std;
	if ( flag & PRINT_MIN) return CALCUL[P].min;
	if ( flag & PRINT_MAX) return CALCUL[P].max;
	if ( flag & PRINT_NUM) return CALCUL[P].num;
	return 0.0;
}
