//---------------------------------------------------------
//
// Eros by Philippe Davy (UMR CNRS 6118 Geosciences Rennes)
// 
//---------------------------------------------------------

#pragma once

using namespace std;


//----------------------------------------------------
// class_tile, define a zone of the grid structure
// Made to fasten calculation
//-----------------------------------------------------
enum tile_style { tile_square, tile_fit };

class class_tile 
{
private:
	class_grid * grid;
public:
	int im, iM, jm, jM;
	int nelts;
	class_tile(class_grid * _grid, int i1=0, int i2=0, int j1=0, int j2=0) : grid(_grid)
	{
		if (!grid || grid->empty()) return;
		im=min(i1,i2); im=grid->clipX(im);
		iM=max(i1,i2); iM=grid->clipX(iM);
		jm=min(j1,j2); jm=grid->clipY(jm);
		jM=max(j1,j2); jM=grid->clipY(jM);
		nelts = (iM-im)*(jM-jm);
	}
	virtual ~class_tile(){}

	bool is_in(int i, int j) { return (i>=im) && (i<=iM) && (j>=im) && (j<=iM); }
	bool is_in(ptr_node Ptr) { return is_in(grid->X(Ptr), grid->Y(Ptr)); }
};
//----------------------------------------------------
// class_gridtile
//-----------------------------------------------------

class_gridtile : public vector<class_tile>
{

private:
	class_grid * grid;
public:
	int tile_x, tile_y;

	class_gridtile(class_grid * _grid, int nb_tiles=100, tile_style style=tile_square) : grid(_grid)
	{
		if (!grid || grid->empty() || !n) return;
		tile_x = tile_y = 0;
	
		if (style==tile_square)
			tile_x = tile_y = (int)ceil(sqrt((double)grid->nbreT / nb_tiles)); 
		if (style==tile_fit) {
			tile_x =(int)ceil(grid->nX / sqrt((double)nb_tiles));
			tile_y =(int)ceil(grid->nY / sqrt((double)nb_tiles));
		}
	

		for (int i=0; i<grid->nX; i+=tile_x)
			for (int j=0; j<grid->nY; j+=tile_y) push_back(class_tile(grid, i, i+tile_x-1, j, j+tile_y-1)); 
	}

	int		get_tile(ptr_node Ptr) {
		int x=grid->X(Ptr)/tile_x, y=grid->Y(Ptr)/tile_y;
		return x*tile_y + y;
	}



};
