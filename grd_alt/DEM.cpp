// Il s'agit du fichier DLL principal.

#include "stdafx.h"

#include "DEM.h"

//----------- freadUNIX (useful)----------------
size_t freadUNIX (void * ptr, size_t sizeelts, size_t nelts, FILE * file)
{
	char * ch = (char *)ptr, * ch1, *CH;
	size_t i, j, test;
	                            
	CH=(char*)calloc(sizeelts,1);
	if (fread (ptr, sizeelts, nelts, file)-nelts) {
		free(CH);return 0; }
	
	for (i=test=0; i<nelts; i++, ch+=sizeelts) {
		ch1=ch+sizeelts;
		memmove(CH,ch,sizeelts);
		for (j=0; j<sizeelts; j++) *(--ch1)=CH[j];
	}                            
	free (CH);
	return nelts;

}
