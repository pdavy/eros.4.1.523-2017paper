#include "StdAfx.h"
#include "grd_alt.h"

grd_alt::grd_alt(void)
{
}

grd_alt::~grd_alt(void)
{
}

FILE * grd_alt::entete(char * fichier, int west, int east, int south, int north)
{
	short nXY[2];
	double xlohi[2];
	double ylohi[2];
	FILE * file;

	if (!strlen(fichier)) return NULL;
	if ( !(file=fopen(fichier, "rb")) ) 
	{
		std::cerr << "le fichier %s n'existe pas " << fichier << endl; 
		return NULL;
	} 

	if ( Fread (EnteteGRD,sizeof(char), 4, file) -4 ) return NULL;
	if ( Fread (nXY, sizeof(short),2, file) -2) return NULL;
	if ( Fread (xlohi, sizeof(double),2,file) -2 ) return NULL;
	if ( Fread (ylohi, sizeof(double),2,file) -2 ) return NULL;
	if ( Fread (elevBounds, sizeof(double),2,file) -2 ) return NULL;

	if ( strncmp (EnteteGRD,"DSB",3)) {
		fclose(file);
		return NULL;
	}

	if ( !(GEOMETRY = atoi(EnteteGRD+3)) ) GEOMETRY=8;

	cols = (int)nXY[0]; // number of columns (in DEM format)
	rows = (int)nXY[1]; // number of rows (in DEM format)

	if ( (west<0) || (south<0) || (east>=cols) || (north>=rows) ) {
		firstX=0; 
		firstY=0;
		nX=cols;
		nY=rows;
	} else {
		firstX=west; 
		firstY=north;
		nX=min(east-west+1, cols-firstX);
		nY=min(north-south+1, rows-firstY);
	}
	
	Size = nX*nY;
	// the x- axis corresponds to the East-to-West direction
	// the y- axis to the South-to-North direction
	groundCoords[NW].u=groundCoords[SW].u=xlohi[0];
	groundCoords[NE].u=groundCoords[SE].u=xlohi[1];
	groundCoords[SW].v=groundCoords[SE].v=ylohi[0];
	groundCoords[NW].v=groundCoords[NE].v=ylohi[1];

	eastMost  = max(groundCoords[NE].u, groundCoords[SE].u);
    westMost  = min(groundCoords[NW].u, groundCoords[SW].u);
    northMost = max(groundCoords[NE].v, groundCoords[NW].v);
    southMost = min(groundCoords[SW].v, groundCoords[SE].v);

	pixelX = (eastMost-westMost)/cols;
	pixelY = (northMost-southMost)/rows;

	westMost += (firstX*pixelX);
	eastMost = westMost+ (nX-1.)*pixelX;

	southMost += (firstY*pixelY);
	northMost = southMost + (nY-1.)*pixelY;

	return file;

}

bool grd_alt::open(char * fichier, int west, int east, int south, int north)
{
	FILE * file=grd_alt::entete(fichier,west,east,south,north);
	if (!file || !Size) return false;

	data = new float[Size];
	if (!data) return false;


	long offset_before=(long)firstX*sizeof(float);
	long offset_after=(cols-(firstX+nX)) *sizeof(float);

	float * p = data;

	// In grd the format is Z[y][x] ..... don't ask me why.....
	fseek(file, (long) firstY*cols*sizeof(float), SEEK_CUR);
	for (int y=0; y<nY;y++) {
		fseek(file, offset_before, SEEK_CUR);
		if ( Fread (p,sizeof(float),nX, file) - nX) {
			std::cerr << "Erreur de Lecture des donnees de " << fichier << endl;

			fclose(file);
			delete[]data;
			return false;
		}
		fseek(file, offset_after, SEEK_CUR);

		p+=nX;
	} 

	fclose (file);

	declare_open();

	return true;
}

bool grd_alt::save(char * fichier)
{
	strcpy(EnteteGRD,"DSBB");
  	short nXs, nYs;
  		
	FILE * file=fopen(fichier, "wb+");

	if ( !file ) return false;

	nXs=(short) nX;
	nYs=(short) nY; 

	fwrite (EnteteGRD,	sizeof(char), 	4, file);
	fwrite (&nXs, 		sizeof(short),	1, file);
	fwrite (&nYs, 		sizeof(short),	1, file);
	fwrite (&westMost, 	sizeof(double),	1, file);
	fwrite (&eastMost, 	sizeof(double),	1, file);
	fwrite (&southMost, sizeof(double),	1, file);
	fwrite (&northMost, sizeof(double),	1, file);
	fwrite (elevBounds, sizeof(double),	1, file);
	fwrite (elevBounds+1,	sizeof(double),	1, file);

	int n_elts=fwrite(data,sizeof(float),(size_t)Size, file);
	
	fclose (file);
	
	return (n_elts==Size);
}
