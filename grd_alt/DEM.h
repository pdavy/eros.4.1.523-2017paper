// DEM.h

#pragma once

#include <string>
using namespace std;

#ifndef DATAFORMAT
#define Fread fread
#else 
#define Fread freadUNIX
#endif

size_t freadUNIX (void * ptr, size_t sizeelts, size_t nelts, FILE * file);

#define ARCSEC_PER_RAD ((360 * 60 * 60) / (2.0 * PI))
#define METERS_PER_ARCSEC (1.0e7 / (90 * 60 * 60))
#define METERS_PER_FEET 0.3048

#define SW     0
#define NW     1
#define NE     2
#define SE     3

typedef struct { double u, v; } UV;

//--------------------------------------
// class definition
//--------------------------------------
template<class T>
class DEM
{
public:
	DEM(void){
		_open=false; 
		data=NULL;
	}
public:
	~DEM(void){	
		if (data) delete[]data;
	}

private:
	bool _open;

public:
	virtual bool open(char * fichier, int west=-1, int east=-1, int south=-1, int north=-1){return true;}
	bool open(string fichier, int west=-1, int east=-1, int south=-1, int north=-1){
		return open(fichier.c_str(),west,east,south,north);
	}

	bool is_open(){return _open;}
	void declare_open(){_open=true;}

	virtual bool save(char * fichier){return true;}
	bool save(string fichier){return save(fichier.c_str());}

	// dimension of the whole grid
	int rows;
	int cols;

	// dimension of the selected grid from [firstX, firstY] with nX row and nY columns
	int firstX, firstY;
	int nX, nY;
	int Size;


	// position of the corners of the whole grid
	UV groundCoords[4];
	double pixelX;
	double pixelY;

	// position of the corners of the selected grid
	double  eastMost, westMost, southMost, northMost;

	// The data. Note that the type of data is dependant on file
	T * data;
};
