#pragma once

#include "dem.h"

class grd_alt : public DEM<float>
{
public:
	grd_alt(void);
public:
	~grd_alt(void);

	FILE * entete(char * fichier, int west=-1, int east=-1, int south=-1, int north=-1);
	bool open(char * fichier, int west=-1, int east=-1, int south=-1, int north=-1);
	bool save(char * fichier);

	char EnteteGRD[4];
	// The min and max of the elevation array
	double elevBounds[2];

	int GEOMETRY;


};
