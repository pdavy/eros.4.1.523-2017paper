/*
 * Project:  io_utilities
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Input/output utility library
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2018 CNRS (Centre National de la Recherche Scientifique), UMR 6118 Geosciences Rennes
 
    This file is part of io_utilities

    io_utilities is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    io_utilities is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with io_utilities.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <float.h>


#ifdef _MSC_VER // Windows
#pragma warning (disable : 4018 4267 4996)
#include <conio.h>
//#include <windows.h>
#include <direct.h>
#include <process.h>
#else // Linux
#include <inttypes.h>
#include <unistd.h>
#define __int64 int64_t
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-result"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wreorder"
#define _chdir	chdir
#define _mkdir	mkdir
#define _getcwd	getcwd
#define _getch	getchar
#define _MAX_PATH   260 // max. length of full pathname
#define _MAX_DRIVE  3   // max. length of drive component
#define _MAX_DIR    256 // max. length of path component
#define _MAX_FNAME  256 // max. length of file name component
#define _MAX_EXT    256 // max. length of extension component
#endif /*_MSC_VER*/


#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <ostream>
#include <fstream>
#include <iostream>
#include <cstdint>
#include <limits>
#include <algorithm>
#include <functional>
#include <iomanip>
#include <cctype>
//#include <experimental/filesystem>

using namespace std;

#define _true_(a) (true || (a) )
#define _false_(a) (false && (a))

#define	PRINT_HEADER	0
#define	PRINT_VALUES	1
#define PRINT_FORCE		2

#define for_vector(i, X) \
	for(size_t i=0, _sz=X.size(); i<_sz; i++)

char * Sscanf (char * buffer, char * result, double * value);
bool	is_absolute_directory(std::string repertoire);
bool	is_file_exist(std::string name);
bool	file_exists (std::string filename);
int		file_lines (std::string filename);
std::vector<std::string> directories(std::string fichier);
int		create_directories(std::string fichier);
std::vector<std::string> list_directory (std::string directory, std::string filter);

char * remove_first_blank(char * str);
char * remove_last_blank(char * str);
char * remove_end_of_line(char * str);
char * remove_end_of_line(char * str, char ch);

std::string		increment(std::string fichier);
std::string		concat_directory (std::string file1, std::string file2, std::string sep=".");
std::string		remove_directory (std::string fichier, char *ch=NULL);
std::string		complete_directory (std::string repertoire, char CH='/');
std::string &	complete_dir (std::string& repertoire, char CH='/');
std::string		directory(std::string fichier);
std::string		extension (std::string fichier);

std::string		remove_extension(std::string fichier);
std::string &	remove_ext(std::string & fichier);

std::string		change_directory (std::string fichier, std::string REPERTOIRE);
std::string &	change_dir (std::string& fichier, std::string REPERTOIRE);

std::string		add_extension (std::string fichier, std::string Ext);
std::string &	add_ext(std::string& fichier, std::string Ext);

std::string		change_extension (std::string fichier, std::string Ext);
std::string &	change_ext (std::string & fichier, std::string Ext);

// Clean a string by removing blanks and indents
std::string		replace_char(std::string fichier, char tobereplaced=' ', char bythis='_');
std::string		replace_blank_and_lower(std::string msg, char ch='_');
std::string		replace_blank(std::string msg, char ch='_');
std::string		remove_first_blank(std::string & fichier);
std::string		remove_last_blank(std::string& str);
std::string		clean_string(std::string& str);
std::string		remove_end_of_line(std::string& str, char ch=' ');

// Lower-case / upper-case functions
std::string		string_lower (const std::string & s);
std::string		string_upper (const std::string & s);
char *			string_lower(char * s);

// Expand a list contained in a file
std::string		expand_File_List (std::vector<std::string> & File_in, std::string ext="list");
std::string		expand_list (std::vector<std::string> & File_in, std::string ext="list");



#define TAB	'\t'
#define CSV ';'

// Read file with different format

class cvs_stream
{
public:
	std::fstream fout;
	int stream_column;
	int stream_line;
	int width;
	int precision;
	char separator;
	std::string filename;
	bool screen;
	std::vector < std::vector<std::string> > ARRAY;
public:

	//---------------------------------------------
	// Define the file, 
	// and set the ARRAY that will contain strings
    //---------------------------------------------
	cvs_stream(	std::string name="", 
				std::ios::openmode mode=std::ios::in|std::ios::out|std::ios::trunc, 
				char _sep=CSV, 
				bool _screen=false, 
				int w_=8, 
				int p_=3);
	//---------------------------------------------
	~cvs_stream();
	//---------------------------------------------
	bool open (std::string name,
		std::ios::openmode mode=std::ios::in|std::ios::out|std::ios::trunc, 
		char _sep=CSV);
	//---------------------------------------------
	// The core of the CVS stream (overloading <<)
	//---------------------------------------------
	template <typename T> cvs_stream & operator << (T arg);
	template <typename T> cvs_stream & operator >> (T & arg);
	//---------------------------------------------
	// Print a series of number
	//---------------------------------------------
	template <typename T> void print (T arg[], int n=1)
	{
		for (int i=0;i<n;i++) *this<<arg[i];
	}
	//---------------------------------------------
	// Read a series of number
	//---------------------------------------------
	template <typename T> size_t read (std::vector<T>&arg);
	//---------------------------------------------
	// Mark the end of a stream_line
	//---------------------------------------------
	void endline(int faire=-1);
	//---------------------------------------------
	// Close the file and reset the ARRAY if rewind
	//---------------------------------------------
	void close(int rewind=0, int faire=-1);
	//---------------------------------------------
	// Close the file and reset the ARRAY
	//---------------------------------------------
	void closeall();
	//---------------------------------------------
	// Test of the file is open
	//---------------------------------------------
	bool is_open();
	void flush();
	bool eof() { return fout.eof(); }
	//---------------------------------------------
	// Print the ARRAY on the screen
	//---------------------------------------------
	void print_screen();
	//---------------------------------------------
	// Print the ARRAY in the file
	//---------------------------------------------
	void print(std::ios::openmode mode=std::ios::trunc, int transpose=0);
	//---------------------------------------------
	// a series of procedure 
	// for defining the ARRAY of stored number
	//---------------------------------------------
	size_t col();
	size_t lin(size_t c=0);
	std::string& operator()(int i, int j);
	void cleararray();
	//---------------------------------------------
	// The echo procedures that treat the 
	// screen echoing:
	// faire=-1 (the default value) --> consider the variable screen defined by echo()
	// faire=0 --> no screen printing
	// faire=1 --> always screen printing
	//---------------------------------------------
	void echo(bool _repeat=true);
	bool _echo(int faire);
	std::string get_name();
	//---------------------------------------------
	// How to print on the screen (same as stl)
	//---------------------------------------------
	void setwidth(int w_);
	void setp(int p_);

};


template <class T> std::string convertInString(const T& arg)
{
    std::ostringstream oss;
    oss << arg; // send the type to the ostringstream
    return oss.str(); // capture the string
}

template <class out_type, class in_type> out_type convert(const in_type & in_value) 
{ 
	std::stringstream stream; 
	stream << in_value; // ins�re une valeur dans le flux 
	out_type result; // stocke le r�sultat de la conversion ici 
	stream >>result; // �crit la valeur dans le r�sultat
	return result; 
} 
template <class out_type, class in_type> bool convert_and_check(const in_type & in_value, out_type & out_value) 
{ 
	std::stringstream stream; 
	stream << in_value; // ins�re une valeur dans le flux 
	out_type result; // stocke le r�sultat de la conversion ici 
	stream >>result; // �crit la valeur dans le r�sultat
	if (!stream.fail()) { out_value=result; return true;}
	return false; 
} 
template <class T> T parse(const std::string & str) 
{ 
	std::stringstream stream(str); 
	T value;
	stream>> value;
	return value;
}

bool what_I_do(char yes='y', char no='n', std::string msg="");
void io_error(std::string message="", bool exit_=false);
std::string format_integer(int i, int nelts);

void increment(double *var,char op,double inc);

void wait_for_key(char &ch);

template <class T> 
void print_V(int write_header, std::string header, T value,  char type='g', int length=0, int dec=-1)
{
	// Print header
	if (write_header==PRINT_HEADER)
	{
		std::string len=length?convert<std::string,int>(abs(length)) : std::string();
		std::string fmt="%-"+len+"s";
		printf(fmt.c_str(), header.c_str());
	}
	else
	{
		std::string len=length?convert<std::string,int>(length) : std::string();
		if (dec>=0) len=len+"."+convert<std::string,int>(dec);
		std::string fmt="%"+len+type;
		printf(fmt.c_str(), value);
	}
	printf("%s"," ");
}

void print_D(int write_header, std::string header, int value, int length=0);
void print_F(int write_header, std::string header, double value, int length=0, int dec=-1);
void print_G(int write_header, std::string header, double value, int length=0, int dec=-1);

bool is_number(const std::string & str);
