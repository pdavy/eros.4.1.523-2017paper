/*
 * Project:  io_utilities
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Input/output utility library
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2018 CNRS (Centre National de la Recherche Scientifique), UMR 6118 Geosciences Rennes
 
    This file is part of io_utilities

    io_utilities is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    io_utilities is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with io_utilities.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"

void dummy_parameter()
{
	int var_int=0;
	double	var_double=0;

	Cparametre<double> D;D.link(var_double);D.link(var_int);
	Cparametre<int> I;I.link(var_double);I.link(var_int);
	Cparametre<short> S;
	Cparametre<long> L;
	Cparametre<long double> LD;
	Cparametre<char> C;
	Cparametre_liste<double>ldouble;
	Cparametre_liste<long double>llongdouble;
	Cparametre_liste<float>lfloat;
	Cparametre_liste<int>lint;
	Cparametre_liste<short>lshort;

}