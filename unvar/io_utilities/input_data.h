/*
 * Project:  io_utilities
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Input/output utility library
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2018 CNRS (Centre National de la Recherche Scientifique), UMR 6118 Geosciences Rennes
 
	This file is part of io_utilities

	io_utilities is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	io_utilities is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with io_utilities.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once

#include "arg.h"

//----------------------------------------------------
// class Cinput
// to defines the input of a program
// either from the command line or from a specific file	
//----------------------------------------------------

enum DATA_type {D_NULL, D_char, D_int, D_double, D_bool, D_string, D_long};

#define in_file	(1<<1)
#define in_arg	(1<<2)
#define	in_text	(1<<3)
#define in_word	(1<<4)
#define in_ext	(1<<5)

enum input_method { INPUT_STRICT=0, INPUT_SUBSET, INPUT_ASK };


class Cinput_data {
public:
	string message;

	string	* data_string;
	float	* data_float;
	double	* data_double;
	int		* data_int;
	bool	* data_bool;
	__int64 * data_long;

	void init(string msg)
	{
		data_string = 0;
		data_double = 0;
		data_float = 0;
		data_int = 0;
		data_bool = 0;
		data_long = 0;
		message = replace_blank_and_lower(msg, '_');
	}

	Cinput_data (string msg, double * data, double default_value, bool init_default=true)
	{
		init(msg);
		data_double=data;
		if (data && init_default) (*data)=default_value;
	}
	Cinput_data(string msg, float * data, float default_value, bool init_default = true)
	{
		init(msg);
		data_float = data;
		if (data && init_default) (*data) = default_value;
	}
	Cinput_data (string msg, string * data, string default_value, bool init_default=true)
	{
		init(msg);
		data_string=data;
		if (data && init_default) (*data)=default_value;
	}
	Cinput_data (string msg, int * data, int default_value, bool init_default=true)
	{
		init(msg);
		data_int=data;
		if (data && init_default) (*data)=default_value;
	}
	Cinput_data (string msg, bool * data, bool default_value, bool init_default=true)
	{
		init(msg);
		data_bool=data;
		if (data && init_default) (*data)=default_value;
	}
	Cinput_data (string msg, __int64 * data, __int64 default_value, bool init_default=true)
	{
		init(msg);
		data_long=data;
		if (data && init_default) (*data)=default_value;
	}

	void * data()
	{
		if (data_string)	return data_string;
		if (data_double)	return data_double;
		if (data_float)		return data_float;
		if (data_int)		return data_int;
		if (data_bool)		return data_bool;
		if (data_long)		return data_long;
		return 0;
	}


	template <class T> bool assign (T value)
	{
		if (data_string)    return convert_and_check<string,T>(value, *data_string);
		if (data_double)	return convert_and_check<double,T>(value, *data_double);
		if (data_float)		return convert_and_check<float, T>(value, *data_float);
		if (data_int)		return convert_and_check<int,T>(value, *data_int);
		if (data_bool)		return convert_and_check<bool,T>(value, *data_bool);
		if (data_long)		return convert_and_check<__int64,T>(value, *data_long);

		return false;
	}
	//---------------------------------------------------------------------------------------------------------------------
	// Obsolete Obsolete Obsolete Obsolete Obsolete Obsolete Obsolete Obsolete Obsolete Obsolete Obsolete Obsolete Obsolete
	// Read parameters from a parameter list
	//---------------------------------------------------------------------------------------------------------------------
	static int read_parameter(vector<Cinput_data> &parameter_list, vector<string> &data, input_method method=INPUT_STRICT)
	{
		//-----------------------------------
		// Read parameters in the vector
		//-----------------------------------
		for (size_t i=0; i<data.size();i++)
		{
			string & line=data[i];
			string value, value_text, comment;

			if (Carg::read_value(line,value_text,value, comment))
			{
				switch (read_parameter(parameter_list,value_text,value,method))
				{
				case 0: 
					//cout << "UNKNOWN [" << setiosflags(ios::left) << setw(50) << value_text << "]  "<< value << endl;
					break;
				case 1:
					cout << "VALID   [" << setiosflags(ios::left) << setw(50) << value_text << "]  " << value << endl;
					break;
				case 2:
					cout << "REINPUT [" << setiosflags(ios::left) << setw(50) << value_text << "]  " << value << endl;
					line += " // the above line replace another one";
					break;
				}
			} // if can read
		}

		return 1;
	}

	//---------------------------------------------------------------------------------------------------------------------
	// Obsolete Obsolete Obsolete Obsolete Obsolete Obsolete Obsolete Obsolete Obsolete Obsolete Obsolete Obsolete Obsolete
	// Read parameters from a parameter list
	//---------------------------------------------------------------------------------------------------------------------
	static int read_parameter (vector<Cinput_data>&parameter_list, string &value_text, string &value, input_method method=INPUT_STRICT)
	{
		static vector<string> data_read;
		// Variable servant � v�rifier que la ligne est trait�e
		int result = 0;
		// Take the value_text
		if (value[0]=='\"') value.erase(value.begin());
		if (value.back()=='\"') value.pop_back();

		// Balayage des valeurs pr�d�finies pour retrouver la cha�ne de 
		// caract�re qui d�crit la variable
		for (size_t i=0; i<parameter_list.size(); i++) if ( parameter_list[i].data() ) 
		{
			Cinput_data & input=parameter_list[i];
			string message=input.message;
			bool _test_=(value_text==message);
			// Another trial...
			if (!_test_ && method) {
				// Try to find a subset of the initial message in the chain value_text
				_test_ = (value_text.find(message)!=value_text.npos);
				if (_test_ && (method==INPUT_ASK) ) {
					cout << "do you accept [" << value_text << "] for [" << message << "] ";
					_test_ = what_I_do ('y','n');
				}
			}

			if (_test_) 
			{
				result=(int)input.assign(value);
				vector<string>::iterator it=find(data_read.begin(),data_read.end(),input.message);
				// if new value/message
				if (it==data_read.end()) 
					data_read.push_back(input.message);
				else  // if not
				{
					(*it)=input.message;
					result = 2;
				}
				// Stop looking for
				break;
			} // if Recherche
		} // for i

		if (!result && (method==INPUT_ASK)) {

			value_text += ": Command line not understood. Exit ? ";
			if ( what_I_do ('y','n',value_text) ) 
			{
				cout << "don't know what to do. Sorry..."<<endl;
				cout << "bye"<<endl;
				exit(-1);
			}
	
		} // if testData

		return result;
	}

};

//---------------------------------
// a class to get data from a file
// and extrapolate values
//---------------------------------

template <class T>
class Cdata : public vector<vector<T>>
{
private:
	ifstream infile;
	map<string, int> header;
public:
	string name;

	Cdata(string _name="")
	{
		if (!_name.empty()) open(_name);
	}
	~Cdata() { infile.close(); }

	// read the file (header and data) check if each line contains the same number of values
	int read(string file_name, bool strict=false)
	{
		if (!open(file_name))
			return 0;
		int n_value = read_header();
		if (!n_value) {
			return 0;
		}

		return read(n_value,strict);
	}
	// open the file
	int open(string _name) {
		infile.open(_name);
		if (!infile.is_open()) {
			cout << "unable to open  " << _name << endl;
			return 0;
		}
		name = _name;
		return 1;
	}
	// read the header
	int read_header(bool _lower = true)
	{
		if (!infile.is_open()) return 0;
		// Try to read header
		string line;
		if (!getline(infile, line)) {
			cout << "fail to read  " << name << endl;
			return 0;
		}

		stringstream stream(line);
		string str;
		bool ascii_ = false;
		for (int n = 0; !stream.fail(); n++){
			stream >> str;
			if (!stream.fail()) {
				if (_lower) string_lower(str);
				header[str] = n;
				if (!is_number(str))ascii_ = true;
			}
			else break;
		}
		int n_value = (int)header.size();
		
		// This is not a header file: rewind
		if (!ascii_)
		{
			infile.clear();
			infile.seekg(0);
			header.clear();
		}
		return n_value;
	}
	// check if the string in the header and return the number
	int check_header(string str, bool _lower=true)
	{
		if (_lower) string_lower(str);
		auto H = header.find(str);
		if (H == header.end()) return -1;
		return H->second;
	}
	// check header for a list 
	int check_header(int n, ...)
	{
		va_list list;
		va_start(list, n);
		for (int i = 0; i < n; i++) {
			int h=check_header(string(va_arg(list, char *)));
			if (h >= 0) return h;
		}
		return -1;
	}
	// read data and check if each line contains n_value
	int read(int n_value, bool strict = false)
	{

		string line;
		int l = 0;
		while (getline(infile, line)) {
			l++;
			stringstream stream(line);
			vector<T> data_vec;
			for (int n = 0; (!n_value || n < n_value); n++) 
			{
				string data;
				stream >> data;
				if (stream.fail()) break;
				T value;
				// Try to convert the data 
				if (convert_and_check<T,string>(data,value)) 
					data_vec.push_back(value);
				else {
					// put a NaN value
					data_vec.push_back(std::numeric_limits<T>::quiet_NaN());
				}
			}

			if (data_vec.size() != header.size()) {
				cout << "error in reading file " << name << endl;
				cout << data_vec.size() << " elements instead of " << n_value << " in line " << l << ": " << line << endl;
				if (strict) { this->clear(); return 0; }
			} else
				this->push_back(data_vec);
			// else break; // If commented, let the possibility to have wrong lines

		}
		return (int)this->size();
	}


	// extrapolate value for the series defined by the header string str for a given time. The time column is 0.
	int extrapolate(T time, string str, T &value)
	{
		int col = check_header(str);
		if (col == -1) return 0;
		return extrapolate(time, col, value);
	}

	// extrapolate value for the series defined by the column col for a given time. The time column is 0.
	int extrapolate(T time, int col, T &value)
	{
		if (col < 0 || col >= this->at(0).size())
			return 0;

		value = this->back()[col]; // the default value if no time is found
		for (auto it = this->begin(); it != this->end(); it++)
		{
			T t = (*it)[0];
			T val = (*it)[col];
			if (fabs(t - time) < FLT_EPSILON) {
				value = val;
				break;
			}
			if (t > time) {
				if (it == this->begin()) {
					value = val;
					break;
				}
				auto prec = *(it - 1);
				value = val + (prec[col] - val)*(t - time) / (t - prec[0]);
				break;
			}
		}
		return 1;
	}
};

template class Cdata<double>;
template class Cdata <int>;
template class Cdata<float>;
template class Cdata<short>;

class input_class : public vector<Cinput_data>
{
public:

	input_class(char _type) { Carg::parser(Carg::init_parser(_type)); }
	input_class(parser_method parse_) { Carg::parser(parse_); }
	input_class() {} // parser value already defined
	~input_class() {}

	// - define Cinput_class as a vector of Cinput_data
	// - put the read_parameter in the Cinput_class
	// Init a parameter list from a vector of string (data) and print it in a file
	int read_parameter(vector<string> &data, ofstream & file_parameter, input_method method = INPUT_STRICT)
	{
		//-----------------------------------
		// Read parameters in the vector
		//-----------------------------------
		string msg_replace(" // the above line replace another one");
		for (auto & line : data)
		{
			//Save results in the .ini file
			if (file_parameter.is_open()) file_parameter << line;

			string value, value_text,comment;
			if (Carg::read_value(line, value_text, value, comment))
			{
				switch (read_parameter(value_text, value, method))
				{
				case 0:
					//cout << "UNKNOWN [" << setiosflags(ios::left) << setw(50) << value_text << "]  "<< value << endl;
					break;
				case 1:
					cout << "VALID   [" << setiosflags(ios::left) << setw(50) << value_text << "]  " << value << endl;
					break;
				case 2:
					cout << "REINPUT [" << setiosflags(ios::left) << setw(50) << value_text << "]  " << value << endl;
					line += msg_replace;
					if (file_parameter.is_open()) file_parameter << endl << msg_replace;
					break;
				}
			} // if can read

		} // for line

		if (file_parameter.is_open()) {
			file_parameter << endl;
			file_parameter.flush();
		}


		return 1;
	}

	// Read parameters from a parameter list
	int read_parameter(string &value_text, string &value, input_method method = INPUT_STRICT)
	{
		static vector<string> data_read;
		// Variable servant � v�rifier que la ligne est trait�e
		int result = 0;
		// Take the value_text
		value_text = replace_blank_and_lower(value_text);
		if (value[0] == '\"') value.erase(value.begin());
		if (value.back() == '\"') value.pop_back();

		// Balayage des valeurs pr�d�finies pour retrouver la cha�ne de 
		// caract�re qui d�crit la variable
		for (auto & input : *this) if (input.data())
		{
			string message = input.message;
			bool _test_ = (value_text == message);
			// Another trial...
			if (!_test_ && method) {
				// Try to find a subset of the initial message in the chain value_text
				_test_ = (value_text.find(message) != value_text.npos);
				if (_test_ && (method == INPUT_ASK)) {
					cout << "do you accept [" << value_text << "] for [" << message << "] ";
					_test_ = what_I_do('y', 'n');
				}
			}

			if (_test_)
			{
				result = (int)input.assign(value);
				auto it = find(data_read.begin(), data_read.end(), input.message);
				// if new value/message
				if (it == data_read.end())
					data_read.push_back(input.message);
				else  // if not
				{
					(*it) = input.message;
					result = 2;
				}
				// Stop looking for
				break;
			} // if Recherche
		} // for input

		if (!result && (method == INPUT_ASK))
		{

			value_text += ": Command line not understood. Exit ? ";
			if (what_I_do('y', 'n', value_text))
			{
				cout << "don't know what to do. Sorry..." << endl;
				cout << "bye" << endl;
				exit(-1);
			}

		} // if testData

		return result;
	}
};


class Cinput
{
public:
	void init()
	{
		type = D_NULL;
	}
	Cinput(int _input, string _msg, double * _variable, double _default = 0.)
		: input(_input), msg(_msg), variable(_variable), default_double(_default)
	{
		init();
		type = D_double;
	}
	Cinput(int _input, string _msg, char * _variable, char _default = 0)
		: input(_input), msg(_msg), variable(_variable), default_char(_default)
	{
		init();
		type = D_char;
	}
	Cinput(int _input, string _msg, int * _variable, int _default = 0)
		: input(_input), msg(_msg), variable(_variable), default_int(_default)
	{
		init();
		type = D_int;
	}
	Cinput(int _input, string _msg, bool * _variable, bool _default = true)
		: input(_input), msg(_msg), default_bool(_default)
	{
		init();
		type = D_bool;
	}
	Cinput(int _input, string _msg, void * _variable, DATA_type _type)
		: input(_input), msg(_msg), variable(_variable), type(_type)
	{
		init();
	}

	~Cinput(void) {}
public:
	// Data type
	DATA_type type;
	// Either read from a file or from the command line
	int input;
	// The string that defines the variable name
	string msg;
	// The variable itself to be read
	void * variable;

	// Default values
	double default_double;
	char default_char;
	int default_int;
	bool default_bool;



	// A function that may post-process the variable
	// void (*function)();
};

//Cinput<int>Input_int_default("",0);
//Cinput<double>Input_double_default("",0);
//typedef vector<Cinput> Input_list;
