/*
 * Project:  io_utilities
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Input/output utility library
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2018 CNRS (Centre National de la Recherche Scientifique), UMR 6118 Geosciences Rennes
 
    This file is part of io_utilities

    io_utilities is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    io_utilities is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with io_utilities.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "io_utilities.h"
#include "input_data.h"



//------------------------------------------
// A class that initialises a parameter list 
// from min, max and step values
//------------------------------------------
class generic_data 
{
private:
	DATA_type type;
	string	* data_string;
	double	* data_double;
	int		* data_int;
	bool	* data_bool;

public:
	generic_data(){clear();}

	void clear(){type=D_NULL;data_string=0;data_double=0;data_int=0;data_bool=0;}

	DATA_type link(int &var){
		data_int=&var;
		return (type= D_int);
	};
	DATA_type link(string &var){
		data_string=&var;
		return (type= D_string);
	};
	DATA_type link(double &var){
		data_double=&var;
		return (type= D_double);
	};
	DATA_type link(bool &var){
		data_bool=&var;
		return (type= D_bool);
	};
	
	bool empty(){return (type==D_NULL);}
	void * data()
	{
		if (data_string)	return data_string;
		if (data_double)	return data_double;
		if (data_int)		return data_int;
		if (data_bool)		return data_bool;
		return NULL;
	}	
	template <class T> bool assign (T value)
	{
		if (data_string)	return convert_and_check(value, *data_string);
		if (data_double)	return convert_and_check(value, *data_double);
		if (data_int)		return convert_and_check(value, *data_int);
		if (data_bool)		return convert_and_check(value, *data_bool);
		return false;
	}
	template <class T> bool get(T &value) {
		if (data_string)	return convert_and_check(*data_string,	value);
		if (data_double)	return convert_and_check(*data_double,	value);
		if (data_int)		return convert_and_check(*data_int,		value);
		if (data_bool)		return convert_and_check(*data_bool,	value);
		return false;
	}

};


template <typename T> class Cparametre: public vector <T>
{
private:
	generic_data link_parameter;
	// Data type

public:
	T min;
	T max;
	T step;
	string operation;
	string label;

	Cparametre(){}
	
	template <class S>
	Cparametre(Carg arg, S &variable, bool b_init=true, bool b_clear=true){
		init(arg, variable, b_init, b_clear);
	}
	~Cparametre(){clear();}

	template <class S>
	bool get(S & value) {
		return link_parameter.get(value);
	}

	void clear(){
		vector<T>::clear();
		link_parameter.clear();
	}

	//** link the parameter class to a variable
	template <class S>
	bool link(S &variable, bool b_init=true)
	{
		DATA_type type=link_parameter.link((S&)variable);
		if (b_init && !this->empty()) set_element(0);
		return true;
	}
	template <class S>
	bool init(Carg arg, S &variable, bool b_init=true, bool b_clear=true)
	{
		init_range(arg,b_clear);
		return link(variable,b_init);
	}

	//** set variables to data
	bool set_value(T val) 
	{
		if (link_parameter.empty()) return false;
		return link_parameter.assign(val);
	}
	bool set_element(size_t i) 
	{
		if ((i<0)||(i>(int)this->size())) return false;
		return set_value(this->at(i));
	}

	//** Init parameter range
	bool init_range(Carg arg, bool b_clear=true)
	{
		// Start from scratch if the clean option is activated
		if (b_clear) clear();

		label = arg.option_string(); // Write the label

		if (!arg.value(min)) return false;
		if (!arg.value(max)) { 
			push_back(min);return true;
		}
		if (!arg.value(operation) || !arg.value(step) || (operation!="*" && operation !="+") ) {
			push_back(min); push_back(max); return true;
		}

		for (T val=min; val<=max; operation=="*" ? val *= step: val+=step) 
			push_back(val);
		
		return true;
	}

	bool is_list() {
		// The parameter is a list if there is more than 1 value
		return !link_parameter.empty() && this->size()>1;
	}

};


template <class T> 
class Cparametre_liste:public vector<Cparametre<T>>
{
public:
	Cparametre_liste(){}
	~Cparametre_liste(){}
	
	size_t init(Cparametre<T> C)
	{	
		push_back(C);
		return this->size();
	}
	template <class S>
	size_t init(Carg arg, S &variable, bool b_init=true, bool b_clear=true)
	{
		Cparametre<T> C(arg,variable,b_init,b_clear);
		return init(C);
	}
	void clean_liste() {
		size_t s= this->size();
		
		for (auto it=this->begin();it!=this->end();) {
			if (!it->is_list()) it=this->erase(it);
			else it++;
		}

		cout << this->size() << " remaining elements over " << s << " :" <<endl;
		for (auto it=this->begin();it!=this->end();it++) {
			cout << it->label << " : " ; for (int i=0; i<it->size(); i++) cout << (*it)[i]<< " ";
			cout << endl;
		}	
	}

	bool init_liste() {
		bool tst=true;
		for (size_t i=0,_size= this->size();i<_size;i++) tst &= this->at(i).set_element(0);
		return tst;
	}
	
	size_t size_liste()
	{
		size_t result=0;
		for (size_t i=0,_size= this->size();i<_size;i++) if (this->at(i).is_list()) result++;
		return result;
	}
};
//---------------------------------------------
// A class that calculate and store the average 
// moments of a parameter list
//---------------------------------------------
class Cparametre_moment:public vector<double>
{
private:
	double * link_parameter;
	double last_value;
public:
	size_t order;
	//-------------------------------------------------------------
	// Initialisation: 
	// order is the moment order that will be calculated and stored
	//		if order is larger than 0, the moment average is stored
	//		otherwise the sum of the values is stored
	// _parameter is the address of the parameter whose moments 
	//		will be stored and calculated
	//-------------------------------------------------------------
	void init(size_t _order=1, double * _parameter=NULL)
	{
		// Sum (order = 0) or average (order>0) the parameter values
		order=_order;
		// link with a specific variable
		link_parameter=_parameter;

		// fix the moment list
		resize(order+1);
		reset();

		// Store the last value
		last_value=0.;
	}

	Cparametre_moment(size_t _order=1, double * _parameter=NULL){
		init(_order, _parameter);
	}
	//----------------
	// Add a new value
	//----------------
	void add(double value)
	{
		last_value=value;

		double &n=(*this)[0];
		if(!order) {
			n+=value;
			return;
		} 

		double n1=n+1.;
		for (size_t i=1;i<=order;i++){
			double &_this = (*this)[i];
			_this=(n * _this + pow(value,(double)i))/n1;

		}
		n=n1;
		return;
	}
	//----------------------------------------
	// Add a new value of the linked parameter
	//----------------------------------------
	bool add()
	{
		if (!link_parameter) return false;
		add(*link_parameter);
		return true;
	}
	//-------------------------------------
	// Give the last parameter value stored
	//-------------------------------------
	double last(){return last_value;}
	
	//----------------------------------
	// The moments of the parameter list
	//----------------------------------
	double value_moment(size_t i=1)
	{
		if ( i<=order+1 ) return (*this)[i];
		return 0.;
	}
	//-----------------------------
	// The average and sigma values
	//-----------------------------
	double n(){return value_moment(0);}
	double average()
	{
		if (order>0)return value_moment(1);
		else return value_moment(0);
	}
	double &operator()(size_t i=1)
	{
		if (i>order)return(*this)[order];
		return(*this)[i];
	}
	double sigma()
	{
		if ( (order<2)||((*this)[0]<=1.) ) return 0.0;
		return sqrt((*this)[2] -(*this)[1]*(*this)[1]);
	}
	// Make the average of two values
	double average(Cparametre_moment p2)
	{
		if ((order<1)&&(p2.order<1))return 0.0;
		double N=n()+p2.n();
		if (N<=0.0) return 0.0;
		return (n()*average()+p2.n()*p2.average())/(n()+p2.n());
	}
	//-------------------------
	// Reset the parameter list
	//-------------------------
	void reset()
	{
		for (size_t i=0;i<=order;i++)(*this)[i]=0.;
	}

};

typedef double (Cparametre_moment::*parameter_function)();
//------------------------------------------------------
// class resultat that stores a list of parameter values
// by using a vector of parameters
//------------------------------------------------------
class Cresultat:public vector< pair<string,Cparametre_moment> >
{
public:
	Cparametre_moment dummy;
	Cresultat(){};
	void associate(string name, int _moment=0, double * _value=NULL)
	{
		push_back(make_pair ( name,Cparametre_moment(_moment+1,_value)) );
	}

	Cparametre_moment& operator[](string name)
	{
		vector< pair<string,Cparametre_moment> >::iterator iter;
		for (iter=this->begin();iter!=this->end();iter++)
		{
			if (iter->first==name) return iter->second;
		}
		return dummy;
	}
	bool is_name(string name)
	{
		vector< pair<string,Cparametre_moment> >::iterator iter;
		for (iter=this->begin();iter!=this->end();iter++)
		{
			if (iter->first==name) return true;
		}
		return false;
	}

	bool reset()
	{
		vector< pair<string,Cparametre_moment> >::iterator iter;
		for (iter=this->begin();iter!=this->end();iter++) 
			iter->second.reset();
		return true;
	}
	bool fprint_name(cvs_stream & file, vector <string> names)
	{
		if (!file.is_open() || names.empty()) return false;
		int sz=(int)names.size();

		for (int i=0; i<sz;i++) file << names[i];
		return true;
	}
	bool fprint_name(cvs_stream & file, string name)
	{
		if (!file.is_open()) return false;
		file << name;
		return true;
	}
	bool fprint_name(cvs_stream & file)
	{
		if (!file.is_open() || empty()) return false;
		vector< pair<string,Cparametre_moment> >::iterator iter;
		for (iter=this->begin();iter!=this->end();iter++) fprint_name(file,iter->first);
		return true;
	}


	bool fprint(cvs_stream & file, string name, double (Cparametre_moment::*function)())
	{
		if (!file.is_open() || !is_name(name)) return false;
		file << ((*this)[name].*function)();
		return true;
	}
	bool fprint(cvs_stream & file, vector <string>names, double (Cparametre_moment::*function)())
	{
		int n=0;
		int sz=(int)names.size();
		for (int i=0; i<sz;i++) {
			if (fprint(file,names[i],function)) n++;
		}
		return (n>0);
	}
	bool fprint(cvs_stream & file, double (Cparametre_moment::*function)())
	{
		if (!file.is_open() || empty()) return false;
		vector< pair<string,Cparametre_moment> >::iterator iter;
		for (iter=this->begin();iter!=this->end();iter++) 
			fprint(file,iter->first,function);
		return true;
	}
	bool fprint(cvs_stream & file, double value)
	{
		if (!file.is_open()) return false;
		file << value;
		return true;
	}


};