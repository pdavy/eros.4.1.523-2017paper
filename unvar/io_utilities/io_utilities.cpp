/*
 * Project:  io_utilities
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Input/output utility library
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2018 CNRS (Centre National de la Recherche Scientifique), UMR 6118 Geosciences Rennes
 
	This file is part of io_utilities

	io_utilities is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	io_utilities is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with io_utilities.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"

// Class CVS for writing in ... CVS
//--------------------------
// Constructor
// Define the file, 
// and set the ARRAY 
// that will contain strings
//--------------------------
cvs_stream::cvs_stream(string name,
					   ios_base::openmode mode,
					   char _sep, 
					   bool _screen, 
					   int w_,
					   int p_
					   ): screen(_screen),separator(_sep),width(w_),precision(p_)
{
	fout.close();
	bool test=open(name, mode, _sep);
	stream_column=stream_line=0;
	cleararray();
	if (screen) {
		cout << endl;
		cout << setfill('-') << setw(80) << right << filename << " : writing CSV";
		cout << setfill(' ') << left << endl;
	}
}
//------------
// Destructor
//------------
cvs_stream::~cvs_stream()
{
	if (fout.is_open())fout.close();
	cleararray();
}
//----------------
// Open the file
//----------------
bool cvs_stream::open(string name, ios_base::openmode mode, char _sep)
{
	if (name.size()) 
		fout.open(name,mode);
	else 
		close();

	filename=name;
	separator=_sep;

	return fout.is_open();
}
//---------------------------------------------
// The core of the CVS stream 
// Overloading operator <<
//---------------------------------------------
template <typename T> 
cvs_stream & cvs_stream::operator << (T arg) 
{
	if (fout.is_open()){
		if (stream_column) { fout << separator;}
		fout << arg;
	}

	string S=convertInString(arg);
	
	if (stream_column>=ARRAY.size()) ARRAY.push_back((vector<string>)0);
	if (stream_line>=ARRAY[stream_column].size()){
		ARRAY[stream_column].push_back(S);
	} else {
		ARRAY[stream_column][stream_line]=S;
	}

	stream_column++;
	if (screen)  cout << setw(width) << setprecision(precision) << arg;
	return *this;
}

template <typename T> 
cvs_stream & cvs_stream::operator >> (T & arg)
{
	std::string s;
	if (fout.is_open())
	{
		char sep=0;
		while (!fout.eof())
		{
			fout >> sep;
			if ((sep-separator)&&(sep-'\n')) s.push_back(sep);
			else break;
			sep=0;
		}
	}

	arg=convert<T>(s);
	return *this;
}

//---------------------------------------------
// Mark the end of a stream_line
//---------------------------------------------
void cvs_stream::endline(int faire)	
{
	if (fout.is_open())fout << endl;
	stream_column=0;
	stream_line++;
	if (_echo(faire)) cout<< endl; 
}

//---------------------------------------------
// Close the file and reset the ARRAY if rewind
//---------------------------------------------
void cvs_stream::close(int rewind, int faire) 
{
	if (fout.is_open())fout.close();

	if (_echo(faire)) 
	{
		cout << setfill('-') << setw(40) << right << filename << " : end of writing CSV";
		cout << setfill(' ') << left << endl;
	}
	if (rewind) {
		stream_column=stream_line=0;
		cleararray();
	}
}
//---------------------------------------------
// Close the file and reset the ARRAY
//---------------------------------------------
void cvs_stream::closeall() 
{ 
	close(1,0); 
}

//---------------------------------------------
// Test of the file is open
//---------------------------------------------
bool cvs_stream::is_open()
{
	return fout.is_open();
}
//---------------------------------------------
// Flush
//---------------------------------------------
void cvs_stream::flush()
{
	fout.flush();
}//---------------------------------------------
// Print the ARRAY on the screen
//---------------------------------------------
void cvs_stream::print_screen()
{
	size_t LIN=0;
	for (size_t c=0;c<col();c++) LIN=max(LIN,lin(c));
	for (size_t l=0;l<LIN;l++)
	{
		for (int c=0;c<col();c++)
		{
			if (l<lin(c))
			{
				cout << setw(width) << setprecision(precision) << ARRAY[c][l];
			} else cout << setw(width) << setprecision(precision) << "-";
		}
		cout << endl;
	}
}
//---------------------------------------------
// Print the ARRAY in the file
//---------------------------------------------
void cvs_stream::print(ios_base::openmode mode, int transpose)
{
	// close the file without modifying the ARRAY
	if (is_open()) close(0,0);
	// re-open the file
	if (filename.size())fout.open(filename.c_str(), mode);
	
	if (!fout.is_open()) return;

	size_t LIN=0;
	for (size_t c=0;c<col();c++) LIN=max(LIN,lin(c));

	if (!transpose) 
	{
		for (int l=0;l<LIN;l++)
		{
			for (int c=0;c<col();c++)
			{
				if (c) fout<<separator;
				if (l<lin(c))
				{
					fout << ARRAY[c][l];
				} else fout << "-";
			}
			fout << endl;
		}
	}
	else 
	{
		for (int c=0;c<col();c++)
		{
			for (int l=0;l<LIN;l++)
			{
				if (l) fout<<separator;
				if (l<lin(c))
				{
					fout << ARRAY[c][l];
				} else fout << "-";
			}
			fout << endl;
		}
	}

}
//---------------------------------------------
// Read a series of number
//---------------------------------------------
template <typename T> 
size_t cvs_stream::read (vector<T>&arg)
{
	string value;

	if ( !is_open() ) return 0;

	while ( !fout.eof() ) {
		*this >> value;
		arg.push_back(convert<T>(value));
	}
	return arg.size();
}
//---------------------------------------------
// a series of procedure 
// for defining the ARRAY of stored number
//---------------------------------------------
size_t cvs_stream::col()
{
	return ARRAY.size();
}
size_t cvs_stream::lin(size_t c)
{
	if (c<ARRAY.size()) return ARRAY[c].size();
	return ARRAY.size();
}
string& cvs_stream::operator()(int i, int j){
	assert((i<col())&&(j<lin(i)));
	return ARRAY[i][j];
}
void cvs_stream::cleararray()
{
	for (int c=0;c<ARRAY.size();c++) ARRAY[c].clear();
	ARRAY.clear();
}
//---------------------------------------------
// The echo procedures that treat the 
// screen echoing:
// faire=-1 (the default value) --> consider the variable screen defined by echo()
// faire=0 --> no screen printing
// faire=1 --> always screen printing
//---------------------------------------------
void cvs_stream::echo(bool _repeat) 
{
	screen=_repeat;
}

bool cvs_stream::_echo(int faire) 
{
	return ((faire==-1) && (screen)) || (faire>0) ;
}	
std::string cvs_stream::get_name()
{
	return filename;
}
//---------------------------------------------
// How to print on the screen (same as stl)
//---------------------------------------------
void cvs_stream::setwidth(int w_){width=w_;}
void cvs_stream::setp(int p_){precision=p_;}


//---------------------------------
// A collection of useful functions
//---------------------------------
void removeFirstChar(char * buffer, char ch)
{
	while (*buffer && (*buffer==ch)) buffer++;
}

//---------------------
// Test if a file exist
//---------------------
bool is_file_exist(string filename)
{
	if (!filename.length()) return false;
	//return std::experimental::filesystem::exists(filename);
	
	ifstream fin(filename);
	if (fin.fail()) return false;
	if (!fin.is_open())return false;
	fin.close();
	return true;
}
bool file_exists(string filename){return is_file_exist(filename);}
//---------------------
// Test if a file exist
//---------------------
int file_lines (string filename) 
{ 
	if (!filename.length()) return 0;

	ifstream fin;
	fin.open(filename.c_str());
	if (fin.fail()) return 0;

	int line=0;
	while (!fin.eof()){
		string str;
		std::getline(fin,str);
		if ( fin.fail() ) break;
		if (!str.empty()) line++;
	}

	fin.close();
	return line;
}

char Sep_of_Data[3]={'\n','\t',';'};
char Blank_char[2]={' ','\n'};

bool is_Blank(char ch)
{
	bool test=(ch==Blank_char[0]);
	int st=(int)sizeof(Blank_char);
	for (int i=1;i<st;i++) test = test || (ch==Blank_char[i]);
	return test;
}

char * Sscanf (char * buffer, char * result, double * value) 
{
	strcpy(result, buffer);
	
	char * buf = strchr(result, Sep_of_Data[0]), *buf0=NULL;
	for (int i=1;i<sizeof(Sep_of_Data);i++) {
		buf0 = strchr(result, Sep_of_Data[i]); 
		if (!buf || (buf0 && (buf0<buf))) buf=buf0;
	}

	if (!buf) {
		strcpy (buffer,""); // the whole line is read and result is buffer
	} else {
		(*buf)=0; // fix result;
		do buf++;while (is_Blank(*buf));
		strcpy(buffer,buf);
	}

	if ( value && (sscanf(result,"%lf", value)>0) ) return result;
	else return NULL;

}

//----------------------------------------------------
// get the series of directories
//----------------------------------------------------
bool is_absolute_directory(string repertoire)
{
	return repertoire.find(':')!=string::npos;
}
vector<string> directories(string fichier)
{
	vector<string> _dir;
	int p,p1,p2;
	do{
		p1 = fichier.find_first_of('\\');
		p2 = fichier.find_first_of('/');
		p=(p1==string::npos ? p2 : (p2==string::npos?p1:max(p2,p1)));
		if (p!=string::npos) {
			_dir.push_back(fichier.substr(0,p));
			fichier=fichier.substr(p+1);
		} else if (!fichier.empty()) _dir.push_back(fichier);
	} while (p!=string::npos);
	return _dir;
}
//----------------------------------------------------
// Create directories
//----------------------------------------------------
int create_directories(string fichier)
{
	vector <string> file_dirs=directories(fichier);
	int size=(int)file_dirs.size();
	string current_dir = ".";
	if (file_dirs[0].find(':') != string::npos) {
		current_dir = _getcwd(NULL, 0);
		string d= file_dirs[0] +"\\";
		_chdir(d.c_str());
	}

	for (int i=0; i<size;i++) if (file_dirs[i]!=".")
	{
		string _dir=file_dirs[i];
#ifdef _MSC_VER // Windows
		int md = _mkdir(_dir.c_str());
#else // Linux
		int md = mkdir(_dir.c_str(), 0777); // notice that 777 is different than 0777
#endif
		if (md == -1)
		{
			if ( errno == ENOENT ) {
				cout << "unable to create the directory " << _dir << endl;
				return 0;
			} 
		} else
			cout << "the directory " << _dir << " has been created" << endl;
		if (_chdir(file_dirs[i].c_str())==-1)
		{
			cout << "unable to move into the directory " << _dir << endl;
			return 0;;
		}
	}
	for (int i=0; i<size;i++) if (file_dirs[i]!=".")
	{
		if (_chdir("..")==-1)
		{
			return 0;;
		}
	}
	_chdir(current_dir.c_str());
	return size;
}
//-----------------------------------
// List files in a directory
//-----------------------------------
/*
vector<string> list_directory (string directory, string filter)
{
	vector<string> list_files;
	DIR *dir;
	struct dirent *ent;

	if (!filter.empty()) filter=string_lower(filter);

	if ((dir = opendir (directory.c_str())) != NULL) 
	{
	  // print all the files and directories within directory
	  while ((ent = readdir (dir)) != NULL) 
	  {
		  string file=string_lower(string(ent->d_name));
		  if (!file.empty() && 
			  (filter.empty() || filter.compare(0,filter.size(),file)==0)  )
			  list_files.push_back(file);
	  }
	  closedir (dir);
	} 
	else 
	{
	  // could not open directory
	  // perror ("");
	}
	return list_files;
}
*/
//----------------------------------------------------
// increment file name
//----------------------------------------------------
string increment(string fichier)
{
	if (!fichier.length()) return fichier;
	string ext=extension(fichier);
	remove_ext(fichier);
	int i=0;
	do {
		i++;
	} while (is_file_exist(fichier+"."+convertInString(i)+"."+ext));
	fichier=fichier+"."+convertInString(i)+"."+ext;
	return fichier;	
}//----------------------------------------------------
// remove the file directory
//----------------------------------------------------
string remove_directory (string fichier, char *_ch)
{
	string file=fichier;
	char character[2]={'\\','/'};
	size_t last_pos=string::npos;
	char ch_pos=character[0];

	for (int i=0;i<2;i++)
	{
		size_t pos = file.find_last_of(character[i]);
		if ( pos!=string::npos ) 
		{
			if (last_pos==string::npos) last_pos=pos;
			else pos=max(pos,last_pos);
			ch_pos=character[i];
		}
	}
		
	if (_ch) (*_ch)=ch_pos;
	if (last_pos!=string::npos) {
		file = file.substr(last_pos+1);
	}
	return file;
}
//----------------------------------------------------
// complete file name with directory
//----------------------------------------------------
string	concat_directory(string file1, string file2, string sep)
{
	if (file1.empty()) return file2;
	if (file2.empty()) return file1;
	if (file1.back()=='/'||file1.back()=='\\')
		return file1+file2;
	return file1+sep+file2;
}
string complete_directory (string repertoire, char CH)
{
	if (!repertoire.length())return repertoire;

	char ch=repertoire.back();
	if ( (ch-'\\')&&(ch-'/') ) repertoire.push_back(CH);
	return repertoire;
}
string& complete_dir (string& repertoire, char CH)
{
	if (!repertoire.length())return repertoire;

	char ch=repertoire.back();
	if ( (ch-'\\')&&(ch-'/') ) repertoire.push_back(CH);
	return repertoire;
}
//----------------------------------------------------
// get file directory
//----------------------------------------------------
string directory(string fichier)
{
	int p1 = fichier.find_last_of('\\');
	int p2 = fichier.find_last_of('/');
	int p=(p1==string::npos ? p2 : (p2==string::npos?p1:max(p2,p1)));
	if (p!=string::npos) fichier = fichier.substr(0,p+1);
	else fichier="";
	return fichier;
}
//----------------------------------------------------
// get file extension
//----------------------------------------------------
string extension (string fichier)
{
	string ext;

	int p=fichier.find_last_of('.');
	if (p!=string::npos) ext=fichier.substr(p+1);
	else ext.erase();
	
	return ext;
}
//----------------------------------------------------
// remove file extension
//----------------------------------------------------
string remove_extension(string fichier)
{
	string file=fichier;
	int p=file.find_last_of('.');
	if (p!=string::npos) file=file.substr(0,p);
	return file;
}
string& remove_ext(string& fichier)
{
	int p=fichier.find_last_of('.');
	if (p!=string::npos) fichier=fichier.substr(0,p);
	return fichier;
}
//----------------------------------------------------
// change file directory
//----------------------------------------------------
string change_directory (string fichier, string Repertoire)
{
	string result=fichier;
	if (Repertoire.empty()) return result;
	char ch;
	result = remove_directory(fichier,&ch);
	result = complete_directory(Repertoire,ch)+result;
	return result;
}
string& change_dir (string& fichier, string Repertoire)
{
	if (!Repertoire.size()) return fichier;
	char ch;
	fichier = remove_directory(fichier,&ch);
	fichier = complete_directory(Repertoire,ch)+fichier;
	return fichier;
}
//----------------------------------------------------
// Add file extension
//----------------------------------------------------
string add_extension (string fichier, string Ext)
{
	string file = fichier + "." + Ext;
	return file;
}
string& add_ext(string& fichier, string Ext)
{
	fichier += ("." + Ext);
	return fichier;
}

//----------------------------------------------------
// change file extension
//----------------------------------------------------
string change_extension (string fichier, string Ext)
{
	string file=remove_extension(fichier);
	file = file + "." + Ext;
	return file;
}
string& change_ext(string& fichier, string Ext)
{
	int p=fichier.find_last_of('.');
	if (p!=string::npos) fichier=fichier.substr(0,p);
	fichier = fichier + "." + Ext;
	return fichier;
}

//----------------------------------------------
// Clean a string by removing blanks and indents
//----------------------------------------------
char * remove_first_blank(char * str)
{
	char * buf=str;
	while ( (*buf) && ( ((*buf)==' ') || ((*buf)=='\t')) ) buf++;
	return buf;
}
char * remove_end_of_line(char * str)
{
	int end=strlen(str);
	if (!end) return str;
	while ((end>0) && (*(str+end-1)=='\n')){*(str+end-1)=0;end--;}
	return str;
}
char * remove_end_of_line(char * str, char ch)
{
	int end = strlen(str);
	if (!end) return str;
	while (end > 0) {
		char & ch_ = *(str + end - 1);
		if (ch_ == '\n' || ch_ == '\t' || ch_ == ch) { ch_ = 0; end--; }
		else break;
	}
	return str;
}
char * remove_last_blank(char * str)
{
	remove_end_of_line(str, ' ');
	return str;
}

string remove_first_blank(string& str)
{
	string::iterator it=str.begin();
	while ( (it!=str.end()) && ( ((*it)==' ') || ((*it)=='\t') || !__isascii(*it) ) ) it=str.erase(it);
	return str;
}

string remove_end_of_line(string& str, char ch)
{
	string::iterator it;
	if (str.empty()) return str;
	do {
		it=str.end()-1;
		if ((*it)==ch || (*it)=='\t' || (*it) == '\n') str.erase(it);
		else break;
	} while (!str.empty());
	return str;
}
string remove_last_blank(string& str)
{
	remove_end_of_line(str, ' ');
	return str;
}
string clean_string(std::string& str) 
{
	remove_first_blank(str);
	remove_last_blank(str);
	return str;
}
//----------------------------------------------
// Check if a string is a number
//----------------------------------------------
bool is_number(const std::string& s)
{
	return !s.empty() && std::find_if(s.begin(),
		s.end(), [](char c) { return !std::isdigit(c); }) == s.end();
}
//----------------------------------------------
// Replace a character by another one
//----------------------------------------------
string replace_char(string fichier, char tobereplaced, char bythis)
{
	std::replace(fichier.begin(),fichier.end(),tobereplaced,bythis);
	return fichier;
}

string replace_blank_and_lower(string msg,char ch)
{
	string str=msg;
	remove_first_blank(str);
	std::replace(str.begin(),str.end(),' ',ch);
	transform(str.begin(),str.end(),str.begin(), (int(*)(int))tolower);
	return str;
}
string replace_blank(string msg, char ch)
{
	return replace_char(msg,' ',ch);
}
//------------------------------------------------
// Lower-cae or upper-case functions
//------------------------------------------------
// returns a lower case version of the string 
string string_lower (const string & s)
{
	string d (s);
	transform (d.begin (), d.end (), d.begin (), (int(*)(int)) tolower);
	return d;
}  // end of tolower

// returns an upper case version of the string 
string string_upper (const string & s)
{
	string d (s);
	transform (d.begin (), d.end (), d.begin (), (int(*)(int)) toupper);
	return d;
}   // end of toupper

char *string_lower(char *str)
{
	unsigned char *p = (unsigned char *)str;

	while (*p) {
		*p = tolower((unsigned char)*p);
		p++;
	}

	return str;
}

//------------------------------------------------------
// The procedure starts from a list of files, 
// expand // all files whose extension is (ext)
// and add to the list the file names contained in these fiiles
//------------------------------------------------------
string expand_File_List (vector<string> & File_in, string ext)
{
	string file_name;

	if (File_in.empty()) return file_name;

	for (int f=0; f<(int)File_in.size(); f++) {

		string fichier = File_in.at(f);

		//---------------------------------------------------------------------
		// If the file attribute is .list 
		// the program open the file and retrieve file names from that list file
		//---------------------------------------------------------------------
		if (extension(fichier)==ext){
			int F=f;
			string repertoire = directory(fichier);
			bool is_read=false;
			// open the file stream
			ifstream file_stream(fichier.c_str());
			while (file_stream.is_open() && !file_stream.eof()) {
				// get the name of a file in the line
				string local_file;	
				getline(file_stream,local_file,'\n');
				// if the name is not empty, insert the name in the list
				if (!file_stream.fail() && local_file.size()) {
					if (!is_absolute_directory(directory(local_file)) && !repertoire.empty() )
						local_file = repertoire +"\\"+local_file; 
					File_in.insert(File_in.begin()+(++F), local_file);
					is_read=true;
				}
			}
			// close the stream
			file_stream.close();
			// erase the .list file from the list
			File_in.erase(File_in.begin()+f);
			// check if the pointer has not reached the end of the list
			//(which can happen if the list file is empty)

			if (is_read) {
				if (!file_name.empty()) file_name+=".";
				file_name += remove_extension(remove_directory(fichier));
			}

			if (f>=(int)File_in.size()) break;
		} 
	}

	if (file_name.empty()) file_name=remove_extension(remove_directory(File_in[0]));

	return file_name;
}
//----------------------------------------------------
// The procedure open a file and return the list 
// of file names contained in
//----------------------------------------------------
string expand_list (vector<string> & File_in, string ext)
{
	string fichier=File_in.back();
	if (fichier.empty()) return fichier;

	//---------------------------------------------------------------------
	// If the file attribute is .list 
	// the program open the file and retrieve file names from that list file
	//---------------------------------------------------------------------
	if (extension(fichier)==ext)
	{
		File_in.pop_back();
		// open the file stream
		ifstream file_stream(fichier.c_str());
		string repertoire = directory(fichier);
		while (file_stream.is_open() && !file_stream.eof()) {
			// get the name of a file in the line
				// get the name of a file in the line
			string local_file;	
			getline(file_stream,local_file,'\n');
			// if the name is not empty, insert the name in the list
			if (!file_stream.fail() && local_file.size()) {
				if (!is_absolute_directory(directory(local_file)) && !repertoire.empty() )
					local_file = repertoire +"\\"+local_file; 
				File_in.push_back(local_file);
			}
		}
		// close the stream
		file_stream.close();
	} 

	return fichier;

}

//-----------------------
// Useful imput functions
//-----------------------

bool what_I_do(char yes, char no, string msg)
{
	yes=(char)tolower(yes);
	no =(char)tolower(no);
	char YES=(char)toupper(yes);
	char NO =(char)toupper(no);

	char ch=0;
	bool _cout_=!msg.empty();
	
	if (_cout_) cout << endl << msg ;
	do{
		ch=(char)_getch();if (_cout_) cout << ch;
	} while ((ch-yes)&&(ch-YES)&&(ch-no)&&(ch-NO));
	if (_cout_) cout << endl;


	return (ch==yes)||(ch==YES);
}

//-------------------------
// Error procedure
//-------------------------
void io_error(string message, bool _exit)
{

	
	if (_exit) 
	{
		cout << " |Fatal error: " << message  << "|" << endl;
		cout << "Bye..." << endl;
		exit (1);
	}
	else 
	{
		cout << " |Error:" << message << "| ";
	}

}
//--------------------------------------------
// Format integer to add 0 in front
//--------------------------------------------
string format_integer(int i, int nelts)
{
	string s = convert<string>(i);
	while (s.size()<nelts){
		s="0"+s;
	}
	return s;
}
//---------------------------------
// Calculate with string
//---------------------------------
void increment(double *var,char op,double inc)
{
	if (!var) return;
	if (op=='+') (*var)+=inc;
	if (op=='*') (*var)*=inc;
	if (op=='-') (*var)-=inc;
	if (op=='/') (*var)/=inc;
}
//---------------
// Wait for a key
//---------------
void check_key( void * var)
{
	char * ch=(char*)var;
	do (*ch) =(char)_getch(); while (true);
#if !defined(_NO_THREAD)&&defined(_MSC_VER)
	_endthread();
#endif
}
void wait_for_key(char &ch)
{
#if !defined(_NO_THREAD)&&defined(_MSC_VER)
	_beginthread( check_key, 0, (void *)&ch);
#endif
}
//------------------------------
// Useful print screen functions
//------------------------------
void print_D(int write_header, string header, int value, int length)
{ 
	return print_V<int>(write_header, header, value, 'd', length, 0); 
}
void print_F(int write_header, string header, double value, int length, int dec)
{ 
	return print_V<double>(write_header, header, value, 'f', length, dec); 
}
void print_G(int write_header, string header, double value, int length, int dec)
{ 
	return print_V<double>(write_header, header, value, 'g', length, dec); 
}

//--------------------------------------
// This function forces the template <<
// to be generated.
// Avoid the linking error LNK2001
//---------------------------------------
void dummy()
{
	cvs_stream d;
	d<<(float)0.;
	d<<(double)0.;
	d<<(int)0;
	d<<(unsigned int)0;
	d << (__int64)0;
	string s=""; d<<s;

	double var; d>>var;


	const char ch[20]="";
	d<<ch;

	s = convertInString((double)0.);
	s = convertInString((int)0);

	double	var_double = convert <double>(s); 
	int		var_int	= convert <int>(s);
	char	var_char = convert <char>(s);
	size_t	var_t = convert<size_t>(s);
	s = convert <string>(var_double);
	s = convert <string>(var_int);
	s = convert <string>(var_char);
	s = convert <string>(var_t);


	vector<double>vd; d.read<double>(vd);
	vector<int>vs; d.read<int>(vs);
	vector<string>vstring; d.read<string>(vstring);
	vector<float>vf; d.read<float>(vf);

	print_D(0, "test_D", 0);
	print_F(0, "test_F", 0.f);
	print_G(0, "test_G", 0.);
}