/*
 * Project:  io_utilities
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Input/output utility library
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2018 CNRS (Centre National de la Recherche Scientifique), UMR 6118 Geosciences Rennes
 
    This file is part of io_utilities

    io_utilities is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    io_utilities is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with io_utilities.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"

char Carg::sep_1=':';
char Carg::sep_2 = ':';
char Carg::sep_option = '-';
char Carg::sep_write = '\t';
string Carg::sep_comment = "//";
parser_method Carg::parser_m=parser_colon;


Carg_list::Carg_list(vector<string>argv, string ext_list, parser_method _parse, bool printing)
{ 
	init_arg_list(argv, ext_list, _parse, printing); 
}

Carg_list::Carg_list(int argc, char * argv[], string ext_list, parser_method _parse, bool printing)
{

	vector<string>Argv;
	for (int i=0;i<argc;i++) Argv.push_back(argv[i]);
	init_arg_list(Argv,ext_list,_parse,printing);
}

void Carg_list::init_arg_list(vector<string>argv, string ext_list, parser_method _parse, bool printing)
{
	if (printing) {
		cout << "_________________________________" << endl;
		cout << "       Program arguments         " << endl;
		cout << "_________________________________" << endl;
	}
	for (int i=0; i<argv.size();i++)
	{
		if ( !fill(argv[i],ext_list,_parse,printing) ) {
			push_back(Carg(argv[i],_parse));
			if (printing) cout<<argv[i]<<" ";
		}
	}
	if (printing) cout<<endl;
}

// Read a line and put it into an argument list
Carg_list::Carg_list(std::string line, parser_method _parse)
{
	while(!line.empty())
	{
		size_t npos = line.find(" ");
		char _line_[23000];
		strcpy(_line_, line.substr(0,npos).c_str());
		push_back( Carg(_line_,_parse) );
		if(npos<string::npos)
			line = line.substr(npos+1);
		else
			line.clear();
	}
}
// Fill the argument list with the file content
int Carg_list::fill(string file, string ext_list, parser_method _parse, bool printing)
{
	int nread=0;

	if (ext_list.empty()) return 0;
	if (extension(file) != extension(ext_list)) return 0; 

	ifstream file_stream(file);
	if (!file_stream.is_open()) return 0;

	string input;

	while ( !file_stream.eof() ) 
	{
		// get the name of a file in the line
		char ch=0;

		if (printing) cout << " ";

		do {
			ch=(char)file_stream.get();
		} while ( !isgraph(ch) && !file_stream.eof());

		if ( isprint(ch) )
		{

			// test if the "quote" character is not encountered
			// This avoid long name in quotes to be cut into different pieces
			if (ch-'\"')
			{
				file_stream >> input;
				input=ch+input;

				// Test if the comment sign (//) is encoutered at the beginning of the line
				// and read up to encountering an end of line
				if (input.find("//",0,2)!=string::npos)
				{
					getline(file_stream,input);
					continue;
				}
				else {
					if (printing) cout << input ;
				}
			} 
			else
			{
				// Read the name in quotes
				while ( ((ch=(char)file_stream.get())!='\"') && 
						isprint(ch) &&
						!file_stream.eof() )
				{
					input+=ch;
				}
			}
			// recursive reading to take account of files in file
			int nr=fill(input,ext_list,_parse,printing);
			if ( !nr) 
			{
				push_back(Carg(input,_parse));
				nread++;
			}
			else 
				nread+=nr;
		}
	}
	// close the stream
	file_stream.close();
	return nread;
}

