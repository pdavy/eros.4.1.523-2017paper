/*
 * Project:  io_utilities
 * Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
 * Purpose:  Input/output utility library
 * Please refer to myself for additional information
 
   Copyright (c) 1998-2018 CNRS (Centre National de la Recherche Scientifique), UMR 6118 Geosciences Rennes
 
	This file is part of io_utilities

	io_utilities is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	io_utilities is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with io_utilities.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "io_utilities.h"
#include <stdarg.h> 


//typedef char Str[256];
typedef string Str;

//-------------------------------------------------------------
//	class Carg
//
//	manage command line option such as
//	-option:value1:value2:etc.
//
//	the first char ('-') and the separator (':') can be changed
//-------------------------------------------------------------

enum parser_method { parser_colon, parser_ini };

class Carg : public vector <string>
{
private:

	size_t	N_values;
	string	line_ref;
	string	option_ref;

	bool	processed;

	string	prt_1st;
	string	prt_mid;
	string	prt_end;
	string	prt_sep;

private:
	static	char	sep_1;
	static	char	sep_2;
	static	char	sep_option;
	static	char	sep_write;
	static	string	sep_comment;
	static  parser_method parser_m;

public:
	// constructor
	//Carg(string line) { read_arg(line); }
	//Carg(char * arg_) :Carg(string(arg_)) {}
	Carg(string line) { read_arg(line); }
	Carg(char * arg_) { read_arg(string(arg_)); }
	Carg(string line, parser_method parser_) { init_parser(parser_); read_arg(line); }

	Carg(char * arg_, parser_method parser_) :Carg(string(arg_), parser_) {}
	Carg(string line, char parse_c) :Carg(line, init_parser(parse_c)) {}
	Carg(char * arg_, char parse_c) :Carg(string(arg_), init_parser(parse_c)) {}
	~Carg(void) {};

	static parser_method & parser() { return parser_m; }
	static string & parser_comment() { return sep_comment; }
	static void parser(parser_method parse_) { parser_m=parse_; }
	static void init_parser(parser_method parse_)
	{
		parser_m = parse_;
		switch (parser_m) {
		case parser_colon:
			sep_1 = ':';
			sep_2 = ':';
			sep_option = '-';
			sep_comment = "//";
			sep_write = '\t';

			break;
		case parser_ini:
		default:
			parser_m = parser_ini;
			sep_1 = '=';
			sep_2 = ':';
			sep_option = '-';
			sep_comment = "#";
			sep_write = '=';
			break;
		}
	}

	static parser_method init_parser(char _type) {
		switch (_type) {
		case':': init_parser(parser_colon); break;
		case'=': init_parser(parser_ini); break;
		}
		return parser_m;
	}


	
	int read_arg(string line)
	{
		processed = false;
		line_ref = line;
		if (line_ref.empty()) return 0;
		// Reset the number of values
		N_values = 0;

		//-------------------------------------------
		//	Case -option 
		//	(required that the first char is defined)
		//-------------------------------------------
		if (sep_option && line_ref[0] - sep_option)
		{
			push_back(line_ref);
			return 1;
		}

		//-------------------------------------------
		//	Case -option:value1:value2:etc..
		//-------------------------------------------
		string arg_string = line_ref;
		if (sep_option) {
			// Necessarily the option is -option:value1:value2:... or simply -option
			option_ref = arg_string.erase(0, 1);
		}
		// Look for the 1st occurence of separator, fix the option name and the value
		size_t found;
		if ((found = arg_string.find(sep_1)) != string::npos)
		{
			option_ref.erase(found);
			arg_string.erase(0, found + 1);
			if (!arg_string.empty()) push_back(arg_string);
			while ((found = arg_string.find(sep_2)) != std::string::npos)
			{
				back().erase(found);
				arg_string.erase(0, found + 1);
				if (!arg_string.empty()) push_back(arg_string);
			}
		}
		return 1;
	}

	//----------------------------------------
	// write a value from arg
	//----------------------------------------

	string write_value(int i=0)
	{
		string arg_str;

		if (value_string(i).empty()) return arg_str;

		if (!option() && !is_processed()) {
			switch (parser_m) {
			case parser_colon:
				return value_string(i) + sep_write + option_string(); // Get the option passed by command line
			case parser_ini:
				return option_string() + sep_write + value_string(i);
			}
		}
		return arg_str;

	}
	string write_value(string value_txt, string value)
	{
		string arg_str;

		if (value.empty()) return arg_str;
		switch (parser_m) {
		case parser_colon:
			return value + sep_write + value_txt;;// Get the option passed by command line
		case parser_ini:
			return value_txt + sep_write + value;
		}
		return arg_str;
	}
	//---------------------------------------------
	// read a value, text and parameter from a line
	//---------------------------------------------
	static int read_value(string & line, string & value_text, string & value, string & comment)
	{
		remove_first_blank(line);
		comment = "";
		if (line.empty() || line[0] == '\n')
			return 0;

		size_t n = line.find(sep_comment);
		if (n != string::npos) {
			comment = line;
			comment.erase(0, n + sep_comment.size());
			line.erase(n);
		}

		if (line.empty()) return 0;

		
		switch (parser_m) {
		case parser_colon:
		{
			std::stringstream stream(line);
			stream >> value;
			if (!stream.fail()) {
				getline(stream, value_text);
				value_text = replace_blank_and_lower(clean_string(value_text));
				return 1;
			}
			return 0;
		} break;
		case parser_ini:
		{
			size_t n = line.find(sep_1);
			if (n == string::npos) return 0;
			value_text = value = line;
			value_text.erase(n);
			value.erase(0, n + 1);

			value_text = replace_blank_and_lower(clean_string(value_text)); 
			clean_string(value);

			return !value.empty() && !value_text.empty();
		}

		}
		return 0;
	}

	string line() { return line_ref; }
	string option_string() { return option_ref; }

	//----------------------------------------
	// derive values
	//----------------------------------------

	bool check(int i)  { return i>=0 && i<(int)size(); }
	bool check() { return check(N_values); }

	string value_string(string sep=":")
	{
		string s;
		for(size_t i=0,_size=size();i<_size;i++)
		{
			if (i) s+=sep;
			s+=at(i);
		}
		return s;
	}
	string value_string(int i) { return check(i) ? at(i) : string(); }
	string before_last() { return value_string(size()-2); }
	string last() { return back(); }
	string next() { return check(N_values) ? at(N_values++) : string(); }
	string n_value() { return check(N_values-1) ? at(N_values-1) : string(); }



	// No argument. Test if option_ref exists. Return true if option_ref does not exist
	bool option() { return (option_ref.empty()); }
	// With argument(OPT). return true if option_ref matches OPT
	bool option(const char * OPT,  bool lwrcase=false) 
	{
		string _opt(OPT);
		bool is_processed;
		if (lwrcase) {
			is_processed = (string_lower(option_ref).compare(string_lower(_opt)) == 0);
		}			
		else 
			is_processed = (option_ref.compare(_opt)==0);
		// The arg is processed if it has been processed once
		processed = processed||is_processed;
		return is_processed;
	}
	
	// get the value
	template <class T> int value(T & val)		
	{
		if (!check()) return 0;
		if (!::convert_and_check<T,string>(at(N_values),val)) return 0;
		return int(++N_values);
	}
	// convert the preceding value
	template <class T> bool convert(T & val)		
	{
		if (!check(N_values-1)) return false;
		if ( !::convert_and_check<T,string>(at(N_values-1),val) ) return false;
		return true;
	}

	
public:
	int value(char & val) { return value<char>(val);}
	int value(int & val)	{ return value<int>(val);}
	int value(long & val) { return value<long>(val);}
	int value(float & val) { return value<float>(val);}
	int value(double & val) { return value<double>(val);}
	/*
	int value(double &val)	
	{
		if (!check(N_values)) return 0;
		char * p_end;
		val = strtod (at(N_values),&p_end);
		//val=atof(at(N_values));
		if ( errno || *p_end ) return 0;
		return ++N_values;
	}
	*/
	
	int value(char * val, bool lowercase=false)	
	{
		if (!check(N_values)) return 0; 
		strcpy(val,at(N_values).c_str());
		if (lowercase) string_lower(val);
		return ++N_values;
	}
	int value(string &val, bool lowercase=false)
	{
		if (!check(N_values)) return 0; 
		val.assign(at(N_values));
		if (lowercase) string_lower(val);
		return ++N_values;
	}
	int value(string &val, string subst, bool lowercase=false)
	{
		if (check(N_values))
		{
			val.assign(at(N_values));
			if (lowercase) string_lower(val);
			return ++N_values;
		} else 
		{
			if (subst.empty()) return 0;
			else {
				val=subst;
				return 1;
			}
		}
	}
	string value() 
	{
		string v=""; 
		value<string>(v); 
		return v;
	}
	bool rewind(int n)
	{
		if (n>=0 && n<=N_values) {
			N_values-=n;
			return true;
		}
		return false;
	}
	bool is_processed() { return processed; }
	bool is_fully_processed() { return empty() || !check(); }


	//----------------------------------
	// Write automatically the file name
	//----------------------------------
	void setprint( string s_first="", string s_middle="(", string s_end=")", string s_sep="+" )
	{ 
		prt_1st=s_first; 
		prt_mid=s_middle; 
		prt_end=s_end; 
		prt_sep=s_sep; 
	}

	string print_name(string str_file, bool is_name, string str_option="")
	{
	
		if (str_file.empty()) return string();

		string str_body;
		//str_file=remove_directory(str_file);
		// Remove file extension if it is similar to the option name
		if (is_name && !str_option.empty() && extension(str_file) == str_option) str_body=remove_extension(str_file);
		else str_body=str_file;
		// Format the output as "option(value)"
		if ( !str_body.empty() && !str_option.empty()) str_body = prt_1st+str_option+prt_mid+str_body+prt_end ;

		return str_body;
	}

	bool reference_name(string val, string &file_name, string &dir_name, bool is_name)
	{
		if (empty()) return false;

		bool res=false;
		static char slash_='\\';

		vector<string>values;
		int name[2]={0,0};
		int short_name[2]={0,0};
		int back_slash=0;

		string str_option=option_string();
		int last=-1;

		if ( str_option == "dir" )
			name[last=1]=2;
		else if ( str_option=="name" )
			name[last=0]=2;


		for (size_t i=0; i<size(); i++)
		{
			string arg_str=at(i);
			string f_name, d_name;

			if ( last>=0 && name[last]==2 )
				values.push_back(arg_str);
			else if (  arg_str=="print" 
					|| arg_str=="print\\"
					|| arg_str=="print_short\\"
					|| arg_str=="print_s\\"
					)
			{
				name[0]=name[1]=1; last=2;
				if (arg_str.back() == slash_) back_slash = 1;
				if (arg_str.find("_s") != string::npos) short_name[0] = short_name[1] = 1;
			} else if ( arg_str=="\\" )
			{
				back_slash=1;
			}
			else if (  arg_str=="name"
					|| arg_str=="file" 
					|| arg_str=="name_short" 
					|| arg_str=="name_s" 
					|| arg_str=="file_short" 
					|| arg_str=="file_s"
					)
			{
				last=0;
				name[last]=1;
				if ( arg_str.find("_s")!=string::npos) short_name[last]=1;
			} 
			else if ( arg_str=="dir"
				   || arg_str=="dir\\" 
				   || arg_str=="dir_short" 
				   || arg_str=="dir_s" 
				   || arg_str=="dir_short\\" 
				   || arg_str=="dir_s\\"
				   )
			{
				last=1;
				name[last]=1;
				if (arg_str.find("_s")!=string::npos) short_name[last]=1;
				if (arg_str.back()==slash_) back_slash=1;
			}
			else if ( arg_str=="short") 
			{
				switch(last) 
				{
				case 0:
				case 1: short_name[last]=1; break;
				case 2: short_name[0]=short_name[1]=1; break;
				}
			}			
			else {
				if (is_name) arg_str=remove_directory(arg_str);
				values.push_back(arg_str);
			}

		}

		string result;
		for (auto it=values.begin(); it!=values.end(); it++) 
		{
			if (!result.empty()) 
			{
				if (str_option=="dir") result+="\\";
				else if (str_option=="name") result+=".";
				else result += prt_sep;
			}
			result+=(*it);
		}

		if (name[0])
		{
			string str_res=result;
			if (!file_name.empty()) file_name+=".";
			if (name[0]==2)
				file_name+=print_name(str_res,is_name);
			else if (short_name[0]==1 && is_name) 
			{
				str_res=remove_extension(str_res);
				file_name+=print_name(str_res,is_name);
			} 
			else
				file_name+=print_name(str_res,is_name,str_option);
			res=true;
		}

		if(name[1])
		{
			string str_res=result;

			if (!dir_name.empty() && dir_name.back()!='\\' && (!str_res.empty()&&str_res[0]!='\\')) dir_name+=".";
			if (name[1]==2)
				dir_name+=print_name(str_res,is_name);
			else if (short_name[1]==1 && is_name ) {
				str_res=remove_extension(str_res);
				dir_name+=print_name(str_res,is_name);
			}
			else
				dir_name+=print_name(str_res,is_name,str_option);
			if (back_slash) dir_name += "\\";
			res=true;
		}
		return res;
	}
	bool reference_name(string &file_name, string &dir_name)
	{ 
		return reference_name(n_value(),file_name,dir_name, true); 
	}
	bool reference_value(string &file_name, string &dir_name)
	{ 
		return reference_name(value_string(0), file_name, dir_name, false); 
	}

};
//------------------------------------------
// Class Carg_list
// contain a list of argumentss
//------------------------------------------
class Carg_list:public vector<Carg>
{
public:
	vector<string>option_list;
	Carg_list(int argc, char * argv[], string ext_list, bool printing = true) :Carg_list(argc, argv, ext_list, Carg::parser(), printing) {}
	Carg_list(vector<string> argv, string ext_list, bool printing = true) :Carg_list(argv, ext_list, Carg::parser(), printing) {}
	Carg_list(vector<string> argv, string ext_list, parser_method parse_, bool printing = true);
	Carg_list(int argc, char * argv[], string ext_list, parser_method parse_, bool printing = true);

	void init_arg_list(vector<string>argv, string ext_list, parser_method _parse, bool printing);
	Carg_list(string line, parser_method parse_ = parser_colon);
	int fill(string file, string ext_list, parser_method _parse, bool printing);

	//----------------------------------------
	// read a data file
	//----------------------------------------
	static int read_file(string file_name, vector<string>&data )
	{
		// read the file
		ifstream fin(file_name.c_str());
		if (!fin.is_open()) {
			cout << "the file " << file_name << " does not exist" << endl;
			return 0;
		}
		// Put data into a vector of string
		while(!fin.eof())
		{
			string str;
			getline(fin,str);
			if (!fin.fail() && !str.empty())
			{
				remove_first_blank(str);
				remove_end_of_line(str,' ');
				data.push_back(str);
			}
		}
		fin.close();

		return 1;
	}


	//----------------------------------------
	// READ A SERIES OF INPUT
	//----------------------------------------
	vector<string> read_input()
	{
		vector<string>data_files;
		vector<string>data;

		int predefined_model=-1;
		for (size_t i=1; i<size(); i++) {

			// The general form of arguments is -option:value
			Carg arg=at(i); arg.setprint("","(",")");
			// define printing convention for directory and file names
			string arg_str, arg_str2;

			// Save the option string for a later processing
			if (arg.option()) { // case when there is no "-"
				if (arg.value(arg_str) && arg_str.find(Carg::parser_comment())!=0 ) {
					// read the argument in a file
					read_file(arg_str, data);
				}
			} else {
				arg_str=arg.write_value();
				if (!arg_str.empty()) data.push_back(arg_str);// Get the option passed by command line
			}
		}		
		return data;
	}

	~Carg_list(){}
};
//-------------------------------------------------
// A useful class that initialises a parameter list 
// from min, max and step values
//-------------------------------------------------
template <class T>
class vector_list: public vector<T> 
{
public:
	vector_list(int n, ...){
		va_list vl;
		va_start(vl,n);
		for (int i=0;i<n;i++)  vector<T>::push_back(va_arg(vl,T));
		va_end(vl);
	}
};
