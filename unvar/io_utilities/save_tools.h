/*
* Project:  io_utilities
* Author:   Philippe Davy, philippe.davy@univ-rennes1.fr
* Purpose:  Input/output utility library
* Please refer to myself for additional information

Copyright (c) 1998-2018 CNRS (Centre National de la Recherche Scientifique), UMR 6118 Geosciences Rennes

This file is part of io_utilities

io_utilities is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

io_utilities is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with io_utilities.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

template<class T>
class save_tool
{
private:
	T time_end;
	T time_step;
	T time_record;
	int stat;
public:

	save_tool(T t_init, T t_end, T t_step) :time_end(t_end), time_step(t_step), time_record(t_init){}
	~save_tool() {}
	int save(T t) {
		stat = 0;
		if (t >= time_record) {
			stat = 1;
		}
		if (t >= time_end)
		{
			stat = 2;
		}
		return stat;
	}
	T get() { return time_record; }
	int status() { return stat; }
	T inc() { time_record += time_step; return get(); }
};