#pragma once

#include <vector>

namespace ridder {

	class func_base {
	public:
		virtual double operator() (double) = 0;
	};
	double zero(double a, double b, double fa, double fb, double tol, func_base& f);
	double zero(double a, double b, double t, func_base& f);
}
