#pragma once

# include <cstdlib>
# include <iostream>
# include <cmath>
# include <ctime>
#include "ridder.h"

namespace ridder{
	// Finds a root of f(x) = 0 with Ridder's method.
	// The root must be bracketed in(a, b).
#define _convergence_ 1
	double zero(double a, double b, double tol, func_base& f)
	{
		return zero(a, b, f(a), f(b), tol, f);
	}
	double zero(double a, double b, double fa, double fb, double tol, func_base& f)
	{
		if (fa == 0.0) return a;
		if (fb == 0.0) return b;
		if (fa*fb > 0.0) {
			std::cout << std::endl << "root is not bracketed" << std::endl;
			std::cout << "f(a=" << a << ")=" << fa << "\tf(b=" << b << ")=" << fb << std::endl;
			return fabs(fa) < fabs(fb) ? a : b;
		}
		double c, fc;
		double s, dx, x, fx, _old;
		for (int i = 0; i<1000; i++)
		{
			// Compute the improved root x from Ridder's formula
			c = 0.5*(a + b);
			fc = f(c);
			s = sqrt(fc*fc - fa*fb);
			if (s == 0.0)return nan("1");
			dx = (c - a)*fc / s;
			if ((fa - fb) < 0.0)dx = -dx;
			x = c + dx;
			fx = f(x);
			// Test for convergence
			if (i > 0 &&
#if _convergence_==0
				fabs(x - _old) < tol *fmax(fabs(x), 1.0))
#else
				fabs(fx - _old) < tol)
#endif
			{
				return x;
			}
#if _convergence_==0
			_old = x;
#else
			_old = fx;
#endif
			// re-bracket the root as tightly as possible
			if (fc*fx > 0.0) {
				if (fa*fx < 0.0) { b = x; fb = fx; }
				else { a = x; fa = fx; }
			}
			else {
				a = c; fa = fc;
				b = x; fb = fx;
			}
		}
		// std::cout << std::endl << "too many iterations" << std::endl;
		return x;
	}
}


