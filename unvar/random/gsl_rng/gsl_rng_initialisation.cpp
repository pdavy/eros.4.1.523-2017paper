
#include <stdio.h>
#include <string.h>
#include "Gsl_Rng_Initialisation.h"


gsl_rng * gsl_generator;

void gsl_init( const gsl_rng_type * T, unsigned long int seed)
{
	gsl_generator = gsl_rng_alloc (T);
	gsl_rng_set (gsl_generator, seed);
}
double gsl_rng_get_exclude_max() 
{
	static double range = (double)gsl_rng_max(gsl_generator) + 1.0;
	return gsl_rng_get (gsl_generator)/range;
}
double gsl_rng_get_exclude_min() 
{
	static double range = (double)gsl_rng_max(gsl_generator) + 1.0;
	return (gsl_rng_get (gsl_generator)+1.)/range;
}
double gsl_rng_get_include() 
{
	static double range = (double)gsl_rng_max(gsl_generator);
	return gsl_rng_get (gsl_generator)/range;
}
double gsl_rng_get_exclude_all() 
{
	static double range = (double)gsl_rng_max(gsl_generator) + 2.0;
	return (gsl_rng_get (gsl_generator)+1.)/range;
}
double gsl_rng_get_exclude() 
{
	return gsl_rng_get_exclude_all();
}

void gsl_rng_alloc_dreuzy ( char type[100], unsigned long int seed, gsl_rng **r)
{
  const gsl_rng_type * T; 
  if(strncmp(type, "taus", strlen("taus"))==0) T = gsl_rng_taus; 

  if(*r==NULL){
	*r = (gsl_rng *) malloc (sizeof (gsl_rng));
	if (*r == 0) { printf("failed to allocate space for rng struct\n");/*GSL_ERROR_VAL ("failed to allocate space for rng struct", GSL_ENOMEM, 0);*/};
	(*r)->state = malloc (T->size);
  }

  if ((*r)->state == 0) { 
	  free ((*r));         /* exception in constructor, avoid memory leak */
       printf("failed to allocate space for rng struct\n");//GSL_ERROR_VAL ("failed to allocate space for rng state", GSL_ENOMEM, 0);
    };

  (*r)->type = T;

  gsl_rng_set ((*r), seed);        /* seed the generator */

  //return r;
}
