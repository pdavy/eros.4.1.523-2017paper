#ifndef _GSL_RNG_INITIALISATION
#define _GSL_RNG_INITIALISATION

#include "gsl_rng.h"


void gsl_rng_alloc_dreuzy ( char type[100], unsigned long int seed, gsl_rng ** r); 
void gsl_init( const gsl_rng_type * T, unsigned long int seed);
double gsl_rng_get_exclude_max();
double gsl_rng_get_exclude_min();
double gsl_rng_get_include();
double gsl_rng_get_exclude_all();
double gsl_rng_get_exclude();


#endif